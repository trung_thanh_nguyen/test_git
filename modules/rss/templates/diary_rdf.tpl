({php})header('Content-type: text/xml; charset=utf-8');({/php})
({php})echo '<?xml version="1.0" encoding="utf-8"?>'."\n";({/php})
<rdf:RDF xmlns="http://purl.org/rss/1.0/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:admin="http://webns.net/mvcb/" xml:lang="({$lang})">
	<channel rdf:about="({$smarty.const.OPENPNE_URL})">
		<title>({$smarty.const.SNS_NAME}) 最新日記</title>
		<link>({$smarty.const.OPENPNE_URL})</link>
		<description>({$member.nickname})さんが参照することができる最新の日記一覧(({$diary_rss_max})件)</description>
		<dc:language>({$lang})</dc:language>
		<dc:rights>({$rights})</dc:rights>
		<dc:date>({$date|date_format:"%Y-%m-%dT%H:%M:%S$locale1"})</dc:date>
		<admin:generatorAgent rdf:resource="({$url})" />
		<admin:errorReportsTo rdf:resource="mailto:({$webmaster})"/>
		<image rdf:resource="({$logo})" />
		<items>
			<rdf:Seq>
({foreach from=$c_diary_list item=key})
				<rdf:li rdf:resource="({$smarty.const.OPENPNE_URL})?m=pc&amp;a=page_fh_diary&amp;target_c_diary_id=({$key.c_diary_id})" />
({/foreach})
			</rdf:Seq>
		</items>
	</channel>
	<image rdf:about="({$logo})">
		<title>({$smarty.const.SNS_NAME})</title>
		<link>({$smarty.const.OPENPNE_URL})</link>
		<url>({$logo})</url>
	</image>
({foreach from=$c_diary_list item=key})
	<item rdf:about="({$smarty.const.OPENPNE_URL})?m=pc&amp;a=page_fh_diary&amp;target_c_diary_id=({$key.c_diary_id})">
		<title>({if $title_diary_type == 2})({$key.org_nickname})：({$key.subject}) (({$key.comment_count}))({elseif $title_diary_type == 3})({$key.subject}) (({$key.comment_count})) - ({$key.org_nickname})({else})({$key.subject}) (({$key.comment_count}))({/if})</title>
		<link>({$smarty.const.OPENPNE_URL})?m=pc&amp;a=page_fh_diary&amp;target_c_diary_id=({$key.c_diary_id})</link>
		<dc:subject><![CDATA[({$key.categories})]]></dc:subject>
		<dc:date>({$key.r_datetime|date_format:"%Y-%m-%dT%H:%M:%S$locale1"})</dc:date>
		<dc:creator>({$key.org_nickname})</dc:creator>
		<description><![CDATA[
({if $diary_display_image})
({if $key.image_filename_1})
<img src="({t_img_url_feed filename=$key.image_filename_1 w=120 h=120 _absolute=true})" /><br />
({/if})
({if $key.image_filename_2})
<img src="({t_img_url_feed filename=$key.image_filename_2 w=120 h=120 _absolute=true})" /><br />
({/if})
({if $key.image_filename_3})
<img src="({t_img_url_feed filename=$key.image_filename_3 w=120 h=120 _absolute=true})" /><br />
({/if})
({/if})
({if $description_diary_max<0})({$key.body|nl2br})({else})({$key.body|t_truncate:$description_diary_max|nl2br})({/if})
({if $desc_diary_type == 2})<br /><br /><br />posted at ({$key.r_datetime|date_format:"%Y-%m-%d %H:%M:%S"})<br />Commented by ({$key.nickname})<br /><a href="({$smarty.const.OPENPNE_URL})?m=pc&amp;a=page_fh_diary&amp;target_c_diary_id=({$key.c_diary_id})#write">write comment</a>({elseif $desc_diary_type == 3})<br /><br /><br />投稿日時：({$key.r_datetime|date_format:"%Y-%m-%d %H:%M:%S"})<br />投稿者：({$key.nickname})<br /><a href="({$smarty.const.OPENPNE_URL})?m=pc&amp;a=page_fh_diary&amp;target_c_diary_id=({$key.c_diary_id})#write">コメントを書く</a>({/if})
		]]></description>
		<content:encoded><![CDATA[
({if $diary_display_image})
({if $key.image_filename_1})
<img src="({t_img_url_feed filename=$key.image_filename_1 w=120 h=120 _absolute=true})" /><br />
({/if})
({if $key.image_filename_2})
<img src="({t_img_url_feed filename=$key.image_filename_2 w=120 h=120 _absolute=true})" /><br />
({/if})
({if $key.image_filename_3})
<img src="({t_img_url_feed filename=$key.image_filename_3 w=120 h=120 _absolute=true})" /><br />
({/if})
({/if})
({if $description_diary_max<0})({$key.body|nl2br})({else})({$key.body|t_truncate:$description_diary_max|nl2br})({/if})
({if $desc_diary_type == 2})<br /><br /><br />posted at ({$key.r_datetime|date_format:"%Y-%m-%d %H:%M:%S"})<br />Commented by ({$key.nickname})<br /><a href="({$smarty.const.OPENPNE_URL})?m=pc&amp;a=page_fh_diary&amp;target_c_diary_id=({$key.c_diary_id})#write">write comment</a>({elseif $desc_diary_type == 3})<br /><br /><br />投稿日時：({$key.r_datetime|date_format:"%Y-%m-%d %H:%M:%S"})<br />投稿者：({$key.nickname})<br /><a href="({$smarty.const.OPENPNE_URL})?m=pc&amp;a=page_fh_diary&amp;target_c_diary_id=({$key.c_diary_id})#write">コメントを書く</a>({/if})
		]]></content:encoded>
	</item>
({/foreach})
</rdf:RDF>
