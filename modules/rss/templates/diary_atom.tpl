({php})header('Content-type: text/xml; charset=utf-8');({/php})
({php})echo '<?xml version="1.0" encoding="utf-8"?>'."\n";({/php})
<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="({$lang})">
	<title type="text">({$smarty.const.SNS_NAME}) 最新日記</title>
	<subtitle type="text">({$member.nickname})さんが参照することができる最新の日記一覧(({$diary_rss_max})件)</subtitle>
	<id>({$smarty.const.OPENPNE_URL})?m=rss&amp;a=page_diary_atom</id>
	<link rel="alternate" type="application/xhtml+xml" href="({$smarty.const.OPENPNE_URL})" />
	<link rel="self" type="application/atom+xml" href="({$smarty.const.OPENPNE_URL})?m=rss&amp;a=page_diary_atom" title="({$smarty.const.SNS_NAME}) 最新日記" />
	<rights>({$rights})</rights>
	<author>
		<name>({$webmaster})</name>
		<uri>({$smarty.const.OPENPNE_URL})</uri>
	</author>
	<updated>({$date|date_format:"%Y-%m-%dT%H:%M:%S$locale"})</updated>
	<generator uri="({$url})" version="({$generator_ver})">({$generator_name})</generator>
	<icon>({$icon})</icon>
	<logo>({$logo})</logo>
({foreach from=$c_diary_list item=key})
	<entry>
		<title type="text">({if $title_diary_type == 2})({$key.org_nickname})：({$key.subject}) (({$key.comment_count}))({elseif $title_diary_type == 3})({$key.subject}) (({$key.comment_count})) - ({$key.org_nickname})({else})({$key.subject}) (({$key.comment_count}))({/if})</title>
		<link rel="alternate" type="text/html" href="({$smarty.const.OPENPNE_URL})?m=pc&amp;a=page_fh_diary&amp;target_c_diary_id=({$key.c_diary_id})" />
		<id>({$smarty.const.OPENPNE_URL})?m=pc&amp;a=page_fh_diary&amp;target_c_diary_id=({$key.c_diary_id})</id>
		<published>({$key.org_r_datetime|date_format:"%Y-%m-%dT%H:%M:%S$locale"})</published>
		<updated>({$key.r_datetime|date_format:"%Y-%m-%dT%H:%M:%S$locale"})</updated>
		({if $key.categories})<category term="({$key.categories})" scheme="({$smarty.const.OPENPNE_URL})" label="({$key.categories})" />({/if})
		<author>
			<name>({$key.org_nickname})</name>
			<uri>({$smarty.const.OPENPNE_URL})?m=pc&amp;a=page_f_home&amp;target_c_member_id=({$key.c_member_id})</uri>
		</author>
		<summary type="xhtml" xml:lang="({$lang})" xml:base="({$smarty.const.OPENPNE_URL})">
			<div xmlns="http://www.w3.org/1999/xhtml" xml:lang="({$lang})">
({if $diary_display_image})
({if $key.image_filename_1})
<img src="({t_img_url_feed filename=$key.image_filename_1 w=120 h=120 _absolute=true})" /><br />
({/if})
({if $key.image_filename_2})
<img src="({t_img_url_feed filename=$key.image_filename_2 w=120 h=120 _absolute=true})" /><br />
({/if})
({if $key.image_filename_3})
<img src="({t_img_url_feed filename=$key.image_filename_3 w=120 h=120 _absolute=true})" /><br />
({/if})
({/if})
({if $summary_diary_max<0})({$key.body|nl2br})({else})({$key.body|t_truncate:$summary_diary_max|nl2br})({/if})
({if $desc_diary_type == 2})<br /><br /><br />posted at ({$key.r_datetime|date_format:"%Y-%m-%d %H:%M:%S"})<br />Commented by ({$key.nickname})<br /><a href="({$smarty.const.OPENPNE_URL})?m=pc&amp;a=page_fh_diary&amp;target_c_diary_id=({$key.c_diary_id})#write">write comment</a>({elseif $desc_diary_type == 3})<br /><br /><br />投稿日時：({$key.r_datetime|date_format:"%Y-%m-%d %H:%M:%S"})<br />投稿者：({$key.nickname})<br /><a href="({$smarty.const.OPENPNE_URL})?m=pc&amp;a=page_fh_diary&amp;target_c_diary_id=({$key.c_diary_id})#write">コメントを書く</a>({/if})
			</div>
		</summary>
		<content type="xhtml" xml:lang="({$lang})" xml:base="({$smarty.const.OPENPNE_URL})">
			<div xmlns="http://www.w3.org/1999/xhtml" xml:lang="({$lang})">
({if $diary_display_image})
({if $key.image_filename_1})
<img src="({t_img_url_feed filename=$key.image_filename_1 w=120 h=120 _absolute=true})" /><br />
({/if})
({if $key.image_filename_2})
<img src="({t_img_url_feed filename=$key.image_filename_2 w=120 h=120 _absolute=true})" /><br />
({/if})
({if $key.image_filename_3})
<img src="({t_img_url_feed filename=$key.image_filename_3 w=120 h=120 _absolute=true})" /><br />
({/if})
({/if})
({if $content_diary_max<0})({$key.body|nl2br})({else})({$key.body|t_truncate:$content_diary_max|nl2br})({/if})
({if $desc_diary_type == 2})<br /><br /><br />posted at ({$key.r_datetime|date_format:"%Y-%m-%d %H:%M:%S"})<br />Commented by ({$key.nickname})<br /><a href="({$smarty.const.OPENPNE_URL})?m=pc&amp;a=page_fh_diary&amp;target_c_diary_id=({$key.c_diary_id})#write">write comment</a>({elseif $desc_diary_type == 3})<br /><br /><br />投稿日時：({$key.r_datetime|date_format:"%Y-%m-%d %H:%M:%S"})<br />投稿者：({$key.nickname})<br /><a href="({$smarty.const.OPENPNE_URL})?m=pc&amp;a=page_fh_diary&amp;target_c_diary_id=({$key.c_diary_id})#write">コメントを書く</a>({/if})
			</div>
		</content>
	</entry>
({/foreach})
</feed>
