({php})header('Content-type: text/xml; charset=utf-8');({/php})
({php})echo '<?xml version="1.0" encoding="utf-8"?>'."\n";({/php})
<rss version="2.0" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" xml:lang="({$lang})">
	<channel>
		<title>be amieタレント・モデル新着日記({if $full_name}) - ({$full_name})({/if})</title>
		<link>({$smarty.const.OPENPNE_URL})</link>
		<description>新着日記一覧(({$diary_noauth_rss_max})件)</description>
		<language>({$lang})</language>
		<copyright>({$rights})</copyright>
		<pubDate>({$date|date_format:"%a, %d %b %Y %H:%M:%S $locale2"})</pubDate>
		<lastBuildDate>({$date|date_format:"%a, %d %b %Y %H:%M:%S $locale2"})</lastBuildDate>
		<generator>({$generator})</generator>
		<docs>http://blogs.law.harvard.edu/tech/rss</docs>
		<webMaster>({$webmaster}) (({$webmaster_name}))</webMaster>
({foreach from=$c_diary_list item=key})
		<item>
			<title>({if $title_diary_noauth_type == 2})({$key.nickname})：({$key.subject}) (({$key.comment_count}))({elseif $title_diary_noauth_type == 3})({$key.subject}) (({$key.comment_count})) - ({$key.nickname})({elseif $title_diary_noauth_type == 1})({$key.subject}) (({$key.comment_count}))({else})({$key.subject})({/if})</title>
			<description>
				({if $key.image_filename_1})
				<![CDATA[
				<p><img src="({$image_path})({$key.image_filename_1})"/></p>
				<p>({$key.body})</p>
				]]>
				({else})
				<![CDATA[
				<p>({$key.body})</p>
				]]>
				({/if})
			</description>
			<link>
			({if $ktai==true})
			({$smarty.const.OPENPNE_URL})?m=ktai&amp;a=page_fh_diary&amp;target_c_diary_id=({$key.c_diary_id})</link>
			({else})
			({$smarty.const.OPENPNE_URL})?m=user&amp;a=blog&amp;k=detail&amp;target_c_diary_id=({$key.c_diary_id})</link>
			({/if})
			<guid isPermaLink="true">
			({if $ktai==true})
			({$smarty.const.OPENPNE_URL})?m=ktai&amp;a=page_fh_diary&amp;target_c_diary_id=({$key.c_diary_id})
			({else})
			({$smarty.const.OPENPNE_URL})?m=user&amp;a=blog&amp;k=detail&amp;target_c_diary_id=({$key.c_diary_id})
			({/if})
			</guid>
			<dc:creator>({$key.nickname})</dc:creator>
			({if $key.categories})<category>({$key.categories})</category>({/if})
			<pubDate>({$key.r_datetime|date_format:"%a, %d %b %Y %H:%M:%S $locale2"})</pubDate>
		</item>
({/foreach})
	</channel>
</rss>
