<?php
/**
 * @copyright 2007-2008 Naoya Shimada
 * @license   http://www.php.net/license/3_01.txt PHP License 3.01
 */

//ジェネレータ
define('RSS_GENERATOR','Rss Generator');

//ジェネレータバージョン
define('RSS_GENERATOR_VER','0.9.3');

//ジェネレータ＆バージョン
define('RSS_GENERATOR_AND_VER', RSS_GENERATOR." Ver.".RSS_GENERATOR_VER);

//著作権
define('RSS_GENERATOR_AUTHOR', '');

//連絡先URL
define('RSS_GENERATOR_URL', 'http://beamie.jp/');

//WebMaster E-mail
define('RSS_WEBMASTER_EMAIL', ADMIN_EMAIL);

//WebMaster名
define('RSS_WEBMASTER_NAME', 'be amie');

//コピーライト表記
define('RSS_RIGHTS','Copyright (c) 2010 '.SNS_NAME);

//アイコンの在処
define('RSS_ICON', OPENPNE_URL.'icon.gif');

//ロゴの在処
define('RSS_LOGO', OPENPNE_URL.'icon.gif');

//MyNETSか否か、と、プレフィックス
if(defined('MYNETS_PREFIX_NAME')){
    define('RSS_IS_MYNETS',true);
    define('RSS_DB_PREFIX_NAME',MYNETS_PREFIX_NAME);
}else{
    define('RSS_IS_MYNETS',false);
    define('RSS_DB_PREFIX_NAME','');
}

//タイムロケール
//DBに登録されている時刻がGMT（Z）か、ローカルか。デフォルト：日本時間（'+09:00'or'+0900'）
//Atom1.0用の設定。標準時（GMT）なら'Z'、または標準時からの差'+09:00'（日本）
define('RSS_TIME_LOCALE', '+09:00');
//RSS1.0用の設定。標準時（GMT）なら'Z'、または標準時からの差'+09:00'（日本）
define('RSS_TIME_LOCALE_1', '+09:00');
//RSS2.0用の設定。標準時なら'GMT'、または標準時からの差'+0900'（日本）
define('RSS_TIME_LOCALE_2', '+0900');

//JSON/JSONP形式での出力を許すか否かのフラグ（true/false）
//サイドバナーにJavascriptでJSON/JSONPで取得した結果を表示したい場合や、
//認証なしにして、外部サイトからJSON/JSONPで呼び出したい場合に有効かも
define('RSS_ALLOW_JSON_FORMAT', false);

/************************************/
/* コミュニティのトピック関連の設定 */
/************************************/

//コミュニティのトピックへの最新コメント取得件数
define('RSS_COM_TOPIC_COMMENT_MAX', 50);

//１つのトピックに対し１つの最新コメントを取得するか否かのフラグ（true/false）
//１つのトピックに対し１つの最新コメントのみ取得する場合、つまり、
//Ver.0.7.0までの仕様（マイホームのコミュニティ最新書き込みと同様）
//に合わせる場合は「true」に設定してください。
//参照可能なトピックのコメント全部から最新のものを取得件数分取得する
//場合は「false」に設定してください。
//ただし、「false」に設定した場合、特定のトピック（人気のあるトピック）
//のコメントしか表示されない・・・などということも起きますので、ご注意
//ください。コメントが１日数件vs１日数十件では、後者が勝つので。
define('RSS_COM_TOPIC_COMMNET_ONE',true);

//本文を引用する長さ
//あまり長いと全文引用になりRSSだけで事足りてしまうので微妙です。
//「-1」（負の整数）をセットすると、全文引用になります。
define('RSS_COMMUNITY_RSS_DESCRIPTION_MAX', 120); //RSS2.0のdscription（概要）
define('RSS_COMMUNITY_ATOM_SUMMARY_MAX', 120); // Atom1.0のsummary（内容）
define('RSS_COMMUNITY_ATOM_CONTENT_MAX', 120); // Atom1.0のcontent（内容）

//<title>出力形式
// 1 は「トピックのタイトル (コメント数)」です。
// 2 は「（コメントを書いた人の）ニックネーム：トピックのタイトル (コメント数)」です。
// 3 は「トピックのタイトル (コメント数) - （コメントを書いた人の）ニックネーム」です。
define('RSS_COMMUNITY_TITLE_TYPE',1);

//<description>出力形式
// 1 はコメントの本文だけを出力します。
// 2 は本文末尾にコメントの投稿日時とコメントを書く部分へのリンクが表示されます。（英語）
// 3 は2と同じですが、日本語で表示します。
define('RSS_COMMUNITY_DESC_TYPE',3);

// 投稿画像の表示を行うか否かのフラグ（true/false）
define('RSS_COMMUNITY_DISPLAY_IMAGE',true);

/************************************/
/* 最新の日記関連の設定             */
/************************************/

//最新の日記の取得件数
define('RSS_DIARY_MAX', 50);

//本文を引用する長さ
//あまり長いと全文引用になりRSSだけで事足りてしまうので微妙です。
//「-1」（負の整数）をセットすると、全文引用になります。
define('RSS_DIARY_RSS_DESCRIPTION_MAX', 120); //RSS2.0のdscription（概要）
define('RSS_DIARY_ATOM_SUMMARY_MAX', 120); // Atom1.0のsummary（内容）
define('RSS_DIARY_ATOM_CONTENT_MAX', 120); // Atom1.0のcontent（内容）

//最新の日記の取得時に、自分自身の日記を含むか否か（true/false）
define('RSS_WITH_USER', true);

//最新の日記の取得時に、マイフレンドの日記のみを取得対象にする設定を
//デフォルトとするか否かのフラグ（true/false）
//"false"の場合は、マイフレンド以外のメンバーの「全員に公開」設定と
//なっている日記も取得対象となる。
//URLの引数でもコントロールできるようにはしてあるが、メンバー数などを
//加味して設定のこと。
define('RSS_ONLY_MYFRIEND', false);

//<title>出力形式
// 1 は「日記のタイトル (コメント数)」です。
// 2 は「（日記またはコメントを書いた人の）ニックネーム：日記のタイトル (コメント数)」です。
// 3 は「日記のタイトル (コメント数) - （日記またはコメントを書いた人の）ニックネーム」です。
define('RSS_DIARY_TITLE_TYPE',1);

//<description>出力形式
// 1 は本文と画像のみを表示する形式です。
// 2 は本文末尾に日記の投稿日時とコメントを書く部分へのリンクが表示されます。（英語）
// 3 は2と同じですが、日本語で表示します。
define('RSS_DIARY_DESC_TYPE',3);

// 投稿画像の表示を行うか否かのフラグ（true/false）
define('RSS_DIARY_DISPLAY_IMAGE',true);

/************************************/
/* 日記の最新コメント関連の設定     */
/************************************/

//日記の最新コメント取得件数
define('RSS_DIARY_COMMENT_MAX', 50);

//本文を引用する長さ
//あまり長いと全文引用になりRSSだけで事足りてしまうので微妙です。
//「-1」（負の整数）をセットすると、全文引用になります。
define('RSS_COMMENT_RSS_DESCRIPTION_MAX', 120); //RSS2.0のdscription（概要）
define('RSS_COMMENT_ATOM_SUMMARY_MAX', 120); // Atom1.0のsummary（内容）
define('RSS_COMMENT_ATOM_CONTENT_MAX', 120); // Atom1.0のcontent（内容）

//<title>出力形式
// 1 は「日記のタイトル (コメント数)」です。
// 2 は「（コメントを書いた人の）ニックネーム：日記のタイトル (コメント数)」です。
// 3 は「日記のタイトル (コメント数) - （コメントを書いた人の）ニックネーム」です。
define('RSS_COMMENT_TITLE_TYPE',1);

//<description>出力形式
// 1 は本文と画像のみを表示する形式です。
// 2 は本文末尾に日記の投稿日時とコメントを書く部分へのリンクが表示されます。（英語）
// 3 は2と同じですが、日本語で表示します。
define('RSS_COMMENT_DESC_TYPE',3);

// 投稿画像の表示を行うか否かのフラグ（true/false）
define('RSS_COMMENT_DISPLAY_IMAGE',true);

/************************************/
/* レビュー関連の設定               */
/************************************/

//レビューの最新取得件数
define('RSS_REVIEW_MAX', 20);

//レビューの取得時に、自分自身のレビューを含むか否か（true/false）
define('RSS_REVIEW_WITH_USER', true);

//レビュー本文を引用する長さ
//あまり長いと全文引用になりRSSだけで事足りてしまうので微妙です。
//「-1」（負の整数）をセットすると、全文引用になります。
define('RSS_REVIEW_RSS_DESCRIPTION_MAX', 120); //RSS2.0のdscription（概要）
define('RSS_REVIEW_ATOM_SUMMARY_MAX', 120); // Atom1.0のsummary（内容）
define('RSS_REVIEW_ATOM_CONTENT_MAX', 120); // Atom1.0のcontent（内容）

//<description>出力形式
// 1 は本文末尾にレビュアが誰かだけを表示します。（英語）
// 2 は本文末尾にレビューの投稿日時とレビューを書くページへのリンクが表示されます。（英語）
// 3 は2と同じですが、日本語で表示します。
define('RSS_REVIEW_DESC_TYPE',3);

//<description>にレビュー対象の概要を表示するか否かのフラグ（true/false）
//trueの場合は、レビュー対象の画像や出版社、著者などの情報を表示します。
define('RSS_REVIEW_PRODUCT_INFO',true);

//<description>に満足度を表示するか否かのフラグ（true/false）
//trueの場合は、満足度を画像で表示します。
define('RSS_REVIEW_SATISFACTION',true);

/***********************************************/
/* 認証なしで最新日記RSS配信機能を使わせる設定 */
/***********************************************/

//認証なしの最新日記RSS配信機能を使うか否か（true/false）
//「false」にしておくと、「/?m=rss&a=page_get_diary_rss」
//が呼ばれても、認証機能が働きます。
//「true」の場合は認証なしで最新日記のRSSが取得できます。
define('RSS_USE_NOAUTH_DIARY', false);

//<title>出力形式
// 1 は「日記のタイトル (コメント数)」です。
// 2 は「（日記またはコメントを書いた人の）ニックネーム：日記のタイトル (コメント数)」です。
// 3 は「日記のタイトル (コメント数) - （日記またはコメントを書いた人の）ニックネーム」です。
// 4 は「日記のタイトル」です。 by e2info 2012.03.15
define('RSS_DIARY_NOAUTH_TITLE_TYPE',4);

//<description>出力形式
// 1 は認証ありの最新日記とほぼ同じ形式です。
// 2 は本文末尾に日記の投稿日時とコメントを書く部分へのリンクが表示されます。（英語）
// 3 は2と同じですが、日本語で表示します。
define('RSS_DIARY_NOAUTH_DESC_TYPE',3);

//最新の日記の取得件数（認証なしの場合）
define('RSS_DIARY_NOAUTH_MAX', 3);

//認証なしの場合の本文を引用する長さ（dscription部分）
//あまり長いと全文引用になりRSSだけで事足りてしまうので微妙です。
//「-1」（負の整数）をセットすると、全文引用になります。
define('RSS_DIARY_RSS_DESCRIPTION_NOAUTH_MAX', 200);

// 投稿画像の表示を行うか否かのフラグ（true/false）
define('RSS_DIARY_NOAUTH_DISPLAY_IMAGE',false);

/*************************************************************/
/* 認証なしで最新コミュニティ情報のRSS配信機能を使わせる設定 */
/*************************************************************/

//認証なしの最新コミュニティ情報のRSS配信機能を使うか否か（true/false）
//「false」にしておくと、「/?m=rss&a=page_get_com_rss」
//が呼ばれても、認証機能が働きます。
//「true」の場合は認証なしでRSSが取得できます。
define('RSS_USE_NOAUTH_COM', false);

//<title>出力形式
// 1 は「トピックのタイトル (コメント数)」です。
// 2 は「（コメントを書いた人の）ニックネーム：トピックのタイトル (コメント数)」です。
// 3 は「トピックのタイトル (コメント数) - （コメントを書いた人の）ニックネーム」です。
define('RSS_COM_NOAUTH_TITLE_TYPE',3);

//<description>出力形式
// 1 は認証ありのコミュニティ情報とほぼ同じ形式です。
// 2 は本文末尾にコメントの投稿日時とコメントを書く部分へのリンクが表示されます。（英語）
// 3 は2と同じですが、日本語で表示します。
define('RSS_COM_NOAUTH_DESC_TYPE',3);

//最新コミュニティ情報の取得件数（認証なしの場合）
define('RSS_COM_NOAUTH_MAX', 50);

//認証なしの場合の本文を引用する長さ（dscription部分）
//あまり長いと全文引用になりRSSだけで事足りてしまうので微妙です。
//「-1」（負の整数）をセットすると、全文引用になります。
define('RSS_COM_RSS_DESCRIPTION_NOAUTH_MAX', 50);

// 投稿画像の表示を行うか否かのフラグ（true/false）
define('RSS_COM_NOAUTH_DISPLAY_IMAGE',false);

/*************************************************************/
/* 認証なしで最新レビュー情報のRSS配信機能を使わせる設定 */
/*************************************************************/

//認証なしの最新レビュー情報のRSS配信機能を使うか否か（true/false）
//「false」にしておくと、「/?m=rss&a=page_get_review_rss」
//が呼ばれても、認証機能が働きます。
//「true」の場合は認証なしでRSSが取得できます。
define('RSS_USE_NOAUTH_REVIEW', false);

//レビューの最新取得件数
define('RSS_REVIEW_NOAUTH_MAX', 50);

//認証なしの場合の本文を引用する長さ（dscription部分）
//あまり長いと全文引用になりRSSだけで事足りてしまうので微妙です。
//「-1」（負の整数）をセットすると、全文引用になります。
define('RSS_REVIEW_RSS_DESCRIPTION_NOAUTH_MAX', 120); //RSS2.0のdscription（概要）

//<description>出力形式
// 1 は本文末尾にレビュアが誰かだけを表示します。（英語）
// 2 は本文末尾にレビューの投稿日時とレビューを書くページへのリンクが表示されます。（英語）
// 3 は2と同じですが、日本語で表示します。
define('RSS_REVIEW_NOAUTH_DESC_TYPE',3);

//<description>にレビュー対象の概要を表示するか否かのフラグ（true/false）
//trueの場合は、レビュー対象の画像や出版社、著者などの情報を表示します。
define('RSS_REVIEW_PRODUCT_NOAUTH_INFO',true);

//<description>に満足度を表示するか否かのフラグ（true/false）
//trueの場合は、満足度を画像で表示します。
define('RSS_REVIEW_NOAUTH_SATISFACTION',true);

/***********************************************/
/* すべてを認証なしにする設定（バッチで生成等）*/
/***********************************************/

//すべてを認証なしにする設定
//「false」なら、各モジュールには認証機能が働きます。
//「true」の場合は認証なしで動作しますが、引数「target_c_member_id」
// でメンバーIDが渡ってこない限り、不十分なRSSフィードしか出力しません。
define('RSS_USE_NOAUTH_ALL', true);

?>
