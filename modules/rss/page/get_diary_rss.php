<?php
/**
 * @copyright 2007 Naoya Shimada
 * @license   http://www.php.net/license/3_01.txt PHP License 3.01
 */
require_once OPENPNE_WEBAPP_DIR . '/modules/api/do/Rss.php';

class rss_page_get_diary_rss extends OpenPNE_Action
{
    function isSecure()
    {
        if(RSS_USE_NOAUTH_ALL){return false;}
        if(!RSS_USE_NOAUTH_ALL){return !RSS_USE_NOAUTH_DIARY;}
    }

    function execute($requests)
    {
    	if($_GET['ktai']=='1'){
        	$this->set('ktai',true);
        }else{
        	$this->set('ktai',false);
        }
        // --- リクエスト変数
        $c_member_id = $requests['target_c_member_id'];
        if(db_member_is_talent($c_member_id)){
        	$list_set = db_rss_all_diary_list4c_member_id($c_member_id, RSS_DIARY_NOAUTH_MAX);
        	$this->set('image_path', AMAZON_S3_PUBLIC_IMAGE_PATH. $c_member_id. '/');
        }else{
        	$list_set=db_rss_all_diary_list4c_member_id(null, RSS_DIARY_NOAUTH_MAX);        	
        	$this->set('image_path', AMAZON_S3_USER_IMAGE_PATH. $c_member_id. '/');
        }

		$rss_obj = new api_do_Rss();
		foreach ($list_set as $key => $diary) {
			$list_set[$key]['body'] = $rss_obj->getBody($diary['body']);
		}
        $this->set('c_diary_list', $list_set);

		// 名前を取得する
		$full_name = db_member_truename($c_member_id);
        $this->set('full_name', $full_name);


        rss_setValue($this,"");

        //JSON/JSONPで出力した場合は抜ける
        if (rss_feed_convert_to_json($this->view,$requests)){
            exit;
        }
        
        

        $view =& $this->getView();
        $view->ext_display('get_diary_rss.tpl');
        exit;
    }
}

?>
