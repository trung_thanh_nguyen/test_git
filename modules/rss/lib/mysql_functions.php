<?php
/**
 * @copyright 2007 Naoya Shimada
 * @license   http://www.php.net/license/3_01.txt PHP License 3.01
 */

//予期しない多重定義を避けるため、必ず関数名にはdb_rss_というprefixをつける

/**
 * 「全員に公開」の最新日記とあるメンバーの「マイフレンド」の最新日記のリストを取得
 * 一番新しいコメントの情報を取得し、そのコメントの日付で日記の日付を置き換えておく。
 * 引数により、取得する範囲は任意に変わる。
 * p_fh_diary_list_diary_list4c_member_id
 * p_h_diary_list_friend_h_diary_list_friend4c_member_id
 * を参考にした。
 *
 * @param int $c_member_id target c_member_id
 * @param int $limit 取得する最大件数
 * @param boolean $is_myfriend マイフレンドのみを対象とするか否か
 * @param boolean $is_mydiary  自分の日記も対象とするか否か
 * @return array 日記リスト
 */
function db_rss_diary_list4c_member_id($c_member_id = null, $limit, $is_myfriend = false, $is_mydiary = false)
{
    if(!$c_member_id){
        return array();
    }else{
        if(function_exists(db_member_c_member_id_block4c_member_id)){
            $blocked = db_member_c_member_id_block4c_member_id($c_member_id);
        }else{
            $blocked = p_h_config_c_member_id_block4c_member_id($c_member_id);
        }
        $blocked2 = db_member_access_block_list4c_member_id_to($c_member_id);
        if ($is_myfriend && !$is_mydiary) {
            $blocked = array_unique(array_merge($blocked,$blocked2,array($c_member_id)));
        } else {
            $blocked = array_unique(array_merge($blocked,$blocked2));
        }
        $except_ids = implode(',', $blocked);

        $friends = db_friend_c_member_id_list($c_member_id, true);
        if ($is_mydiary) {
            array_push($friends,$c_member_id);
        }
        $ids = implode(',', array_map('intval', $friends));

        $sql = "SELECT cd.*,cm.nickname FROM " . RSS_DB_PREFIX_NAME . "c_diary AS cd LEFT JOIN " . RSS_DB_PREFIX_NAME . "c_member AS cm USING (c_member_id)".
               " WHERE ".((count($blocked)>0) ? "cd.c_member_id NOT IN (" . $except_ids . ") AND " : "" ).
               " ( ( (cd.public_flag = 'public' OR cd.public_flag = 'open' )".(($is_myfriend) ? " AND cd.c_member_id IN ( ". $ids . ")" : "")." )".
               " OR  ( cd.public_flag = 'friend' AND cd.c_member_id IN ( ". $ids . ") ) )".
               " ORDER BY cd.r_datetime DESC LIMIT 0,".$limit;

        $list = db_get_all($sql);
        if (!$list) {
            return array();
        }
        foreach ($list as $key => $c_diary) {
            if(function_exists(db_diary_category_list4c_diary_id)){
                $list[$key]['category_list'] = db_diary_category_list4c_diary_id($c_diary['c_diary_id']);
                $categories = array();
                foreach ($list[$key]['category_list'] as $item => $value) {
                    $categories[] = $value['category_name'];
                }
                $list[$key]['categories'] = implode(',',$categories);
            } else {
                $list[$key]['categories'] = '';
            }
            if(!RSS_IS_MYNETS){
                $list[$key]['comment_count'] = db_diary_count_c_diary_comment4c_diary_id($c_diary['c_diary_id']);
            }
            // コメントの情報を取得し、最新情報として出力する
            if ( $list[$key]['comment_count'] > 0 ) {
                $comment = db_rss_diary_last_comment4c_diary_id(intval($c_diary['c_diary_id']));
                // 一部を最新のコメントで置き換える
                $list[$key] = rss_set_comment_list($list[$key],$comment);
            } else {
                $list[$key] = rss_set_comment_list($list[$key],array());
            }
        }
    }
    return $list;
}

/**
 * 日記に付いた最新のコメントの情報を返す
 *
 * @param int $c_diary_id c_diary_id
 * @return array コメントの情報
 */
function db_rss_diary_last_comment4c_diary_id($c_diary_id = 0)
{
    // コメントの情報をセットして返す
    $sql = 'SELECT cdc.*,cm.nickname' .
           ' FROM ' . RSS_DB_PREFIX_NAME . 'c_diary_comment AS cdc LEFT JOIN ' . RSS_DB_PREFIX_NAME . 'c_member AS cm USING (c_member_id)' .
           ' WHERE cdc.c_diary_id = ? ORDER BY cdc.r_datetime DESC LIMIT 0,1';
    $list = db_get_all($sql,array($c_diary_id));
    if (!$list) {
        return array();
    }
    $ret = array();
    foreach ($list as $key => $c_comment) {
        $ret['r_datetime'] = $c_comment['r_datetime'];
        $ret['nickname'] = $c_comment['nickname'];
        $ret['body'] = $c_comment['body'];
        $ret['image_filename_1'] = $c_comment['image_filename_1'];
        $ret['image_filename_2'] = $c_comment['image_filename_2'];
        $ret['image_filename_3'] = $c_comment['image_filename_3'];
    }
    return $ret;
}

/*
 * あるメンバーのコミュニティのトピックリストと最新コメントを取得
 * db_commu_c_commu_topic_comment_list4c_member_id
 * を参考にした。
 *
 * @param int $c_member_id target c_member_id
 * @param boolean $is_onecomment １トピック１最新コメントか否か
 * @param int $limit 取得する最大件数
 * @return array コミュニティのトピックリストと最新コメント
 *
 */
function db_rss_commu_c_commu_topic_comment_list4c_member_id_with_body($c_member_id, $is_onecomment = true, $limit = 10)
{
    $c_commu_id_list = db_rss_c_commu_list4c_member_id($c_member_id);
    $ids = implode(", ", $c_commu_id_list);

    if(RSS_IS_MYNETS){
        $hint = '';
    }else{
        $hint = db_mysql_hint('USE INDEX (r_datetime_c_commu_id)');
    }

    if($is_onecomment){
        //１トピック１最新コメント
        $sql = 'SELECT cct.c_commu_topic_id, cct.name AS c_commu_topic_name, cct.c_commu_id,'.
            ' MAX(cctc.r_datetime) AS r_datetime, cct.r_datetime AS org_r_datetime, cct.c_member_id'.
            ' FROM ' . RSS_DB_PREFIX_NAME . 'c_commu_topic AS cct, '.
            RSS_DB_PREFIX_NAME . 'c_commu_topic_comment AS cctc'. $hint .
            ' WHERE cct.c_commu_id IN (' . $ids . ') AND cctc.c_commu_topic_id = cct.c_commu_topic_id'.
            ' GROUP BY cctc.c_commu_topic_id'.
            ' ORDER BY r_datetime DESC LIMIT 0,'.$limit;
    }else{
        //コメント全部から最新のものを指定件数分
        $sql = 'SELECT cct.c_commu_topic_id, cct.name AS c_commu_topic_name, cct.c_commu_id,'.
            ' cct.r_datetime AS org_r_datetime, cctc.r_datetime AS r_datetime,'.
            ' cctc.*, cm.nickname'.
            ' FROM ' . RSS_DB_PREFIX_NAME . 'c_commu_topic_comment AS cctc'.
            ' LEFT JOIN ' . RSS_DB_PREFIX_NAME . 'c_member AS cm ON (cctc.c_member_id=cm.c_member_id), '.
            RSS_DB_PREFIX_NAME . 'c_commu_topic AS cct '. $hint .
            ' WHERE cct.c_commu_id IN (' . $ids . ') AND cctc.c_commu_topic_id = cct.c_commu_topic_id'.
            ' ORDER BY cctc.r_datetime DESC LIMIT 0,'.$limit;
    }

    $c_commu_topic_list = db_get_all($sql);

    if (!$c_commu_topic_list) {
        return array();
    }

    foreach ($c_commu_topic_list as $key => $value) {
        $c_commu_topic_list[$key]['c_commu_name'] = db_rss_c_commu_name4c_commu_id($value['c_commu_id']);

        if($is_onecomment){
            //最新のコメント情報
            $topic = db_rss_c_commu_topic_comment_newest4c_commu_topic_id($value['c_commu_topic_id'], $value['r_datetime']);
            
            foreach ($topic as $key2 => $value2) {
                $c_commu_topic_list[$key]['number'] = $value2['number'];
                $c_commu_topic_list[$key]['body'] = $value2['body'];
                $c_commu_topic_list[$key]['image_filename1'] = $value2['image_filename1'];
                $c_commu_topic_list[$key]['image_filename2'] = $value2['image_filename2'];
                $c_commu_topic_list[$key]['image_filename3'] = $value2['image_filename3'];
                $c_commu_topic_list[$key]['c_member_id'] = $value2['c_member_id'];
                $c_commu_topic_list[$key]['nickname'] = $value2['nickname'];
            }
        }
    }

    return $c_commu_topic_list;
}

function db_rss_c_commu_list4c_member_id($c_member_id)
{
    $sql = 'SELECT c_commu_id FROM ' . RSS_DB_PREFIX_NAME . 'c_commu_member WHERE c_member_id = ?';
    return db_get_col($sql, array(intval($c_member_id)));
}

function db_rss_c_commu_name4c_commu_id($c_commu_id)
{
    $sql = 'SELECT name FROM ' . RSS_DB_PREFIX_NAME . 'c_commu WHERE c_commu_id = ?';
    return db_get_one($sql, array($c_commu_id));
}

function db_rss_c_commu_topic_name4c_commu_topic_id($c_commu_topic_id)
{
    $sql = 'SELECT name FROM ' . RSS_DB_PREFIX_NAME . 'c_commu_topic WHERE c_commu_topic_id = ?';
    return db_get_one($sql, array($c_commu_topic_id));
}

function db_rss_c_commu_topic_comment_newest4c_commu_topic_id($c_commu_topic_id,$r_datetime)
{
        //最新のコメント情報
        $sql = 'SELECT cctc.*,cm.nickname'.
               ' FROM ' . RSS_DB_PREFIX_NAME . 'c_commu_topic_comment AS cctc LEFT JOIN ' . RSS_DB_PREFIX_NAME . 'c_member AS cm USING (c_member_id)'.
               ' WHERE cctc.c_commu_topic_id = ? AND cctc.r_datetime >= ?';
        $params = array(intval($c_commu_topic_id), $r_datetime);
        return db_get_all($sql, $params);
}

/**
 * あるメンバーの日記についたコメントのリストを返す
 *
 * @param int $c_member_id target c_member_id
 * @param int $limit 取得する最大件数
 * @return array コメントリスト
 */
function db_rss_comment_list4c_member_id($c_member_id = null, $limit)
{
    if(!$c_member_id){
        return array();
    }else{

        $sql = "SELECT cd.*,cd.body as org_body,cd.c_member_id as org_c_member_id,cd.r_datetime as org_r_datetime,".
               "cd.image_filename_1 as org_image_filename_1,cd.image_filename_2 as org_image_filename_2,cd.image_filename_3 as org_image_filename_3,".
               "cdc.*,cm.nickname".
               " FROM " . RSS_DB_PREFIX_NAME . "c_diary AS cd," . RSS_DB_PREFIX_NAME . "c_diary_comment AS cdc".
                    " LEFT JOIN " . RSS_DB_PREFIX_NAME . "c_member AS cm ON (cdc.c_member_id = cm.c_member_id)".
               " WHERE cd.c_member_id = ? AND cd.public_flag <> 'private' AND cdc.c_diary_id = cd.c_diary_id".
               " ORDER BY cdc.r_datetime DESC LIMIT 0,".$limit;

        $list = db_get_all($sql,array(intval($c_member_id)));

        if (!$list) {
            return array();
        }

        $c_comment_cnt = array();
        $c_comment_max = array();
        foreach ($list as $key => $c_diary) {
            if(function_exists(db_diary_category_list4c_diary_id)){
                $list[$key]['category_list'] = db_diary_category_list4c_diary_id($c_diary['c_diary_id']);
                $categories = array();
                foreach ($list[$key]['category_list'] as $item => $value) {
                    $categories[] = $value['category_name'];
                }
                $list[$key]['categories'] = implode(',',$categories);
            } else {
                $list[$key]['categories'] = '';
            }
            if (!$c_comment_max[$c_diary['c_diary_id']]) {
                if(RSS_IS_MYNETS){
                    $c_comment_max[$c_diary['c_diary_id']] = $c_diary['comment_count'];
                }else{
                    $c_comment_max[$c_diary['c_diary_id']] = db_diary_count_c_diary_comment4c_diary_id($c_diary['c_diary_id']);
                }
            }
            $list[$key]['comment_count'] = ($c_comment_max[$c_diary['c_diary_id']]>0)?($c_comment_max[$c_diary['c_diary_id']]-$c_comment_cnt[$c_diary['c_diary_id']]++):0;
        }
    }
    return $list;
}

/**
 * 「全員に公開」の最新日記を取得
 * 日記を日付の新しいものから順に、指定された件数ゴッソリ取得する。
 * コメントは件数のみ取得してセット。
 *
 * @param int $c_member_id target c_member_id
 * @param int $limit 取得する最大件数
 * @return array 日記リスト
 */
function db_rss_all_diary_list4c_member_id($c_member_id = null, $limit)
{	
    if(!$c_member_id){
        $sql = "SELECT cd.*,cm.nickname".
               " FROM " . RSS_DB_PREFIX_NAME . "c_diary AS cd LEFT JOIN " . RSS_DB_PREFIX_NAME . "c_member AS cm USING (c_member_id)".
               " WHERE cd.public_flag = 'public'   and c_member_id in (select c_member_id from c_member_profile where c_profile_id=".MEMBER_TALENT_PROFILE_ID." and c_profile_option_id=".MEMBER_KIND_TALENT_ID." )".
    	       " ORDER BY cd.r_datetime DESC LIMIT 0,".$limit;        
        $list = db_get_all($sql);
    }else{
        $sql = "SELECT cd.*,cm.nickname".
               " FROM " . RSS_DB_PREFIX_NAME . "c_diary AS cd LEFT JOIN " . RSS_DB_PREFIX_NAME . "c_member AS cm USING (c_member_id)".
               " WHERE ( cd.public_flag = 'public'  ) AND cd.c_member_id = ?".
               " ORDER BY cd.r_datetime DESC LIMIT 0,".$limit;
        $list = db_get_all($sql,array($c_member_id));
    }

    if (!$list) {
        return array();
    }

    foreach ($list as $key => $c_diary) {
        if(function_exists(db_diary_category_list4c_diary_id)){
            $list[$key]['category_list'] = db_diary_category_list4c_diary_id($c_diary['c_diary_id']);
            $categories = array();
            foreach ($list[$key]['category_list'] as $item => $value) {
                $categories[] = $value['category_name'];
            }
            $list[$key]['categories'] = implode(',',$categories);
        } else {
            $list[$key]['categories'] = '';
        }
        if(!RSS_IS_MYNETS){
            $list[$key]['comment_count'] = db_diary_count_c_diary_comment4c_diary_id($c_diary['c_diary_id']);
        }
    }

    return $list;
}


/*
 * コミュニティの情報（コメント）を取得して返す
 *
 * @param int $c_member_id target c_member_id
 * @param boolean $is_onecomment １トピック１最新コメントか否か
 * @param int $limit 取得する最大件数
 * @return array コミュニティのコメント
 *
 */
function db_rss_all_commu_c_commu_topic_comment_list4c_member_id_with_body($c_member_id, $is_onecomment = true, $limit = 10)
{
    if(!$c_member_id){
        $sql = 'SELECT c_commu_id FROM ' . RSS_DB_PREFIX_NAME . 'c_commu_member';
        $c_commu_id_list = db_get_col($sql);
    }else{
        $sql = 'SELECT c_commu_id FROM ' . RSS_DB_PREFIX_NAME . 'c_commu_member WHERE c_member_id = ?';
        $c_commu_id_list = db_get_col($sql, array(intval($c_member_id)));
    }
    $ids = implode(", ", $c_commu_id_list);

    if(RSS_IS_MYNETS){
        $hint = '';
    }else{
        $hint = db_mysql_hint('USE INDEX (r_datetime_c_commu_id)');
    }

    if($is_onecomment){
        //１トピック１最新コメント
        $sql = 'SELECT cct.c_commu_topic_id, cct.name AS c_commu_topic_name, cct.c_commu_id, MAX(cctc.r_datetime) as r_datetime, cct.r_datetime as org_r_datetime, cct.c_member_id'.
            ' FROM ' . RSS_DB_PREFIX_NAME . 'c_commu_topic as cct, ' . RSS_DB_PREFIX_NAME . 'c_commu_topic_comment as cctc'. $hint .
            ' WHERE cct.c_commu_id IN (' . $ids . ') AND cctc.c_commu_topic_id = cct.c_commu_topic_id'.
            ' GROUP BY cctc.c_commu_topic_id'.
            ' ORDER BY r_datetime DESC LIMIT 0,'.$limit;
    }else{
        //コメント全部から最新のものを指定件数分
        $sql = 'SELECT cct.c_commu_topic_id, cct.name AS c_commu_topic_name, cct.c_commu_id,'.
            ' cct.r_datetime as org_r_datetime, cctc.r_datetime as r_datetime,'.
            ' cctc.*, cm.nickname'.
            ' FROM ' . RSS_DB_PREFIX_NAME . 'c_commu_topic_comment AS cctc'.
            ' LEFT JOIN ' . RSS_DB_PREFIX_NAME . 'c_member AS cm ON (cctc.c_member_id=cm.c_member_id), '.
            RSS_DB_PREFIX_NAME . 'c_commu_topic AS cct '. $hint .
            ' WHERE cct.c_commu_id IN (' . $ids . ') AND cctc.c_commu_topic_id = cct.c_commu_topic_id'.
            ' ORDER BY cctc.r_datetime DESC LIMIT 0,'.$limit;
    }

    $c_commu_topic_list = db_get_all($sql);

    if (!$c_commu_topic_list) {
        return array();
    }

    foreach ($c_commu_topic_list as $key => $value) {
        $c_commu_topic_list[$key]['c_commu_name'] = db_rss_c_commu_name4c_commu_id($value['c_commu_id']);

        if($is_onecomment){
            //最新のコメント情報
            $topic = db_rss_c_commu_topic_comment_newest4c_commu_topic_id($value['c_commu_topic_id'], $value['r_datetime']);

            foreach ($topic as $key2 => $value2) {
                $c_commu_topic_list[$key]['number'] = $value2['number'];
                $c_commu_topic_list[$key]['body'] = $value2['body'];
                $c_commu_topic_list[$key]['image_filename1'] = $value2['image_filename1'];
                $c_commu_topic_list[$key]['image_filename2'] = $value2['image_filename2'];
                $c_commu_topic_list[$key]['image_filename3'] = $value2['image_filename3'];
                $c_commu_topic_list[$key]['c_member_id'] = $value2['c_member_id'];
                $c_commu_topic_list[$key]['nickname'] = $value2['nickname'];
            }
        }
    }

    return $c_commu_topic_list;
}

/*
 * レビューのコメント情報を取得して返す
 *
 * @param int $c_member_id target c_member_id
 * @param int $limit 取得する最大件数
 * @return array レビューのコメントリスト
 */
function db_rss_review_list($c_member_id,$limit = 10,$is_myreview = false)
{
    if(!$c_member_id){
        return array();
    }

    if(function_exists(db_member_c_member_id_block4c_member_id)){
        $blocked = db_member_c_member_id_block4c_member_id($c_member_id);
    }else{
        $blocked = p_h_config_c_member_id_block4c_member_id($c_member_id);
    }
    $blocked2 = db_member_access_block_list4c_member_id_to($c_member_id);
    if (!$is_myreview) {
        $blocked = array_unique(array_merge($blocked,$blocked2,array($c_member_id)));
    } else {
        $blocked = array_unique(array_merge($blocked,$blocked2));
    }

    $sql = 'SELECT crc.*,cm.nickname'.
           ' FROM ' . RSS_DB_PREFIX_NAME . 'c_review_comment AS crc'.
           ' INNER JOIN ' . RSS_DB_PREFIX_NAME . 'c_member AS cm USING (c_member_id)'.
           ' WHERE '.((count($blocked)>0) ? 'crc.c_member_id NOT IN (' . $except_ids . ')' : '1').
           ' ORDER BY crc.r_datetime DESC LIMIT 0,' . $limit;
    $review_list = db_get_all($sql);

    if (!$review_list) {
        return array();
    }

    foreach ($review_list as $key => $value) {
        $review = db_rss_review4c_review_id($value['c_review_id']);
        foreach($review as $key2 => $value2){
            if($key2!='r_datetime'){
                $review_list[$key][$key2] = $value2;
            }
        }
    }

    return $review_list;
}

/*
 * レビューの情報を取得して返す
 *
 * @param int $c_review_id c_review_id
 * @return array レビューの情報
 */
function db_rss_review4c_review_id($c_review_id)
{
    $sql = 'SELECT cr.*, crc2.*'.
           ' FROM ' . RSS_DB_PREFIX_NAME . 'c_review AS cr'.
           ' INNER JOIN ' . RSS_DB_PREFIX_NAME . 'c_review_category AS crc2 ON (cr.c_review_category_id = crc2.c_review_category_id)'.
           ' WHERE cr.c_review_id = ?';
    return db_get_row($sql, array($c_review_id));
}

/*
 * レビューのコメント情報を取得して返す
 *
 * @param int $c_member_id target c_member_id
 * @param int $limit 取得する最大件数
 * @return array レビューのコメントリスト
 */
function db_rss_all_review_list4c_member_id($c_member_id,$limit = 10)
{
    if(!$c_member_id){
        $blocked = array();
    }else{
        if(function_exists(db_member_c_member_id_block4c_member_id)){
            $blocked = db_member_c_member_id_block4c_member_id($c_member_id);
        }else{
            $blocked = p_h_config_c_member_id_block4c_member_id($c_member_id);
        }
        $blocked2 = db_member_access_block_list4c_member_id_to($c_member_id);
        $blocked = array_unique(array_merge($blocked,$blocked2));
    }

    $sql = 'SELECT crc.*,cm.nickname'.
           ' FROM ' . RSS_DB_PREFIX_NAME . 'c_review_comment AS crc'.
           ' INNER JOIN ' . RSS_DB_PREFIX_NAME . 'c_member AS cm USING (c_member_id)'.
           ' WHERE '.((count($blocked)>0) ? 'crc.c_member_id NOT IN (' . $except_ids . ')' : '1').
           ' ORDER BY crc.r_datetime DESC LIMIT 0,' . $limit;
    $review_list = db_get_all($sql);

    if (!$review_list) {
        return array();
    }

    foreach ($review_list as $key => $value) {
        $review = db_rss_review4c_review_id($value['c_review_id']);
        foreach($review as $key2 => $value2){
            if($key2!='r_datetime'){
                $review_list[$key][$key2] = $value2;
            }
        }
    }

    return $review_list;
}

?>
