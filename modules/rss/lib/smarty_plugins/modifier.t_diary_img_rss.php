<?php
function smarty_modifier_t_diary_img_rss($string,$module="rss",$pic1="" ,$pic2="",$pic3="",$pic4="",$pic5="",$pic6="")
{
	$GLOBALS['diary_pic1']=$pic1;
	$GLOBALS['diary_pic2']=$pic2;
	$GLOBALS['diary_pic3']=$pic3;
	$GLOBALS['diary_pic4']=$pic4;
	$GLOBALS['diary_pic5']=$pic5;
	$GLOBALS['diary_pic6']=$pic6;
	$GLOBALS['diary_pic_module']=$module;
	$str="";
	if($module=="rss"){ //pc
		
		if($pic1 && preg_match('/\[pic1(\/[bsm])?(\/[lcr])?\]/', $string)==false){
			$str .="[pic1]<br />";
		}
		if($pic2 && preg_match('/\[pic2(\/[bsm])?(\/[lcr])?\]/', $string)==false){
			$str .="[pic2]<br />";
		}
		if($pic3 && preg_match('/\[pic3(\/[bsm])?(\/[lcr])?\]/', $string)==false){
			$str .="[pic3]<br />";
		}
		if($pic4 && preg_match('/\[pic4(\/[bsm])?(\/[lcr])?\]/', $string)==false){
			$str .="[pic4]<br />";
		}
		if($pic5 && preg_match('/\[pic5(\/[bsm])?(\/[lcr])?\]/', $string)==false){
			$str .="[pic5]<br />";
		}
		if($pic6 && preg_match('/\[pic6(\/[bsm])?(\/[lcr])?\]/', $string)==false){
			$str .="[pic6]<br />";
		}
		if($str){			
			$string=$str.$string;
		}
	}
	
	$str= preg_replace_callback('/\[pic([1-6])(\/[bsm])?(\/[lcr])?\]/',"_modify_t_diary_img_rss2",$string);
	return $str;	
}

function _modify_t_diary_img_rss2($matches){
	$result="";
	$count=count($matches);
	switch ($matches[1]) {
		case 1: //pic1
			if($count>=3){
				$urls=_modify_make_img_url_rss($GLOBALS['diary_pic1'],$matches[2]);
			}else{
				$urls=_modify_make_img_url_rss($GLOBALS['diary_pic1'],null);
			}			
			break;
		case 2:
			if($count>=3){
				$urls=_modify_make_img_url_rss($GLOBALS['diary_pic2'],$matches[2]);
			}else{
				$urls=_modify_make_img_url_rss($GLOBALS['diary_pic2'],null);
			}			
			break;
		case 3:			
			if($count>=3){
				$urls=_modify_make_img_url_rss($GLOBALS['diary_pic3'],$matches[2]);
			}else{
				$urls=_modify_make_img_url_rss($GLOBALS['diary_pic3'],null);
			}
			break;	
		case 4: //pic4
			if($count>=3){
				$urls=_modify_make_img_url_rss($GLOBALS['diary_pic4'],$matches[2]);
			}else{
				$urls=_modify_make_img_url_rss($GLOBALS['diary_pic4'],null);
			}			
			break;
		case 5:
			if($count>=3){
				$urls=_modify_make_img_url_rss($GLOBALS['diary_pic5'],$matches[2]);
			}else{
				$urls=_modify_make_img_url_rss($GLOBALS['diary_pic5'],null);
			}			
			break;
		case 6:			
			if($count>=3){
				$urls=_modify_make_img_url_rss($GLOBALS['diary_pic6'],$matches[2]);
			}else{
				$urls=_modify_make_img_url_rss($GLOBALS['diary_pic6'],null);
			}
			break;					
		default:
			break;
	}
	if($urls){		
		$result = $urls['src'];
	}
	return $result;	
	
}

function _modify_make_img_url_rss($filename,$size){
	$url_arr=array();
	$p =array();
	//http://beamie.jp/rss/pic/縦size_横size/画像文件名的格式
	$url = OPENPNE_URL .'rss/pic/';
	switch ($size){
		case "/b":
		case "/B": //big 420
			$p['w']=420;
			$p['h']=420;
			break;
		case "/m":
		case "/M":
			$p['w']=300;
			$p['h']=300;
			break;
		case "/s":
		case "/S":
			$p['w']=180;
			$p['h']=180;
			break;
		default:
			$p['w']=300;
			$p['h']=300;
			break;
	}
	$url .= $p['h'].'_'.$p['w'].'/'.$filename;
	$url_arr['src']= $url;
	return $url_arr;
	
}