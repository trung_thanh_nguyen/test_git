<?php
/**
 * @copyright 2007 Naoya Shimada
 * @license   http://www.php.net/license/3_01.txt PHP License 3.01
 */

//RSS配信モジュール用 雑多関数ライブラリ

//予期しない多重定義を避けるため、必ず関数名にはrss_というprefixをつける

/*
 * 共通する値をセットして返す
 */
function rss_setValue($smarty_set, $member)
{
    $smarty_set->set("topic_comment_rss_max",RSS_COM_TOPIC_COMMENT_MAX);
    $smarty_set->set("description_com_max",RSS_COMMUNITY_RSS_DESCRIPTION_MAX);
    $smarty_set->set("summary_com_max",RSS_COMMUNITY_ATOM_SUMMARY_MAX);
    $smarty_set->set("content_com_max",RSS_COMMUNITY_ATOM_CONTENT_MAX);
    $smarty_set->set("title_com_type",RSS_COMMUNITY_TITLE_TYPE);
    $smarty_set->set("desc_com_type",RSS_COMMUNITY_DESC_TYPE);
    $smarty_set->set("com_display_image", RSS_COMMUNITY_DISPLAY_IMAGE);

    $smarty_set->set("diary_rss_max",RSS_DIARY_MAX);
    $smarty_set->set("description_diary_max",RSS_DIARY_RSS_DESCRIPTION_MAX);
    $smarty_set->set("summary_diary_max",RSS_DIARY_ATOM_SUMMARY_MAX);
    $smarty_set->set("content_diary_max",RSS_DIARY_ATOM_CONTENT_MAX);
    $smarty_set->set("title_diary_type",RSS_DIARY_TITLE_TYPE);
    $smarty_set->set("desc_diary_type",RSS_DIARY_DESC_TYPE);
    $smarty_set->set("diary_display_image", RSS_DIARY_DISPLAY_IMAGE);

    $smarty_set->set("comment_rss_max",RSS_DIARY_COMMENT_MAX);
    $smarty_set->set("description_comment_max",RSS_COMMENT_RSS_DESCRIPTION_MAX);
    $smarty_set->set("summary_comment_max",RSS_COMMENT_ATOM_SUMMARY_MAX);
    $smarty_set->set("content_comment_max",RSS_COMMENT_ATOM_CONTENT_MAX);
    $smarty_set->set("title_comment_type",RSS_COMMENT_TITLE_TYPE);
    $smarty_set->set("desc_comment_type",RSS_COMMENT_DESC_TYPE);
    $smarty_set->set("comment_display_image", RSS_COMMENT_DISPLAY_IMAGE);

    $smarty_set->set("review_rss_max",RSS_REVIEW_MAX);
    $smarty_set->set("description_review_max",RSS_REVIEW_RSS_DESCRIPTION_MAX);
    $smarty_set->set("summary_review_max",RSS_REVIEW_ATOM_SUMMARY_MAX);
    $smarty_set->set("content_review_max",RSS_REVIEW_ATOM_CONTENT_MAX);
    $smarty_set->set("desc_review_type",RSS_REVIEW_DESC_TYPE);
    $smarty_set->set("review_product_info",RSS_REVIEW_PRODUCT_INFO);
    $smarty_set->set("review_satisfaction",RSS_REVIEW_SATISFACTION);

    $smarty_set->set("diary_noauth_rss_max",RSS_DIARY_NOAUTH_MAX);
    $smarty_set->set("desc_diary_noauth_max",RSS_DIARY_RSS_DESCRIPTION_NOAUTH_MAX);
    $smarty_set->set("title_diary_noauth_type",RSS_DIARY_NOAUTH_TITLE_TYPE);
    $smarty_set->set("desc_diary_noauth_type",RSS_DIARY_NOAUTH_DESC_TYPE);
    $smarty_set->set("diary_noauth_display_image", RSS_DIARY_NOAUTH_DISPLAY_IMAGE);

    $smarty_set->set("com_noauth_rss_max",RSS_COM_NOAUTH_MAX);
    $smarty_set->set("desc_com_noauth_max",RSS_COM_RSS_DESCRIPTION_NOAUTH_MAX);
    $smarty_set->set("title_com_noauth_type",RSS_COM_NOAUTH_TITLE_TYPE);
    $smarty_set->set("desc_com_noauth_type",RSS_COM_NOAUTH_DESC_TYPE);
    $smarty_set->set("com_noauth_display_image",RSS_COM_NOAUTH_DISPLAY_IMAGE);

    $smarty_set->set("review_noauth_rss_max",RSS_REVIEW_NOAUTH_MAX);
    $smarty_set->set("desc_review_noauth_max",RSS_REVIEW_RSS_DESCRIPTION_NOAUTH_MAX);
    $smarty_set->set("desc_review_noauth_type",RSS_REVIEW_NOAUTH_DESC_TYPE);
    $smarty_set->set("review_product_noauth_info",RSS_REVIEW_PRODUCT_NOAUTH_INFO);
    $smarty_set->set("review_noauth_satisfaction",RSS_REVIEW_NOAUTH_SATISFACTION);

    $smarty_set->set('member', $member);
    $smarty_set->set('lang', 'ja');

    $smarty_set->set('webmaster', RSS_WEBMASTER_EMAIL);
    $smarty_set->set('webmaster_name', RSS_WEBMASTER_NAME);

    if (RSS_GENERATOR) {
        $smarty_set->set('generator_name', RSS_GENERATOR);
    } else {
        $smarty_set->set('generator_name', OPENPNE_URL);
    }

    if (RSS_GENERATOR_VER) {
        $smarty_set->set('generator_ver', RSS_GENERATOR_VER);
    }

    if (RSS_GENERATOR_AND_VER) {
        $smarty_set->set('generator', RSS_GENERATOR_AND_VER);
    } else {
        $smarty_set->set('generator', OPENPNE_URL);
    }

    if (RSS_GENERATOR_AUTHOR) {
        $smarty_set->set('author', RSS_GENERATOR_AUTHOR);
    } else {
        $smarty_set->set('author', $smarty_set->get('webmaster'));
	}

    if (RSS_COM_TOPIC_COMMNET_ONE) {
        $smarty_set->set('config_is_onecomment', 1 );
    } else {
        $smarty_set->set('config_is_onecomment', 0 );
    }

    if (RSS_WITH_USER) {
        $smarty_set->set('is_mydiary', 1 );
    } else {
        $smarty_set->set('is_mydiary', 0 );
    }

    if (RSS_ONLY_MYFRIEND) {
        $smarty_set->set('config_is_myfriend', 1 );
    } else {
        $smarty_set->set('config_is_myfriend', 0 );
    }

    if (RSS_REVIEW_WITH_USER) {
        $smarty_set->set('config_is_myreview', 1 );
    } else {
        $smarty_set->set('config_is_myreview', 0 );
    }

    if (RSS_GENERATOR_URL) {
        $smarty_set->set('url', RSS_GENERATOR_URL);
    } else {
        $smarty_set->set('url', OPENPNE_URL);
    }

    if (RSS_RIGHTS) {
        $smarty_set->set('rights', RSS_RIGHTS);
    } else {
        $smarty_set->set('rights', 'Copyright 2010 '.SNS_NAME);
    }

    if (RSS_ICON) {
        $smarty_set->set('icon', RSS_ICON);
    }

    if (RSS_LOGO) {
        $smarty_set->set('logo', RSS_LOGO);
    }

    if (RSS_TIME_LOCALE) {
        $smarty_set->set('locale', RSS_TIME_LOCALE);
    } else {
        $smarty_set->set('locale', 'Z');
    }

    if (RSS_TIME_LOCALE_1) {
        $smarty_set->set('locale1', RSS_TIME_LOCALE_1);
    } else {
        $smarty_set->set('locale1', 'Z');
    }

    if (RSS_TIME_LOCALE_2) {
        $smarty_set->set('locale2', RSS_TIME_LOCALE_2);
    } else {
        $smarty_set->set('locale2', 'GMT');
    }

    return $smarty_set;
}

/**
 * 必要なコメントの情報で日記のデータを書き換えて返す
 * 
 * @param int $list 日記のデータ
 * @param int $comment コメントのデータ
 * @return array 置換後の日記のデータ
 */
function rss_set_comment_list($list,$comment)
{
    if ( count($comment) > 0 ) {
        $list['org_r_datetime'] = $list['r_datetime'];
        $list['r_datetime'] = $comment['r_datetime'];
        $list['org_nickname'] = $list['nickname'];
        $list['nickname'] = $comment['nickname'];
        $list['org_body'] = $list['body'];
        $list['body'] = $comment['body'];
        $list['org_image_filename_1'] = $list['image_filename_1'];
        $list['image_filename_1'] = $comment['image_filename_1'];
        $list['org_image_filename_2'] = $list['image_filename_2'];
        $list['image_filename_2'] = $comment['image_filename_2'];
        $list['org_image_filename_3'] = $list['image_filename_3'];
        $list['image_filename_3'] = $comment['image_filename_3'];
    } else {
        $list['org_r_datetime'] = $list['r_datetime'];
        $list['org_nickname'] = $list['nickname'];
        $list['org_body'] = $list['body'];
        $list['org_image_filename_1'] = $list['image_filename_1'];
        $list['org_image_filename_2'] = $list['image_filename_2'];
        $list['org_image_filename_3'] = $list['image_filename_3'];
    }
    return $list;
}

/**
 * １トピック１コメントとするか否かのフラグをチェックして返す
 * 
 * @param int $is_onecomment １トピック１コメントとする(1)か否か(0以外)
 * @return boolean １トピック１コメントとするか否か
 */
function rss_is_onecomment($is_onecomment)
{
    if (isset($is_onecomment) && preg_match("/^[0-9]+$/", $is_onecomment)) {
        if ( intval($is_onecomment) == 0 ) {
            return false;
        }
        else if (intval($is_onecomment) == 1) {
            return true;
        }
    }
    return RSS_COM_TOPIC_COMMNET_ONE;
}

/**
 * マイフレンドの日記のみを対象とするか否かのフラグをチェックして返す
 * 
 * @param int $is_myfriend マイフレンドの日記のみを対象とする(0以外)か否か(0)
 * @return boolean マイフレンドのみのデータを取得するか否か
 */
function rss_is_myfriend($is_myfriend)
{
    if (isset($is_myfriend) && preg_match("/^[0-9]+$/", $is_myfriend)) {
        if (intval($is_myfriend) == 0) {
            return false;
        }
        else if (intval($is_myfriend) == 1) {
            return true;
        }
    }
    return RSS_ONLY_MYFRIEND;
}

/**
 * 自分の日記を対象とするか否かのフラグをチェックして返す
 * 
 * @param int $is_mydiary 自分の日記を対象とする(1)か否か(0以外)
 * @return boolean 自分の日記を対象とするか否か
 */
function rss_is_mydiary($is_mydiary)
{
    if (isset($is_mydiary) && preg_match("/^[0-9]+$/", $is_mydiary)) {
        if ( intval($is_mydiary) == 0 ) {
            return false;
        }
        else if (intval($is_mydiary) == 1) {
            return true;
        }
    }
    return RSS_WITH_USER;
}

/**
 * 自分のレビューを対象とするか否かのフラグをチェックして返す
 * 
 * @param int $is_myreview 自分のレビューを対象とする(1)か否か(0以外)
 * @return boolean 自分のレビューを対象とするか否か
 */
function rss_is_myreview($is_myreview)
{
    if (isset($is_myreview) && preg_match("/^[0-9]+$/", $is_myreview)) {
        if ( intval($is_myreview) == 0 ) {
            return false;
        }
        else if (intval($is_myreview) == 1) {
            return true;
        }
    }
    return RSS_REVIEW_WITH_USER;
}

/**
 * ターゲットのフラグがマイフレンドか否かをチェックして返す
 * 
 * @param string $target 日記の対象フラグ
 * @return boolean ターゲットのフラグがマイフレンドか否か
 */
function rss_check_t_is_myfriend($target)
{
    if (isset($target)) {
        if ( !strcasecmp($target,"myfriend") ) {
            return true;
        }
        return false;
    }
    return false;
}

/**
 * ターゲットのフラグが全員に公開か否かをチェックして返す
 * 
 * @param string $target 日記の対象フラグ
 * @return boolean ターゲットのフラグが全員に公開か否か
 */
function rss_check_t_is_alldiary($target)
{
    if (isset($target)) {
        if ( !strcasecmp($target,"alldiary") ) {
            return true;
        }
        return false;
    }
    return false;
}

/**
 * JSON/JSONP指定の場合は、フィードをそのままではなく、JSON/JSONP形式にして出力する
 * 
 * @param string $view OpenPNE_Action->view
 * @param array $requests HTTP REQUEST
 * @return boolean JSON/JSONPに変換した場合はtrue、していない場合はfalse
 */
function rss_feed_convert_to_json(&$view,$requests)
{
    if (!defined('RSS_ALLOW_JSON_FORMAT') || !RSS_ALLOW_JSON_FORMAT) {
        return false;
    }

    $type = strtolower(trim($requests['type']));

    //JSON/JSONP指定なら、変換して出力
    if (strcmp($type,'json') == 0 || strcmp($type,'jsonp') == 0) {
        $action = $GLOBALS['__Framework']['current_action'];

        require_once 'XML/Unserializer.php';
        $xml = &new XML_Unserializer();
        $xml->setOption(XML_UNSERIALIZER_OPTION_ATTRIBUTES_PARSE, true);
        $xml->unserialize($view->ext_fetch("{$action}.tpl"), false);

        header('Content-type: text/javascript; charset=utf-8');

        //JSON形式に変換
        require_once openpne_ext_search('/rss/lib/Jsphon.php');
        $json = Jsphon::encode($xml->getUnserializedData(),true,true);

        if (strcmp($type,'jsonp') == 0) {
            $callback = trim($requests['callback']);
            if (empty($callback)) {
                 $callback = 'handle';
            }
            echo $callback . '(' . $json . ');';
        } else {
            echo $json;
        }
        return true;
    }

    return false;
}

?>
