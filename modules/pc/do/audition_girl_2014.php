<?php
require_once OPENPNE_WEBAPP_DIR . '/lib/OpenPNE/KtaiUA.php';
require_once OPENPNE_WEBAPP_DIR . '/lib/util/audition.php';

class pc_do_audition_girl_2014 extends OpenPNE_Action
{
    function execute($requests)
    {
    	$u = get_login_member_id();

		// イベントID
		$formval['audition_event_id'] = $requests['a_id'];

		// 基本データ
		$fields=array(
			'family_name', 'first_name', 'kana_family_name', 'kana_first_name',
			'birthday_year', 'birthday_month', 'birthday_day',
			'age',
			'zip', 'address_pref', 'address_1', 'address_2',
			'tel_1', 'tel_2', 'tel_3',
			'email',
			'education', 'graduation_year',
			'speciality',
			'interest',
			'self_pr',
			'wish_department',
			'body_1', 'body_2', 'body_3', 'body_4', 'body_5',
			'photo_filename_1', 'photo_filename_2','consent',
		);
    	foreach ($fields as $value){
    		$formval[$value]=$requests[$value];
			if ($value === 'zip') {
				$formval[$value] = str_replace('-', '', $requests[$value]);
			}
    	}    	
    	$formval['c_member_id']=$u;

		// 家族構成はシリアライズしてfamilysに登録する。
		$family_fields = array(
			'family_f', 'family_f_name',
			'family_m', 'family_m_name',
			'family_b', 'family_b_name',
			'family_yb', 'family_yb_name',
		);
		$familys = array();
		foreach ($family_fields as $val) {
			$familys[$val] = (empty($requests[$val]) === false) ? $requests[$val] : '';
		}
		$formval['familys'] = serialize($familys);
    	$formval['regist_date']=date("Y-m-d");
    	$audition_id=db_audition_member_insert_audition($formval);
    	
    	if($audition_id){
            $data=array();
            // イメージ登録
            if($requests['tmpphotofile_1']){
                $photo_filename_1=image_move_4tmp($requests['a_id'], "augc_2014_{$audition_id}_d_1", $requests['tmpphotofile_1']);
                $data['photo_filename_1']=$photo_filename_1;
            }
            if($requests['tmpphotofile_2']){
                $photo_filename_2=image_move_4tmp($requests['a_id'], "augc_2014_{$audition_id}_d_2", $requests['tmpphotofile_2']);
                $data['photo_filename_2']=$photo_filename_2;
            }

            // PDF出力用のデータ作成(ignore_fieldsを除いて全てのデータをシリアライズする)
            $ignore_fields = array(
                'msg', 'msg1', 'msg2', 'msg3',
                'sessid',
                'message',
                'email_check', 'email_login_check',
                'tmpphotofile_1','tmpphotofile_2',
                'photo_filename_1', 'photo_filename_2',
                'submit',
            );
            $ext_usermeta = array();
            foreach ($requests as $key => $val) {
                if (in_array($key, $ignore_fields) === true) continue;
                $ext_usermeta[$key] = $val;
            }
            $ext_usermeta['photo_filename_1'] = $photo_filename_1;
            $ext_usermeta['photo_filename_2'] = $photo_filename_2;
            $data['ext_usermeta'] = serialize($ext_usermeta);

	    	if($data){
	    		$res = db_audition_member_update_data($audition_id, $data);
	    	}
	    	
	    	// send mail 
	    	$mail=$requests['email'];	    	
			// メール形式が正しく無い場合c_memberテーブルからアドレスを取得する。
	    	if(db_common_is_mailaddress($mail)==false){
	    		$member=db_member_c_member4c_member_id($u,true);
	    		$mail=$member['secure']['regist_address'];
	    	}
	    	$requests['c_member_to']=$member;
			// ./webapp/lib/OpenPNE/Config.php ADMIN_AUDITION_EMAILは空
    		if(ADMIN_AUDITION_EMAIL !=''){
    			fetch_send_mail($mail,'m_pc_audition_girl',$requests,true,ADMIN_AUDITION_EMAIL);//To:応募者
				fetch_send_mail(ADMIN_AUDITION_EMAIL, 'm_pc_audition_girl', $requests,true,ADMIN_AUDITION_EMAIL);//BCC:be amie事務局	    			
    		}else{
    			fetch_send_mail($mail,'m_pc_audition_girl',$requests);//To:応募者
				fetch_send_mail(ADMIN_EMAIL, 'm_pc_audition_girl', $requests);//BCC:be amie事務局
    		}
	    	//openpne_redirect('pc', 'page_audition_girl_2014_end', array());
			$params['_data_area'] = 'pub';
	    	openpne_redirect('pc', 'page_audition_girl_2014_end', $params);
	    	
    	}else{
    		$formval['message']='error';
			$formval['_data_area'] = 'pub';
    		openpne_redirect('pc', 'page_audition_girl_2014', $formval);
    	}
    	
    	exit();
    }
}
