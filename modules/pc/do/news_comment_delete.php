<?php
class pc_do_news_comment_delete extends OpenPNE_Action
{
    function execute($requests)
    {
    	$u = $GLOBALS['AUTH']->uid();
    	if(TALENT === true)
    	{
    		$target_c_comment_id = $requests['target_c_comment_id'];
    		
    		$comment= db_news_get_comment($target_c_comment_id);
    		if($comment){
    			if($comment['c_member_id'] == $u){
    				db_news_delete_news_comment($target_c_comment_id);
    			}
    			openpne_redirect('pc', "page_newsdetail",array("newsid"=>$comment['t_e2_news_contens_id']));
    		}else{
    			openpne_redirect('portal', "page_user_top");
    		}
    		
    		
    	}
    	openpne_redirect('portal', "page_user_top");  	 
    	
    	
    }
}