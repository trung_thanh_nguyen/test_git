<?php
require_once OPENPNE_WEBAPP_DIR . '/lib/OpenPNE/KtaiUA.php';
require_once OPENPNE_WEBAPP_DIR . '/lib/util/audition.php';

class pc_do_audition_miss20_2016 extends OpenPNE_Action
{
    function execute($requests)
    {
    	$u = get_login_member_id();

    	$fields=array(
		'family_name', 'first_name', 'kana_family_name', 'kana_first_name',
		'birthday_year', 'birthday_month', 'birthday_day', 'age',
		'zip', 'address_pref', 'address_1', 'address_2',
		'tel_1', 'tel_2', 'tel_3',
		'email',
		'job',
		'speciality',
		'interest',
		'self_pr',
		'wish_department',
		'body_1', 'body_2', 'body_3', 'body_4', 'body_5',
		'photo_filename_1', 'photo_filename_2',
		'belong_production',
		'other_experience',
		'apply_on_behalf',
		'r_email',
		'r_nickname',
		'r_family_name', 'r_first_name',
		'r_kana_family_name', 'r_kana_first_name',
		'r_zip', 'r_address_pref', 'r_address_1', 'r_address_2',
		'r_tel_1', 'r_tel_2', 'r_tel_3',
		'r_relationship',
//		'familys_1','familys_r1','familys_2','familys_r2','familys_3','familys_r3','familys_4','familys_r4'
    	);
    	
		$formval['audition_event_id'] = $requests['a_id'];

    	foreach ($fields as $value){
    		$formval[$value]=$requests[$value];
    	}    	
    	$formval['c_member_id']=$u;
    	$formval['regist_date']=date("Y-m-d");

    	$audition_id=db_audition_member_insert_audition($formval);
    	
    	if($audition_id){
	    	$data=array();
	    	if($requests['tmpphotofile_1']){
	    		$photo_filename_1=image_move_4tmp($requests['a_id'], "au_{$audition_id}_d_1", $requests['tmpphotofile_1']);
	    		$data['photo_filename_1']=$photo_filename_1;
	    	}
	    	if($requests['tmpphotofile_2']){
	    		$photo_filename_2=image_move_4tmp($requests['a_id'], "au_{$audition_id}_d_2", $requests['tmpphotofile_2']);
	    		$data['photo_filename_2']=$photo_filename_2;
	    	}
	    	if($requests['tmpphotofile_3']){
	    		$photo_filename_3=image_move_4tmp($requests['a_id'], "au_{$audition_id}_d_3", $requests['tmpphotofile_3']);
	    		$data['photo_filename_3']=$photo_filename_3;
	    	}
	    	if($requests['tmpmoviefile']){
	    		$movie_filename=movie_move_4tmp($requests['a_id'], "au_{$audition_id}_m", $requests['tmpmoviefile']);
	    		$data['movie_filename']=$movie_filename;
	    	}

            // PDF出力用のデータ作成(ignore_fieldsを除いて全てのデータをシリアライズする)
            $ignore_fields = array(
                'msg', 'msg1', 'msg2', 'msg3',
                'sessid',
                'message',
                'email_check', 'email_login_check',
                'tmpphotofile_1','tmpphotofile_2','tmpphotofile_3','tmpmoviefile',
                'photo_filename_1', 'photo_filename_2','photo_filename_3', 'movie_filename',
                'submit',
            );
            $ext_usermeta = array();
            foreach ($requests as $key => $val) {
                if (in_array($key, $ignore_fields) === true) continue;
                $ext_usermeta[$key] = $val;
            }
            $ext_usermeta['photo_filename_1'] = $photo_filename_1;
            $ext_usermeta['photo_filename_2'] = $photo_filename_2;
            $ext_usermeta['photo_filename_3'] = $photo_filename_3;
            $ext_usermeta['movie_filename'] = $movie_filename;
            $data['ext_usermeta'] = serialize($ext_usermeta);

	    	if($data){	    		
	    		$res = db_audition_member_update_data($audition_id, $data);
	    	}
	    	
	    	// send mail 
	    	$mail=$requests['email'];	    	
	    	if(db_common_is_mailaddress($mail)==false){
	    		$member=db_member_c_member4c_member_id($u,true);
	    		$mail=$member['secure']['regist_address'];
	    	}
	    	$requests['c_member_to']=$member;

//    		if(ADMIN_AUDITION_EMAIL !=''){
    			fetch_send_mail($mail,'m_pc_audition',$requests,true,ADMIN_AUDITION_EMAIL);//To:応募者
//				fetch_send_mail(ADMIN_AUDITION_EMAIL, 'm_pc_audition', $requests,true,ADMIN_AUDITION_EMAIL);//BCC:be amie事務局	    			
//    		}else{
//    			fetch_send_mail($mail,'m_pc_audition',$requests);//To:応募者
//				fetch_send_mail(ADMIN_EMAIL, 'm_pc_audition', $requests);//BCC:be amie事務局
//    		}
/*	    	
	    	if($requests['r_email']!=''){
    			if(ADMIN_AUDITION_EMAIL !=''){
    				fetch_send_mail($requests['r_email'],'m_pc_audition',$requests,true,ADMIN_AUDITION_EMAIL);//To:応募完了時に送信されるメールについてです
    			}else{
    				fetch_send_mail($requests['r_email'],'m_pc_audition',$requests);
    			}
	    	}
*/	    	
			$params['_data_area'] = 'pub';
	    	openpne_redirect('pc', 'page_audition_miss20_2016_end', $params);
	    	
    	}else{
    		$formval['message']='error';
			$formval['_data_area'] = 'pub';
    		openpne_redirect('pc', 'page_audition_miss20_2016', $formval);
    	}
    	
    	exit();
    }
}
