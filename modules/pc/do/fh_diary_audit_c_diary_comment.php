<?php
/**
 * @copyright 2005-2008 OpenPNE Project
 * @license   http://www.php.net/license/3_01.txt PHP License 3.01
 */


/**
 * 日記コメント削除
 */
class pc_do_fh_diary_audit_c_diary_comment extends OpenPNE_Action
{
    function execute($requests)
    {  
    	
        $u = $GLOBALS['AUTH']->uid();

        $target_c_diary_comment_id =  $_REQUEST['target_c_diary_comment_id'];
        if(TALENT){
	       // foreach ($target_c_diary_comment_id as $val) {
	
	            //--- 権限チェック
	            //日記作成者 or コメント作成者
	
	            $target_c_diary_comment = _do_c_diary_comment4c_diary_comment_id((intval($target_c_diary_comment_id)));
	            $target_c_diary_id = $target_c_diary_comment['c_diary_id'];
	
	            $c_diary = db_diary_get_c_diary4id($target_c_diary_id); 
	            //It's add by e2info 2011-3-29====================================
	            if(TALENT && $c_diary['c_member_id'] == $u){
	            	
	            } else{
	            	handle_kengen_error();
	            	die();
	            } 
	            if($_REQUEST['audit']=='1'){	
	            	db_diary_audit_c_diary_comment($target_c_diary_comment_id,true);
	            }else{
	            	db_diary_audit_c_diary_comment($target_c_diary_comment_id,false);
	            }
		   // }
	
	        $p = array('target_c_diary_id' => $target_c_diary_id);
	        openpne_redirect('pc', 'page_fh_diary', $p);
        }else{
        	
        	 openpne_redirect('pc', 'page_h_err_fh_diary');
        }
    }
}

?>
