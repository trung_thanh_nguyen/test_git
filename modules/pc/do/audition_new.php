<?php
require_once OPENPNE_WEBAPP_DIR . '/lib/OpenPNE/KtaiUA.php';
class pc_do_audition_new extends OpenPNE_Action
{
    function execute($requests)
    {
    	$u = $GLOBALS['AUTH']->uid();
    	//$fields=array("msg","msg1","msg2","msg3","sessid","message");
    	//$formval=$requests;
    	$fields=array(
    	"name_1",
    	"name_2",
    	"kananame_1",
    	"kananame_2",
    	"birthday_year",
    	"birthday_month",
    	"birthday_day",
    	"age",
    	"zip",
    	"address_pref",
    	"address_1",
    	"address_2",
    	"tel_1",
    	"tel_2",
    	"tel_3",
    	"mobile_tel_1",
    	"mobile_tel_2",
    	"mobile_tel_3",
    	"email",
    	"education",
    	"in_school",
    	"graduation_year",
    	"job",
    	"speciality",
    	"interest",
    	"owner_PR",
    //	"wish_department",
    	"wish_addr",
    	"body_1",
    	"body_2",
    	"body_3",
    	"body_4",
    	"body_5",
    	//"photo_filename_1",
    	//"photo_filename_2",
    	//"photo_filename_3",
    	//"movie_filename",
    	"production_radio",
    	"production",
    	"contact_period",
    	"audition_radio",
    	"audition_name",
    	"audition_result",
    	"other_experience",
    	"email_login",
    	"login_nickname",
//    	"r_email",
//    	"r_nickname",
    	"r_name_1",
    	"r_name_2",
    	"r_kana_name_1",
    	"r_kana_name_2",
    	"r_zip",
    	"r_address_pref",
    	"r_address_1",
    	"r_address_2",
    	"r_tel_1",
    	"r_tel_2",
    	"r_tel_3",
    	"r_relationship",    	
    	);
    	
    	
    	foreach ($fields as $value){
    		$formval[$value]=$requests[$value];
    	}    	
    	$formval['member_id']=$u;
    	$formval['add_date']=date("Y-m-d");
    	$formval['audition_type']='new';
    	$audition_id=db_audition_insert_audition($formval);
    	if($audition_id){
	    	$data=array();
	    	if($requests["tmpphotofile_1"]){
	    		if(in_array(m_extname($requests["tmpphotofile_1"]),array(".zip",".rar"))){
	    			$data["photo_filename_1"]=movie_insert_c_movie4tmp("au_{$audition_id}_d_1", $requests["tmpphotofile_1"], $u);
	    		}else{
	    			$data["photo_filename_1"]=image_insert_c_image4tmp("au_{$audition_id}_d_1", $requests["tmpphotofile_1"], $u);
	    		}
	    		
	    	}
	    	if($requests["tmpphotofile_2"]){
	    		if(in_array(m_extname($requests["tmpphotofile_2"]),array(".zip",".rar"))){
	    			$data["photo_filename_2"]=movie_insert_c_movie4tmp("au_{$audition_id}_d_2", $requests["tmpphotofile_2"], $u);
	    		}else{
	    			$data["photo_filename_2"]=image_insert_c_image4tmp("au_{$audition_id}_d_2", $requests["tmpphotofile_2"], $u);
	    		}
	    	}
//	    	if($requests["tmpphotofile_3"]){
//	    		if(in_array(m_extname($requests["tmpphotofile_1"]),array(".zip",".rar"))){	    		
//	    			$data["photo_filename_3"]=movie_insert_c_movie4tmp("au_{$audition_id}_d_3", $requests["tmpphotofile_3"], $u);
//	    		}else{
//	    			$data["photo_filename_3"]=image_insert_c_image4tmp("au_{$audition_id}_d_3", $requests["tmpphotofile_3"], $u);
//	    		}
//	    	}
//	    	if($requests["tmpmoviefile"]){
//	    		$data['movie_filename']= movie_insert_c_movie4tmp("au_{$audition_id}_m", $requests['tmpmoviefile'], $u);
//	    	}
	    	if($data){	    		
	    		db_audition_update_audition_data($audition_id,$data);
	    	}
	    	
	    	// send mail 
	    	$mail=$requests['email'];	    	
	    	if(db_common_is_mailaddress($mail)==false){
	    		$member=db_member_c_member4c_member_id($u,true);
	    		$mail=$member["secure"]["regist_address"];
	    	}
	    	$requests['c_member_to']=$member;
    		if(ADMIN_AUDITION_EMAIL !=""){
    			fetch_send_mail($mail,"m_pc_audition_new",$requests,true,ADMIN_AUDITION_EMAIL);//To:応募者
				fetch_send_mail(ADMIN_AUDITION_EMAIL, "m_pc_audition_new", $requests,true,ADMIN_AUDITION_EMAIL);//BCC:be amie事務局	    			
    		}else{
    			fetch_send_mail($mail,"m_pc_audition_new",$requests);//To:応募者
				fetch_send_mail(ADMIN_EMAIL, "m_pc_audition_new", $requests);//BCC:be amie事務局
    		}
	    	
//	    	if($requests['r_email']!=""){
//	    		if(is_ktai_mail_address($requests['r_email'])==true){
//	    			if(ADMIN_AUDITION_EMAIL !=""){
//	    				fetch_send_mail($requests['r_email'],"m_ktai_audition",$requests,true,ADMIN_AUDITION_EMAIL);//To:応募完了時に送信されるメールについてです
//	    			}else{
//	    				fetch_send_mail($requests['r_email'],"m_ktai_audition",$requests);
//	    			}
//	    		}elseif(db_common_is_mailaddress($requests['r_email'])==true){
//	    			if(ADMIN_AUDITION_EMAIL !=""){
//	    				fetch_send_mail($requests['r_email'],"m_pc_audition",$requests,true,ADMIN_AUDITION_EMAIL);//To:応募完了時に送信されるメールについてです
//	    			}else{
//	    				fetch_send_mail($requests['r_email'],"m_pc_audition",$requests);
//	    			}
//	    		}
//	    	}
	    	
	    	//check iphone
	    	$ktaiUA = new OpenPNE_KtaiUA(); 
	    	if($ktaiUA->is_iphone()==true){
	    		openpne_redirect('pc', 'page_audition_new_end', array("target_c_audition_id"=>$audition_id));
	    	}else{
	    		openpne_redirect('pc', 'page_audition_new_end', array());
	    	}
	    	
    	}else{
    		$formval["message"]="error";
    		openpne_redirect('pc', 'page_audition_new', $formval);
    	}
    	
    	exit();
    }
}