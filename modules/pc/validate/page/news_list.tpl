	<div id="newsmain" >    
        <div id="pagebar"><img src="../../../skin/default/common/pagebar-beamie_news.gif" width="185" height="25" alt="be amieニュース" /></div>
        <div>
        ({foreach from=$newtypenames item=item key=key name=newstypename })
        ({if $key eq $nownewtypenames})
        ({$item})
        ({else})
        <a href="({t_url m=pc a=page_newslist})&amp;type=({$key})&amp;day=({$day})"> ({$item})</a>
        ({/if})
        ({if $smarty.foreach.newstypename.last neq 1})|({/if})
        ({/foreach})
        </div>
        <div class="textpr_area">          
        	<ul>
        	({$layout_free18|smarty:nodefaults|t_url2cmd:"":"":0|t_cmd})          
        	</ul>
        </div>
        
        <div class="tit_category">
          ({if $typename_ja eq 'R-25'})
          <div class="r25news">
            <h2>({$typename_ja})</h2>
          </div>
          ({else})
          <div class="kyodonews">
            <h2>({$typename_ja})</h2>
          </div>
          ({/if})
        </div>
        <div class="tit_category_list">
          <ul>
          ({foreach from=$newslist item=item name=newslist})
            <li><a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$item.t_e2_news_contents_id})">({$item.t_e2_news_title})</a>
            ({if $item.t_e2_news_associated_with})&nbsp; <img src="./skin/default/common/pu_news_cam.jpg" /> ({/if})            
             <span class="date">({$item.t_e2_news_public_dateandtime|date_format:"%m月%d日%H時%M分"}) </span>
            </li>
            ({/foreach})
          </ul>
        </div>
        <div class="banner_area1"> 
        ({advbanner field=news_page dev=pc})
        </div>
        <div class="link_box1">
        ({foreach from=$days item=day name=daylink})
        ({if $current_day eq $day})
        ({$day|date_format:'%m/%d'})
        ({else})
        <a href="({t_url m=pc a=page_newslist})&amp;type=({$type})&amp;day=({$day|date_format:'%Y-%m-%d'})">({$day|date_format:'%m/%d'})</a>
        ({/if})
        ({if $smarty.foreach.daylink.last neq 1})|({/if})
        ({/foreach})
        </div>
        <div class="pr_area1">
          ({$layout_free19|smarty:nodefaults|t_url2cmd:"":"":0|t_cmd})
        </div>
        <div class="link_box1">
        ({foreach from=$newtypenames item=item key=key name=newstypename })
        ({if $key eq $nownewtypenames})
        ({$item})
        ({else})
        <a href="({t_url m=pc a=page_newslist})&amp;type=({$key})&amp;day=({$day})"> ({$item})</a>
        ({/if})
        ({if $smarty.foreach.newstypename.last neq 1})|({/if})
        ({/foreach})
        </div> 
    </div>