<?php
require_once OPENPNE_WEBAPP_DIR . '/lib/OpenPNE/KtaiUA.php';
class pc_page_audition_girl_confirm extends OpenPNE_Action
{
 	function handleError($errors)
    {
        $_REQUEST['msg1'] = $errors['subject'];
        $_REQUEST['msg2'] = $errors['body'];
        $_REQUEST['msg3'] = $errors['public_flag'];        
        openpne_forward('pc', 'page', 'audition', $errors);
        exit;
    }
    
    function execute($requests)
    {    	
    	if(ISGUEST==true){
    		openpne_redirect("portal", "page_user_top");
    		exit();
    	}
    	$u = $GLOBALS['AUTH']->uid(); 
    	$fields=array("msg","msg1","msg2","msg3","sessid","message");
    	$formval=$requests;
    	foreach ($fields as $value){
    		unset($formval[$value]);
    	}
    	
    	$errors=$this->checkformerror($formval);
    	if($errors){
    		openpne_forward("pc", "page","audition_girl",$errors);    		
    		exit();  		
    	}
    		
		$ktaiUA = new OpenPNE_KtaiUA();     	
    	if($ktaiUA->is_iphone()==true){
    		$this->set('is_iphone',true);
    	}else{
    		$this->set('is_iphone',false);
    	}
    	
    	$sessid = session_id();
//         t_image_clear_tmp($sessid);
    	$upfiles = array(
            1 => $_FILES['photo_filename_1'],
            $_FILES['photo_filename_2'],
//             $_FILES['photo_filename_3'], 
                                  
        );
       
        $tmpfiles = array(
            1 => '',
            '',
//             '',            
        );
        
//         $upmovies=array(
//         	1 => $_FILES['movie_filename'],
//         );
//         $tmpmovies = array(
//         	1 => '',
//         );
        
        $data = array('member_id'=>$u,'ext_data'=>serialize($formval),'status'=>'0','adddate'=>date("Y-m-d H:i:s"));
        $audition_id=db_audition_gc_insert_audition($data);
        $formval['audition_id']=$audition_id;
        
        $filesize = 0;
        foreach ($upfiles as $key => $upfile) {        	        	
            if (!empty($upfile) && $upfile['error'] !== UPLOAD_ERR_NO_FILE) {
            	$filesize = $upfile['size']; 
            	$fileformat=m_extname($upfile['name']);
            	
            	if($filesize>10*1024*1024 || in_array($fileformat, array(".jpg",".jpeg",".png"))==false){  //10M
            		$_REQUEST['msg'] = '画像は10MB以内のJPEG形式にしてください';
                    openpne_forward('pc', 'page', 'audition_girl');
                    exit;
            	}else {
            		
            		$target_filename = sprintf("au_%s_d_%s_%s%s",$audition_id,$key,substr(md5(microtime()), 0,5),$fileformat);
            		if(t_image_save2_audition_cache($upfile,$target_filename,$audition_id)){
            			$formval["photo_filename_$key"]=$target_filename;
            		}
            		
            		//file save to cache
//             		$data["photo_filename_1"]=image_insert_c_image4tmp_to_cache("au_{$audition_id}_d_1", $requests["tmpphotofile_1"], $u);
//                     $formval["photo_filename_".$key]=$upfile['name']; 
                }
            }
        }
        
        if($formval['photo_filename_1'] || $formval['photo_filename_2']){
        	$data = array('photo_filename_1'=>$formval['photo_filename_1'],'photo_filename_2'=>$formval['photo_filename_2']);
        	db_audition_gc_update_audition_data($audition_id, $data);
        }
        
      
        foreach ($tmpfiles as $key=>$value){
        	if($value){        		
        		$formval["tmpphotofile_".$key]=$value;
        	}
        }
//      	foreach ($tmpmovies as $key=>$value){
//         	if($value){
//         		$formval["tmpmoviefile"]=$value;
//         	}
//         }
        
    	$this->set('form',$formval);    	
    	return 'success';
    
    }
    
    function checkformerror($formval){
    	$errors=array();
    	if($formval['name_1']==""){ 
    		$errors[]="姓を入力してください。";    		
    	}
    	if($formval['name_2']==""){
    		$errors[]="名を入力してください。";
    	}
    	if($formval['kananame_1']==""){
    		$errors[]="セイを入力してください。";
    	}
    	if($formval['kananame_2']=="" ){
    		$errors[]="メイを入力してください。";
    	}
    	if($formval['birthday_year']==""){
    		$errors[]="生年を入力してください。";
    	}
    	if($formval['birthday_month']==""){
    		$errors[]="生月を入力してください。";
    	}
    	if($formval['birthday_day']==""){
    		$errors[]="生日を入力してください。";
    	}
    	if($formval['age']==""){
    		$errors[]="年齢を入力してください。";
    	} else
    	if(is_numeric($formval['age'])==false){
    		$errors[]="年齢は半角数字で入力してください";
    	}
    	if($formval['zip']==""){
    		$errors[]="郵便番号を入力してください。";
    	} else
    	if(is_numeric($formval['zip'])==false){
    		$errors[]="郵便番号は半角数字で入力してください。";
    	}
    	if($formval['address_pref']==""){
    		$errors[]="都道府県を入力してください。";
    	}
    	if($formval['address_1']==""){
    		$errors[]="市区町村・番地を入力してください。";
    	}
//    	if($formval['address_2']==""){
//    		$errors[]="建物名・号室 を入力してください。";
//    	}
    	
    	if($formval['tel_1']=="" || $formval['tel_2']=="" || $formval['tel_3']=="" ){
    		$errors[]="ご連絡先の電話番号を入力してください。";
    	} else
    	if((is_numeric($formval['tel_1'])==false && $formval['tel_1']=="") ||
    		(is_numeric($formval['tel_2'])==false && $formval['tel_2']=="") ||
    		(is_numeric($formval['tel_3'])==false && $formval['tel_3']=="")){
    		$errors[]="ご連絡先の電話番号は半角数字で入力してください。";
    	}
//    	if($formval['mobile_tel_1']=="" || $formval['mobile_tel_2']=="" || $formval['mobile_tel_3']=="" ){
//    		$errors[]="mobile を入力してください。";
//    	}
    	if($formval['email']=="" || db_common_is_mailaddress($formval['email'])==false){
    		$errors[]="メールアドレスを入力してください。";
    	}    	
    	if($formval['email_check']==""){
    		$errors[]="メールアドレスを正しく入力してください。";
    	}
    	if($formval['email']!=$formval['email_check']){
    		$errors[]="メールアドレス(確認)欄とメールアドレス欄に同じ内容を入力してください。";
    	}
    	if($formval['education']==""){
    		$errors[]="学校名を入力してください。";
    	}
    	if($this->utf8_strlen($formval['education'])>48){
    		$errors[]="学校名は24文字まで入力してください。";
    	}
    	
    	
    	if($formval['speciality']==""){
    		$errors[]="特技を入力してください。";
    	}
    	if($this->utf8_strlen($formval['speciality'])>40){    		
    		$errors[]="特技を20文字まで入力してください。";
    	}
    	if($this->utf8_strlen($formval['interest'])>40){
    		$errors[]="趣味を20文字まで入力してください。";
    	}
        if($formval['interest']==""){
    		$errors[]="趣味を入力してください。";
    	}
    	if($formval['owner_PR']==""){
    		$errors[]="自己PRを入力してください。";
    	}
    	if($this->utf8_strlen($formval['owner_PR'])>300){
    		$errors[]="自己PRは150文字まで入力してください。";
    	}
        if($formval['wish_department']==""){
    		$errors[]="将来の希望を入力してください。";
    	} 
//     	if(strlen($formval['speciality'])>75){
//     		$errors[]="特技は全角25文字まで入力してください。";
//     	}
//     	if(strlen($formval['interest'])>75){
//     		$errors[]="趣味は全角25文字まで入力してください。";
//     	}
    	//check iphone
    	$ktaiUA = new OpenPNE_KtaiUA(); 
    	if($ktaiUA->is_iphone()==false){
// 	    	if($formval['wish_department']=="パーツモデル"){
// 		    	if($_FILES['photo_filename_3']['size']==0){
// 		    		$errors[]="写真3を入力してください。";
// 		    	}  		
// 	    	}else{
		    	if($_FILES['photo_filename_1']['size']==0){
		    		$errors[]="写真1をアップロードしてください。";
		    	}
		    	if($_FILES['photo_filename_2']['size']==0){
		    		$errors[]="写真2をアップロードしてください。 ";
		    	}
// 	    	}
    	}
    	
    	if($formval['body_1']==""){
    		$errors[]="身長を入力してください。";
    	} else
    	if(is_numeric($formval['body_1'])==false){
    		$errors[]="身長は半角数字で入力してください。";
    	}
        if($formval['body_2']==""){
    		$errors[]="バストを入力してください。";
    	} else
    	if(is_numeric($formval['body_2'])==false){
    		$errors[]="バストは半角数字で入力してください。";
    	}
    	if($formval['body_3']==""){
    		$errors[]="ウェストを入力してください。";
    	} else
    	if(is_numeric($formval['body_3'])==false){
    		$errors[]="ウェストは半角数字で入力してください。";
    	}
        if($formval['body_4']==""){
    		$errors[]="ヒップを入力してください。";
    	} else
    	if(is_numeric($formval['body_4'])==false){
    		$errors[]="ヒップは半角数字で入力してください。";
    	}
    	if($formval['body_5']==""){
    		$errors[]="シューズのサイズを入力してください。";
    	} else
    	if(is_numeric($formval['body_5'])==false){
    		$errors[]="シューズは半角数字で入力してください。";
    	}
    	
       
       
    	if($formval['email_login']=="" || db_common_is_mailaddress($formval['email_login'])==false){
    		$errors[]="be amie登録情報のメールアドレスを入力してください 。";
    	}
        if($formval['email_login_check']==""){
    		$errors[]="be amie登録情報のメールアドレスを正しく入力してください。";
    	}
    	if($formval['email_login_check']!=$formval['email_login']){
    		$errors[]="be amie登録情報のメールアドレス(確認)欄とメールアドレス欄に同じ内容を入力してください。";
    	}
    	if($this->utf8_strlen($formval['family_f'])>34 ||
    		$this->utf8_strlen($formval['family_m'])>34 ||
    		$this->utf8_strlen($formval['family_b'])>34 ||
    		$this->utf8_strlen($formval['family_yb'])>34
    	){
    		$errors[]="家族構成の氏名は17文字まで入力してください。";
    	}
	    $now = date('Ymd');
	    $birthday = sprintf('%04d%02d%02d', $formval['birthday_year'], $formval['birthday_month'], $formval['birthday_day']);
	    $age = floor(($now-$birthday)/10000);

    	if($formval['allow']!="1" && $age<20){
    		$errors[] = "保護者の同意が必要です";
    	}
    	if($formval['consent']!="1"){
    		$errors[] = "プライバシーポリシーおよび注意事項に同意が必要です";
    	}
    	return $errors;
    }
    
    function utf8_strlen($str) {
    	$count = 0;
    	for($i = 0; $i < strlen($str); $i++){
    		$value = ord($str[$i]);
    		if($value > 127) {
    			$count++;
    			if($value >= 192 && $value <= 223) $i++;
    			elseif($value >= 224 && $value <= 239) $i = $i + 2;
    			elseif($value >= 240 && $value <= 247) $i = $i + 3;
    			else die('not a utf-8 compatible string');
    		}
    		$count++;
    	}
    	return $count;
    }
    
  	
}