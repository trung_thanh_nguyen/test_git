<?php
require_once OPENPNE_WEBAPP_DIR . '/lib/OpenPNE/KtaiUA.php';
class pc_page_audition_biz_2014_end extends OpenPNE_Action
{
    function execute($requests)
    {
        if ($_SESSION['SAFE_CHECK'] == 0) {
            $_SESSION = array();
            session_destroy();
            $url = "http://" . $_SERVER['SERVER_NAME'] . "/?m=portal&a=page_user_top";
            client_redirect_absolute($url);
        }
        
		$main = array("audition_biz_2014_end_main.tpl");
        $this->set('main', $main);
    	$member_id = get_login_member_id();
        $results = get_member_banner($member_id);
	    $this->set('side_top_contents', $results['side_top_contents']);
	    $this->set('side_bannar'      , $results['side_contents']);
        $this->set('aside', $_SESSION['aside']);
        $this->set('sp', $_SESSION['SMARTPHONE_CHECK']);
		$this->set('login_check', $_SESSION['SAFE_CHECK']);
    	
    	return 'success';
    
    }
}
