<?php 
class pc_page_newspicture extends OpenPNE_Action
{
       
    function execute($requests)
    {
//    	$this->set('layout_free18',portal_get_pc_contents(PORTAL_LAYOUT_FREE18));
//		$this->set('layout_free19',portal_get_pc_contents(PORTAL_LAYOUT_FREE19));
//		$this->set('layout_free20',portal_get_pc_contents(PORTAL_LAYOUT_FREE20));
    	$newtypenames=array(
			"Main"=>'メイン',
    		"Culture/Entertainment"=>'エンタメ',
    		"National"=>'社会',
    		"Sports"=>'スポーツ',
    		"R-25"=>'R-25'
    	);
    	
    	$this->set('title','news picture');
    	$newsid = $requests['newsid'];  
    	$this->set('newsid',$newsid);  	
    	$news=db_news_get_news_by_id($newsid);    	
    	$this->set('newstypename_ja',$newtypenames[$news['t_e2_news_genre']]);
    	
    	$this->set('news',$news);
    	//It's for side bar news ================
    	switch ($news['t_e2_news_genre']){
    		case 'Culture/Entertainment':
    			$this->set('newstype','Entertainment_Trends');
    			break;
    		case 'r25':
    			$this->set('newstype','R-25');
    			break;
    		default :
    			$this->set('newstype',$news['t_e2_news_genre']);
    			break;    			
    	}
    	//==============================================
    	
    	$this->set('day',$news['t_e2_news_date']);

    	$this->set('newtypenames',$newtypenames);
      	$this->set('nownewtypenames',$news['t_e2_news_genre']);
      	$this->set('linkURL',"http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] );
     	$facebook_metas=array();
    	$facebook_metas['og:url']= "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
   		$facebook_metas['og:image'] = "http://".$_SERVER['SERVER_NAME']."/img.php?filename=".$news['t_e2_news_main_filename']."&m=pc";
    	$this->set('facebook_metas',$facebook_metas);
   	
     	
    	return 'success';     	
    }
}