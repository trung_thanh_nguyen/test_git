<?php
require_once OPENPNE_WEBAPP_DIR . '/lib/OpenPNE/KtaiUA.php';
class pc_page_audition_new_confirm extends OpenPNE_Action
{
 	function handleError($errors)
    {
        $_REQUEST['msg1'] = $errors['subject'];
        $_REQUEST['msg2'] = $errors['body'];
        $_REQUEST['msg3'] = $errors['public_flag'];        
   		openpne_forward("pc", "page","audition_new",$errors);    		
        exit;
    }
    
    function execute($requests)
    {    	
    	if(ISGUEST==true){
    		openpne_redirect("portal", "page_user_top");
    		exit();
    	}
    	$u = $GLOBALS['AUTH']->uid(); 
    	$fields=array("msg","msg1","msg2","msg3","sessid","message");
    	$formval=$requests;
    	foreach ($fields as $value){
    		unset($formval[$value]);
    	}
    	
    	$errors=$this->checkformerror($formval);
    	if($errors){
    		openpne_forward("pc", "page","audition_new",$errors);    		
    		exit();  		
    	}
    		
    	
    	$sessid = session_id();
        t_image_clear_tmp($sessid);
    	$upfiles = array(
            1 => $_FILES['photo_filename_1'],
            $_FILES['photo_filename_2'],
                                  
        );
       
        $tmpfiles = array(
            1 => '',
            '',           
        );
        
       
        
    
        $filesize = 0;
        foreach ($upfiles as $key => $upfile) {        	        	
            if (!empty($upfile) && $upfile['error'] !== UPLOAD_ERR_NO_FILE) {
            	$filesize = $upfile['size']; 
            	$fileformat=m_extname($upfile['name']);
            	
            	if($filesize>5*1024*1024 || in_array($fileformat, array(".jpg",".jpeg",".png"))==false){  //5M
            		$_REQUEST['msg'] = '画像は5MB以内のJPEG形式にしてください';
               		openpne_forward("pc", "page","audition_new",$errors);    		
                    exit;
            	}else { 
            		if(in_array($fileformat, array(".zip",".rar"))){                  
                    	$tmpfiles[$key] = t_movie_save2tmp($upfile, $sessid, "d_{$key}", $fileformat); 
            		}else{
            			$tmpfiles[$key] = t_image_save2tmp($upfile, $sessid, "d_{$key}", $fileformat);
            		}
                    $formval["photo_filename_".$key]=$upfile['name']; 
                }
            }
        }
        
              
        foreach ($tmpfiles as $key=>$value){
        	if($value){        		
        		$formval["tmpphotofile_".$key]=$value;
        	}
        }
     	        
    	$this->set('form',$formval);    	
    	return 'regist';
    
    }
    
    function checkformerror($formval){
    	$errors=array();
    	if($formval['name_1']==""){ 
    		$errors[]="姓を入力してください。";    		
    	}
    	if($formval['name_2']==""){
    		$errors[]="名を入力してください。";
    	}
    	if($formval['kananame_1']==""){
    		$errors[]="セイを入力してください。";
    	}
    	if($formval['kananame_2']=="" ){
    		$errors[]="メイを入力してください。";
    	}
    	if($formval['birthday_year']==""){
    		$errors[]="生年を入力してください。";
    	}
    	if($formval['birthday_month']==""){
    		$errors[]="生月を入力してください。";
    	}
    	if($formval['birthday_day']==""){
    		$errors[]="生日を入力してください。";
    	}
    	if($formval['age']==""){
    		$errors[]="年齢を入力してください。";
    	}
    	if($formval['zip']=="" ){
    		$errors[]="郵便番号を入力してください。";
    	}
    	if(is_numeric($formval['zip'])==false){
    		$errors[]="郵便番号必ず半角数字で入力してください。";
    	}
    	if($formval['address_pref']==""){
    		$errors[]="都道府県を入力してください。";
    	}
    	if($formval['address_1']==""){
    		$errors[]="市区町村・番地を入力してください。";
    	}
//    	if($formval['address_2']==""){
//    		$errors[]="建物名・号室 を入力してください。";
//    	}
    	
    	if($formval['tel_1']=="" || $formval['tel_2']=="" || $formval['tel_3']=="" ){
    		$errors[]="自宅電話番号を入力してください。";
    	}
//    	if($formval['mobile_tel_1']=="" || $formval['mobile_tel_2']=="" || $formval['mobile_tel_3']=="" ){
//    		$errors[]="mobile を入力してください。";
//    	}
    	if($formval['email']=="" || db_common_is_mailaddress($formval['email'])==false){
    		$errors[]="メールアドレスを入力してください。";
    	}    	
    	if($formval['email_check']==""){
    		$errors[]="メールアドレスを正しく入力してください。";
    	}
    	if($formval['email']!=$formval['email_check']){
    		$errors[]="メールアドレス(確認)欄とメールアドレス欄に同じ内容を入力してください。";
    	}
    	if($formval['education']==""){
    		$errors[]="学校名を入力してください。";
    	}
    	if($formval['in_school']==""){
    		$errors[]="在学を入力してください。";
    	}
    	if($formval['in_school']=="2" && $formval['graduation_year']==""){
    		$errors[]="卒業年を入力してください。";
    	}
        if($formval['job']==""){
    		$errors[]="現在の職業を入力してください。";
    	}
    	if($formval['speciality']==""){
    		$errors[]="特 技を入力してください。";
    	}
        if($formval['interest']==""){
    		$errors[]="趣 味を入力してください。";
    	}
    	if($formval['owner_PR']==""){
    		$errors[]="自己PRを入力してください。";
    	}
    	if(strlen($formval['owner_PR'])>1500){
    		$errors[]="自己PRは全角450文字まで入力してください。";
    	}
//        if($formval['wish_department']==""){
//    		$errors[]="希望部門を入力してください。";
//    	} 
		if($formval['wish_addr']==""){
			$errors[] = "ご希望応募地区を入力してください。";
		}
    	if(strlen($formval['other_experience'])>1500){
    		$errors[]="その他、経験は全角450文字まで入力してください。";
    	}

    	if(strlen($formval['speciality'])>1500){
    		$errors[]="特技は全角450文字まで入力してください。";
    	}
    	if(strlen($formval['interest'])>1500){
    		$errors[]="趣味は全角450文字まで入力してください。";
    	}
    	if($_FILES['photo_filename_1']['size']==0){
    		$errors[]="写真1をアップロードしてください。";
    	}
    	if($_FILES['photo_filename_2']['size']==0){
    		$errors[]="写真2をアップロードしてください。 ";
    	}
    	if($formval['production_radio']==1){
    		if($formval['production']==""){
    			$errors[]="所属事務所名を入力してください。 ";
    		}
    		if($formval['contact_period']==""){
    			$errors[]="契約期間を入力してください。 ";
    		}
    	}
    	if($formval['audition_radio']==1){
    		if($formval['audition_name']==""){
    			$errors[]="オーディション名を入力してください。 ";
    		}
    		if($formval['audition_result']==""){
    			$errors[]="結果を入力してください。 ";
    		}
    	}
    	
    	if($formval['body_1']==""){
    		$errors[]="身長を入力してください。";
    	}
        if($formval['body_2']==""){
    		$errors[]="バストを入力してください。";
    	}
    	if($formval['body_3']==""){
    		$errors[]="ウェストを入力してください。";
    	}
        if($formval['body_4']==""){
    		$errors[]="ヒップを入力してください。";
    	}
    	if($formval['body_5']==""){
    		$errors[]="靴のサイズを入力してください。";
    	}
       
       
    	if($formval['email_login']=="" || db_common_is_mailaddress($formval['email_login'])==false){
    		$errors[]="メールアドレスを入力してください 。";
    	}
        if($formval['email_login_check']==""){
    		$errors[]="メールアドレスを正しく入力してください。";
    	}
    	if($formval['email_login_check']!=$formval['email_login']){
    		$errors[]="メールアドレス(確認)欄とメールアドレス欄に同じ内容を入力してください。";
    	}
    	if($formval['r_zip']){
    		if(is_numeric($formval['r_zip'])==false){
    			$errors[]="保護者の郵便番号を必ず半角数字で入力してください。";
    		}
    	}
    	return $errors;
    }
    
  	
}