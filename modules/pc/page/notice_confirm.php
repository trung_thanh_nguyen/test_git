<?php

class pc_page_notice_confirm extends OpenPNE_Action
{
    function execute($requests)
    {
    	$u = $GLOBALS['AUTH']->uid();
    	$oid = $requests['oid'];
    	$type= $requests['type'];
    	$forward=$requests['forward'];
    	$message=$requests['message'];
    	$this->set('oid',$oid);
    	$this->set('type',$type);
    	$this->set('forward',$forward);
    	$this->set('message',$message);
    	return 'success';
    }
}