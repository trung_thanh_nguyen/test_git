<?php
class pc_page_img extends OpenPNE_Action
{
	function execute($requests)
	{
		$u = $GLOBALS['AUTH']->uid();
		if(ISGUEST==true){
			//openpne_redirect("pc", "page_o_login",array("login_params"=>"m=pc&a=page_img&filename=".$requests['filename']));
			openpne_redirect("pc", "page_o_login",array("login_params"=> OPENPNE_URL."wallpaper.html?filename=".$requests['filename']));
			exit();
		}		
		
		$file_name=$requests['filename'];
		$img_dir_path = OPENPNE_VAR_DIR . "/img/wallpaper/";		
		ob_start();
		header('Content-type: image/jpg');
		header('Content-Disposition:attachment;filename="'.$file_name.'"');
		readfile($img_dir_path.$file_name);
		ob_flush();
		ob_clean();
		die();
		
	}
}