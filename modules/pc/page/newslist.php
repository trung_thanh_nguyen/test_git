<?php 
class pc_page_newslist extends OpenPNE_Action
{
       
    function execute($requests)
    {

		$newtypenames=array(
			"Main"=>'メイン',
    		"Culture/Entertainment"=>'エンタメ',
//    		"National"=>'社会',
		"OVO"=>'OVO',
    		"Sports"=>'スポーツ',
    		"R-25"=>'R-25'
    	);
    	
    	$this->set('title','news list');
    	$type=$requests['type'];
    	if(!$type){
    		$type="Main";
    	}
    	$this->set('type',$type);
    	//It's for side bar news ================
    	$this->set('newstype',$type);
    	//==end========================================
    	
    	$this->set('typename_ja',$newtypenames[$type]);
    	$day = $requests['day'];
    	
    	
    	if($type == 'Entertainment_Trends' || $type == "Culture/Entertainment"){
    		$type = "Culture/Entertainment";
    		
    		$sub='culture/entertainment';
    		 		
    	}elseif($type == 'National'){
    		$sub='national';
    	}elseif($type=='Sports'){
    		//$sub='sports';
    	}elseif($type=='R-25'){
    		$sub='';
    	}elseif($type == 'Main'){
    		$sub='';
	}elseif($type=='OVO'){
		$sub='';
    	}else{
    		die();
    	}    	
    	    	
    	$days=array(
    		date("Y-m-d",time()),
    		date("Y-m-d",strtotime('-1 day')),
    		date("Y-m-d",strtotime('-2 day')),
    		date("Y-m-d",strtotime('-3 day')),
    		date("Y-m-d",strtotime('-4 day')),
    		date("Y-m-d",strtotime('-5 day')),
    		date("Y-m-d",strtotime('-6 day')),
    		date("Y-m-d",strtotime('-7 day')),
    	);
    	
   		 foreach ($days as $key=> $d){
    		if(db_news_get_count_by_type($type,$sub,$d)>0 ){
    		}else{
    			unset ( $days[$key] ); 
    		}
    	} 
    	
    	if($days){
	    	if(!$day){
	    		$day=reset($days);
	    	}
    	}else{
	    	if(!$day){
	    		$day=date('Y-m-d',time());
	    	}
    	}
    	if(!isdate($day)){
    		die('');
    	}
    	
    	$this->set('days',$days);
    	$this->set('nowday',$day);
    	$this->set('current_day',$day);
    	
    	$list=db_new_get_by_type_list($type,$sub,$day,50);
    	
    	$this->set('newslist',$list);
    	
    	$this->set('newtypenames',$newtypenames);
    	$this->set('nownewtypenames',$type);
    	
        $this->set('login_check', $_SESSION['SAFE_CHECK']);
        /* content set*/
        /* Slide Show, Tile, Pick Up, CategoryL, Topics */
        $main = array("common/news_list.tpl");
        
        $this->set('main', $main);
        
        $results = get_member_banner($member_id);
	    $this->set('side_top_contents', $results['side_top_contents']);
	    $this->set('side_bannar'      , $results['side_contents']);
        
        $this->set('aside', $_SESSION['aside']);
        
        $this->set('sp', $_SESSION['SMARTPHONE_CHECK']);
    	
    	return 'success';    	
    }
}
