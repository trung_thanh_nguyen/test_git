<?php
require_once OPENPNE_WEBAPP_DIR . '/lib/OpenPNE/KtaiUA.php';
class pc_page_audition_biz_2014_confirm extends OpenPNE_Action
{
 	function handleError($errors)
    {
        $_REQUEST['msg1'] = $errors['subject'];
        $_REQUEST['msg2'] = $errors['body'];
        $_REQUEST['msg3'] = $errors['public_flag'];        
        openpne_forward('pc', 'page', 'audition_biz_2014', $errors);
        exit;
    }
    
    function execute($requests)
    {
        if ($_SESSION['SAFE_CHECK'] == 0) {
            $_SESSION = array();
            session_destroy();
            $url = "http://" . $_SERVER['SERVER_NAME'] . "/?m=portal&a=page_user_top";
            client_redirect_absolute($url);
        }

    	$member_id = get_login_member_id();
    	$fields=array("msg","msg1","msg2","msg3","sessid","message");
    	$formval=$requests;
	    $this->set('login_check', $_SESSION['SAFE_CHECK']);
    	
    	foreach ($fields as $value){
    		unset($formval[$value]);
    	}

    	$errors=$this->checkformerror($formval);
    	if($errors){
    		openpne_forward("pc", "page","audition_biz_2014",$errors);
    		exit();  		
    	}
    	
    	$sessid = session_id();
        t_image_clear_tmp($sessid);
    	$upfiles = array(
            1 => $_FILES['photo_filename_1'],
            $_FILES['photo_filename_2'],
        );
       
        $tmpfiles = array(
            1 => '',
            '',
        );
        
        $filesize = 0;
        foreach ($upfiles as $key => $upfile) {        	        	
            if (!empty($upfile) && $upfile['error'] !== UPLOAD_ERR_NO_FILE) {
            	$filesize = $upfile['size']; 
            	$fileformat=m_extname($upfile['name']);
            	
            	if($filesize>10*1024*1024 || in_array($fileformat, array(".jpg",".jpeg"))==false){  //10M
            		//$_REQUEST['msg'] = '画像は10MB以内のJPEG形式にしてください';
            		$error[] = '画像は10MB以内のJPEG形式にしてください';
                    openpne_forward('pc', 'page', 'audition_biz_2014', $error);
                    exit;
            	}else { 
            		if(in_array($fileformat, array(".zip",".rar"))){                  
                    	$tmpfiles[$key] = t_movie_save2tmp($upfile, $sessid, "d_{$key}", $fileformat); 
            		}else{
            			$tmpfiles[$key] = t_image_save2tmp($upfile, $sessid, "d_{$key}", $fileformat);
            		}
                    $formval["photo_filename_".$key]=$upfile['name']; 
                }
            }
        }
              
        foreach ($tmpfiles as $key=>$value){
        	if($value){        		
        		$formval["tmpphotofile_".$key]=$value;
        	}
        }

        $formval["apply_on_behalf"] = (empty($formval["r_email"]))? 0: 1;

		$main = array("audition_biz_2014_confirm_main.tpl");
        $this->set('main', $main);
        $results = get_member_banner($member_id);
	    $this->set('side_top_contents', $results['side_top_contents']);
	    $this->set('side_bannar'      , $results['side_contents']);
        
        //$this->set('aside', $_SESSION['aside']);
        
        $this->set('sp', $_SESSION['SMARTPHONE_CHECK']);
    	$this->set('form',$formval);

    	return 'success';
    
    }

    function checkformerror($formval){
    	$errors=array();
    	if($formval['family_name']==""){ 
    		$errors[]="姓を入力してください。";    		
    	}
    	if($formval['first_name']==""){
    		$errors[]="名を入力してください。";
    	}
    	if($formval['kana_family_name']==""){
    		$errors[]="セイを入力してください。";
    	}
    	if($formval['kana_first_name']=="" ){
    		$errors[]="メイを入力してください。";
    	}
    	if($formval['birthday_year']==""){
    		$errors[]="生年を入力してください。";
    	}
    	if($formval['birthday_month']==""){
    		$errors[]="生月を入力してください。";
    	}
    	if($formval['birthday_day']==""){
    		$errors[]="生日を入力してください。";
    	}
    	if($formval['age']==""){
    		$errors[]="年齢を入力してください。";
    	}

    	if($formval['address_pref']==""){
    		$errors[]="都道府県を入力してください。";

		// 郵便番号正当性チェック
    	} elseif ($formval['address_pref'] != "日本国外" && preg_match("/^\d{7}$/", $formval['zip']) == false) {
    		$errors[]="郵便番号を正しく入力してください。";

		}

    	if($formval['address_1']==""){
    		$errors[]="市区町村・番地を入力してください。";
    	}
    	if($formval['tel_1']=="" || $formval['tel_2']=="" || $formval['tel_3']=="" ){
    		$errors[]="自宅電話番号を入力してください。";
    	}
    	if($formval['email']=="" || db_common_is_mailaddress($formval['email'])==false){
    		$errors[]="メールアドレスを正しく入力してください。";
    	}    	
    	if($formval['email_check']==""){
    		$errors[]="メールアドレス(確認)を入力してください。";
    	}
    	if($formval['email']!=$formval['email_check']){
    		$errors[]="メールアドレス(確認)欄とメールアドレス欄に同じ内容を入力してください。";
    	}
        if($formval['gender']==""){
            $errors[]="性別を入力してください。";
        }
        if($formval['job']==""){
            $errors[]="職業・学年を入力してください。";
        }
        if (empty($formval['job']) === false && mb_strlen($formval['job'], 'UTF-8') > 10) {
            $errors[]="職業・学年を10文字まで入力してください。";
        }
    	if($formval['speciality']==""){
    		$errors[]="特技を入力してください。";
    	}
        if($formval['interest']==""){
    		$errors[]="趣味を入力してください。";
    	}
    	if($formval['self_pr']==""){
    		$errors[]="自己PRを入力してください。";
    	}
		if (empty($formval['family_f']) === false && mb_strlen($formval['family_f'], 'UTF-8') > 17) {
    		$errors[]="家族構成の氏名を17文字まで入力してください。";
		}
		if (empty($formval['family_m']) === false && mb_strlen($formval['family_m'], 'UTF-8') > 17) {
    		$errors[]="家族構成の氏名を17文字まで入力してください。";
		}
		if (empty($formval['family_b']) === false && mb_strlen($formval['family_b'], 'UTF-8') > 17) {
    		$errors[]="家族構成の氏名を17文字まで入力してください。";
		}
		if (empty($formval['family_yb']) === false && mb_strlen($formval['family_yb'], 'UTF-8') > 17) {
    		$errors[]="家族構成の氏名を17文字まで入力してください。";
		}
    	if(mb_strlen($formval['self_pr'], 'UTF-8')>150){
    		$errors[]="自己PRは150文字まで入力してください。";
    	}
        if($formval['wish_department']==""){
    		$errors[]="芸能界での目標を選択してください。";
    	} 
    	if(mb_strlen($formval['speciality'], 'UTF-8')>20){
    		$errors[]="特技は20文字まで入力してください。";
    	}
    	if(mb_strlen($formval['interest'], 'UTF-8')>20){
    		$errors[]="趣味は20文字まで入力してください。";
        }
        if($_FILES['photo_filename_1']['size']==0){
            $errors[]="写真1をアップロードしてください。";
        }
        if($_FILES['photo_filename_2']['size']==0){
            $errors[]="写真2をアップロードしてください。 ";
        }
        if($formval['body_1']=="" || intval($formval['body_1']) > 1000){
    		$errors[]="身長を正しく入力してください。";
    	}
        if($formval['body_2']=="" || intval($formval['body_2']) > 1000){
    		$errors[]="バストを正しく入力してください。";
    	}
    	if($formval['body_3']=="" || intval($formval['body_3']) > 1000){
    		$errors[]="ウェストを正しく入力してください。";
    	}
        if($formval['body_4']=="" || intval($formval['body_4']) > 1000){
    		$errors[]="ヒップを正しく入力してください。";
    	}
    	if($formval['body_5']=="" || intval($formval['body_5']) > 1000){
    		$errors[]="靴のサイズを正しく入力してください。";
    	}
    	if($formval['email_login']=="" || db_common_is_mailaddress($formval['email_login'])==false){
    		$errors[]="be amie登録メールアドレスを正しく入力してください 。";
    	}
        if($formval['email_login_check']==""){
    		$errors[]="be amie登録メールアドレス(確認)を入力してください。";
    	}
    	if($formval['email_login_check']!=$formval['email_login']){
    		$errors[]="be amie登録メールアドレス(確認)欄とbe amie登録メールアドレス欄に同じ内容を入力してください。";
    	}
        // 未成年の場合
        if (intval($formval['age']) < 20 ) {
            if($formval['r_family_name']==""){
                $errors[]="(保護者同意)姓を入力してください。";
            }
            if($formval['r_first_name']==""){
                $errors[]="(保護者同意)名を入力してください。";
            }
            if($formval['r_kana_family_name']==""){
                $errors[]="(保護者同意)セイを入力してください。";
            }
            if($formval['r_kana_first_name']=="" ){
				$errors[]="(保護者同意)メイを入力してください。";
			}

			if($formval['r_address_pref']==""){
				$errors[]="(保護者同意)都道府県を入力してください。";

			// 郵便番号正当性チェック
			} elseif ($formval['r_address_pref'] != "日本国外" && preg_match("/^\d{7}$/", $formval['r_zip']) == false) {
				$errors[]="(保護者同意)郵便番号を正しく入力してください。";

			}

			if($formval['r_address_1']==""){
				$errors[]="(保護者同意)市区町村・番地を入力してください。";
            }
            if($formval['r_tel_1']=="" || $formval['r_tel_2']=="" || $formval['r_tel_3']=="" ){
                $errors[]="(保護者同意)自宅電話番号を入力してください。";
            }
            if($formval['r_relationship']=="" ){
                $errors[]="(保護者同意)本人との関係を入力してください。";
            }
            if (is_null($formval['allow']) === TRUE){
                $errors[]="保護者の同意の上、保護者の同意欄にチェックしてください。";
            }
        }
        if (is_null($formval['consent']) === TRUE){
    		$errors[]="プライバシーポリシー及び注意事項をご確認の上、同意欄にチェックしてください。";
    	}
		
    	return $errors;
    }
    
  	
}
