<?php
require_once OPENPNE_MODULES_DIR . '/admin/lib/db_admin.php';
class pc_page_audition_biz extends OpenPNE_Action
{
    function execute($requests)
    {
    	//e2info 2012-8-30===========================
//commented by oscar request    	
//     	header('Location: '. 'http://beamie.jp/bizcontest.html');    	
//     	exit();
// var_dump($_SESSION);die();
 		
    	//end=========================================
    	$u = $GLOBALS['AUTH']->uid();
		if(ISGUEST==true){
			openpne_redirect("pc", "page_o_login",array("login_params"=>"m=pc&a=page_audition_biz"));
			exit();
		}
		@session_start();
/*		
		if ($_GET['sec']=='beamie2010') {
			$_SESSION['sec']='beamie2010';
		} else if ($_GET['sec']=='beamie2010off') {
			$_SESSION['sec']='';
		}
		if($_SESSION['sec']!='beamie2010'){
			die('メンテナンス中です');
		}
*/		
		$ktaiUA = new OpenPNE_KtaiUA();     	
    	if($ktaiUA->is_iphone()==true){
    		$this->set('is_iphone',true);
    	}else{
    		$this->set('is_iphone',false);
    	}
		$member=db_member_c_member4c_member_id($u,true,false);
    	if($requests['email_login']==""){
    		$requests['email_login']=$member['secure']['regist_address'];
    		$requests['email_login_check']=$member['secure']['regist_address'];
    	} 
    	if($requests['login_nickname']==""){
    		$requests['login_nickname']=$member['nickname'];
    	}	
    	
		
    	$this->set('form',$requests);   
    	//var_dump($requests['wish_department']);

    	$years=array();
    	$current_year=1996;
    	for($i=1962;$i<=$current_year;$i++){
    		$years[]=$i;
    	}
    	$months=array();
    	for($i=1;$i<=12;$i++){
    		$months[]=$i;
    	}
    	$days=array();
    	for($i=1;$i<=31;$i++){
    		$days[]=$i;
    	}
    	
    	$this->set('years',$years);
    	$this->set('months',$months);
    	$this->set('days',$days);
    	
    	$pref=array(
    	'北海道',
    	'青森県',
    	'岩手県',
    	'宮城県',
    	'秋田県',
    	'山形県',
    	'福島県',
    	'茨城県',
    	'栃木県',
    	'群馬県',
    	'埼玉県',
    	'千葉県',
    	'東京都',
    	'神奈川県',
    	'新潟県',
    	'富山県',
    	'石川県',
    	'福井県',
    	'山梨県',
    	'長野県',
    	'岐阜県',
    	'静岡県',
    	'愛知県',
    	'三重県',
    	'滋賀県',
    	'京都府',
    	'大阪府',
    	'兵庫県',
    	'奈良県',
    	'和歌山県',
    	'鳥取県',
    	'島根県',
    	'岡山県',
    	'広島県',
    	'山口県',
    	'徳島県',
    	'香川県',
    	'愛媛県',
    	'高知県',
    	'福岡県',
    	'佐賀県',
    	'長崎県',
    	'熊本県',
    	'大分県',
    	'宮崎県',
    	'鹿児島県',
    	'沖縄県',
    	);
    	$this->set('pref',$pref);
//     	$department = array("女優","男性俳優","タレント","キャスター・レポーター","バラエティ","お笑い","ファッションモデル","CM・雑誌モデル","キャンペーンモデル","男性モデル","子供モデル","シルバーモデル","パーツモデル");
    	$department = array("俳優","女優","歌手","タレント","モデル");
    	$this->set("department",$department);
    	$contact = array('テレビ','ラジオ','新聞','雑誌','インターネット','携帯電話サイト','その他');
    	$this->set('contact',$contact);
    	$c_free_page = db_admin_get_c_free_page_one(14);
    	$this->set('privacy_policy', $c_free_page);
    	
    	
    	return 'success';
    
    }
}
