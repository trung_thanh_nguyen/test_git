<?php
/**
 * @copyright 2005-2008 OpenPNE Project
 * @license   http://www.php.net/license/3_01.txt PHP License 3.01
 */

class pc_page_o_search extends OpenPNE_Action
{
    function isSecure()
    {
        return false;
    }

    function execute($requests)
    {
        $freearea_info = pne_cache_call(90, 'portal_get_pc_free_contents_multiple',
        		array(PORTAL_LAYOUT_FREE1,  PORTAL_LAYOUT_FREE2,  PORTAL_LAYOUT_FREE3,  PORTAL_LAYOUT_FREE4,  PORTAL_LAYOUT_FREE5
        				,PORTAL_LAYOUT_FREE11, PORTAL_LAYOUT_FREE12, PORTAL_LAYOUT_FREE13, PORTAL_LAYOUT_FREE14, PORTAL_LAYOUT_FREE15
        				,PORTAL_LAYOUT_FREE16, PORTAL_LAYOUT_FREE17, PORTAL_LAYOUT_FREE18, PORTAL_LAYOUT_FREE19, PORTAL_LAYOUT_FREE20));
        
        $this->set('layout_free1' ,$freearea_info[1]);
        $this->set('layout_free2' ,$freearea_info[2]);
        $this->set('layout_free3' ,$freearea_info[3]);
        $this->set('layout_free4' ,$freearea_info[4]);
        $this->set('layout_free5' ,$freearea_info[5]);
        $this->set('layout_free11',$freearea_info[11]);
        $this->set('layout_free12',$freearea_info[12]);
        $this->set('layout_free13',$freearea_info[13]);
        $this->set('layout_free14',$freearea_info[14]);
        $this->set('layout_free15',$freearea_info[15]);
        $this->set('layout_free16',$freearea_info[16]);
        $this->set('layout_free17',$freearea_info[17]);
        $this->set('layout_free18',$freearea_info[18]);
        $this->set('layout_free19',$freearea_info[19]);
        $this->set('layout_free20',$freearea_info[20]);
        
        
        $this->set('k',urldecode($requests['k']));
        $this->set('pchkpage',$requests['pchkpage']);
    	portal_get_pc_right_common($this);
        
        
        return 'portal';
    }
}