<?php
require_once OPENPNE_MODULES_DIR . '/admin/lib/db_admin.php';
require_once OPENPNE_WEBAPP_DIR . '/lib/util/audition.php';
require_once OPENPNE_WEBAPP_DIR . '/lib/db/audition_common.php';

class pc_page_audition extends OpenPNE_Action
{
    function execute($requests)
    {
        if ($_SESSION['SAFE_CHECK'] == 0) {
            $_SESSION = array();
            session_destroy();
            $url = "/?m=pc&a=page_o_public_invite&ssl_param=1";
            client_redirect_absolute($url);
        }

        $this->set('login_check', $_SESSION['SAFE_CHECK']);

        // オーディションイベント情報取得
        $event_info = selectAuditionEvent($requests['a_id']);
        $this->set('a_id', $requests['a_id']);

        // オーディション期間確認
        $on_audition = isAuditionValidityDate($event_info['apply_start_date'], $event_info['apply_end_date']);
        $this->set('on_audition', $on_audition);

		// オーディション期間中
		if ($on_audition === TRUE) {

			$member_id = get_login_member_id();
			$member=db_member_c_member4c_member_id($member_id,true,false);
			if($requests['email_login']==""){
				$requests['email_login']=$member['secure']['regist_address'];
				$requests['email_login_check']=$member['secure']['regist_address'];
			} 
			if($requests['login_nickname']==""){
				$requests['login_nickname']=$member['nickname'];
			}	

			$this->set('form',$requests);   
			//var_dump($requests['wish_department']);*/

			$years=array();
			$current_year=Date("Y");                           
			for($i=1931;$i<=$current_year;$i++){               
				$years[]=$i;                                   
			}                                                  
			$months=array();                                   
			for($i=1;$i<=12;$i++){                             
				$months[]=$i;
			}
			$days=array();
			for($i=1;$i<=31;$i++){
				$days[]=$i;
			}

			$this->set('years',$years);
			$this->set('months',$months);
			$this->set('days',$days);

			$pref=array(
					'北海道',
					'青森県',
					'岩手県',
					'宮城県',
					'秋田県',
					'山形県',
					'福島県',
					'茨城県',
					'栃木県',
					'群馬県',
					'埼玉県',
					'千葉県',
					'東京都',
					'神奈川県',
					'新潟県',
					'富山県',
					'石川県',
					'福井県',
					'山梨県',
					'長野県',
					'岐阜県',
					'静岡県',
					'愛知県',
					'三重県',
					'滋賀県',
					'京都府',
					'大阪府',
					'兵庫県',
					'奈良県',
					'和歌山県',
					'鳥取県',
					'島根県',
					'岡山県',
					'広島県',
					'山口県',
					'徳島県',
					'香川県',
					'愛媛県',
					'高知県',
					'福岡県',
					'佐賀県',
					'長崎県',
					'熊本県',
					'大分県',
					'宮崎県',
					'鹿児島県',
					'沖縄県',
					);
			$this->set('pref',$pref);
			$department = array("女優","男性俳優","タレント","キャスター・レポーター","バラエティ","お笑い","ファッションモデル","CM・雑誌モデル","キャンペーンモデル","男性モデル","子供モデル","シルバーモデル","パーツモデル");
			$this->set("department",$department);
		}
    	
    	//$c_free_page = db_admin_get_c_free_page_one(14);
    	//$this->set('privacy_policy', $c_free_page);
        
		$main = array("audition_main.tpl");
        
        $this->set('main', $main);
        
        $results = get_member_banner($member_id);
	    $this->set('side_top_contents', $results['side_top_contents']);
	    $this->set('side_bannar'      , $results['side_contents']);
        
        //$this->set('aside', $_SESSION['aside']);
        
        $this->set('sp', $_SESSION['SMARTPHONE_CHECK']);

	    $this->set('domain', ($_SESSION['SMARTPHONE_CHECK']) ? '/sp/new/' : '/new/');

        // Add sample breadcrumbs
        $breadcrumbs = array(
            array('name' => 'TOP', 'link' => '../'),
            array('name' => 'オーディション', 'link' => '../'),
            array('name' => 'オーディション応募フォーム', 'link' => null)
        );

        $this->set('breadcrumbs', $breadcrumbs);
    	
    	
    	return 'success';
    
    }
}
