<?php
require_once OPENPNE_WEBAPP_DIR . '/lib/OpenPNE/KtaiUA.php';
class pc_page_audition_summer_2015_confirm extends OpenPNE_Action
{
 	function handleError($errors)
    {
        $_REQUEST['msg1'] = $errors['subject'];
        $_REQUEST['msg2'] = $errors['body'];
        $_REQUEST['msg3'] = $errors['public_flag'];        
        openpne_forward('pc', 'page', 'audition', $errors);
        exit;
    }
    
    function execute($requests)
    {
        if ($_SESSION['SAFE_CHECK'] == 0) {
            $_SESSION = array();
            session_destroy();
            $url = "http://" . $_SERVER['SERVER_NAME'] . "/?m=portal&a=page_user_top";
            client_redirect_absolute($url);
        }
        
    	$member_id = get_login_member_id();
    	$fields=array("msg","msg1","msg2","msg3","sessid","message");
    	$formval=$requests;
	    $this->set('login_check', $_SESSION['SAFE_CHECK']);
    	
    	foreach ($fields as $value){
    		unset($formval[$value]);
    	}
    	
    	$errors=$this->checkformerror($formval);
    	if($errors){
    		openpne_forward("pc", "page","audition_summer_2015",$errors);    		
    		exit();  		
    	}
    		
    	
    	$sessid = session_id();
        t_image_clear_tmp($sessid);
    	$upfiles = array(
            1 => $_FILES['photo_filename_1'],
            $_FILES['photo_filename_2'],
            $_FILES['photo_filename_3'], 
                                  
        );
       
        $tmpfiles = array(
            1 => '',
            '',
            '',            
        );
        
        $upmovies=array(
        	1 => $_FILES['movie_filename'],
        );
        $tmpmovies = array(
        	1 => '',
        );
        
    
        $filesize = 0;
        foreach ($upfiles as $key => $upfile) {        	        	
            if (!empty($upfile) && $upfile['error'] !== UPLOAD_ERR_NO_FILE) {
            	$filesize = $upfile['size']; 
            	$fileformat=m_extname($upfile['name']);
            	
            	if($filesize>10*1024*1024 || in_array($fileformat, array(".jpg",".jpeg",".png"))==false){  //10M
            		$error[] = '画像は10MB以内のJPEG形式にしてください';
                    openpne_forward('pc', 'page', 'audition', $error);
                    exit;
            	}else { 
            		if(in_array($fileformat, array(".zip",".rar"))){                  
                    	$tmpfiles[$key] = t_movie_save2tmp($upfile, $sessid, "d_{$key}", $fileformat); 
            		}else{
            			$tmpfiles[$key] = t_image_save2tmp($upfile, $sessid, "d_{$key}", $fileformat);
            		}
                    $formval["photo_filename_".$key]=$upfile['name']; 
                }
            }
        }
        
        $movietypes=array(
        ".wmv",
        ".3gp",
        ".3g2",
        ".mov",
        ".mp4",
        ".mpeg",
        ".flv",
        ".mkv",
        );        
    	foreach($upmovies as $key => $upfile){
        	if(!empty($upfile)&&  $upfile['error'] !== UPLOAD_ERR_NO_FILE){
        		$extname=m_extname($upfile['name']);
        		$extname=strtolower($extname);
        		$filesize = $upfile['size'];     		
        		if($filesize>20*1024*1024){  //20M
        			$error[] = 'ファイルサイズは２０ＭＢに以内にしてください';
                    openpne_forward('pc', 'page', 'audition', $error);
                    exit;
        		}
        		if(!in_array($extname, $movietypes)){
        			$error[] = '3GP、3G2、WMV、MOV、MP4、MPEG、FLV、MKVのみのファイルアップロードが可能です。';
                    openpne_forward('pc', 'page', 'audition', $error);
                    exit;
        		}else{
        			$tmpmovies[$key] = t_movie_save2tmp($upfile, $sessid, "m_{$key}", $extname);
        			if($tmpmovies[$key]==""){
        				$error[] = '動画ファイルのアップロード失敗しました。';
                    	openpne_forward('pc', 'page', 'audition', $error);
                    	exit;
        			}
        			$formval["movie_filename"]=$upfile['name'];       			            	
        		}
        		
        	}
        }       
        foreach ($tmpfiles as $key=>$value){
        	if($value){        		
        		$formval["tmpphotofile_".$key]=$value;
        	}
        }
     	foreach ($tmpmovies as $key=>$value){
        	if($value){
        		$formval["tmpmoviefile"]=$value;
        	}
        }
        
        $formval["apply_on_behalf"] = (empty($formval["r_email"]))? 0: 1;
        
		$main = array("audition_summer_2015_confirm_main.tpl");
        
        $this->set('main', $main);
        
        $results = get_member_banner($member_id);
	    $this->set('side_top_contents', $results['side_top_contents']);
	    $this->set('side_bannar'      , $results['side_contents']);
        
        //$this->set('aside', $_SESSION['aside']);
        
        $this->set('sp', $_SESSION['SMARTPHONE_CHECK']);
        
    	$this->set('form',$formval);
    	return 'success';
    
    }
    
    function checkformerror($formval){
    	$errors=array();
    	if($formval['family_name']==""){ 
    		$errors[]="姓を入力してください。";    		
    	}
    	if($formval['first_name']==""){
    		$errors[]="名を入力してください。";
    	}
    	if($formval['kana_family_name']==""){
    		$errors[]="セイを入力してください。";
    	}
    	if($formval['kana_first_name']=="" ){
    		$errors[]="メイを入力してください。";
    	}
    	if($formval['birthday_year']==""){
    		$errors[]="生年を入力してください。";
    	}
    	if($formval['birthday_month']==""){
    		$errors[]="生月を入力してください。";
    	}
    	if($formval['birthday_day']==""){
    		$errors[]="生日を入力してください。";
    	}
    	if($formval['age']==""){
    		$errors[]="年齢を入力してください。";
    	}
    	if($formval['zip']==""){
    		$errors[]="郵便番号を入力してください。";
    	} elseif (preg_match("/^\d{7}$/", $formval['zip']) == false) {
    		$errors[]="郵便番号を正しく入力してください。";
		}

    	if($formval['address_pref']==""){
    		$errors[]="都道府県を入力してください。";
    	}
    	if($formval['address_1']==""){
    		$errors[]="市区町村・番地を入力してください。";
    	}
//    	if($formval['address_2']==""){
//    		$errors[]="建物名・号室 を入力してください。";
//    	}
    	
    	if($formval['tel_1']=="" || $formval['tel_2']=="" || $formval['tel_3']=="" ){
    		$errors[]="自宅電話番号を入力してください。";
    	}
//    	if($formval['mobile_tel_1']=="" || $formval['mobile_tel_2']=="" || $formval['mobile_tel_3']=="" ){
//    		$errors[]="mobile を入力してください。";
//    	}
    	if($formval['email']=="" || db_common_is_mailaddress($formval['email'])==false){
    		$errors[]="メールアドレスを正しく入力してください。";
    	}    	
    	if($formval['email_check']==""){
    		$errors[]="メールアドレスを入力してください。";
    	}
    	if($formval['email']!=$formval['email_check']){
    		$errors[]="メールアドレス(確認)欄とメールアドレス欄に同じ内容を入力してください。";
    	}
    	if($formval['education']==""){
    		$errors[]="学校名を入力してください。";
    	}
    	if($formval['in_school']==""){
    		$errors[]="在学を入力してください。";
    	}
    	if($formval['in_school']=="2" && $formval['graduation_year']==""){
    		$errors[]="卒業年を入力してください。";
    	}
        if($formval['job']==""){
    		$errors[]="現在の職業を入力してください。";
    	}
    	if($formval['speciality']==""){
    		$errors[]="特 技を入力してください。";
    	}
        if($formval['interest']==""){
    		$errors[]="趣 味を入力してください。";
    	}
    	if($formval['self_pr']==""){
    		$errors[]="自己PRを入力してください。";
    	}
    	if(mb_strlen($formval['self_pr'], 'UTF-8')>450){
    		$errors[]="自己PRは全角450文字まで入力してください。";
    	}
    	if(mb_strlen($formval['other_experience'], 'UTF-8')>450){
    		$errors[]="その他、経験は全角450文字まで入力してください。";
    	}

    	if(mb_strlen($formval['speciality'], 'UTF-8')>20){
    		$errors[]="特技は全角20文字まで入力してください。";
    	}
    	if(mb_strlen($formval['interest'], 'UTF-8')>20){
    		$errors[]="趣味は全角20文字まで入力してください。";
		}
	if($_FILES['photo_filename_1']['size']==0){
		$errors[]="写真1をアップロードしてください。";
	}
	if($_FILES['photo_filename_2']['size']==0){
		$errors[]="写真2をアップロードしてください。 ";
	}
    	if($formval['belong_production']==1){
    		if($formval['production_name']==""){
    			$errors[]="所属事務所名を入力してください。 ";
    		}
    		if($formval['contact_period']==""){
    			$errors[]="契約期間を入力してください。 ";
    		}
    	}
    	if($formval['past_audition_exp']==1){
    		if($formval['past_audition_name']==""){
    			$errors[]="オーディション名を入力してください。 ";
    		}
    		if($formval['past_audition_result']==""){
    			$errors[]="結果を入力してください。 ";
    		}
    	}
    	
    	if($formval['body_1']=="" || intval($formval['body_1']) > 1000){
    		$errors[]="身長を正しく入力してください。";
    	}
        if($formval['body_2']=="" || intval($formval['body_2']) > 1000){
    		$errors[]="バストを正しく入力してください。";
    	}
    	if($formval['body_3']=="" || intval($formval['body_3']) > 1000){
    		$errors[]="ウェストを正しく入力してください。";
    	}
        if($formval['body_4']=="" || intval($formval['body_4']) > 1000){
    		$errors[]="ヒップを正しく入力してください。";
    	}
    	if($formval['body_5']=="" || intval($formval['body_5']) > 1000){
    		$errors[]="靴のサイズを正しく入力してください。";
    	}
       
       
    	if($formval['email_login']=="" || db_common_is_mailaddress($formval['email_login'])==false){
    		$errors[]="be amie登録メールアドレスを正しく入力してください。";
    	}
        if($formval['email_login_check']==""){
    		$errors[]="be amie登録メールアドレス(確認)を入力してください 。";
    	}
    	if($formval['email_login_check']!=$formval['email_login']){
    		$errors[]="be amie登録メールアドレス(確認)欄とbe amie登録メールアドレス欄に同じ内容を入力してください。";
    	}

		if (empty($formval['r_zip']) == false && preg_match("/^\d{7}$/", $formval['r_zip']) == false) {
    		$errors[]="推奨者の郵便番号を正しく入力してください。";
		}
    	return $errors;
    }
    
  	
}
