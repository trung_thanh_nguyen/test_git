<?php 
class pc_page_news_comment_confirm extends OpenPNE_Action
{

    function execute($requests)
    {
    	$u = $GLOBALS['AUTH']->uid();

        // --- リクエスト変数
        $newsid = $requests['newsid'];
        $body = $requests['body'];
        
       
        if($ng_word_list = filter_ng_word($body)){
        	$_REQUEST['msg'] = NGWORDERR;
        	$_REQUEST['msg1'] = NGWORDERR_DETAIL . $ng_word_list;
        	openpne_forward('pc', 'page', 'newsdetail');
            exit;
        	
        }
        
       
        $badtags=util_get_ng_html_tag($body);
        $badtag=implode(",",$badtags); 
        if($badtag){        	
        	//$_REQUEST['msg1'] = BADTAG.":" . $badtag;
        	$_REQUEST['msg1'] = sprintf(BADTAG,$badtag);
            openpne_forward('pc', 'page', 'newsdetail');
            exit();
        }
        
        $news=db_news_get_news_by_id($newsid);
        if(!$news){
        	openpne_redirect("pc","page_newslist",array());        	
        }
        
        $this->set('newsid',$newsid);
        $this->set('body',$body);
        return "success";
    	
    }
}