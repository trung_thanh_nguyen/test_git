<?php 
class pc_page_newsdetail extends OpenPNE_Action
{
       
    function execute($requests)
    {
		$u = $GLOBALS['AUTH']->uid();
		$this->set('u',$u);
    	$newtypenames=array(
			"Main"=>'メイン',
    		"Culture/Entertainment"=>'エンタメ',
//    		"National"=>'社会',
    		"Sports"=>'スポーツ',
    		"R-25"=>'R-25',
		"OVO"=>'OVO'
    	);
    	
    	$this->set('title','news detail');
    	$newsid = $requests['newsid'];  
    	$this->set('newsid',$newsid);  	
    	$news=db_news_get_news_by_id($newsid);    	
    	if(!$news || ($news['t_e2_news_hidden_flag']=='1')){  //e2info 2012-9-12 add $news['t_e2_news_hidden_flag']=='1'
    		openpne_redirect("portal","page_user_top");
    		die();
    	}   	
    	$this->set('newstypename_ja',$newtypenames[$news['t_e2_news_genre']]);
    	if($news['t_e2_news_type']=='r25'){
    		$news['t_e2_news_content']=html_entity_decode($news['t_e2_news_content']);
    	}
    	$this->set('news',$news);
    	$this->set('body',$requests['body']);
    	//It's for side bar news ================
    	switch ($news['t_e2_news_genre']){
    		case 'Culture/Entertainment':
    			$this->set('newstype','Entertainment_Trends');
    			break;
    		case 'r25':
    			$this->set('newstype','R-25');
    			break;
    		default :
    			$this->set('newstype',$news['t_e2_news_genre']);
    			break;    			
    	}
    	//=============================================
    	
    	$comment_list=db_news_comment_list($newsid);
    	$this->set('comment_list',$comment_list);
    	
    	$this->set('prev',db_news_get_prev_news($newsid));
    	$this->set('next',db_news_get_next_news($newsid));
    	
    	$this->set('day',$news['t_e2_news_date']);
    	
    	$this->set('newtypenames',$newtypenames);
    	$this->set('nownewtypenames',$news['t_e2_news_genre']);    	
    	$this->set('linkURL',"http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
     	$facebook_metas=array();
    	$facebook_metas['og:url']= "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
    	
	if($news['t_e2_news_main_filename'] ){
		if( 0 === strpos( $news['t_e2_news_main_filename'], 'http' ) ){
			$image_headers = get_headers( $news['t_e2_news_main_filename'] );
			if( false !== strpos( $image_headers[0], 'OK' ) ){
				$this->set( 'pic_flg' , true );
				$this->set('t_e2_news_main_filename' , $news['t_e2_news_main_filename'] );
				if( $news['t_e2_news_caption']){
					$this->set('t_e2_news_caption', $news['t_e2_news_caption'] );
				}
			}else{
				$this->set('pic_flg' , false);
			}
		}
    	}elseif ($news['t_e2_news_thumbnail_filename']){
 		$this->image_create($news['t_e2_news_thumbnail_filename']);
    		if (file_exists("http://".$_SERVER['SERVER_NAME']."/assets/new_img/" .$news['t_e2_news_thumbnail_filename'])){
		    	if ( filesize("http://".$_SERVER['SERVER_NAME']."/assets/new_img/" .$news['t_e2_news_thumbnail_filename']) > 0){
		    		$this->set('t_e2_news_thumbnail_filename' , "http://".$_SERVER['SERVER_NAME']."/assets/new_img/" .$news['t_e2_news_thumbnail_filename']);
		    		$this->set('pic_flg' , true);
		    	} else {
		    		$this->set('pic_flg' , false);
		    	}
		}
    	}
    	
    	
    	if ($news['t_e2_news_main_filename']) {
       		$facebook_metas['og:image'] = "http://".$_SERVER['SERVER_NAME']."/img.php?filename=".$news['t_e2_news_main_filename']."&m=pc";
       	}
    	else {
    		$facebook_metas['og:image'] = "http://".$_SERVER['SERVER_NAME']."/skin/default/img/logo.gif";
    	}
    	$this->set('facebook_metas',$facebook_metas);
    	
    	$this->set('og_title','be amieニュース:'.$news['t_e2_news_title']); //e2info 2011-10-27
    	
        $main = array("common/news_detail.tpl");
        
        $this->set('main', $main);
        
        $this->set('login_check', $_SESSION['SAFE_CHECK']);
        
        $results = get_member_banner($member_id);
	    $this->set('side_top_contents', $results['side_top_contents']);
	    $this->set('side_bannar'      , $results['side_contents']);
        
        $this->set('aside', $_SESSION['aside']);
        
        $this->set('sp', $_SESSION['SMARTPHONE_CHECK']);
		
    	return 'success';    	
    }
    
    function image_create($filename){
		$sql = "SELECT bin FROM c_image where filename = ? ";
		$res = db_get_one($sql, array($filename));

		$fp = fopen('assets/new_img/' . $filename, 'ab');
		
		if ($fp){
		    if (flock($fp, LOCK_EX)){
		        if (fwrite($fp,  base64_decode($res)) === FALSE){
		            print('ファイル書き込みに失敗しました');
		        }else{
		            //print($data.'をファイルに書き込みました');
		        }

		        flock($fp, LOCK_UN);
		    }else{
		        print('ファイルロックに失敗しました');
		    }
		}
		
		fclose($fp);
    }
    
    
}
