<?php
require_once OPENPNE_WEBAPP_DIR . '/lib/OpenPNE/KtaiUA.php';
class pc_page_audition_girl_end extends OpenPNE_Action
{
    function execute($requests)
    {
    	$u = $GLOBALS['AUTH']->uid(); 
    	$ktaiUA = new OpenPNE_KtaiUA();
    	if($ktaiUA->is_iphone()==true){
    		$this->set('is_iphone',true);
    	}
    	$this->set('message',$requests['message']);
    	$target_c_audition_id=$requests['target_c_audition_id'];
    	
    	if (MAIL_ADDRESS_HASHED) {
            $mail_address1 = "augc{$target_c_audition_id}-".t_get_user_hash($u)."-image1".'@'.MAIL_SERVER_DOMAIN;
            $mail_address2 = "augc{$target_c_audition_id}-".t_get_user_hash($u)."-image2".'@'.MAIL_SERVER_DOMAIN;
        } 
        else {
            $mail_address1 = "augc{$target_c_audition_id}-image1".'@'.MAIL_SERVER_DOMAIN;
            $mail_address2 = "augc{$target_c_audition_id}-image2".'@'.MAIL_SERVER_DOMAIN;
        }
        $mail_address = MAIL_ADDRESS_PREFIX . $mail_address;
        $this->set('mail_address1', $mail_address1);
        $this->set('mail_address2', $mail_address2);
    	
    	return 'success';
    
    }
}