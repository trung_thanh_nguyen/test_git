<!-- MAIN BEGIN -->
<div id="mainer">
	<div id="mainnertop"></div>
    <div id="main2" class="page_talent_mail">

({if $form_error})    
<div class="dparts alertBox"><div class="parts">
<table width="100%"><tr>
<td align="center">
<div  style="margin-left:160px;padding:20px;color:#FF0000">
({$form_error})
</div>
</td>
</tr></table>
</div></div>
({/if})

<div style="text-align:center;">
<img src="modules/pc/img/audition.gif" />
</div>


<div class='explain1' style="text-align: right;">
<span class="red">（必須）</span>と書かれている項目は必ずご記入をお願いします。
</div>
<table width="100%" >
<tr>
	<td class="mail_cap"><div class="left">ご希望応募地区<span class="red">（必須）</span></div></td>
	<td>
		<div class="right">
		({$form.wish_addr})		
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap" width="200px;" ><div class="left">氏名<span class="red">（必須）</span></div></td>
	<td><div class="right">
		<table>
		<tbody><tr>
		<td>姓</td><td>({$form.name_1})</td>
		<td>名</td><td>({$form.name_2})</td>
		</tr>
		<tr>
		<td>セイ</td><td>({$form.kananame_1})</td>
		<td>メイ</td><td>({$form.kananame_2})</td>
		</tr>
		</tbody></table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap"><div class="left">生年月日<span class="red">（必須）</span></div></td>
	<td>({$form.birthday_year})年 ({$form.birthday_month})月({$form.birthday_day})日 ({$form.age}) 歳
  </td>

</tr>
<tr>
	<td class="mail_cap">
	<div class="left">ご住所<span class="red">（必須）</span><p>建物名、号室は<br>必須入力項目ではありません。</p></div>
	</td>
	<td>
		<div class="right">
		<table>
		<tbody><tr>
		<td>郵便番号</td><td>({$form.zip})</td>
		</tr>
		<tr>
		<td>都道府県</td><td>
		({$form.address_pref})		
		</td>
		</tr>
		<tr>
		<td>市区町村・番地</td>
		<td>({$form.address_1})</td>
		</tr>
		<tr>
		<td>建物名・号室</td>
		<td>({$form.address_2})</td>
		</tr>
		</tbody></table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">ご連絡先の電話番号<span class="red">（必須）</span><p>携帯電話番号は<br>必須入力項目ではありません。</p></div>
	</td>
	<td>
	<div class="right">
	<table>
	<tbody><tr>
	<td >自宅電話番号</td><td>({$form.tel_1}) - ({$form.tel_2}) - ({$form.tel_3})</td>
	</tr>
	<tr>
	<td >携帯電話番号</td><td>({if $form.mobile_tel_1 || $form.mobile_tel_2 || $form.mobile_tel_3})({$form.mobile_tel_1}) - ({$form.mobile_tel_2}) - ({$form.mobile_tel_3})({/if})</td>
	</tr>
	</tbody></table>
	</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">メールアドレス<span class="red">（必須）</span></div>
	</td>
	<td>({$form.email})	
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">最終学歴<span class="red">（必須）</span></div>
	</td>
	<td>
	<div class="right">
	<table>
	<tbody><tr>
	<td>
	({$form.education})（学校）
	</td>
	</tr>
	<tr>
	<td>
	({if $form.in_school=='1' || $form.in_school=='3' })
		({if $form.in_school==1})在学({else})中退({/if})
	({else}) 
		({$form.graduation_year})年 卒業 
	({/if})
	</td>
	</tr>
	</tbody></table>
	</div>
	</td>
</tr>

<tr>
	<td class="mail_cap"><div class="left">現在の職業<span class="red">（必須）</span></div></td>
	<td><div class="right">
		({$form.job})
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap"><div class="left">特  技<span class="red">（必須）</span></div></td>
	<td><div class="right">
		({$form.speciality})
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap"><div class="left">趣  味<span class="red">（必須）</span></div></td>
	<td><div class="right">
		({$form.interest})
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap"><div class="left">自己PR<span class="red">（必須）</span></div></td>
	<td><div class="right">
		({$form.owner_PR})
		</div>
	</td>
</tr>

<tr>
	<td class="mail_cap"><div class="left">サイズ<span class="red">（必須）</span></div></td>
	<td>
		<div class="right">
		<table>
		<tr>
			<td>身長</td><td>バスト</td><td>ウェスト</td><td>ヒップ</td><td>靴</td>
		</tr>
		<tr>
			<td>({$form.body_1})cm</td><td>({$form.body_2})cm</td><td>({$form.body_3})cm</td><td>({$form.body_4})cm</td><td>({$form.body_5})cm</td>
		</tr>
		</table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap"><div class="left">写真<span class="red">（必須）</span></div></td>
	<td>
		<table>
			<tr><td>写真1 ({$form.photo_filename_1})</td></tr>	
			<tr><td>写真2({$form.photo_filename_2})</td></tr>	
			<tr><td></td></tr>						
		</table>
	</td>
</tr>

<tr>
	<td class="mail_cap"><div class="left">所属事務所</div></td>
	<td>
	<table>
	<tr><td>({if $form.production_radio=='1' }) はい ({else}) いいえ ({/if})</td></tr>
	<tr><td>({$form.production})</td></tr>
	<tr><td>({$form.contact_period})</td></tr>	
	</table>
	</td>
</tr>
<tr>
	<td class="mail_cap"><div class="left">今までにオーディションを受けたことがありますか？</div></td>
	<td>
	<table>
	<tr><td>({if $form.audition_radio=="1"})はい({else})いいえ({/if})</td></tr>
	<tr><td>オーディション名({$form.audition_name})</td></tr>
	<tr><td>結果({$form.audition_result})</td></tr>	
	</table>
	</td>
</tr>
<tr>
	<td class="mail_cap"><div class="left">その他、経験</div></td>
	<td>
	({$form.other_experience})
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">beamie<span class="red">（必須）</span>be amieご登録のメールアドレスをご記入ください</div>
	</td>
	<td>
	<div class="right">
	<table>
	<tbody><tr>
	<td>メールアドレス</td><td>({$form.email_login})</td>
	</tr>	
	<tr>
	<td>ニックネーム</td><td>({$form.login_nickname})</td>
	</tr>
	</tbody></table>
	</div>
	</td>
</tr>
<tr><td colspan="2" >※保護者同意欄（未成年の方のみ）
	</td>
</tr>
<tr>
	<td class="mail_cap"><div class="left">保護者氏名</div></td>
	<td><div class="right">
		<table>
		<tbody><tr>
		<td>姓</td><td>({$form.r_name_1})</td>
		<td>名</td><td>({$form.r_name_2})</td>
		</tr>
		<tr>
		<td>セイ</td><td>({$form.r_kana_name_1})</td>
		<td>メイ</td><td>({$form.r_kana_name_2})</td>
		</tr>
		</tbody></table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">保護者ご住所<p>建物名、号室は<br>必須入力項目ではありません。</p></div>
	</td>
	<td>
		<div class="right">
		<table>
		<tbody><tr>
		<td>郵便番号</td><td>({$form.r_zip})</td>
		</tr>
		<tr>
		<td>都道府県</td><td>
		({$form.r_address_pref})		
		</td>
		</tr>
		<tr>
		<td>市区町村・番地</td>
		<td>({$form.r_address_1})</td>
		</tr>
		<tr>
		<td>建物名・号室</td>
		<td>({$form.r_address_2})</td>
		</tr>
		</tbody></table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">保護者連絡先</div>
	</td>
	<td>
	({$form.r_tel_1}) - ({$form.r_tel_2}) - ({$form.r_tel_3})	
	</td>
</tr>

<tr>
	<td class="mail_cap">
	<div class="left"><h4>応募者とのご関係</h4></div>
	</td>
	<td>
	<div class="right">
		<table>
		<tbody><tr>
		<td ></td><td>({$form.r_relationship })</td>
		</tr>
		</tbody></table>
	</div>
	</td>
</tr>
</table>

({t_form_block m=pc a=do_audition_new})
<div class="bt" style="float: left;margin-left:300px;margin-top:50px;">
	({foreach from=$form key=key item=item})
	<input type="hidden" name="({$key})" value="({$item})" />
	({/foreach})	
	<input type="submit" class="input_submit" value="　送信　" />
</div>
({/t_form_block})

({t_form_block m=pc a=page_audition_new})
<div class="bt" style="float: left;margin-left:50px;margin-top:50px;">
	({foreach from=$form key=key item=item})
	<input type="hidden" name="({$key})" value="({$item})" />
	({/foreach})
	<input type="submit" class="input_submit" value="　戻る　" />
</div>
({/t_form_block})


</div>

</div>
<!-- MAIN END -->