				<!-- MAIN BEGIN -->
				<h1 class="headerLined">第2回　美ジネスマン＆美ジネスウーマンコンテスト応募フォーム</h1>

                ({if $on_audition == true})
				<section class="boxBase">

				<p class="message">(必須)と書かれている項目は必ずご記入をお願いします。</p>

				({if $errors})    
				<table width="100%">
					<tr><td>
						({foreach from=$errors item=error})
						<p style="margin:10px 0px 0px 30px; text-align: left; color: #ff0000;">* ({$error})</p>
						({/foreach})
					</td></tr>
				</table>
				({/if})
				

				<form action="./" method="post" enctype="multipart/form-data" class="boxInput" novalidate>
					<input type="hidden" name="m" value="pc" />
					<input type="hidden" name="a" value="page_audition_biz_2014_confirm" />
                    <input type="hidden" name="a_id" value="({$a_id})" />
					<input type="hidden" name="ssl_param" value="1" />

					<dl class="listHr">
						<dt>氏名<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p>
							<label for="">姓　</label><input type="text" value="({$form.family_name})" name="family_name" class="w70">
							<label for="">名　</label><input type="text" value="({$form.first_name})" name="first_name" class="w70">（全角）
							<p>
							<label for="">セイ</label><input type="text" value="({$form.kana_family_name})" name="kana_family_name" class="w70">
							<label for="">メイ</label><input type="text" value="({$form.kana_first_name})" name="kana_first_name" class="w70">（全角カナ）</p></dd>
						<dt>生年月日<span class="errorTxt">（必須）</span></dt>
						<dd>
						<p>
						<select name="birthday_year">
							<option value="" >--</option>
							({foreach from=$years item=item })
							<option  value="({$item})" ({if $form.birthday_year==$item }) selected="selected" ({/if}) >({$item})</option>
							({/foreach})
						</select>
						&nbsp;年
						<select name="birthday_month">
							<option value="" >--</option>
							({foreach from=$months item=item })
							<option  value="({$item})" ({if $form.birthday_month==$item }) selected="selected" ({/if}) >({$item})</option>
							({/foreach})
						</select>
						&nbsp;月
						<select name="birthday_day">
							<option value="" >--</option>
							({foreach from=$days item=item })
							<option  value="({$item})" ({if $form.birthday_day==$item }) selected="selected" ({/if}) >({$item})</option>
							({/foreach})
						</select>&nbsp;日</p>
						<p>(満 <input name="age" type="tel" value="({$form.age})" class="w60"/> 歳)（半角数字）</p></dd>
						<dt>現住所<span class="errorTxt">（日本国外からの応募の場合は郵便番号は不要です）</span></dt>
						<dd>
							<p><label>郵便番号</label>
							<input type="tel" value="({$form.zip})" name="zip" class="w70" maxlength="7" id="zip_textbox1">(ハイフン不要・半角数字) <button class="btn btn-m" id="zip_search_button1" onclick="return false;" style="font-size:12px;padding:10px 0;width:150px">郵便番号から住所を入力</button></p>
							<p><label>都道府県</label>
							<select name="address_pref" id="address_pref_select1">
								<option value="">&#8213; 選択してください &#8213;</option>
								({foreach from=$pref item=item})
								<option value="({$item})"  ({if $item==$form.address_pref}) selected ({/if}) >({$item})</option>		
								({/foreach})
							</select>
							</p>
							<label>市区町村・番地</label>
							<p><input type="text" value="({$form.address_1})" name="address_1" id="address_1_textbox1">（全角・半角英数）</p>
							<label>建物名・号室（必須入力項目ではありません）</label>
							<p><input type="text" value="({$form.address_2})" name="address_2">（全角・半角英数）</p></dd>
						<dt>ご連絡先の電話番号<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p>
							<input type="tel" value="({$form.tel_1})" name="tel_1" class="w60"> -
							<input type="tel" value="({$form.tel_2})" name="tel_2" class="w60"> -
							<input type="tel" value="({$form.tel_3})" name="tel_3" class="w60">（半角数字）</p></dd>
						<dt>メールアドレス<span class="errorTxt">（必須）</span></dt>
						<dd>
							<label for="">メールアドレス</label>
							<p>
							<input type="text" value="({$form.email})" name="email">（半角英数）</p>
							<label for="">メールアドレス確認</label>
							<p>
							<input type="text" value="({$form.email_check})" name="email_check">（半角英数）</p></dd>
                       	<dt>性別<span class="errorTxt">（必須）</span></dt>
						<dd>
                            <p>
							<input type="radio" name="gender" value="male" ({if $form.gender == 'male'}) checked="checked" ({/if}) ><label for="">男性 </label>
                            <input type="radio" name="gender" value="female" ({if $form.gender == 'female'}) checked="checked" ({/if}) ><label for="">女性 </label>
                            </p>
                  		    </dd>
                        <dt>職業・学年<span class="errorTxt">（必須）</span></dt>
                        <dd>
                            <p><input type="text" value="({$form.job})" name="job"><font color="red"> (10文字まで)</font></p></dd>
						<dt>特 技<span class="errorTxt">（必須）</span></dt>
						<dd>
							<input type="text" value="({$form.speciality})" name="speciality"><font color="red"> (20文字まで)</font>（全角・半角英数）</dd>
						<dt>趣  味<span class="errorTxt">（必須）</span></dt>
						<dd>
							<input type="text" value="({$form.interest})" name="interest"><font color="red"> (20文字まで)</font>（全角・半角英数）</dd>
						<dt>自己PR<span class="errorTxt">（必須）</span>
						志望動機、芸能界での目標を必ずご記入ください。</dt>
						<dd>
							<p>
							<textarea rows="12" name="self_pr"  >({$form.self_pr})</textarea></p>
							<font color="red">（150文字まで）</font></dd>
						<dt>家族構成</dt>
						<dd>
							<p><label>氏名</label><input type="text" name="family_f" maxlength="17" value="({$form.family_f})" style="width: 200px;"/> <label>続柄</label><input type="text" name="family_f_name" value="({$form.family_f_name})" style="width: 80px;"/></p><br>
							<p><label>氏名</label><input type="text" name="family_m" maxlength="17" value="({$form.family_m})" style="width: 200px;"/> <label>続柄</label><input type="text" name="family_m_name" value="({$form.family_m_name})" style="width: 80px;"/></p><br>
							<p><label>氏名</label><input type="text" name="family_b" maxlength="17" value="({$form.family_b})" style="width: 200px;"/> <label>続柄</label><input type="text" name="family_b_name" value="({$form.family_b_name})" style="width: 80px;"/></p><br>
							<p><label>氏名</label><input type="text" name="family_yb" maxlength="17" value="({$form.family_yb})" style="width: 200px;"/> <label>続柄</label><input type="text" name="family_yb_name" value="({$form.family_yb_name})" style="width: 80px;"/></p>
<font color="red">（氏名:17文字まで）</font>
						</dd>
						<dt>芸能界での目標<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p>１つ選択してください</p>
							<ul class="listHopeType">
								({foreach from=$department key=key item=item})
								<li><input type="radio" name="wish_department" value="({$item})" ({if $item==$form.wish_department}) checked ({/if}) ><label for="">({$item}) </label></li>
								({/foreach})
							</ul></dd>
						<dt>サイズ<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p>
							<label>身長　　</label>
							<input type="number" name="body_1" value="({$form.body_1})" class="w60" />cm</p>
							<p>
							<label>バスト　</label>
							<input type="number" name="body_2" value="({$form.body_2})" class="w60" />cm</p>
							<p>
							<label>ウェスト</label>
							<input type="number" name="body_3" value="({$form.body_3})" class="w60" />cm</p>
							<p>
							<label>ヒップ　</label>
							<input type="number" name="body_4" value="({$form.body_4})" class="w60" />cm</p>
							<label>靴　　　</label>
							<input type="number" name="body_5" value="({$form.body_5})" class="w60" />cm（半角英数）</dd>
						<dt>写真アップロード<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p>10MB以内のJPEGでお願いします</p>
							<p>
							<label>写真1（アップ）　　</label>
							<input type="file" name="photo_filename_1" value="" /></p>
							<p>
							<label>写真2（全身）　　　</label>
							<input type="file" name="photo_filename_2" value="" /></p>
							<p>※最近6ヶ月以内に撮影したものを送付ください。<br>
								※撮影イメージは下記の応募写真イメージ画像をご覧ください。<br>
								※写真は縦長で頭が上方向の写真を送付ください。<br>
								横撮影のカメラを立てて撮影すると、見た目は縦長写真ですが写真データは横向きの場合があります。<u>写真確認の際に頭が上になるよう回転し保存してから送付をお願いします。</u><br/>
								※プリクラや撮影後加工した写真は不可です。
							<p>
						<dt>be amie登録情報<span class="errorTxt">（必須）</span>be amieご登録のメールアドレスをご記入ください。</dt>
						<dd>
							<label for="">メールアドレス</label>
							<p><input type="text" value="({$form.email_login})" name="email_login" />（半角英数）</p>
							<label for="">メールアドレス確認</label>
							<p><input type="text" value="({$form.email_login_check})" name="email_login_check" />（半角英数）</p>
							<label for="">ニックネーム</label>
							<p><input type="text" name="login_nickname" value="({$form.login_nickname})" />（全角・半角英数）</p></dd>
						</dl>

						<h2 class="header-article">保護者同意</h2>
						<p><font color="red">※未成年の方は保護者の同意が必要です。</font> <br>
						※面接に進まれた方は別途保護者の同意書をご用意いただきます。<br/>
						</p>
						
						<dl class="listHr">
							<dt>保護者氏名</dt>
							<dd>
							<p>
							<label for="">姓　</label><input type="text" value="({$form.r_family_name})" name="r_family_name" class="w70">
							<label for="">名　</label><input type="text" value="({$form.r_first_name})" name="r_first_name" class="w70">（全角）
							<p>
							<label for="">セイ</label><input type="text" value="({$form.r_kana_family_name})" name="r_kana_family_name" class="w70">
							<label for="">メイ</label><input type="text" value="({$form.r_kana_first_name})" name="r_kana_first_name" class="w70">（全角カナ）</p></dd>
							<dt>保護者住所
							<dd>
								<p><label>郵便番号</label><input type="tel" value="({$form.r_zip})" name="r_zip" class="w70" maxlength="7" id="zip_textbox2">(ハイフン不要・半角数字) <button class="btn btn-m" id="zip_search_button2" onclick="return false;"  style="font-size:12px;padding:10px 0;width:150px">郵便番号から住所を入力</button></p>
								<p>
								<label>都道府県</label>
								<select name="r_address_pref" id="address_pref_select2">
									<option value="">&#8213; 選択してください &#8213;</option>
									({foreach from=$pref item=item})
									<option value="({$item})"  ({if $item==$form.r_address_pref}) selected ({/if}) >({$item})</option>		
									({/foreach})
								</select></p>
								<label>市区町村・番地</label>
								<p>
								<input type="text" value="({$form.r_address_1})" name="r_address_1" id="address_1_textbox2">（全角・半角英数）</p>
								<label>建物名・号室</label>
								<p>
								<input type="text" value="({$form.r_address_2})" name="r_address_2">（全角・半角英数）<p></dd>
							<dt>保護者電話番号</dt>
							<dd>
								<p>
								<input type="tel" value="({$form.r_tel_1})" name="r_tel_1" class="w60"> -
								<input type="tel" value="({$form.r_tel_2})" name="r_tel_2" class="w60"> -
								<input type="tel" value="({$form.r_tel_3})" name="r_tel_3" class="w60">（半角数字）</p></dd>
							<dt>保護者の同意</dt>
							<dd><input type="checkbox" name="allow" value="1" id="allow" ({if $form.allow=='1'})checked({/if})> <label for="allow" >同意する</label></dd>
							<dt>本人との関係</dt>
							<dd>
								<input type="text" value="({$form.r_relationship })" name="r_relationship" class="w60">（全角）</dd>
						</dl>

						<h2 class="header-article">アンケート</h2>
						<dl class="listHr">
						<dt>今回の美ジネスマン＆美ジネスウーマンコンテストを何で知りましたか？</dt>
						<dd>
						<input type="radio" name="enquete" id="enqsel1" value="テレビ" ({if "テレビ"==$form.enquete}) checked ({/if})/><label>1.テレビ</label><br />
						<input type="radio" name="enquete" id="enqsel2" value="ラジオ" ({if "ラジオ"==$form.enquete}) checked ({/if})/><label>2.ラジオ</label><br />
						<input type="radio" name="enquete" id="enqsel3" value="新聞" ({if "新聞"==$form.enquete}) checked ({/if})/><label>3.新聞</label>　(<input type="text" name="enqtext3" value="({$form.enqtext3})" onchange="AutoCheck('enqsel3');" class="enqtxt" />)<br />
						<input type="radio" name="enquete" id="enqsel4" value="雑誌" ({if "雑誌"==$form.enquete}) checked ({/if})/><label>4.雑誌</label>　(<input type="text" name="enqtext4" value="({$form.enqtext4})" onchange="AutoCheck('enqsel4');" class="enqtxt" />)<br />
						<input type="radio" name="enquete" id="enqsel5" value="インターネット" ({if "インターネット"==$form.enquete}) checked ({/if})/><label>5.インターネット</label><br />
						<input type="radio" name="enquete" id="enqsel6" value="携帯電話サイト" ({if "携帯電話サイト"==$form.enquete}) checked ({/if})/><label>6.携帯電話サイト</label><br />
						<input type="radio" name="enquete" id="enqsel7" value="その他" ({if "その他"==$form.enquete}) checked ({/if})/><label>7.その他</label>　(<input type="text" name="enqtext7" value="({$form.enqtext7})" onchange="AutoCheck('enqsel7');" class="enqtxt" />)<br />
						</dd>
						</dl>

						<h3>※アップロードいただく写真の撮影イメージ</h3>
						<p class="txtCenter"><img src="http://beamie.jp/modules/pc/img/img.gif" /></p>

						<div class="boxPrivacy" style="height:auto;overflow:none;">
                        <!--
                        <p>※ビジネススーツ・リクルートスーツ・レディススーツ(パンツスタイル・スカートスタイルは問いません)などを着用。</p>
                        -->
                        <p>※アップイメージ<br>
                        ビジネススーツ・リクルートスーツ・レディススーツを着用し、真正面を向き、顔がはっきり写っているもの。帽子や前髪で顔の輪郭が隠れないようにしましょう。
                        </p>
                        <p>※全身イメージ<br>
                        ビジネススーツ・リクルートスーツ・レディススーツを着用し、人物が小さすぎたり、背景が目立ったりしないようにしましょう。レディススーツはパンツスタイル・スカートスタイルは問いません。
                        </p>
                        <p><font color="red">※ビジネススーツ以外のカジュアル等の私服・学校の制服等の写真での応募は審査対象外とします。</font></p>
						<p><font color="red">※写真は縦長で頭が上方向の写真を送付ください。</font><br>
						横撮影のカメラを立てて撮影すると、見た目は縦長写真ですが写真データは横向きの場合があります。<u>写真確認の際に頭が上になるよう回転し保存してから送付を
お願いします。</u>
						</p>
						<p><font color="red">※スマートフォンをご利用の方へ</font> <br>
						iOS5以下、一部のAndroidでは写真のアップロードができません。お手数ですが、郵送にてご応募ください。
						</p>

						<p>
						※いずれの場合も応募写真の送付または添付が必要です。
						</p>
						
						<p>
						※応募写真は最近６ヶ月以内に撮影した顔アップ・全身のカラーの写真をアップロードしてください。
						</p>
						</div>

						<h3>※プライバシーポリシー（個人情報保護方針）</h3>
						<div class="boxPrivacy">
						({$privacy_policy.body|smarty:nodefaults})
						</div>

						<h3>※注意事項 (必ずご確認ください。)</h3>
						<div class="boxPrivacy" style="height:auto;overflow:none;">
						・一次審査(書類審査)を通過された方に限り、二次の面接審査を美ジネスマン＆美ジネスウーマンコンテスト実行委員会事務局より通知いたします。<br/>
						・お送り頂いた個人情報は、コンテストにのみ使用し、コンテスト終了後は、他の目的で使用することはありません。<br/>
						・審査状況・合否に関わるお問い合わせはお答えいたしかねます。<br/>
						・<font color="red">be amie内でオスカープロモーション公認スカウト以外の悪質なスカウト行為には充分ご注意ください！！<br/>
                         美ジネスマン＆美ジネスウーマンコンテスト実行委員会より皆様宛に直接メッセージをお送りすることは絶対にございません。</font><br/>
                        ・一次審査(書類審査)を通過した方には「洋服の青山」にてスーツをご購入頂き、二次の面接審査にご参加頂きます。予めご了承の上コンテストにご応募ください。<br/>
                        ・合格通知を持参のうえご来店いただきますと「第1次審査通過者限定」特別割引にてご購入いただけます。<br/>
						・応募書類は一切返却いたしませんので、予めご了承ください。 <br/>
						・コンテストの内容・スケジュール等は予告なく変更となる場合がございます。予めご了承ください。<br/><br/>
						もしbe amieを通じて不信なメッセージが届いた場合は必ずご家族にご相談いただき、下記までご連絡をお願いいたします。<br/>
                        美ジネスマン＆美ジネスウーマンコンテスト実行委員会事務局<br/>
						TEL:03-6427-2816（平日11:00～17:00受付）<br/>
						</div>

<p class="txtCenter"><input type="checkbox" name="consent" value="1" id="consent" ({if $form.consent=='1'})checked({/if})><label for="consent" ><font color="red">プライバシーポリシーおよび({if $smarty.session.SMARTPHONE_CHECK == true})<br>({/if})注意事項に同意します。</font></label></p>

						<p class="txtCenter">
							<input type="submit" name="submit" value="同意の上、確認する" class="btn btn-m" />
						</p>

					</form>
				
				</section>
		<script type="text/javascript">
		({literal})
		$('document').ready(function(){
			$('#zip_search_button1').click(function(){
				var zip_code = $('#zip_textbox1').val();
				$.ajax({
					'type': 'GET',
					'url': '/?m=api&a=do_GetAddress',
					'data': {
						'zip_code':zip_code
					},
					'dataType': 'json',
					'success': function(data){
						if( 'undefined' != typeof data.prefecture && 'undefined' != typeof data.address ){
							var prefecture = data.prefecture;
							var address = data.address;
							$('#address_pref_select1').val(prefecture);
							$('#address_1_textbox1').val(address);
						}
					},
					'error':function(){}
				});
			});
			$('#zip_search_button2').click(function(){
				var zip_code = $('#zip_textbox2').val();
				$.ajax({
					'type': 'GET',
					'url': '/?m=api&a=do_GetAddress',
					'data': {
						'zip_code':zip_code
					},
					'dataType': 'json',
					'success': function(data){
						if( 'undefined' != typeof data.prefecture && 'undefined' != typeof data.address ){
							var prefecture = data.prefecture;
							var address = data.address;
							$('#address_pref_select2').val(prefecture);
							$('#address_1_textbox2').val(address);
						}
					},
					'error':function(){}
				});
			});
		});
		({/literal})
		</script>
                ({else})
                <p class="message">応募受付期間外です。</p>
                ({/if})

                <style type='text/css'>
                    input[type=number]::-webkit-inner-spin-button,
                    input[type=number]::-webkit-outer-spin-button {
                        -webkit-appearance: none;
                        margin: 0;
                    }

					input[type="tel"], input[type="number"]{
						padding:5px;
						border-radius:5px;
					}

					.boxInput .w70{
						width:70px;
						margin:0 5px 0 0;
					}

                </style>
