<!-- MAIN BEGIN -->
    <div id="newsmain" >
        
        <div id="pagebar"><img src="../../../skin/default/common/pagebar-beamie_news.gif" width="185" height="25" alt="be amieニュース" /></div>
        <div>
        ({foreach from=$newtypenames item=item key=key name=newstypename })
        ({if $key eq $nownewtypenames})
        ({$item})
        ({else})
        <a href="({t_url m=pc a=page_newslist})&amp;type=({$key})&amp;day=({$day})"> ({$item})</a>
        ({/if})
        ({if $smarty.foreach.newstypename.last neq 1})|({/if})
        ({/foreach})
        </div>
        <div class="textpr_area">
        	<ul>
         	({$layout_free18|smarty:nodefaults|t_url2cmd:"":"":0|t_cmd})
         	</ul>
        </div>
        
        <div class="pankuzu"><a href="({t_url m=pc a=page_newslist})&amp;type=({$news.t_e2_news_genre})">({$newstypename_ja})</a> &gt; ({$news.t_e2_news_title})</div>
       
        <div class="tit_news">
          ({if $news.t_e2_news_genre eq 'R-25'})
          <div class="r25news">
            <h1>({$news.t_e2_news_title})</h1>
          </div>
          ({else})
          <div class="kyodonews">
            <h1>({$news.t_e2_news_title})</h1>
          </div>
          ({/if})
        </div>
        <div class="news_dataetc_btns" style="overflow:hidden;">
          <div style="" class="news_dataetc">({$news.t_e2_news_public_dateandtime|date_format:"%Y年%m月%d日  %H時%M分"}) 提供元：({if $news.t_e2_news_genre eq 'R-25'})<a href="http://r25.yahoo.co.jp/" target="_blank">R-25</a>({else})<a href="http://www.kyodo.co.jp/" target="_blank">共同通信</a>({/if})</div>
          <div class="news_btns">
            
            <ul>
              <li>
                <a href="http://mixi.jp/share.pl" class="mixi-check-button" data-key="f6226353980fab0f840415617ede900bb0f8d2fa">mixiチェック</a>
                <script type="text/javascript" src="http://static.mixi.jp/js/share.js"></script>
              </li>
              <li><iframe frameborder="0" scrolling="no" allowtransparency="true" style="border:none; overflow:hidden; width:120px; height:21px;" src="http://www.facebook.com/plugins/like.php?href=({$linkURL|escape:'url'})&amp;send=false&amp;layout=button_count&amp;width=120&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21"></iframe></li>
              <li><a class="twitter-share-button" href="http://twitter.com/share/">Tweet</a>
                <script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script></li></ul>
                 
           </div>
       </div>
       
       
       <div class="photoarea_news_txt">
          <div class="photoarea_b">
            <div class="photo_big"><img  alt="" src="({t_img_url filename=$news.t_e2_news_main_filename h=283 noimg=no_image})"></div>
            <p class="link_tobig"><a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$newsid})">記事本文へ</a></p>
          </div>
        </div>
       <div class="banner_area1"> ({advbanner field=news_page dev=pc})</div>
       <div class="pr_area1">
        	({$layout_free19|smarty:nodefaults|t_url2cmd:"":"":0|t_cmd})           
        </div>
        <div class="link_box1">
        ({foreach from=$newtypenames item=item key=key name=newstypename })
        ({if $key eq $nownewtypenames})
        ({$item})
        ({else})
        <a href="({t_url m=pc a=page_newslist})&amp;type=({$key})&amp;day=({$day})"> ({$item})</a>
        ({/if})
        ({if $smarty.foreach.newstypename.last neq 1})|({/if})
        ({/foreach})
        </div>
     
    </div>

<!-- MAIN END -->
