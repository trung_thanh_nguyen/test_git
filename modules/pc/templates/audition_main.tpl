				<!-- MAIN BEGIN -->
				
				<h1 class="headerLined">オーディション応募フォーム</h1>

                ({if $on_audition == true})
				<section class="boxBase">
				
				<p class="message">(必須)と書かれている項目は必ずご記入をお願いします。</p>

				({if $errors})    
				<table width="100%">
					<tr><td>
						({foreach from=$errors item=error})
						<p style="margin:10px 0px 0px 30px; text-align: left; color: #ff0000;">* ({$error})</p>
						({/foreach})
					</td></tr>
				</table>
				({/if})
	
				<form action="./" method="post" enctype="multipart/form-data" class="boxInput" novalidate>
					<input type="hidden" name="m" value="pc" />
					<input type="hidden" name="a" value="page_audition_confirm" />
					<input type="hidden" name="a_id" value="({$a_id})" />
					<input type="hidden" name="ssl_param" value="1" />

					<dl class="listHr">
						<dt>氏名<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p>
							<label for="">姓　</label><input type="text" value="({$form.family_name})" name="family_name" class="w70">
							<label for="">名　</label><input type="text" value="({$form.first_name})" name="first_name" class="w70">（全角）
							<p>
							<label for="">セイ</label><input type="text" value="({$form.kana_family_name})" name="kana_family_name" class="w70">
							<label for="">メイ</label><input type="text" value="({$form.kana_first_name})" name="kana_first_name" class="w70">（全角カナ）</p></dd>
						<dt>生年月日<span class="errorTxt">（必須）</span></dt>
						<dd>
						<p>
						<select name="birthday_year">
							<option value="" >--</option>
							({foreach from=$years item=item })
							<option  value="({$item})" ({if $form.birthday_year==$item }) selected="selected" ({/if}) >({$item})</option>
							({/foreach})
						</select>
						&nbsp;年
						<select name="birthday_month">
							<option value="" >--</option>
							({foreach from=$months item=item })
							<option  value="({$item})" ({if $form.birthday_month==$item }) selected="selected" ({/if}) >({$item})</option>
							({/foreach})
						</select>
						&nbsp;月
						<select name="birthday_day">
							<option value="" >--</option>
							({foreach from=$days item=item })
							<option  value="({$item})" ({if $form.birthday_day==$item }) selected="selected" ({/if}) >({$item})</option>
							({/foreach})
						</select>&nbsp;日</p>
						<p>(満 <input name="age" type="tel" value="({$form.age})" class="w60" /> 歳)（半角数字）</p></dd>
						<dt>現住所<span class="errorTxt">（日本の場合は郵便番号必須）</span></dt>
						<dd>
							<p><label>郵便番号</label>
							<input type="tel" value="({$form.zip})" name="zip" class="w70" maxlength="7" id="zip_textbox1">(ハイフン不要・半角数字) <button class="btn btn-m" id="zip_search_button1" onclick="return false;" style="font-size:12px;padding:10px 0;width:150px">郵便番号から住所を入力</button></p>
							<p><label>都道府県</label>
							<select name="address_pref" id="address_pref_select1">
								<option value="">&#8213; 選択してください &#8213;</option>
								({foreach from=$pref item=item})
								<option value="({$item})"  ({if $item==$form.address_pref}) selected ({/if}) >({$item})</option>		
								({/foreach})
							</select>
							</p>
							<label>市区町村・番地</label>
							<p><input type="text" value="({$form.address_1})" name="address_1" id="address_1_textbox1">（全角・半角英数）</p>
							<label>建物名・号室（必須入力項目ではありません）</label>
							<p><input type="text" value="({$form.address_2})" name="address_2">（全角・半角英数）</p></dd>
						<dt>ご連絡先の電話番号<span class="errorTxt">（必須）</span></dt>
						<dd>
							<label>自宅電話番号</label>
							<p>
							<input type="tel" value="({$form.tel_1})" name="tel_1" class="w60"> - 
							<input type="tel" value="({$form.tel_2})" name="tel_2" class="w60"> - 
							<input type="tel" value="({$form.tel_3})" name="tel_3" class="w60">（半角数字）</p>
							<label>携帯電話番号（必須入力項目ではありません）</label>
							<p>
							<input type="tel" value="({$form.mobile_tel_1})" name="mobile_tel_1" class="w60"> - 
							<input type="tel" value="({$form.mobile_tel_2})" name="mobile_tel_2" class="w60"> - 
							<input type="tel" value="({$form.mobile_tel_3})" name="mobile_tel_3" class="w60">（半角数字）</p></dd>
						<dt>メールアドレス<span class="errorTxt">（必須）</span></dt>
						<dd>
							<label for="">メールアドレス</label>
							<p>
							<input type="text" value="({$form.email})" name="email">（半角英数）</p>
							<label for="">メールアドレス確認</label>
							<p>
							<input type="text" value="({$form.email_check})" name="email_check">（半角英数）</p></dd>
						<dt>最終学歴<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p>
							<input type="text" value="({$form.education})" name="education">（学校）（全角・半角英数）</p>
							<p>
							<input type="radio" name="in_school" value="1" id="inSchool" ({if $form.in_school=='1'})checked({/if})><label for="inSchool">在学</label>
							<input type="radio" name="in_school" value="3" id="dropOut" ({if $form.in_school=='3'})checked({/if})><label for="dropOut">中退</label>
							<input type="radio" name="in_school" value="2" id="graduated" ({if $form.in_school=='2'})checked({/if})><label for="graduated">卒業</label>
							</p>
							<p>(西暦 <input type="text" name="graduation_year" value="({$form.graduation_year})" class="w60" >年)（半角英数）</p>
						</dd>
						<dt>現在の職業<span class="errorTxt">（必須）</span></dt>
						<dd>
							<input type="text" value="({$form.job})" name="job">（全角・半角英数）</dd>
						<dt>特 技<span class="errorTxt">（必須）</span></dt>
						<dd>
							<input type="text" value="({$form.speciality})" name="speciality">（全角・半角英数）</dd>
						<dt>趣  味<span class="errorTxt">（必須）</span></dt>
						<dd>
							<input type="text" value="({$form.interest})" name="interest">（全角・半角英数）</dd>
						<dt>自己PR<span class="errorTxt">（必須）</span>
						志望動機、芸能界での目標を必ずご記入ください。</dt>
						<dd>
							<p>
							<textarea rows="12" name="self_pr"  >({$form.self_pr})</textarea></p>
							（全角450文字まで）</dd>
						<dt>希望部門<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p>１つ選択してください</p>
							<ul class="listHopeType">
								({foreach from=$department key=key item=item})
								<li><input type="radio" name="wish_department" value="({$item})" ({if $item==$form.wish_department}) checked ({/if}) ><label for="">({$item}) </label></li>
								({/foreach})
							</ul></dd>
						<dt>サイズ<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p>
							<label>身長　　</label>
							<input type="number" name="body_1" value="({$form.body_1})" class="w60" />cm</p>
							<p>
							<label>バスト　</label>
							<input type="number" name="body_2" value="({$form.body_2})" class="w60" />cm</p>
							<p>
							<label>ウェスト</label>
							<input type="number" name="body_3" value="({$form.body_3})" class="w60" />cm</p>
							<p>
							<label>ヒップ　</label>
							<input type="number" name="body_4" value="({$form.body_4})" class="w60" />cm</p>
							<label>靴　　　</label>
							<input type="number" name="body_5" value="({$form.body_5})" class="w60" />cm（半角英数）</dd>
						<dt>写真アップロード<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p>10MB以内のJPEGでお願いします</p>
							<p>
							<label>写真1（アップ）　　</label>
							<input type="file" name="photo_filename_1" value="" /></p>
							<p>
							<label>写真2（全身）　　　</label>
							<input type="file" name="photo_filename_2" value="" /></p>
							<p>※撮影イメージは下記の応募写真イメージ画像をご覧ください。<br>
								※パーツモデル志願者は写真1、2は必須ではありません。
							<p>
							<label>写真3（各部位写真）</label>
							<input type="file" name="photo_filename_3" value="" /></p>
							<p>※写真3はパーツモデル志望者のみ必須</p></dd>
						<dt>動画アップロード</dt>
						<dd>
							<p>
							必須ではありません。<br>
							画面サイズ640×480以内、ファイルサイズ20MB以内、３分以内とします。</p>
							動画（自己紹介）<input type="file" name="movie_filename"  value="" ></dd>
						<dt>現在、他の芸能プロダクションに所属していますか？<span class="errorTxt">（必須）</span></dt>
						<dd>
							<input type="radio" name="belong_production" value="1" id="yesBelong" ({if $form.belong_production==1})checked({/if})>
							<label for="yesBelong">はい（以下もご記入ください）</label>
							<p><label>所属事務所名</label><p>
							<p><input type="text" name="production_name" value="({$form.production_name})" />（全角・半角英数）</p>
							<label>契約期間</label> <p><input type="text" name="contact_period" value="({$form.contact_period})" />（全角・半角英数）</p>
							<input type="radio" name="belong_production" value="2" id="noBelong" ({if $form.belong_production==2})checked({/if})>
							<label for="noBelong">いいえ</label></dd>
						<dt>今までにオーディションを受けたことがありますか？<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p>
							<input type="radio" name="past_audition_exp" value="1" id="yesAudition" ({if $form.past_audition_exp=="1"})checked({/if})>
							<label for="yesAudition">はい（以下もご記入ください）</label>
							<p><label for="auditionName">オーディション名</label><p>
							<p><input type="text" name="past_audition_name" value="({$form.past_audition_name})">（全角・半角英数）</p>
							<label>結果</label>
							<p><input type="text" name="past_audition_result" value="({$form.past_audition_result})">（全角・半角英数）</p>
							<label>その他、経験 （全角450文字まで）</label>
							<p><textarea rows="8" name="other_experience" >({$form.other_experience})</textarea></p>
							<input type="radio" name="past_audition_exp" value="2" id="noAudition" ({if $form.past_audition_exp=="2"})checked({/if})>
							<label for="noAudition">いいえ</label></p></dd>

						<dt>be amie登録情報<span class="errorTxt">（必須）</span>be amieご登録のメールアドレスをご記入ください。</dt>
						<dd>
							<label for="">メールアドレス</label>
							<p><input type="text" value="({$form.email_login})" name="email_login" />（半角英数）</p>
							<label for="">メールアドレス確認</label>
							<p><input type="text" value="({$form.email_login_check})" name="email_login_check" />（半角英数）</p>
							<label for="">ニックネーム</label>
							<p><input type="text" name="login_nickname" value="({$form.login_nickname})" />（全角・半角英数）</p></dd>
						</dl>

						<h2 class="header-article">推薦者のbe amie登録情報</h2>
						<p class="errorTxt">※推薦の場合：以下は推薦者様に関する内容をご記入ください。</p>
						
						<dl class="listHr">
							<dt>メールアドレス</dt>
							<dd>
								<p><input type="text" value="({$form.r_email})" name="r_email">（半角英数）</p>
								<label>メールアドレス確認</label>
								<p>
								<input type="text" value="({$form.r_email_check})" name="r_email_check">（半角英数）</p></dd>
							<dt>ニックネーム</dt>
							<dd>
								<input type="text" name="r_nickname" value="({$form.r_nickname})" />（全角・半角英数）</dd>
							<dt>氏名</dt>
							<dd>
								<p>
								<label>姓　</label><input type="text" value="({$form.r_family_name})" name="r_family_name" class="w70">
								<label>名　</label><input type="text" value="({$form.r_first_name})" name="r_first_name" class="w70">（全角）</p>
								<p>
								<label>セイ</label><input type="text" value="({$form.r_kana_family_name})" name="r_kana_family_name" class="w70">
								<label>メイ</label><input type="text" value="({$form.r_kana_first_name})" name="r_kana_first_name" class="w70">（全角カナ）</p></dd>

							<dt>推薦者のご住所
							<dd>
								<p><label>郵便番号</label><input type="tel" value="({$form.r_zip})" name="r_zip" class="w70" maxlength="7" id="zip_textbox2">(ハイフン不要・半角数字) <button class="btn btn-m" id="zip_search_button2" onclick="return false;"  style="font-size:12px;padding:10px 0;width:150px">郵便番号から住所を入力</button></p>
								<p>
								<label>都道府県</label>
								<select name="r_address_pref" id="address_pref_select2">
									<option value="">&#8213; 選択してください &#8213;</option>
									({foreach from=$pref item=item})
									<option value="({$item})"  ({if $item==$form.r_address_pref}) selected ({/if}) >({$item})</option>		
									({/foreach})
								</select></p>
								<label>市区町村・番地</label>
								<p>
								<input type="text" value="({$form.r_address_1})" name="r_address_1" id="address_1_textbox2">（全角・半角英数）</p>
								<label>建物名・号室</label>
								<p>
								<input type="text" value="({$form.r_address_2})" name="r_address_2">（全角・半角英数）<p></dd>
							<dt>推薦者の連絡先</dt>
							<dd>
								<p>
								<label>電話番号</label>
								<input type="tel" value="({$form.r_tel_1})" name="r_tel_1" class="w60"> - 
								<input type="tel" value="({$form.r_tel_2})" name="r_tel_2" class="w60"> - 
								<input type="tel" value="({$form.r_tel_3})" name="r_tel_3" class="w60">（半角数字）</p></dd>
							<dt>応募者と推薦者のご関係</dt>
							<dd>
								<input type="text" value="({$form.r_relationship })" name="r_relationship" class="w60">（全角）</dd>
						</dl>
						
						<h3>※アップロードいただく写真の撮影イメージ</h3>
						<p class="txtCenter"><img src="http://beamie.jp/modules/pc/img/img.gif" /></p>

						<div class="boxPrivacy">
						ご応募にはbe amie会員登録(無料)をしていただく必要があります。(be amie会員特典として通常820円分の切手（審査受付料）、返信用封筒（82円切手添付）等の審査にかかる費用は一切戴きません)
						一次選考（ご応募）合格者のみにオスカープロモーションから通知をさせていただきます。
						合格の通知までに約１カ月ほどかかる場合があります。
						お電話やメールによる審査結果のお問合わせに関しては一切応じかねます。
						何度でもご応募可能です。
						自薦、他薦を問わずご応募いただけます。（この場合、応募者、推薦者共にbe amieの会員登録が必要です）
						ブログやＳＮＳの日記等で極力、応募されたことを告知しないようにお願いします。悪質なスカウト等から連絡がくる場合がございます。
						オーディション選考の流れは<a href="({$smarty.const.OPENPNE_URL})?m=pc&a=page_s_selection">こちら</a>からご確認ください。
						</div>

						<p class="txtCenter">
							<input type="submit" name="submit" value="同意の上、確認する" class="btn btn-m" />
						</p>

					</form>
				
				</section>
					({literal})
					<script type="text/javascript" src="/js/zip_code.js"></script>

					<style type='text/css'>
					input[type=number]::-webkit-inner-spin-button,
					input[type=number]::-webkit-outer-spin-button {
						-webkit-appearance: none;
						margin: 0;
					}

					input[type="tel"], input[type="number"]{
						padding:5px;
						border-radius:5px;
					}

					.boxInput .w70{
						width:70px;
						margin:0 5px 0 0;
					}
					</style>

					({/literal})

                ({else})
                <p class="message">応募受付期間外です。</p>
                ({/if})
