<!-- MAIN BEGIN -->

<h1 class="headerLined">オーディション応募完了</h1>

	<section class="boxBase">
	  <div class="explain1">
		<p>	  
		ご応募ありがとうございました。<br>
		<br>

		ご登録いただいたメールアドレスに「応募完了メール」をお送りいたしました。<br>
		ドメイン指定受信されている方は、「beamie.jp」からのメールが受信できるよう設定をお願いいたします。<br>
		<br>
		・選考には１ヶ月程度かかります。<br>
		・第一次審査に通過された方にのみご連絡差し上げます。<br>
		・選考の流れは<a href="({$smarty.const.OPENPNE_URL})?m=pc&a=page_s_selection"><u>こちら</u></a>をご確認ください。<br>
		<br>
		オスカープロモーションのスカウト担当、be amieで活動中！<br>
		スカウト担当がbe amie上でもスカウト活動しています。ぜひスカウト担当をマイアミー登録してください。<br>
		<br>
		<a href="({$smarty.const.OPENPNE_URL})?m=pc&a=page_h_home&target_c_member_id=3676"><u>スカウト担当のページを見る</u></a><br>
		<br>
		</p>
	  </div>
	</section>
<!-- MAIN END -->
