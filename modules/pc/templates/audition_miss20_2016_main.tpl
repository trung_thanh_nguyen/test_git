				<!-- MAIN BEGIN -->
				
				<h1 class="headerLined">第1回ミス美しい20代コンテスト応募フォーム</h1>

                ({if $on_audition == true})
				<section class="boxBase">
				
				<p class="message">(必須)と書かれている項目は必ずご記入をお願いします。</p>

				({if $errors})    
				<table width="100%">
					<tr><td>
						({foreach from=$errors item=error})
						<p style="margin:10px 0px 0px 30px; text-align: left; color: #ff0000;">* ({$error})</p>
						({/foreach})
					</td></tr>
				</table>
				({/if})
	
				<form action="./" method="post" enctype="multipart/form-data" class="boxInput" novalidate>
					<input type="hidden" name="m" value="pc" />
					<input type="hidden" name="a" value="page_audition_miss20_2016_confirm" />
					<input type="hidden" name="a_id" value="({$a_id})" />
					<input type="hidden" name="ssl_param" value="1" />

					<dl class="listHr">
						<dt>氏名<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p>
							<label for="">姓　</label><input type="text" value="({$form.family_name})" name="family_name" class="w70">
							<label for="">名　</label><input type="text" value="({$form.first_name})" name="first_name" class="w70">（全角）
							<p>
							<label for="">セイ</label><input type="text" value="({$form.kana_family_name})" name="kana_family_name" class="w70">
							<label for="">メイ</label><input type="text" value="({$form.kana_first_name})" name="kana_first_name" class="w70">（全角カナ）</p></dd>
						<dt>生年月日<span class="errorTxt">（必須）</span></dt>
						<dd>
						<p>
						<select name="birthday_year">
							<option value="" >--</option>
							({foreach from=$years item=item })
							<option  value="({$item})" ({if $form.birthday_year==$item }) selected="selected" ({/if}) >({$item})</option>
							({/foreach})
						</select>
						&nbsp;年
						<select name="birthday_month">
							<option value="" >--</option>
							({foreach from=$months item=item })
							<option  value="({$item})" ({if $form.birthday_month==$item }) selected="selected" ({/if}) >({$item})</option>
							({/foreach})
						</select>
						&nbsp;月
						<select name="birthday_day">
							<option value="" >--</option>
							({foreach from=$days item=item })
							<option  value="({$item})" ({if $form.birthday_day==$item }) selected="selected" ({/if}) >({$item})</option>
							({/foreach})
						</select>&nbsp;日</p>
						<p>(満 <input name="age" type="tel" value="({$form.age})" class="w60" /> 歳)（半角数字）</p></dd>
						<dt>現住所</dt>
						<dd>
							<p><label>郵便番号</label>
							<input type="tel" value="({$form.zip})" name="zip" class="w70" maxlength="7" id="zip_textbox1">(ハイフン不要・半角数字) 
							<button type="button" class="btn btn-m" id="zip_search_button1" onclick="return false;" style="font-size:12px;padding:10px 0;width:150px">郵便番号から住所を入力</button></p>
							<p><label>都道府県</label>
							<select name="address_pref" id="address_pref_select1">
								<option value="">&#8213; 選択してください &#8213;</option>
								({foreach from=$pref item=item})
								<option value="({$item})"  ({if $item==$form.address_pref}) selected ({/if}) >({$item})</option>		
								({/foreach})
							</select>
							</p>
							<label>市区町村・番地</label>
							<p><input type="text" value="({$form.address_1})" name="address_1" id="address_1_textbox1">（全角・半角英数）</p>
							<label>建物名・号室（必須入力項目ではありません）</label>
							<p><input type="text" value="({$form.address_2})" name="address_2">（全角・半角英数）</p></dd>
						<dt>ご連絡先の電話番号<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p>
							<input type="tel" value="({$form.tel_1})" name="tel_1" class="w60"> - 
							<input type="tel" value="({$form.tel_2})" name="tel_2" class="w60"> - 
							<input type="tel" value="({$form.tel_3})" name="tel_3" class="w60">（半角数字）</p>
						<dt>メールアドレス<span class="errorTxt">（必須）</span></dt>
						<dd>
							<label for="">メールアドレス</label>
							<p>
							<input type="text" value="({$form.email})" name="email">（半角英数）</p>
							<label for="">メールアドレス確認</label>
							<p>
							<input type="text" value="({$form.email_check})" name="email_check">（半角英数）</p></dd>
						</dd>
						<dt>職業・学年<span class="errorTxt">（必須）</span></dt>
						<dd>
							<input type="text" value="({$form.job})" name="job">（全角・半角英数）</dd>
						<dt>特 技<span class="errorTxt">（必須）</span></dt>
						<dd>
							<input type="text" value="({$form.speciality})" name="speciality">（全角・半角英数）</dd>
						<dt>趣  味<span class="errorTxt">（必須）</span></dt>
						<dd>
							<input type="text" value="({$form.interest})" name="interest">（全角・半角英数）</dd>
						<dt>自己PR<span class="errorTxt">（必須）</span>
						志望動機、芸能界での目標を必ずご記入ください。</dt>
						<dd>
							<p>
							<textarea rows="12" name="self_pr"  >({$form.self_pr})</textarea></p>
							（全角130文字まで）</dd>
						<dt>将来の希望<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p>１つ選択してください</p>
							<ul class="listHopeType">
								({foreach from=$department key=key item=item})
								<li><input type="radio" name="wish_department" value="({$item})" ({if $item==$form.wish_department}) checked ({/if}) ><label for="">({$item}) </label></li>
								({/foreach})
							</ul></dd>
						<dt>サイズ<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p>
							<label>身長　　</label>
							<input type="number" name="body_1" value="({$form.body_1})" class="w60" />cm</p>
							<p>
							<label>バスト　</label>
							<input type="number" name="body_2" value="({$form.body_2})" class="w60" />cm</p>
							<p>
							<label>ウェスト</label>
							<input type="number" name="body_3" value="({$form.body_3})" class="w60" />cm</p>
							<p>
							<label>ヒップ　</label>
							<input type="number" name="body_4" value="({$form.body_4})" class="w60" />cm</p>
							<label>シューズ　　　</label>
							<input type="number" name="body_5" value="({$form.body_5})" class="w60" />cm（半角英数）</dd>
						<dt>写真アップロード<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p>10MB以内のJPEGでお願いします</p>
							<p>
							<label>写真1（アップ）　　</label>
							<input type="file" name="photo_filename_1" value="" /></p>
							<p>
							<label>写真2（全身）　　　</label>
							<input type="file" name="photo_filename_2" value="" /></p>
							<p>※撮影イメージは下記の応募写真イメージ画像をご覧ください。<br>
						<dt>芸能活動の有無<span class="errorTxt">（必須）</span></dt>
						<dd>
							<input type="radio" name="belong_production" value="1" id="yesBelong" ({if $form.belong_production==1})checked({/if})>
							<label for="yesBelong">はい</label>
                                                        <input type="radio" name="belong_production" value="2" id="noBelong" ({if $form.belong_production==2})checked({/if})>
							<label for="noBelong">いいえ</label></dd>

						<dt>家族構成</dt>
						<dd>
						<p>
                                                        <label for="">氏名　</label><input type="text" value="({$form.familys_1})" name="familys_1" >
                                                        <label for="">続柄　</label><input type="text" value="({$form.familys_r1})" name="familys_r1" class="w70">
						</p>
						<p>
							<label for="">氏名　</label><input type="text" value="({$form.familys_2})" name="familys_2" >
							<label for="">続柄　</label><input type="text" value="({$form.familys_r2})" name="familys_r2" class="w70">
						</p>
						<p>
							<label for="">氏名　</label><input type="text" value="({$form.familys_3})" name="familys_3" >
							<label for="">続柄　</label><input type="text" value="({$form.familys_r3})" name="familys_r3" class="w70">
						</p>
						<p>
							<label for="">氏名　</label><input type="text" value="({$form.familys_4})" name="familys_4" >
							<label for="">続柄　</label><input type="text" value="({$form.familys_r4})" name="familys_r4" class="w70">
						</p>
	
						</dd>
						</dl>

						<h2 class="header-article">推薦者</h2>
						<p class="errorTxt">※推薦の場合：以下は推薦者様に関する内容をご記入ください。</p>
						
						<dl class="listHr">
							<dt>氏名</dt>
							<dd>
								<p>
								<label>姓　</label><input type="text" value="({$form.r_family_name})" name="r_family_name" class="w70">
								<label>名　</label><input type="text" value="({$form.r_first_name})" name="r_first_name" class="w70">（全角）</p>
								<p>
								<label>セイ</label><input type="text" value="({$form.r_kana_family_name})" name="r_kana_family_name" class="w70">
								<label>メイ</label><input type="text" value="({$form.r_kana_first_name})" name="r_kana_first_name" class="w70">（全角カナ）</p></dd>

							<dt>推薦者の連絡先</dt>
							<dd>
								<p>
								<label>電話番号</label>
								<input type="tel" value="({$form.r_tel_1})" name="r_tel_1" class="w60"> - 
								<input type="tel" value="({$form.r_tel_2})" name="r_tel_2" class="w60"> - 
								<input type="tel" value="({$form.r_tel_3})" name="r_tel_3" class="w60">（半角数字）</p></dd>
							<dt>応募者と推薦者のご関係</dt>
							<dd>
								<input type="text" value="({$form.r_relationship })" name="r_relationship" class="w60">（全角）</dd>
						</dl>
						
						<h3>※アップロードいただく写真の撮影イメージ</h3>
						<p class="txtCenter"><img src="http://beamie.jp/modules/pc/img/img.gif" /></p>

						<div class="boxPrivacy" style="height:100px;">
						ご応募にはbe amie会員登録(無料)をしていただく必要があります。
						一次選考（ご応募）合格者のみにオスカープロモーションから通知をさせていただきます。
						合格の通知までに約１カ月ほどかかる場合があります。
						お電話やメールによる審査結果のお問合わせに関しては一切応じかねます。
						自薦、他薦を問わずご応募いただけます。（この場合、応募者、推薦者共にbe amieの会員登録が必要です）
						ブログやＳＮＳの日記等で極力、応募されたことを告知しないようにお願いします。悪質なスカウト等から連絡がくる場合がございます。
						</div>

						<p class="txtCenter">
							<input type="submit" name="submit" value="同意の上、確認する" class="btn btn-m" />
						</p>

					</form>
				
				</section>
					({literal})
					<script type="text/javascript" src="/js/zip_code.js"></script>

					<style type='text/css'>
					input[type=number]::-webkit-inner-spin-button,
					input[type=number]::-webkit-outer-spin-button {
						-webkit-appearance: none;
						margin: 0;
					}

					input[type="tel"], input[type="number"]{
						padding:5px;
						border-radius:5px;
					}

					.boxInput .w70{
						width:70px;
						margin:0 5px 0 0;
					}
					</style>

					({/literal})

                ({else})
                <p class="message">応募受付期間外です。</p>
                ({/if})
