				<h1 class="headerLined">オーディション応募フォーム</h1>

				({if $form_error})    
				<div class="dparts alertBox"><div class="parts">
				<table width="100%"><tr>
				<td align="center">
				<div  style="margin-left:160px;padding:20px;color:#FF0000">
				({$form_error})
				</div>
				</td>
				</tr></table>
				</div></div>
				({/if})
				
				<section class="boxBase">
				
				<form action="./" method="post" enctype="multipart/form-data" class="boxInput">
					<input type="hidden" name="m" value="pc" />
					<input type="hidden" name="a" value="page_audition_confirm" />
                    <input type="hidden" name="a_id" value="({$a_id})" />
					<input type="hidden" name="ssl_param" value="1" />

					<dl class="listHr">
						<dt>氏名</dt>
						<dd>
							<p>
							({$form.family_name})　({$form.first_name})</p>
							<p>
							({$form.kana_family_name})　({$form.kana_first_name})</p></dd>
						<dt>生年月日</dt>
						<dd>
						<p>
						&nbsp;({$form.birthday_year})年
						&nbsp;({$form.birthday_month})月
						&nbsp;({$form.birthday_day})日 
						（満 ({$form.age}) 歳）</p></dd>
						<dt>現住所</dt>
						<dd>
							<p>({$form.zip})</p>
							<p>({$form.address_pref})</p>
							<p>({$form.address_1})</p>
							<p>({$form.address_2})</p></dd>
						<dt>ご連絡先の電話番号</dt>
						<dd>
							<label>自宅電話番号</label>
							<p>
							({$form.tel_1}) - ({$form.tel_2}) - ({$form.tel_3})</p>
							({if empty($form.mobile_tel_1) === false || empty($form.mobile_tel_2) === false || empty($form.mobile_tel_3) === false})
							<label>携帯電話番号（必須入力項目ではありません）</label>
							<p>
							({$form.mobile_tel_1}) - ({$form.mobile_tel_2}) - ({$form.mobile_tel_3})</p></dd>
							({/if})
						<dt>メールアドレス</dt>
						<dd>({$form.email})</dd>
						<dt>最終学歴</dt>
						<dd><p>({$form.education})</p>
							({if $form.in_school=='1' || $form.in_school=='3' })
								({if $form.in_school==1})在学({else})中退({/if})
							({else}) 
								({$form.graduation_year})年 卒業 
							({/if})
							<!-- <label for="graduated">卒業</label>（西暦2010年）</dd> -->
						<dt>現在の職業</dt>
						<dd>
							({$form.job})</dd>
						<dt>特 技</dt>
						<dd>
							({$form.speciality})</dd>
						<dt>趣  味</dt>
						<dd>
							({$form.interest})</dd>
						<dt>自己PR</dt>
						<dd>
							({$form.self_pr})</dd>
						<dt>希望部門</dt>
						<dd>
							({$form.wish_department})</dd>
						<dt>サイズ</dt>
						<dd>
							<p>
							<label>身長</label>
							({$form.body_1})cm</p>
							<p>
							<label>バスト</label>
							({$form.body_2})cm
							<label>ウェスト</label>
							({$form.body_3})cm
							<label>ヒップ</label>
							({$form.body_4})cm</p>
							<label>靴</label>
							({$form.body_5})cm（半角英数）</dd>
						<dt>写真アップロード</dt>
						<dd>
							({if empty($form.photo_filename_1) == false})
							<p>
							<label>写真1（アップ）</label>
                            <img src="./img_temp.php?temp_name=({$form.tmpphotofile_1})" width="200" ></p>
							({/if})
							({if empty($form.photo_filename_2) == false})
							<p>
							<label>写真2（全身）</label>
                            <img src="./img_temp.php?temp_name=({$form.tmpphotofile_2})" width="200" ></p>
							({/if})
							({if empty($form.photo_filename_3) == false})
							<p>
							<label>写真3（部位）</label>
                            <img src="./img_temp.php?temp_name=({$form.tmpphotofile_3})" width="200" ></p>
							({/if})
						({if empty($form.movie_filename) == false})
						<dt>動画アップロード</dt>
						<dd>
							({$form.movie_filename})</dd>
						({/if})
						<dt>現在、他の芸能プロダクションに所属していますか？</dt>
						<dd>
							<p>({if $form.belong_production=='1' }) はい ({else}) いいえ ({/if})</p>
							<label>所属事務所名</label>
							<p>({$form.production_name})</p>
							<label>契約期間</label>
							<p>({$form.contact_period})</p></dd>
						<dt>今までにオーディションを受けたことがありますか？</dt>
						<dd>
							<p>({if $form.past_audition_exp=="1"})はい({else})いいえ({/if})</p>
							<label>オーディション名</label>
							<p>({$form.past_audition_name})</p>
							<label>結果</label>
							<p>({$form.past_audition_result})</p>
							<label>その他、経験 </label>
							<p>({$form.other_experience})</p></dd>

						<dt>be amie登録情報</dt>
						<dd>
							<label for="">メールアドレス</label>
							<p>({$form.email_login})</p>
							<label for="">ニックネーム</label>
							<p>({$form.login_nickname})</p></dd>
						</dl>

						({if empty($form.r_email) == false || empty($form.r_nickname) == false || empty($form.r_family_name) == false || empty($form.r_first_name) == false || empty($form.r_kana_family_name) == false || empty($form.r_kana_first_name) == false || empty($form.r_zip) == false || empty($form.r_address_pref) == false || empty($form.r_address_1) == false || empty($form.r_address_2) == false || empty($form.r_tel_1) == false|| empty($form.r_tel_2) == false|| empty($form.r_tel_3) == false|| empty($form.r_relationship) == false})
						<h2 class="header-article">推薦者のbe amie登録情報</h2>
						
						<dl class="listHr">
							({if empty($form.r_email) == false})
							<dt>メールアドレス</dt>
							<dd>
								<p>({$form.r_email})</p></dd>
							({/if})
							({if empty($form.r_nickname) == false})
							<dt>ニックネーム</dt>
							<dd>
								({$form.r_nickname})</dd>
							({/if})
							({if empty($form.r_family_name) == false || empty($form.r_first_name) == false || empty($form.r_kana_family_name) == false || empty($form.r_kana_first_name) == false})
							<dt>氏名</dt>
							<dd>
								<p>({$form.r_family_name})　({$form.r_first_name})</p>
								<p>({$form.r_kana_family_name})　({$form.r_kana_first_name})</p></dd>
							({/if})
							({if empty($form.r_zip) == false || empty($form.r_address_pref) == false || empty($form.r_address_1) == false || empty($form.r_address_2) == false})
							<dt>推薦者のご住所
							<dd>
								({if empty($form.r_zip) == false})
								<p><label>郵便番号</label>({$form.r_zip})</p>
								({/if})
								({if empty($form.r_address_pref) == false})
								<p>
								<label>都道府県</label>
								({$form.r_address_pref})</p>
								({/if})
								({if empty($form.r_address_1) == false})
								<label>市区町村・番地</label>
								<p>
								({$form.r_address_1})</p>
								({/if})
								({if empty($form.r_address_2) == false})
								<label>建物名・号室</label>
								<p>
								({$form.r_address_2})<p>
								({/if})
							</dd>
							({/if})
							({if empty($form.r_tel_1) == false || empty($form.r_tel_2) == false || empty($form.r_tel_3) == false})
							<dt>推薦者の連絡先</dt>
							<dd>
								<p>
								<label>電話番号</label>
								({$form.r_tel_1}) - ({$form.r_tel_2}) - ({$form.r_tel_3})</p></dd>
							({/if})
							({if empty($form.r_relationship) == false})
							<dt>応募者と推薦者のご関係</dt>
							<dd>
								({$form.r_relationship })</dd>
							({/if})
						</dl>
						({/if})

						({t_form_block m=pc a=page_audition})
						<div class="txtCenter">
							({foreach from=$form key=key item=item})
							<input type="hidden" name="({$key})" value="({$item})" />
							({/foreach})
							<input type="submit" class="btn-m btnCancel" value="　戻る　" />
						</div>
						({/t_form_block})
						({t_form_block m=pc a=do_audition})
						<div class="txtCenter">
							({foreach from=$form key=key item=item})
							<input type="hidden" name="({$key})" value="({$item})" />
							({/foreach})	
							<input type="submit" class="btn btn-m" value="上記内容で送信する" />
						</div>
						({/t_form_block})

						</form>
				</section>
