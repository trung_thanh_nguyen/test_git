<!-- MAIN BEGIN -->
<div id="mainer">
	<div id="mainnertop"></div>
    <div id="main" class="page_talent_mail">

({if $form_error})    
<div class="dparts alertBox"><div class="parts">
<table width="100%"><tr>
<td align="center">
<div  style="margin-left:160px;padding:20px;color:#FF0000">
({$form_error})
</div>
</td>
</tr></table>
</div></div>
({/if})

<div style="text-align:center;">
<img src="modules/pc/img/13th_audition.gif" />
</div>


<table width="100%" >
<tr>
	<td class="mail_cap" width="200px;" ><div class="left">氏名<span class="red">（必須）</span></div></td>
	<td><div class="right">
		<table>
		<tbody><tr>
		<td>姓</td><td>({$form.name_1})</td>
		<td>名</td><td>({$form.name_2})</td>
		</tr>
		<tr>
		<td>セイ</td><td>({$form.kananame_1})</td>
		<td>メイ</td><td>({$form.kananame_2})</td>
		</tr>
		</tbody></table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap"><div class="left">生年月日<span class="red">（必須）</span></div></td>
	<td>({$form.birthday_year})年 ({$form.birthday_month})月({$form.birthday_day})日 ({$form.age}) 歳
  </td>

</tr>
<tr>
	<td class="mail_cap">
	<div class="left">ご住所<span class="red">（必須）</span><p>建物名、号室は<br>必須入力項目ではありません。</p></div>
	</td>
	<td>
		<div class="right">
		<table>
		<tbody><tr>
		<td>郵便番号</td><td>({$form.zip})</td>
		</tr>
		<tr>
		<td>都道府県</td><td>
		({$form.address_pref})		
		</td>
		</tr>
		<tr>
		<td>市区郡～番地</td>
		<td>({$form.address_1})</td>
		</tr>
		<tr>
		<td>建物名・号室</td>
		<td>({$form.address_2})</td>
		</tr>
		</tbody></table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">ご連絡先の電話番号<span class="red">（必須）</span></div>
	</td>
	<td>
	<div class="right">
	<table>
	<tbody><tr>
	<td >電話番号</td><td>({$form.tel_1}) - ({$form.tel_2}) - ({$form.tel_3})</td>
	</tr>
	</tbody></table>
	</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">メールアドレス<span class="red">（必須）</span></div>
	</td>
	<td>({$form.email})	
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">学校名・学年<span class="red">（必須）</span></div>
	</td>
	<td>
	<div class="right">
	<table>
	<tbody><tr>
	<td>
	({$form.education})
	</td>
	</tr>
	<tr>
	<td>
	学年 ({$form.graduation_year})年
	</td>
	</tr>
	</tbody></table>
	</div>
	</td>
</tr>

<tr>
	<td class="mail_cap"><div class="left">特  技<span class="red">（必須）</span></div></td>
	<td><div class="right">
		({$form.speciality})
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap"><div class="left">趣  味<span class="red">（必須）</span></div></td>
	<td><div class="right">
		({$form.interest})
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap"><div class="left">自己PR<span class="red">（必須）</span></div></td>
	<td><div class="right">
		({$form.owner_PR})
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">家族構成</td>
	<td>
		<table>
		<tr><th><div style="text-align:center;">氏名</div></th><th style="text-align:center;">続柄</th></tr>
		<tr><td>({$form.family_f})</td><td>({$form.family_f_name})</td></tr>
		<tr><td>({$form.family_m})</td><td>({$form.family_m_name})</td></tr>
		<tr><td>({$form.family_b})</td><td>({$form.family_b_name})</td></tr>
		<tr><td>({$form.family_yb})</td><td>({$form.family_yb_name})</td></tr>
		</table>
	</td>
	
</tr>
<tr>
	<td class="mail_cap"><div class="left">将来の希望<span class="red">（必須）</span></div></td>
	<td>
		<div class="right">
		({$form.wish_department})		
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap"><div class="left">サイズ<span class="red">（必須）</span></div></td>
	<td>
		<div class="right">
		<table>
		<tr>
			<td>身長</td><td>バスト</td><td>ウェスト</td><td>ヒップ</td><td>シューズ</td>
		</tr>
		<tr>
			<td>({$form.body_1})cm</td><td>({$form.body_2})cm</td><td>({$form.body_3})cm</td><td>({$form.body_4})cm</td><td>({$form.body_5})cm</td>
		</tr>
		</table>
		</div>
	</td>
</tr>
({if $is_iphone==false})
<tr>
	<td class="mail_cap"><div class="left">写真<span class="red">（必須）</span></div></td>
	<td>
		<table>
			<tr><td>写真1 ({$form.photo_filename_1})</td></tr>	
			<tr><td>写真2({$form.photo_filename_2})</td></tr>	
			<tr><td></td></tr>		
		</table>
	</td>
</tr>
({/if})
<tr>
	<td class="mail_cap">
	<div class="left">beamie<span class="red">（必須）</span>be amieご登録のメールアドレスをご記入ください</div>
	</td>
	<td>
	<div class="right">
	<table>
	<tbody><tr>
	<td>メールアドレス</td><td>({$form.email_login})</td>
	</tr>	
	<tr>
	<td>ニックネーム</td><td>({$form.login_nickname})</td>
	</tr>
	</tbody></table>
	</div>
	</td>
</tr>
<tr><td colspan="2" >※未成年の方は保護者の同意が必要です。<br/>※面接に進まれた方は別途保護者の同意書をご用意いただきます。
　　
	</td>
</tr>

<tr>
	<td class="mail_cap"><div class="left">保護者氏名</div></td>
	<td><div class="right">
		<table>
		<tbody><tr>
		<td>姓</td><td>({$form.r_name_1})</td>
		<td>名</td><td>({$form.r_name_2})</td>
		</tr>
		<tr>
		<td>セイ</td><td>({$form.r_kana_name_1})</td>
		<td>メイ</td><td>({$form.r_kana_name_2})</td>
		</tr>
		</tbody></table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">保護者住所</div>
	</td>
	<td>
		<div class="right">
		<table>
		<tbody><tr>
		<td>郵便番号</td><td>({$form.r_zip})</td>
		</tr>
		<tr>
		<td>都道府県</td><td>
		({$form.r_address_pref})		
		</td>
		</tr>
		<tr>
		<td>市区郡～番地</td>
		<td>({$form.r_address_1})</td>
		</tr>
		<tr>
		<td>建物名・号室</td>
		<td>({$form.r_address_2})</td>
		</tr>
		</tbody></table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">保護者連絡先</div>
	</td>
	<td>
	({$form.r_tel_1}) - ({$form.r_tel_2}) - ({$form.r_tel_3})	
	</td>
</tr>


<tr>
	<td class="mail_cap">
	<div class="left"> 本人との関係　（全角・半角英数）</div>
	</td>
	<td>
	<div class="right">
		<table>
		<tbody><tr>
		<td ></td><td>({$form.r_relationship })</td>
		</tr>
		</tbody></table>
	</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">アンケート</div>
	</td>
	<td>
		<div class="right">
			({$form.enquete})
			({if $form.enquete=="新聞"})( ({$form.enqtext3}) )({/if})
			({if $form.enquete=="雑誌"})( ({$form.enqtext4}) )({/if})
			({if $form.enquete=="その他"})( ({$form.enqtext7}) )({/if})
		</div>
	</td>
</tr>
<tr>
	<td></td><td></td>
</tr>
</table>

({t_form_block m=pc a=do_audition_girl})
<div class="bt" style="float: left;margin-left:300px;margin-top:50px;">
	({foreach from=$form key=key item=item})
	<input type="hidden" name="({$key})" value="({$item})" />
	({/foreach})	
	<input type="submit" class="input_submit" value="　送信　" />
</div>
({/t_form_block})

({t_form_block m=pc a=page_audition_girl})
<div class="bt" style="float: left;margin-left:50px;margin-top:50px;">
	({foreach from=$form key=key item=item})
	<input type="hidden" name="({$key})" value="({$item})" />
	({/foreach})
	<input type="submit" class="input_submit" value="　戻る　" />
</div>
({/t_form_block})


</div>

</div>
<!-- MAIN END -->