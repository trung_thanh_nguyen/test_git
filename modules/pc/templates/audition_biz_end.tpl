<!-- MAIN BEGIN -->
<div id="mainer">
	<div id="mainnertop"></div>
  <div id="main" class="page_talent_mail_end">  
    <div style="text-align:center;">
<img src="modules/pc/img/biz_audition.gif" />
</div>
	({if $is_iphone==false})
	  <div class="explain1">
<p>	  
オーディションへの応募を承りました。ご応募ありがとうございました。
</p>
<p>
・審査状況・合否に関わるお問い合わせはお答えいたしかねます。<br/>
・一次の書類審査合格者の方に限り、二次の面接審査を通知いたします。<br/>
<a href="http://beamie.jp/" target=_top>be amieトップページへ</a><br/>
</p>
	  </div>
	({/if})
	({if $is_iphone==true})
	<table width="100%">
	
		<tr>
		<td>
		<p>	  
		<span class="red">応募はまだ完了していません。</span>最後に写真を送信して応募が完了となります。以下の写真１、写真２を押すとメール作成画面が開きますので、写真を添付し送信してください。<br/>
		</p>
		</td>
		</tr>
		<tr align="center">
		<td style="text-align: center;">
		<br/>
		<a href="mailto:({$mail_address1})" style="font-size: larger;">写真1（アップ）を送る</a><br/><br/>
		</td>
		</tr>
		<tr>
		<td style="text-align: center;">
		<a href="mailto:({$mail_address2})" style="font-size: larger;">写真2（全身）を送る</a><br/><br/>
		</td>
		</tr>
		<tr>
			<td>
			※最近6ヶ月以内に撮影したものを送付ください。<br/><br/>
			<span class="red">※iPhoneをご利用の方へ</span><br/>
			iPhoneの場合、上のリンクを押してメール作成画面を開いた後、写真アルバムを開きます。<br/>
			送信する写真を開き長押しして「コピー」、その後Safariに戻るとメール作成画面になっていますので、本文欄を長押しして写真をペーストし送信します。<br/>
			件名は何を入力しても構いません。<br/><br/>
			※写真イメージは下記の応募写真イメージ画像をご覧ください。<br/>
			<span class="red">※写真は縦長で頭が上方向の写真を送付ください。</span><br/>
			横撮影のカメラを立てて撮影すると、見た目は縦長写真ですが写真データは横向きの場合があります。<u>写真確認の際に頭が上になるよう回転し保存してから送付をお願いします。</u><br/>
			※プリクラや撮影後加工した写真は不可です。<br/>
			</td>
		</tr>
		<tr>
			<td>
			※アップロードいただく写真の撮影イメージ<br/>
			<img src="./modules/pc/img/photoimg.gif" alt="">	
			</td>
		</tr>
		<tr>
			<td>
			<span class="red">写真の送信で応募は完了です。</span><br/><br/>
			・審査状況・合否に関わるお問い合わせはお答えいたしかねます。<br/>
			・一次審査(書類審査)を通過された方に限り、二次の面接審査を美ジネスマン＆美ジネスウーマンコンテスト実行委員会事務局より通知いたします。<br/>
			・be amie内でオスカープロモーション公認スカウト以外の悪質なスカウト行為には充分ご注意ください！！<br/>
			<span class="red">美ジネスマン＆美ジネスウーマンコンテスト実行委員会事務局より皆様宛に直接メッセージをお送りすることは絶対にございません。<br/>
			もしbeamieを通じて不信なメッセージが届いた場合は、下記までご連絡をお願いいたします。</span><br/>
			美ジネスマン＆美ジネスウーマンコンテスト実行委員会事務局<br/>
			03-6427-2816（平日11:00～17:00受付）<br/>

			</td>
		</tr>
	</table>
	({/if})
	</div>	
</div>
<!-- MAIN END -->