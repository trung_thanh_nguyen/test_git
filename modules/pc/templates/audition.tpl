({ext_include file="inc/head.tpl"})
<body>

({ext_include file="inc/tags_top.tpl"})
<div class="wrapper">

	({ext_include file="inc/header.tpl"})

	<div id="main" class="main-content">
		({if !$sp})
		<div class="container">
			<ul class="breadcrumb list-inline">
				<li><a href="../">TOP</a></li>
				<li class="arrow">&gt;</li>
				<li><a href="./">オーディション</a></li>
				<li class="arrow">&gt;</li>
				<li>オーディション応募フォーム</li>
			</ul><!-- End .breadcrumb -->
			({else})
			<div class="content-wrapper">
				({/if})

				<div class="audition-apply-form ({if $sp})box-gray({/if})">
					({if !$sp})<h2>オーディション応募フォーム</h2>({/if})
					({if $on_audition})
					<div class="frame">
						({ext_include file="inc/audition_edit_form.tpl"})
					</div>
					({else})
					<div class="text-center">
						応募受付期間外です。
					</div>
					({/if})

				</div>

			</div>

			<!-- End #main --></div>


		({ext_include file="inc/footer.tpl"})
	</div><!-- end .wrapper -->

	({ext_include file="inc/js.tpl"})
	({ext_include file="inc/form_login.tpl"})

</body>
</html>