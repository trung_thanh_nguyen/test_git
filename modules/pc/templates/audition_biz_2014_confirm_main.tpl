				<h1 class="headerLined">第2回　美ジネスマン＆美ジネスウーマンコンテスト　入力内容確認</h1>

				({if $form_error})    
				<div class="dparts alertBox"><div class="parts">
				<table width="100%"><tr>
				<td align="center">
				<div  style="margin-left:160px;padding:20px;color:#FF0000">
				({$form_error})
				</div>
				</td>
				</tr></table>
				</div></div>
				({/if})
				
				<section class="boxBase">
				
				<form action="./" method="post" enctype="multipart/form-data" class="boxInput">
					<input type="hidden" name="m" value="pc" />
					<input type="hidden" name="a" value="page_audition_biz_2014_confirm" />
					<input type="hidden" name="ssl_param" value="1" />

					<dl class="listHr">
						<dt>氏名</dt>
						<dd>
							<p>
							({$form.family_name})　({$form.first_name})</p>
							<p>
							({$form.kana_family_name})　({$form.kana_first_name})</p></dd>
						<dt>生年月日</dt>
						<dd>
						<p>
						&nbsp;({$form.birthday_year})年
						&nbsp;({$form.birthday_month})月
						&nbsp;({$form.birthday_day})日 
						（満 ({$form.age}) 歳）</p></dd>
						<dt>現住所</dt>
						<dd>
							<p>({$form.zip})</p>
							<p>({$form.address_pref})</p>
							<p>({$form.address_1})</p>
							<p>({$form.address_2})</p></dd>
						<dt>ご連絡先の電話番号</dt>
						<dd>
							<p>
							({$form.tel_1}) - ({$form.tel_2}) - ({$form.tel_3})</p>
						<dt>メールアドレス</dt>
						<dd>({$form.email})</dd>
                        <dt>性別</dt>
                        <dd><p>({if $form.gender == 'male'})男性({else})女性({/if})</p>
						<dt>職種・学年</dt>
						<dd><p>({$form.job})</p>
						<dt>特 技</dt>
						<dd>
							({$form.speciality})</dd>
						<dt>趣  味</dt>
						<dd>
							({$form.interest})</dd>
						<dt>自己PR</dt>
						<dd>
							({$form.self_pr})</dd>
						({if empty($form.family_f_name) == false || empty($form.family_f) == false || empty($form.family_m_name) == false || empty($form.family_m) == false || empty($form.family_b_name) == false || empty($form.family_b) == false || empty($form.family_yb_name) == false || empty($form.family_yb) == false})
						<dt>家族構成</dt>
						<dd>
						({if empty($form.family_f_name) == false})（({$form.family_f_name})）({/if}) ({$form.family_f})<br>
						({if empty($form.family_m_name) == false})（({$form.family_m_name})）({/if})({$form.family_m})<br>
						({if empty($form.family_b_name) == false})（({$form.family_b_name})）({/if})({$form.family_b})<br>
						({if empty($form.family_yb_name) == false})（({$form.family_yb_name})）({/if})({$form.family_yb})
						</dd>
						({/if})
						<dt>芸能界での目標</dt>
						<dd>
							({$form.wish_department})</dd>
						<dt>サイズ</dt>
						<dd>
							<p>
							<label>身長</label>
							({$form.body_1})cm</p>
							<p>
							<label>バスト</label>
							({$form.body_2})cm
							<label>ウェスト</label>
							({$form.body_3})cm
							<label>ヒップ</label>
							({$form.body_4})cm</p>
							<label>靴</label>
							({$form.body_5})cm</dd>
						<dt>写真アップロード</dt>
						<dd>
							<label>写真1（アップ）</label><br>
                            <p><img src="./img_temp.php?temp_name=({$form.tmpphotofile_1})" width="200" ></p>
							<label>写真2（全身）</label>
                            <p><img src="./img_temp.php?temp_name=({$form.tmpphotofile_2})" width="200" ></p>
						<dt>be amie登録情報</dt>
						<dd>
							<label for="">メールアドレス</label>
							<p>({$form.email_login})</p>
							<label for="">ニックネーム</label>
							<p>({$form.login_nickname})</p></dd>
						</dl>

						({if empty($form.r_family_name) == false || empty($form.r_first_name) == false || empty($form.r_kana_family_name) == false || empty($form.r_kana_first_name) == false || empty($form.r_zip) == false || empty($form.r_address_pref) == false || empty($form.r_address_1) == false || empty($form.r_address_2) == false})
						<h2 class="header-article">保護者情報</h2>
						
						<dl class="listHr">
							({if empty($form.r_family_name) == false || empty($form.r_first_name) == false || empty($form.r_kana_family_name) == false || empty($form.r_kana_first_name) == false})
							<dt>氏名</dt>
							<dd>
								<p>({$form.r_family_name})　({$form.r_first_name})</p>
								<p>({$form.r_kana_family_name})　({$form.r_kana_first_name})</p></dd>
							({/if})

							({if empty($form.r_zip) == false || empty($form.r_address_pref) == false || empty($form.r_address_1) == false || empty($form.r_address_2) == false})
							<dt>保護者のご住所
							<dd>
								({if empty($form.r_zip) == false})
								<p><label>郵便番号</label>({$form.r_zip})</p>
								({/if})
								({if empty($form.r_address_pref) == false})
								<p>
								<label>都道府県</label>
								({$form.r_address_pref})</p>
								({/if})
								({if empty($form.r_address_1) == false})
								<label>市区町村・番地</label>
								<p>
								({$form.r_address_1})</p>
								({/if})
								({if empty($form.r_address_2) == false})
								<label>建物名・号室</label>
								<p>
								({$form.r_address_2})<p>
								({/if})
							</dd>
							({/if})
							({if empty($form.r_tel_1) == false || empty($form.r_tel_2) == false || empty($form.r_tel_3) == false})
							<dt>保護者電話番号</dt>
							<dd>
								<p>
								({$form.r_tel_1}) - ({$form.r_tel_2}) - ({$form.r_tel_3})</p></dd>
							({/if})
							({if empty($form.r_relationship) == false})
							<dt>本人との関係</dt>
							<dd>
								({$form.r_relationship })</dd>
							({/if})
						</dl>
						({/if})

						({if empty($form.enquete) == false})
						<h2 class="header-article">アンケート</h2>
						<dl class="listHr">
							<dt>今回の美ジネスマン＆美ジネスウーマンコンテストを何で知りましたか？</dt>
							<dd>
							<div>
							({$form.enquete})
							({if $form.enquete=="新聞"})( ({$form.enqtext3}) )({/if})
							({if $form.enquete=="雑誌"})( ({$form.enqtext4}) )({/if})
							({if $form.enquete=="その他"})( ({$form.enqtext7}) )({/if})
							</div>
							</dd>
						</dl>
						({/if})

						({t_form_block m=pc a=page_audition_biz_2014})
						<div class="txtCenter">
							({foreach from=$form key=key item=item})
							<input type="hidden" name="({$key})" value="({$item})" />
							({/foreach})
							<input type="submit" class="btn-m btnCancel" value="　戻る　" />
						</div>
						({/t_form_block})
						({t_form_block m=pc a=do_audition_biz_2014})
						<div class="txtCenter">
							({foreach from=$form key=key item=item})
							<input type="hidden" name="({$key})" value="({$item})" />
							({/foreach})	
							<input type="submit" class="btn btn-m" value="上記内容で送信する" />
						</div>
						({/t_form_block})

						</form>
				</section>
