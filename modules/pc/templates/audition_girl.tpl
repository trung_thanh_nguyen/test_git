<!-- MAIN BEGIN -->
<div id="mainer">
({if $is_iphone==false})
({assign var="enctype" value="file"})
({else})
({assign var="enctype" value=""})
({/if})
({t_form_block _enctype=$enctype m=pc a=page_audition_girl_confirm})
	<div id="mainnertop"></div>
    <div id="main" class="page_talent_mail">

({if $errors})    
<div class="dparts alertBox"><div class="parts">
<table width="100%"><tr>
<td align="center">
<div  style="margin-left:160px;padding:20px;color:#FF0000">
<ul>
({foreach from=$errors item=error})
<li style="color: #ff0000;">({$error})</li>
({/foreach})
</ul>
</div>
</td>
</tr></table>
</div></div>
({/if})

<div style="text-align:center;">
<img src="modules/pc/img/13th_audition.gif" />
</div>

<div class='explain1' style="text-align: right;">
<span class="red">(必須)</span>と書かれている項目は必ずご記入をお願いします。
</div>
<table>
<tr>
	<td class="mail_cap" width="200px;"><div class="left">氏名<span class="red">(必須)</span></div></td>
	<td><div class="right">
		<table>
		<tbody><tr>
		<td>姓</td><td><input type="text" value="({$form.name_1})" style="width: 100px;" name="name_1"></td>
		<td>名</td><td><input type="text" value="({$form.name_2})" style="width: 100px;" name="name_2">(全角)</td>
		</tr>
		<tr>
		<td>セイ</td><td><input type="text" value="({$form.kananame_1})" style="width: 100px;" name="kananame_1"></td>
		<td>メイ</td><td><input type="text" value="({$form.kananame_2})" style="width: 100px;" name="kananame_2">(全角カナ)</td>
		</tr>
		</tbody></table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap"><div class="left">生年月日<span class="red">(必須)</span></div></td>
	<td>
	<select name="birthday_year" style="">
    <option value="" >--</option>
    ({foreach from=$years item=item })
    <option  value="({$item})" ({if $form.birthday_year==$item }) selected="selected" ({/if}) >({$item})</option>
    ({/foreach})
    </select>
       &nbsp;年
    <select name="birthday_month" style="">
    <option value="" >--</option>
    ({foreach from=$months item=item })
    <option  value="({$item})" ({if $form.birthday_month==$item }) selected="selected" ({/if}) >({$item})</option>
    ({/foreach})
    </select>
    &nbsp;月
    <select name="birthday_day" style="">
    <option value="" >--</option>
    ({foreach from=$days item=item })
    <option  value="({$item})" ({if $form.birthday_day==$item }) selected="selected" ({/if}) >({$item})</option>
    ({/foreach})
    </select>&nbsp;日 (満<input style="width:50px;" name="age" type="text" value="({$form.age})" /> 歳)(半角英数)
  </td>

</tr>
<tr>
	<td class="mail_cap">
	<div class="left">現住所<span class="red">(必須)</span><p>建物名、号室は<br>必須入力項目ではありません。</p></div>
	</td>
	<td>
		<div class="right">
		<table>
		<tbody><tr>
		<td nowrap="">郵便番号</td><td><input type="text" value="({$form.zip})" style="width: 100px;" name="zip">(ハイフン不要・半角数字)</td>
		</tr>
		<tr>
		<td nowrap="">都道府県</td><td>
		<select class="gray" name="address_pref">
		<option value="">&#8213; 選択してください &#8213;</option>
		({foreach from=$pref item=item})
		<option value="({$item})"  ({if $item==$form.address_pref}) selected ({/if}) >({$item})</option>		
		({/foreach})
		</select>
		</td>
		</tr>
		<tr>
		<td nowrap="">市区郡～番地</td>
		<td><input type="text" value="({$form.address_1})" style="width: 225px;" name="address_1">(全角・半角英数)</td>
		</tr>
		<tr>
		<td nowrap="">建物名・号室</td>
		<td><input type="text" value="({$form.address_2})" style="width: 225px;" name="address_2">(全角・半角英数)</td>
		</tr>
		</tbody></table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">ご連絡先の電話番号<span class="red">(必須)</span></div>
	</td>
	<td>
	<div class="right">
	<table>
	<tbody><tr>
	<td >電話番号</td><td><input type="text" value="({$form.tel_1})" style="width: 40px;" name="tel_1"> - <input type="text" value="({$form.tel_2})" style="width: 60px;" name="tel_2"> - <input type="text" value="({$form.tel_3})" style="width: 60px;" name="tel_3">(半角数字)</td>
	</tr>
	</tbody></table>
	</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">
	メールアドレス<span class="red">(必須)</span>
	</div>
	</td>
	<td>
	<div class="right">
	<table>
	<tbody><tr>
	<td >メールアドレス</td><td><input type="text" value="({$form.email})" style="width: 240px;" name="email">(半角英数)</td>
	</tr>
	<tr>
	<td >メールアドレス確認</td><td><input type="text" value="({$form.email_check})" style="width: 240px;" name="email_check">(半角英数)</td>
	</tr>
	</tbody></table>
	</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">学校名・学年<span class="red">(必須)</span><br/>在学中の学校名あるいは卒業校名。卒業されている場合、学年は空欄。</div></td>
	<td>
	<div class="right">
	<table>
	<tbody><tr>
	<td>
	<input type="text" value="({$form.education})" style="width: 300px;" name="education" maxlength="24" onpropertychange="checkLength(this,24);" oninput="checkLength(this,24);"><span class="red">(24文字まで)</span>(全角・半角英数)
	</td>
	</tr>
	<tr>
	<td>学年<input type="text" value="({$form.graduation_year})" name="graduation_year" size="2"  maxlength="2"/>年(半角数字)</td>
	</tr>
	<tr>
	</tr>
	</tbody></table>
	</div>
	</td>
</tr>

<tr>
	<td class="mail_cap"><div class="left">特  技<span class="red">(必須)(20文字まで)</span></div></td>
	<td><div class="right">
		<input type="text" value="({$form.speciality})" style="width: 300px;" name="speciality" maxlength="20" onpropertychange="checkLength(this,20);" oninput="checkLength(this,20);">(全角・半角英数)
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap"><div class="left">趣  味<span class="red">(必須)(20文字まで)</span></div></td>
	<td><div class="right">
		<input type="text" value="({$form.interest})" style="width: 300px;" name="interest" maxlength="20" onpropertychange="checkLength(this,20);" oninput="checkLength(this,20);">(全角・半角英数)
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap"><div class="left">自己PR<span class="red">(必須)(150文字まで)</span><br/>志望動機、芸能界での目標を必ずご記入ください。<br/>※改行も１文字にカウントします。</div></td>
	<td><div class="right">
		<textarea style="width: 380px;" rows="6" name="owner_PR" maxlength="150" onpropertychange="checkLength(this,150);" oninput="checkLength(this,150);">({$form.owner_PR})</textarea>
		</div>
	</td>
</tr>

<tr>
	<td class="mail_cap">家族構成</td>
	<td>
		<table >
		<tr><th><div style="text-align:center;">氏名<span class="red">(17文字まで)</span></div></th><th style="text-align:center;">続柄</th></tr>
		<tr><td><input type="text" name="family_f" maxlength="17" value="({$form.family_f})" style="width: 220px;" onpropertychange="checkLength(this,17);" oninput="checkLength(this,17);" /></td><td><input type="text" name="family_f_name" value="({$form.family_f_name})" /></td></tr>
		<tr><td><input type="text" name="family_m" maxlength="17" value="({$form.family_m})" style="width: 220px;"  onpropertychange="checkLength(this,17);" oninput="checkLength(this,17);"/></td><td><input type="text" name="family_m_name" value="({$form.family_m_name})"</td></tr>
		<tr><td><input type="text" name="family_b" maxlength="17" value="({$form.family_b})" style="width: 220px;"  onpropertychange="checkLength(this,17);" oninput="checkLength(this,17);"/></td><td><input type="text" name="family_b_name" value="({$form.family_b_name})" /></td></tr>
		<tr><td><input type="text" name="family_yb" maxlength="17" value="({$form.family_yb})" style="width: 220px;"  onpropertychange="checkLength(this,17);" oninput="checkLength(this,17);"/></td><td><input type="text" name="family_yb_name" value="({$form.family_yb_name})" /></td></tr>
		</table>
	</td>
	
</tr>
<tr>
	<td class="mail_cap"><div class="left">将来の希望<span class="red">(必須)</span><br/>
	(１つ選択してください)<br/>
	</div></td>
	<td>
		<div class="right" >	
		
		({foreach from=$department key=key item=item})
		<input type="radio" name="wish_department" value="({$item})" ({if $item==$form.wish_department}) checked ({/if}) >({$item}) <br />
		({/foreach})		
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap"><div class="left">サイズ<span class="red">(必須)</span></div></td>
	<td>
		<div class="right">
		<table>
		<tr>
			<td>身長</td><td>バスト</td><td>ウェスト</td><td>ヒップ</td><td>シューズ</td>
		</tr>
		<tr>
			<td><input type="text" name="body_1" value="({$form.body_1})" style="width:50px;" maxlength="5" />cm</td><td><input type="text" name="body_2" value="({$form.body_2})" style="width:50px;" maxlength="5" />cm</td><td><input type="text" name="body_3" value="({$form.body_3})" style="width:50px;" maxlength="5" />cm</td><td><input type="text" name="body_4" value="({$form.body_4})" style="width:50px;" maxlength="5" />cm</td><td><input type="text" name="body_5" value="({$form.body_5})" style="width:50px;" maxlength="5" />cm  (半角数字)</td>
		</tr>
		</table>
		</div>
	</td>
</tr>
({if $is_iphone==false})
<tr>
	<td class="mail_cap"><div class="left">写真アップロード<span class="red">(必須)</span><br/>
	10MB以内のJPEG形式。
	</div></td>
	<td>
		<table>
			<tr><td>写真1　(アップ)</td><td><input type="file" name="photo_filename_1" value="" /></td></tr>	
			<tr><td>写真2　(全身)</td><td><input type="file" name="photo_filename_2" value="" /></td></tr>
			<tr><td colspan="2">※最近6ヶ月以内に撮影したものを送付ください。<br/>
			※撮影イメージは下記の応募写真イメージ画像をご覧ください。<br/>
			<span class="red">※写真は縦長で頭が上方向の写真を送付ください。</span><br/>
			横撮影のカメラを立てて撮影すると、見た目は縦長写真ですが写真データは横向きの場合があります。<u>写真確認の際に頭が上になるよう回転し保存してから送付をお願いします。</u><br/>
			※プリクラや撮影後加工した写真は不可です。</td></tr>
		</table>
	</td>
</tr>
({/if})
<tr>
	<td class="mail_cap">
	<div class="left">be amie登録情報<span class="red">(必須)</span><br/>be amieご登録のメールアドレスをご記入ください。</div>
	</td>
	<td>
	<div class="right">
	<table>
	<tbody><tr>
	<td nowrap="">メールアドレス</td><td><input type="text" value="({$form.email_login})" style="width: 240px;" name="email_login">(半角英数)</td>
	</tr>
	<tr>
	<td nowrap="">メールアドレス確認</td><td><input type="text" value="({$form.email_login_check})" style="width: 240px;" name="email_login_check">(半角英数)</td>
	</tr>
	<tr>
	<td>ニックネーム</td><td><input type="text" name="login_nickname" value="({$form.login_nickname})" />(全角・半角英数)</td>
	</tr>
	</tbody></table>
	</div>
	</td>
</tr>
<tr><td colspan="2" >
<span class="red">※未成年の方は保護者の同意が必要です。</span><br/>
※面接に進まれた方は別途保護者の同意書をご用意いただきます。<br/>
</td></tr>

<tr>
	<td class="mail_cap"><div class="left">保護者氏名</div></td>
	<td><div class="right">
		<table>
		<tbody><tr>
		<td>姓</td><td><input type="text" value="({$form.r_name_1})" style="width: 100px;" name="r_name_1">(全角)</td>
		<td>名</td><td><input type="text" value="({$form.r_name_2})" style="width: 100px;" name="r_name_2">(全角)</td>
		</tr>
		<tr>
		<td>セイ</td><td><input type="text" value="({$form.r_kana_name_1})" style="width: 100px;" name="r_kana_name_1">(全角カナ)</td>
		<td>メイ</td><td><input type="text" value="({$form.r_kana_name_2})" style="width: 100px;" name="r_kana_name_2">(全角カナ)</td>
		</tr>
		</tbody></table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">保護者住所</div>
	</td>
	<td>
		<div class="right">
		<table>
		<tbody><tr>
		<td nowrap="">郵便番号</td><td><input type="text" maxlength="7" value="({$form.r_zip})" style="width: 100px;" name="r_zip">(ハイフン不要・半角数字)</td>
		</tr>
		<tr>
		<td nowrap="">都道府県</td><td>
		<select class="gray" name="r_address_pref">
		<option value="">&#8213; 選択してください &#8213;</option>
		({foreach from=$pref item=item})
		<option value="({$item})"  ({if $item==$form.r_address_pref}) selected ({/if}) >({$item})</option>		
		({/foreach})
		</select>
		</td>
		</tr>
		<tr>
		<td nowrap="">市区郡～番地</td>
		<td><input type="text" value="({$form.r_address_1})" style="width: 225px;" name="r_address_1">(全角・半角英数)</td>
		</tr>
		<tr>
		<td nowrap="">建物名・号室</td>
		<td><input type="text" value="({$form.r_address_2})" style="width: 225px;" name="r_address_2">(全角・半角英数)</td>
		</tr>
		</tbody></table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">保護者連絡先</div>
	</td>
	<td>
	<div class="right">
	<table>
	<tbody><tr>
	<td>電話番号</td><td><span style="padding-left:30px;"></span><input type="text" value="({$form.r_tel_1})" style="width: 40px;" name="r_tel_1"> - <input type="text" value="({$form.r_tel_2})" style="width: 60px;" name="r_tel_2"> - <input type="text" value="({$form.r_tel_3})" style="width: 60px;" name="r_tel_3">(半角数字)</td>
	</tr>	
	</tbody></table>
	</div>
	</td>
</tr>
<tr>
	<td class="mail_cap"><div class="left">保護者の同意</div></td>
	<td>
		<input type="checkbox" name="allow" value="1" id="allow" ({if $form.allow=='1'})checked({/if})> <label for="allow" >同意する</label>
	</td>
</tr>


<tr>
	<td class="mail_cap">
	<div class="left">本人との関係　(全角・半角英数)</div>
	</td>
	<td>
	本人との関係<span style="padding-left:20px;"></span><input type="text" value="({$form.r_relationship })" name="r_relationship" style="width:200px;">(全角)
	
	</td>
</tr>
<tr>
<td colspan="2">※アップロードいただく写真の撮影イメージ<br/>
<div style="width:100%; text-align:center;padding-top:10px;padding-bottom:10px;">
<img src="modules/pc/img/photoimg.gif" />
</div>
<span class="red">※写真は縦長で頭が上方向の写真を送付ください。</span><br/>
横撮影のカメラを立てて撮影すると、見た目は縦長写真ですが写真データは横向きの場合があります。<u>写真確認の際に頭が上になるよう回転し保存してから送付をお願いします。</u><br/>
※プリクラや撮影後加工した写真は不可です。<br/><br/>
<span class="red">※スマートフォンをご利用の方へ</span><br/>
スマートフォンをご利用の場合、このページでは写真をアップロードしません。このまま先に進み画面の案内に従いメールで送信をお願いします。<br/>
</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">アンケートのお願い</div>
	</td>
	<td>
	  今回の国民的美少女コンテストを何で知りましたか？<br/>
      <input type="radio" name="enquete" id="enqsel1" value="テレビ" ({if "テレビ"==$form.enquete}) checked ({/if})/><label>1.テレビ</label><br />
      <input type="radio" name="enquete" id="enqsel2" value="ラジオ" ({if "ラジオ"==$form.enquete}) checked ({/if})/><label>2.ラジオ</label><br />
      <input type="radio" name="enquete" id="enqsel3" value="新聞" ({if "新聞"==$form.enquete}) checked ({/if})/><label>3.新聞</label>　(<input type="text" name="enqtext3" value="({$form.enqtext3})" onchange="AutoCheck('enqsel3');" />)<br />
      <input type="radio" name="enquete" id="enqsel4" value="雑誌" ({if "雑誌"==$form.enquete}) checked ({/if})/><label>4.雑誌</label>　(<input type="text" name="enqtext4" value="({$form.enqtext4})" onchange="AutoCheck('enqsel4');" />)<br />
      <input type="radio" name="enquete" id="enqsel5" value="インターネット" ({if "インターネット"==$form.enquete}) checked ({/if})/><label>5.インターネット</label><br />
      <input type="radio" name="enquete" id="enqsel6" value="携帯電話サイト" ({if "携帯電話サイト"==$form.enquete}) checked ({/if})/><label>6.携帯電話サイト</label><br />
      <input type="radio" name="enquete" id="enqsel7" value="その他" ({if "その他"==$form.enquete}) checked ({/if})/><label>7.その他</label>　(<input type="text" name="enqtext7" value="({$form.enqtext7})" onchange="AutoCheck('enqsel7');" />)<br />
    </td>
</tr>

</table>

<div class="explain4">
	<div class="privacybox">
	({$privacy_policy.body|smarty:nodefaults})
	</div>
</div>
<div style="margin:0 0 0 0;" >
<img src="modules/pc/img/img2.gif" />
</div>
<div class="explain4">
	<div class="privacybox" style="font-size: 15px;" >
・一次審査(書類審査)を通過された方に限り、二次の面接審査を全日本国民的美少女コンテスト実行委員会より通知いたします。<br/>
・お送り頂いた個人情報は、オーディションにのみ使用し、オーディション終了後は、他の目的で使用することはありません。<br/>
・審査状況・合否に関わるお問い合わせはお答えいたしかねます。<br/>
・<span class="red">be amie内でオスカープロモーション公認スカウト以外の悪質なスカウト行為には充分ご注意ください！！<br/>
全日本国民的美少女コンテスト実行委員会より皆様宛に直接メッセージをお送りすることは絶対にございません。</span><br/>
もしbeamieを通じて不信なメッセージが届いた場合は必ずご家族にご相談いただき、下記までご連絡をお願いいたします。<br/>
全日本国民的美少女コンテスト実行委員会<br/>
03-6833-0650（平日11:00～17:00pm受付）<br/>
	</div>
</div>
<div class="bt">
	<input type="checkbox" name="consent" value="1" id="consent" ({if $form.consent=='1'})checked({/if})><label for="consent" ><span class="red">プライバシーポリシーおよび注意事項に同意します。</span></label><br/><br/>
	<input type="image" src="./skin/default/img/new_img/org_mail_button.jpg" name="submit" value="確認画面へ" />
</div>



</div>
({/t_form_block})
</div>
<script type="text/javascript"><!--
function checkLength(obj,maxlength){
    if(obj.value.length > maxlength){
        obj.value = obj.value.substring(0,maxlength);
    }
}
function AutoCheck(checkname) {
    document.getElementById(checkname).checked = true;
}
// --></script> 
<!-- MAIN END -->