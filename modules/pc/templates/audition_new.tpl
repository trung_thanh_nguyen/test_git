<!-- MAIN BEGIN -->
<div id="mainer">
({if $is_iphone==false})
({assign var="enctype" value="file"})
({else})
({assign var="enctype" value=""})
({/if})
({if $is_iphone == true })
<div style="text-align:center;">
<img src="modules/pc/img/form_header.jpg" />
</div>
<div style="text-align:center;">
<br>
申し訳ございませんがスマートフォンでは応募いただけません。</br>
パソコンからの応募をお願いいたします。
</div>
({else})
<div style="text-align:center;">
<img src="modules/pc/img/form_header.jpg" />
</div>
({t_form_block _enctype=$enctype m=pc a=page_audition_new_confirm})
	<div id="mainnertop"></div>
    <div id="main2"  class="page_talent_mail" >

({if $errors})    
<div class="dparts alertBox"><div class="parts">
<table width="100%"><tr>
<td align="center">
<div  style="margin-left:160px;padding:20px;color:#FF0000">
<ul>
({foreach from=$errors item=error})
<li style="color: #ff0000;">({$error})</li>
({/foreach})
</ul>
</div>
</td>
</tr></table>
</div></div>
({/if})

<div style="text-align:center;">
<img src="modules/pc/img/audition.gif" />
</div>

<div class='explain1' style="text-align: right;">
<span class="red">（必須）</span>と書かれている項目は必ずご記入をお願いします。
</div>
<table>
<tr>
	<td class="mail_cap2"><div class="left">ご希望応募地区<span class="red">（必須）</span><br/></div></td>
	<td>
		<div class="right" >			
		<select name="wish_addr">
		<option value="">選択してください</option>
		({foreach from=$wish_addrs key=key item=item})
		<option value="({$item})" ({if $item==$form.wish_addr}) selected ({/if}) >({$item})</option>
		({/foreach})
		</select>
		<div class="right" style="padding-top:3px;">
		※応募は居住地域をお選びください。ただし山梨県在住の方は関東地区となります。
		</div>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap2" width="200px;"><div class="left">氏名<span class="red">（必須）</span></div></td>
	<td><div class="right">
		<table>
		<tbody><tr>
		<td>姓</td><td><input type="text" value="({$form.name_1})" style="width: 100px;" name="name_1"></td>
		<td>名</td><td><input type="text" value="({$form.name_2})" style="width: 100px;" name="name_2">（全角）</td>
		</tr>
		<tr>
		<td>セイ</td><td><input type="text" value="({$form.kananame_1})" style="width: 100px;" name="kananame_1"></td>
		<td>メイ</td><td><input type="text" value="({$form.kananame_2})" style="width: 100px;" name="kananame_2">（全角カナ）</td>
		</tr>
		</tbody></table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap2"><div class="left">生年月日<span class="red">（必須）</span></div></td>
	<td>
	<select name="birthday_year" style="">
    <option value="" >--</option>
    ({foreach from=$years item=item })
    <option  value="({$item})" ({if $form.birthday_year==$item }) selected="selected" ({/if}) >({$item})</option>
    ({/foreach})
    </select>
       &nbsp;年
    <select name="birthday_month" style="">
    <option value="" >--</option>
    ({foreach from=$months item=item })
    <option  value="({$item})" ({if $form.birthday_month==$item }) selected="selected" ({/if}) >({$item})</option>
    ({/foreach})
    </select>
    &nbsp;月
    <select name="birthday_day" style="">
    <option value="" >--</option>
    ({foreach from=$days item=item })
    <option  value="({$item})" ({if $form.birthday_day==$item }) selected="selected" ({/if}) >({$item})</option>
    ({/foreach})
    </select>&nbsp;日 （満<input style="width:50px;" name="age" type="text" value="({$form.age})" /> 歳）（半角英数）
  </td>

</tr>
<tr>
	<td class="mail_cap2">
	<div class="left">現住所<span class="red">（必須）</span><p>建物名、号室は<br>必須入力項目ではありません。</p></div>
	</td>
	<td>
		<div class="right">
		<table>
		<tbody><tr>
		<td nowrap="">郵便番号</td><td><input type="text" maxlength="7" value="({$form.zip})" style="width: 100px;" name="zip">（ハイフン不要・半角数字）</td>
		</tr>
		<tr>
		<td nowrap="">都道府県</td><td>
		<select class="gray" name="address_pref">
		<option value="">&#8213; 選択してください &#8213;</option>
		({foreach from=$pref item=item})
		<option value="({$item})"  ({if $item==$form.address_pref}) selected ({/if}) >({$item})</option>		
		({/foreach})
		</select>
		</td>
		</tr>
		<tr>
		<td nowrap="">市区町村・番地</td>
		<td><input type="text" value="({$form.address_1})" style="width: 225px;" name="address_1">（全角・半角英数）</td>
		</tr>
		<tr>
		<td nowrap="">建物名・号室</td>
		<td><input type="text" value="({$form.address_2})" style="width: 225px;" name="address_2">（全角・半角英数）</td>
		</tr>
		</tbody></table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap2">
	<div class="left">ご連絡先の電話番号<span class="red">（必須）</span><p>携帯電話番号は必須入力項目ではありません。</p></div>
	</td>
	<td>
	<div class="right">
	<table>
	<tbody><tr>
	<td >自宅電話番号</td><td><input type="text" value="({$form.tel_1})" style="width: 40px;" name="tel_1"> - <input type="text" value="({$form.tel_2})" style="width: 60px;" name="tel_2"> - <input type="text" value="({$form.tel_3})" style="width: 60px;" name="tel_3">（半角数字）</td>
	</tr>
	<tr>
	<td >携帯電話番号</td><td><input type="text" value="({$form.mobile_tel_1})" style="width: 40px;" name="mobile_tel_1"> - <input type="text" value="({$form.mobile_tel_2})" style="width: 60px;" name="mobile_tel_2"> - <input type="text" value="({$form.mobile_tel_3})" style="width: 60px;" name="mobile_tel_3">（半角数字）</td>
	</tr>
	</tbody></table>
	</div>
	</td>
</tr>
<tr>
	<td class="mail_cap2">
	<div class="left">
	メールアドレス<span class="red">（必須）</span>
	</div>
	</td>
	<td>
	<div class="right">
	<table>
	<tbody><tr>
	<td >メールアドレス</td><td><input type="text" value="({$form.email})" style="width: 240px;" name="email">（半角英数）</td>
	</tr>
	<tr>
	<td >メールアドレス確認</td><td><input type="text" value="({$form.email_check})" style="width: 240px;" name="email_check">（半角英数）</td>
	</tr>
	</tbody></table>
	</div>
	</td>
</tr>
<tr>
	<td class="mail_cap2">
	<div class="left">最終学歴<span class="red">（必須）</span></div>
	</td>
	<td>
	<div class="right">
	<table>
	<tbody><tr>
	<td>
	<input type="text" value="({$form.education})" style="width: 300px;" name="education">（学校） （全角・半角英数）
	</td>
	</tr>
	<tr>
	<td>
	<input type="radio" name="in_school" value="1"  ({if $form.in_school=='1'})checked({/if})> 在学
	<span style="padding-left: 20px;"></span><input type="radio" name="in_school" value="3"  ({if $form.in_school=='3'})checked({/if})> 中退
	<span style="padding-left: 20px;"></span>  <input type="radio" name="in_school" value="2" ({if $form.in_school=='2'})checked({/if})> 卒業	
	<span style="padding-left: 10px;"></span> 西暦 <input style="width:50px;" type="text" name="graduation_year"  value="({$form.graduation_year})" > 年 （半角英数） 
	</td>
	</tr>
	</tbody></table>
	</div>
	</td>
</tr>

<tr>
	<td class="mail_cap2"><div class="left">現在の職業<span class="red">（必須）</span></div></td>
	<td><div class="right">
		<input type="text" value="({$form.job})" style="width: 300px;" name="job">（全角・半角英数）
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap2"><div class="left">特  技<span class="red">（必須）</span></div></td>
	<td><div class="right">
		<input type="text" value="({$form.speciality})" style="width: 300px;" name="speciality" 　maxlength="500">（全角・半角英数）
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap2"><div class="left">趣  味<span class="red">（必須）</span></div></td>
	<td><div class="right">
		<input type="text" value="({$form.interest})" style="width: 300px;" name="interest" 　maxlength="500">（全角・半角英数）
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap2"><div class="left">自己PR<span class="red">（必須）（全角450文字まで）</span><br/>志望動機、芸能界での目標を必ずご記入ください。</div></td>
	<td><div class="right">
		<textarea style="width: 380px;" rows="12" name="owner_PR"  >({$form.owner_PR})</textarea>
		</div>
	</td>
</tr>

<tr>
	<td class="mail_cap2"><div class="left">サイズ<span class="red">（必須）</span></div></td>
	<td>
		<div class="right">
		<table>
		<tr>
			<td>身長</td><td>バスト</td><td>ウェスト</td><td>ヒップ</td><td>靴</td>
		</tr>
		<tr>
			<td><input type="text" name="body_1" value="({$form.body_1})" style="width:50px;" />cm</td><td><input type="text" name="body_2" value="({$form.body_2})" style="width:50px;" />cm</td><td><input type="text" name="body_3" value="({$form.body_3})" style="width:50px;" />cm</td><td><input type="text" name="body_4" value="({$form.body_4})" style="width:50px;" />cm</td><td><input type="text" name="body_5" value="({$form.body_5})" style="width:50px;"  />cm  （半角英数）</td>
		</tr>
		</table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap2"><div class="left">写真アップロード<span class="red">（必須）</span><br/>
	5MB以内のJPEG形式。
	</div></td>
	<td>
		<table>
			<tr><td>写真1　（アップ）</td><td><input type="file" name="photo_filename_1" value="" /></td></tr>	
			<tr><td>写真2　（全身）</td><td><input type="file" name="photo_filename_2" value="" /></td></tr>	
			<tr><td colspan="2">※撮影イメージは下記の応募写真イメージ画像をご覧ください。
		</table>
	</td>
</tr>
<tr>
	<td class="mail_cap2"><div class="left">現在、他の芸能プロダクションに所属していますか？<span class="red">（必須）</span></div></td>
	<td>
	<table>
	<tr><td><input type="radio" name="production_radio" value="1" ({if $form.production_radio==1})checked({/if})>はい（「はい」の場合は以下もご記入ください）
			<input type="radio" name="production_radio" value="2" ({if $form.production_radio==2})checked({/if})>いいえ</td></tr>
	<tr><td>所属事務所名<input type="text" name="production" value="({$form.production})" />（全角・半角英数）</td></tr>
	<tr><td>契約期間<input type="text" name="contact_period" value="({$form.contact_period})" />（全角・半角英数）</td></tr>	
	</table>
	</td>
</tr>
<tr>
	<td class="mail_cap2"><div class="left">今までにオーディションを受けたことがありますか？<span class="red">（必須）</span></div></td>
	<td>
	<table>
	<tr><td>
		<input type="radio" name="audition_radio" value="1" ({if $form.audition_radio=="1"})checked({/if})>はい（「はい」の場合は以下もご記入ください）
		<input type="radio" name="audition_radio" value="2" ({if $form.audition_radio=="2"})checked({/if})>いいえ
	</td></tr>
	<tr><td>オーディション名<input type="text" name="audition_name" value="({$form.audition_name})">（全角・半角英数）</td></tr>
	<tr><td>結果<input type="text" name="audition_result" value="({$form.audition_result})">（全角・半角英数）</td></tr>	
	</table>
	</td>
</tr>
<tr>
	<td class="mail_cap2"><div class="left">その他、経験 <span class="red">（全角450文字まで）</span></div></td>
	<td>
	<textarea rows="8" style="width: 380px;" name="other_experience" >({$form.other_experience})</textarea>
	</td>
</tr>
<tr>
	<td class="mail_cap2">
	<div class="left">be amie登録情報<span class="red">（必須）</span><br/>be amieご登録のメールアドレスをご記入ください。</div>
	</td>
	<td>
	<div class="right">
	<table>
	<tbody><tr>
	<td nowrap="">メールアドレス</td><td><input type="text" value="({$form.email_login})" style="width: 240px;" name="email_login">（半角英数）</td>
	</tr>
	<tr>
	<td nowrap="">メールアドレス確認</td><td><input type="text" value="({$form.email_login_check})" style="width: 240px;" name="email_login_check">（半角英数）</td>
	</tr>
	<tr>
	<td>ニックネーム</td><td><input type="text" name="login_nickname" value="({$form.login_nickname})" />（全角・半角英数）</td>
	</tr>
	</tbody></table>
	</div>
	</td>
</tr>
<tr><td colspan="2" >※保護者同意欄（未成年の方のみ）</td></tr>
<tr>
	<td class="mail_cap"><div class="left">保護者氏名</div></td>
	<td><div class="right">
		<table>
		<tbody><tr>
		<td>姓</td><td><input type="text" value="({$form.r_name_1})" style="width: 100px;" name="r_name_1">（全角）</td>
		<td>名</td><td><input type="text" value="({$form.r_name_2})" style="width: 100px;" name="r_name_2">（全角）</td>
		</tr>
		<tr>
		<td>セイ</td><td><input type="text" value="({$form.r_kana_name_1})" style="width: 100px;" name="r_kana_name_1">（全角カナ）</td>
		<td>メイ</td><td><input type="text" value="({$form.r_kana_name_2})" style="width: 100px;" name="r_kana_name_2">（全角カナ）</td>
		</tr>
		</tbody></table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">保護者ご住所</div>
	</td>
	<td>
		<div class="right">
		<table>
		<tbody><tr>
		<td nowrap="">郵便番号</td><td><input type="text" maxlength="7" value="({$form.r_zip})" style="width: 100px;" name="r_zip">（ハイフン不要・半角数字）</td>
		</tr>
		<tr>
		<td nowrap="">都道府県</td><td>
		<select class="gray" name="r_address_pref">
		<option value="">&#8213; 選択してください &#8213;</option>
		({foreach from=$pref item=item})
		<option value="({$item})"  ({if $item==$form.r_address_pref}) selected ({/if}) >({$item})</option>		
		({/foreach})
		</select>
		</td>
		</tr>
		<tr>
		<td nowrap="">市区町村・番地</td>
		<td><input type="text" value="({$form.r_address_1})" style="width: 225px;" name="r_address_1">（全角・半角英数）</td>
		</tr>
		<tr>
		<td nowrap="">建物名・号室</td>
		<td><input type="text" value="({$form.r_address_2})" style="width: 225px;" name="r_address_2">（全角・半角英数）</td>
		</tr>
		</tbody></table>
		</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">保護者連絡先</div>
	</td>
	<td>
	<div class="right">
	<table>
	<tbody><tr>
	<td>電話番号</td><td><span style="padding-left:30px;"></span><input type="text" value="({$form.r_tel_1})" style="width: 40px;" name="r_tel_1"> - <input type="text" value="({$form.r_tel_2})" style="width: 60px;" name="r_tel_2"> - <input type="text" value="({$form.r_tel_3})" style="width: 60px;" name="r_tel_3">（半角数字）</td>
	</tr>	
	</tbody></table>
	</div>
	</td>
</tr>
<tr>
	<td class="mail_cap">
	<div class="left">応募者とのご関係</div>
	</td>
	<td>
	本人との関係<span style="padding-left:20px;"></span><input type="text" value="({$form.r_relationship })" name="r_relationship" style="width:200px;">（全角・半角英数）
	
	</td>
</tr>
<tr>
<td colspan="2">※アップロードいただく写真の撮影イメージ<br/>
<div style="width:100%; text-align:center;padding-top:10px;padding-bottom:10px;">
<img src="modules/pc/img/img.gif" />
</div>
</td>
</tr>

</table>

<div class="explain4">
	<div class="privacybox">
	({$privacy_policy.body|smarty:nodefaults})
	</div>
</div>
<div style="margin:0 0 0 0;" >
<img src="modules/pc/img/img2.gif" />
</div>
<div class="explain4">
	<div class="privacybox2" style="font-size: 15px;" >
①ご応募にはbe amie会員登録(無料)をしていただく必要があります。<br/>
②書類選考合格者のみにガールズオーディション2011実行委員会から通知をさせていただきます。<br/>
③お電話やメールによる審査結果のお問合わせに関しては一切応じかねます。<br/>
④ブログやＳＮＳの日記等で極力、応募されたことを告知しないようにお願いします。悪質なスカウト等から連絡がくる場合がございます。<br/>
	</div>
</div>
<div class="bt">
	<input type="image" src="./skin/default/img/new_img/org_mail_button.jpg" name="submit" value="確認画面へ" />
</div>



</div>
({/t_form_block})
({/if})
</div>
<!-- MAIN END -->