<div id="LayoutC">
<div id="Center">

({* {{{ commentList *})
<div class="dparts commentList"><div class="parts">
<div class="partsHeading"><h3>下記のコメントを削除しますか？</h3></div>


<dl>
<dt>({$target_news_comment.r_datetime|date_format:"%Y年%m月%d日<br />%H:%M"})</dt>
<dd>
<div class="title">
<p class="heading"> <a href="({t_url m=pc a=page_f_home})&amp;target_c_member_id=({$target_news_comment.c_member_id})">({$target_news_comment.c_member.nickname|default:""})</a></p>
</div>
<div class="body">
<p class="text">
({$target_news_comment.body|smarty:nodefaults|nl2br|default:"&nbsp;"|t_url2cmd:'diary':$target_diary_comment.c_member_id|t_cmd:'diary'})
</p>
</div>
</dd>
</dl>


<div class="operation">
<ul class="moreInfo button">
<li>
({t_form_block m=pc a=do_news_comment_delete})
<input type="hidden" name="target_c_comment_id" value="({$target_news_comment.t_e2_news_comment_id})" />
<input type="submit" class="input_submit" value="　は　い　" />
({/t_form_block})
</li><li>
({t_form_block _method=get m=pc a=page_newsdetail})
<input type="hidden" name="newsid" value="({$target_news_comment.t_e2_news_contens_id})" />
<input type="submit" class="input_submit" value="　いいえ　" />
({/t_form_block})
</li>
</ul>
</div>

</div></div>
({* }}} *})

</div><!-- Center -->
</div><!-- LayoutC -->
