
				
				<h1 class="headerLined">be amieニュース</h1>
				
				<ul class="tab">
					({foreach from=$newtypenames item=item key=key name=newstypename })
					({if $key eq $nownewtypenames})
					<li><a href="" class="on">({$item})</a></li>
					({else})
					<li><a href="({t_url m=pc a=page_newslist})&amp;type=({$key})&amp;day=({$day})">({$item})</a></li>
					({/if})
					({/foreach})
				</ul>
				<section class="boxBase">
				
				<a href="({t_url m=pc a=page_newslist})&amp;type=({$news.t_e2_news_genre})">({$newstypename_ja})</a> &gt; ({$news.t_e2_news_title})

				<article>
				<header class="header-article">
					({if $news.t_e2_news_genre eq 'R-25'})
					<div class="r25news">
					  <h1>({$news.t_e2_news_title})</h1>
					</div>
					({else})
					<div class="kyodonews">
					  <h1>({$news.t_e2_news_title})</h1>
					</div>
					({/if})
					<span>({$news.t_e2_news_public_dateandtime|date_format:"%Y年%m月%d日  %H時%M分"}) 提供元： ({if $news.t_e2_news_genre eq 'R-25'})<a href="http://r25.yahoo.co.jp/" target="_blank">R-25</a>({else})<a href="http://www.kyodo.co.jp/" target="_blank">共同通信</a>({/if})</span>
					<ul class="btnSocial">
						<li>
							<a href="http://mixi.jp/share.pl" class="mixi-check-button" data-key="f6226353980fab0f840415617ede900bb0f8d2fa">mixiチェック</a>
							<script type="text/javascript" src="http://static.mixi.jp/js/share.js"></script>
						</li>
						<li>
							<iframe frameborder="0" scrolling="no" allowtransparency="true" style="border:none; overflow:hidden; width:120px; height:21px;" src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fdev.beamie.jp%2F%3Fm%3Dpc%26amp%3Ba%3Dpage_newsdetail%26amp%3Bnewsid%3D142701&amp;send=false&amp;layout=button_count&amp;width=120&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21"></iframe>
						</li>
						<li>
							<a class="twitter-share-button" href="http://twitter.com/share/">Tweet</a>
							<script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>
						</li>
					</ul>
				</header>


				<div class="news_txt">
					({if $pic_flg})
						<div class="photoarea">
						({if $t_e2_news_main_filename})
							<div class="photo_small" style="float:right;width:300px">
								<img alt="" width="300" src="({$t_e2_news_main_filename})">
								<p style="color:#666;font-size:12px;">
									({$t_e2_news_caption|smarty:nodefaults})
								</p>
							</div>
						({elseif $t_e2_news_thumbnail_filename})
							<div class="photo_small"><img alt="" src="({$t_e2_news_thumbnail_filename})"></div>
							<!--<p class="link_tobig"><a href="({$t_e2_news_thumbnail_filename})&amp;newsid=({$newsid})">写真拡大</a></p>-->
						({/if})
						</div>
					({/if})
				<p>({$news.t_e2_news_content|smarty:nodefaults}) </p>
				</div>

				<div class="link_box1"> 
				({if $prev}) <a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$prev.t_e2_news_contents_id})" class="pagingArticle prevArticle left">&laquo;</a>({else})({/if})
				({if $next}) <a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$next.t_e2_news_contents_id})" class="pagingArticle nextArticle right">&raquo;</a> ({else})({/if}) 
				</div>

				</article>

				
				</section>
				
