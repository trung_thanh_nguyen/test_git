
				
				<h1 class="headerLined">be amieニュース</h1>
				
				<ul class="tab">
					({foreach from=$newtypenames item=item key=key name=newstypename })
					({if $key eq $nownewtypenames})
					<li><a href="" class="on">({$item})</a></li>
					({else})
					<li><a href="({t_url m=pc a=page_newslist})&amp;type=({$key})&amp;day=({$nowday})">({$item})</a></li>
					({/if})
					({/foreach})
				</ul>
				
				<article>
				<header class="header-article boxBase">
					({if $typename_ja eq 'R-25'})
					<div class="r25news">
						<h2>({$typename_ja})</h2>
					</div>
					({else})
					<div class="kyodonews">
					  <h2>({$typename_ja})</h2>
					</div>
					({/if})
					<ul class="tit_category_list">
							({foreach from=$newslist item=item name=newslist})
							<li><a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$item.t_e2_news_contents_id})">({$item.t_e2_news_title})</a>
							({if $item.t_e2_news_associated_with})&nbsp; <img src="./skin/default/common/pu_news_cam.jpg" /> ({/if})            
							<span class="date">({$item.t_e2_news_public_dateandtime|date_format:"%m月%d日%H時%M分"}) </span>
							</li>
							({/foreach})
					</ul>
				</header>
				
				
				
				
		        <div class="link_box1">
		        ({foreach from=$days item=day name=daylink})
		        	({if $current_day eq $day})
		        		({$day|date_format:'%m/%d'})
		        	({else})
		        		<a href="({t_url m=pc a=page_newslist})&amp;type=({$type})&amp;day=({$day|date_format:'%Y-%m-%d'})">({$day|date_format:'%m/%d'})</a>
		        	({/if})
		        	({if $smarty.foreach.daylink.last neq 1})|({/if})
		        ({/foreach})
		        </div>