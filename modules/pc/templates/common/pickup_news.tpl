        <!-- NEWS -->             
        <div id="pu_news">
          <div id="pu_news_top">
            <h3>be amie ピックアップニュース</h3>
          </div>
          <div class="pu_news_gbtn"> 
          ({if !$newstype || $newstype == 'Main'})        
           <a id="news_bar1"  onfocus="this.blur();" class="pu_news_gbtn1_active" href="?m=pc&a=page_newslist&type=Main"><span>メイン</span></a>
           ({else})
           <a id="news_bar1"  onfocus="this.blur();" class="pu_news_gbtn1" href="javascript:LiiSideTab('news',1,5);"><span>メイン</span></a>
           ({/if})
           ({if $newstype=='Entertainment_Trends'})
           <a id="news_bar2" onfocus="this.blur();" class="pu_news_gbtn2_active" href="?m=pc&a=page_newslist&type=Entertainment_Trends"><span>エンタメ</span></a>
           ({else})
           <a id="news_bar2" onfocus="this.blur();" class="pu_news_gbtn2" href="javascript:LiiSideTab('news',2,5);"><span>エンタメ</span></a>
           ({/if})
           ({if $newstype=='National'})
            <a id="news_bar3" onfocus="this.blur();" class="pu_news_gbtn3_active" href="?m=pc&a=page_newslist&type=National"><span>社会</span></a>
            ({else})
             <a id="news_bar3" onfocus="this.blur();" class="pu_news_gbtn3" href="javascript:LiiSideTab('news',3,5);"><span>社会</span></a>
            ({/if})
            ({if $newstype=='Sports'})
            <a id="news_bar4" onfocus="this.blur();" class="pu_news_gbtn4_active" href="?m=pc&a=page_newslist&type=Sports"><span>スポーツ</span></a>
            ({else})
            <a id="news_bar4" onfocus="this.blur();" class="pu_news_gbtn4" href="javascript:LiiSideTab('news',4,5);"><span>スポーツ</span></a>
            ({/if})
            ({if $newstype=='R-25' })
            <a id="news_bar5" onfocus="this.blur();" class="pu_news_gbtn5_active" href="?m=pc&a=page_newslist&type=R-25"><span>R-25</span></a>
            ({else})
            <a id="news_bar5" onfocus="this.blur();" class="pu_news_gbtn5" href="javascript:LiiSideTab('news',5,5);"><span>R-25</span></a>
            ({/if})
            <br class="clear" />
          </div>
          <div id="news_box1" ({if $newstype && $newstype!='Main' })style="display:none;"({/if})>
          <ul>
          	({foreach from=$News_Main item=item })
            	<li>
            		<a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$item.t_e2_news_contents_id})">({$item.t_e2_news_title|t_truncate:30:'…'})</a>
            		({if $item.t_e2_news_associated_with})<img src="./skin/default/common/pu_news_cam.jpg" />({/if})
            	</li>
            ({/foreach})
          </ul>
          ({if $News_Main_Picture.t_e2_news_contents_id})
          <div class="pu_news_boxarea">
            <div class="pu_news_boxarea_left"><a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$News_Main_Picture.t_e2_news_contents_id})">
            <img border="0"  alt="" src="({t_img_url filename=$News_Main_Picture.t_e2_news_thumbnail_filename h=86 w=100 noimg=no_image})"></a></div>
            <div class="pu_news_boxarea_right"><a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$News_Main_Picture.t_e2_news_contents_id})">({$News_Main_Picture.t_e2_news_title|t_truncate:50:'…'})</a></div>
            <br class="clear">
          </div>
          ({/if})
          
          <div class="pu_news_listlink"><a href="({t_url m=pc a=page_newslist})&amp;type=Main">ニュース一覧</a></div>
           <div class="pu_news_bottomarea"><a href="({t_url m=pc a=page_newslist})&amp;type=Entertainment_Trends&amp;day=({$current_day})">エンタメ</a>｜<a href="({t_url m=pc a=page_newslist})&amp;type=National&amp;day=({$current_day})">社会</a>｜<a href="({t_url m=pc a=page_newslist})&amp;type=Sports&amp;day=({$current_day})">スポーツ</a>｜<a href="({t_url m=pc a=page_newslist})&amp;type=R-25&amp;day=({$current_day})">R-25</a></div>
          </div>
          
          <div id="news_box2" ({if $newstype!='Entertainment_Trends' })style="display:none;"({/if}) >
          <ul>
           ({foreach from=$News_Entertainment_Trends item=item })
            <li><a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$item.t_e2_news_contents_id})">・({$item.t_e2_news_title|t_truncate:30:'…'})</a>({if $item.t_e2_news_associated_with})<img src="./skin/default/common/pu_news_cam.jpg" />({/if})</li>
            ({/foreach})
          </ul>
          ({if $News_Entertainment_Trends_Picture.t_e2_news_contents_id})
          <div class="pu_news_boxarea">
            <div class="pu_news_boxarea_left"><a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$News_Entertainment_Trends_Picture.t_e2_news_contents_id})">
            <img border="0"  alt="" src="({t_img_url filename=$News_Entertainment_Trends_Picture.t_e2_news_thumbnail_filename h=86 w=100 noimg=no_image})"></a></div>
            <div class="pu_news_boxarea_right"><a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$News_Entertainment_Trends_Picture.t_e2_news_contents_id})">({$News_Entertainment_Trends_Picture.t_e2_news_title|t_truncate:50:'…'})</a></div>
            <br class="clear">
          </div>
          ({/if})
          <div class="pu_news_listlink"><a href="({t_url m=pc a=page_newslist})&amp;type=Entertainment_Trends">ニュース一覧</a></div>
          <div class="pu_news_bottomarea"><a href="({t_url m=pc a=page_newslist})&amp;type=Main&amp;day=({$current_day})">メイン</a>｜<a href="({t_url m=pc a=page_newslist})&amp;type=National&amp;day=({$current_day})">社会</a>｜<a href="({t_url m=pc a=page_newslist})&amp;type=Sports&amp;day=({$current_day})">スポーツ</a>｜<a href="({t_url m=pc a=page_newslist})&amp;type=R-25&amp;day=({$current_day})">R-25</a></div>
          </div>
          
           <div id="news_box3" ({if $newstype!='National' })style="display:none;"({/if}) >
          <ul>
            ({foreach from=$News_National item=item })
            <li><a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$item.t_e2_news_contents_id})">・({$item.t_e2_news_title|t_truncate:30:'…'})</a>({if $item.t_e2_news_associated_with})<img src="./skin/default/common/pu_news_cam.jpg" />({/if})</li>
            ({/foreach})
          </ul>
          ({if $News_National_Picture.t_e2_news_contents_id})
         <div class="pu_news_boxarea">
            <div class="pu_news_boxarea_left"><a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$News_National_Picture.t_e2_news_contents_id})">
            <img border="0"  alt="" src="({t_img_url filename=$News_National_Picture.t_e2_news_thumbnail_filename h=86 w=100 noimg=no_image})"></a></div>
            <div class="pu_news_boxarea_right"><a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$News_National_Picture.t_e2_news_contents_id})">({$News_National_Picture.t_e2_news_title|t_truncate:50:'…'})</a></div>
            <br class="clear">
          </div>
          ({/if})
          <div class="pu_news_listlink"><a href="({t_url m=pc a=page_newslist})&amp;type=National">ニュース一覧</a></div>
          <div class="pu_news_bottomarea"><a href="({t_url m=pc a=page_newslist})&amp;type=Main&amp;day=({$current_day})">メイン</a>｜<a href="({t_url m=pc a=page_newslist})&amp;type=Entertainment_Trends&amp;day=({$current_day})">エンタメ</a>｜<a href="({t_url m=pc a=page_newslist})&amp;type=Sports&amp;day=({$current_day})">スポーツ</a>｜<a href="({t_url m=pc a=page_newslist})&amp;type=R-25&amp;day=({$current_day})">R-25</a></div>
          </div>
          
          <div id="news_box4" ({if $newstype!='Sports' })style="display:none;"({/if})>
          <ul>
           ({foreach from=$News_Sports item=item })
            <li><a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$item.t_e2_news_contents_id})">・({$item.t_e2_news_title|t_truncate:30:'…'})</a>({if $item.t_e2_news_associated_with})<img src="./skin/default/common/pu_news_cam.jpg" />({/if})</li>
            ({/foreach})
          </ul>
          ({if $News_Sports_Picture.t_e2_news_contents_id})
          <div class="pu_news_boxarea">
            <div class="pu_news_boxarea_left"><a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$News_Sports_Picture.t_e2_news_contents_id})">
            <img border="0"  alt="" src="({t_img_url filename=$News_Sports_Picture.t_e2_news_thumbnail_filename h=86 w=100 noimg=no_image})"></a></div>
            <div class="pu_news_boxarea_right"><a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$News_Sports_Picture.t_e2_news_contents_id})">({$News_Sports_Picture.t_e2_news_title|t_truncate:50:'…'})</a></div>
            <br class="clear">
          </div>
          ({/if})
          <div class="pu_news_listlink"><a href="({t_url m=pc a=page_newslist})&amp;type=Sports">ニュース一覧</a></div>
          <div class="pu_news_bottomarea"><a href="({t_url m=pc a=page_newslist})&amp;type=Main&amp;day=({$current_day})">メイン</a>｜<a href="({t_url m=pc a=page_newslist})&amp;type=Entertainment_Trends&amp;day=({$current_day})">エンタメ</a>｜<a href="({t_url m=pc a=page_newslist})&amp;type=National&amp;day=({$current_day})">社会</a>｜<a href="({t_url m=pc a=page_newslist})&amp;type=R-25&amp;day=({$current_day})">R-25</a></div>
          </div>
          
           <div id="news_box5" ({if $newstype!='R-25' })style="display:none;"({/if})>
          <ul>
            ({foreach from=$News_R_25 item=item })
            <li><a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$item.t_e2_news_contents_id})">・({$item.t_e2_news_title|t_truncate:30:'…'})</a>({if $item.t_e2_news_associated_with})<img src="./skin/default/common/pu_news_cam.jpg" />({/if})</li>
            ({/foreach}) 
          </ul>
          ({if $News_R_25_Picture.t_e2_news_contents_id})
          <div class="pu_news_boxarea">
            <div class="pu_news_boxarea_left"><a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$News_R_25_Picture.t_e2_news_contents_id})">
            <img border="0"  alt="" src="({t_img_url filename=$News_R_25_Picture.t_e2_news_thumbnail_filename h=86 w=100 noimg=no_image})"></a></div>
            <div class="pu_news_boxarea_right"><a href="({t_url m=pc a=page_newsdetail})&amp;newsid=({$News_R_25_Picture.t_e2_news_contents_id})">({$News_R_25_Picture.t_e2_news_title|t_truncate:50:'…'})</a></div>
            <br class="clear">
          </div>
          ({/if})
          <div class="pu_news_listlink"><a href="({t_url m=pc a=page_newslist})&amp;type=R-25">ニュース一覧</a></div>
          <div class="pu_news_bottomarea"><a href="({t_url m=pc a=page_newslist})&amp;type=Main&amp;day=({$current_day})">メイン</a>｜<a href="({t_url m=pc a=page_newslist})&amp;type=Entertainment_Trends&amp;day=({$current_day})">エンタメ</a>｜<a href="({t_url m=pc a=page_newslist})&amp;type=National&amp;day=({$current_day})">社会</a>｜<a href="({t_url m=pc a=page_newslist})&amp;type=Sports&amp;day=({$current_day})">スポーツ</a></div>
          </div>          
          <div id="pu_news_bottom"></div>
        </div>
        
        <!-- NEWS END -->