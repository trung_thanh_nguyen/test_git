({$inc_ktai_header|smarty:nodefaults})

({if $inc_ktai_entry_point[1]})
({$inc_ktai_entry_point[1]|smarty:nodefaults})
({/if})


({if $inc_ktai_entry_point[2]})
({$inc_ktai_entry_point[2]|smarty:nodefaults})
({/if})

({foreach from=$top item=data})
    ({ext_include file="inc_portal_Contents_Layout_ktai.tpl"})
({/foreach})
<!-- ▼contents -->
<table width="100%">
<tr>
<td align="center" bgcolor="#FECECE">
<font size="2">
<font color="#E20080"><a name="top">第1回 美ｼﾞﾈｽﾏﾝ&美ｼﾞﾈｽｳｰﾏﾝｺﾝﾃｽﾄ応募ﾌｫｰﾑ</a></font><br>
</font>
</td>
</tr>
({foreach from=$errors item=error})
({if $error})
<tr>
<td>
<font color="red">({$error})</font>
</td>
</tr>
({/if})
({/foreach})
</table>

({if $msg})<p class="actionMsg">({$msg})</p>({/if})

({t_form_block m=ktai a=page_audition_biz_confirm})
<font color="#FF00FF">STEP①　応募情報入力＞＞</font>STEP②　応募情報確認＞＞ STEP③　写真の送信＞＞完了<br/>
「<a href="/?m=ktai&a=page_o_free_page&c_free_page_id=15" target="_blank">ﾌﾟﾗｲﾊﾞｼｰﾎﾟﾘｼｰ</a>」をお読みいただき、必須項目にご記入の上応募をお願いします。<br/><br/>

<input type="hidden" name="ksid" value="({$PHPSESSID})">

■氏名 <font color="#FF0000">(必須)</font><br>
<table>
<tbody>
<tr><td>姓</td><td><input type="text" value="({$form.name_1})" name="name_1" size="12" tabindex="10">(全角)</td></tr>
<tr><td>名</td><td><input type="text" value="({$form.name_2})" name="name_2" size="12" tabindex="20">(全角)</td></tr>
<tr><td>ｾｲ</td><td><input type="text" value="({$form.kananame_1})" size="12" name="kananame_1" tabindex="30">(全角ｶﾅ)</td></tr>
<tr><td>ﾒｲ</td><td><input type="text" value="({$form.kananame_2})" size="12" name="kananame_2" tabindex="40">(全角ｶﾅ)</td></tr>
</tbody>
</table>
<br><br>


■生年月日<font color="#FF0000">(必須)</font><br>
<select name="birthday_year" style="" tabindex="50">
    <option value="" >--</option>
    ({foreach from=$years item=item })
    <option  value="({$item})" ({if $form.birthday_year==$item }) selected="selected" ({/if}) >({$item})</option>
    ({/foreach})
</select>
    -
<select name="birthday_month" style="" tabindex="60">
    <option value="" >--</option>
    ({foreach from=$months item=item })
    <option  value="({$item})" ({if $form.birthday_month==$item }) selected="selected" ({/if}) >({$item})</option>
    ({/foreach})
</select>
    -
<select name="birthday_day" style="" tabindex="70">
    <option value="" >--</option>
    ({foreach from=$days item=item })
    <option  value="({$item})" ({if $form.birthday_day==$item }) selected="selected" ({/if}) >({$item})</option>
    ({/foreach})
</select><br>
年齢
<input name="age" type="text" value="({$form.age})" size=4 istyle="4" mode="numeric" tabindex="80">歳<br><br>

■ご住所<font color="#FF0000">(必須)</font><br>
<font>建物名､号室は<br>必須入力項目ではありません｡</font><br>
<table>
<tbody>
<tr><td width="60px">郵便番号</td><td><input type="text" value="({$form.zip})" size=7 name="zip" istyle="4" mode="numeric" tabindex="90"></td><td>(ﾊｲﾌﾝ不要･半角数字)</td></tr>
<tr><td width="60px">都道府県</td><td colspan="2">
<select class="gray" name="address_pref" tabindex="100">
	<option value="">&#8213; 選択してください &#8213;</option>
	({foreach from=$pref item=item})
	<option value="({$item})"  ({if $item==$form.address_pref}) selected ({/if}) >({$item})</option>		
	({/foreach})
</select></td></tr>
<tr><td colspan="3">市区郡～番地(全角･半角英数)</td></tr>
<tr><td colspan="3"><input type="text" value="({$form.address_1})" name="address_1" tabindex="110"></td></tr>
<tr><td colspan="3">建物名･号室(全角･半角英数)</td></tr>
<tr><td colspan="3"><input type="text" value="({$form.address_2})" name="address_2" tabindex="120"></td></tr>
</tbody>
</table>
<br/>

■ご連絡先の電話番号<font color="red">(必須)</font><br>
電話番号(半角数字)<br>
<input type="text" value="({$form.tel_1})" size=4 name="tel_1" istyle="4" mode="numeric" tabindex="130"> - <input type="text" value="({$form.tel_2})" size=4 name="tel_2" istyle="4" mode="numeric" tabindex="140"> - <input type="text" value="({$form.tel_3})" size=4 name="tel_3" istyle="4" mode="numeric" tabindex="150">
<br><br>

■ﾒｰﾙｱﾄﾞﾚｽ<font color="red">(必須)</font><br>
<br>
ﾒｰﾙｱﾄﾞﾚｽ(半角英数)<br>
<input type="text" value="({$form.email})" name="email" istyle="3" mode="alphabet" tabindex="160"><br>
ﾒｰﾙｱﾄﾞﾚｽ確認(半角英数)<br>
<input type="text" value="({$form.email_check})" name="email_check" istyle="3" mode="alphabet" tabindex="170"><br><br>

■性別<font color="red">(必須)</font><br>
<input type="radio" name="education" value="男性" ({if $form.education == '' || $form.education  == '男性'}) checked="checked" ({/if}) tabindex="180" />男性  <input type="radio" name="education" value="女性" ({if $form.education == '女性'}) checked="checked" ({/if}) tabindex="181" />女性
<br><br>

■職業・学年<font color="red">(必須)(10文字まで)</font><br>
<input type="text" name="graduation_year"  value="({$form.graduation_year})"   tabindex="190"><br/>
<br><br>

■特技<font color="red">(必須)(20文字まで)</font>(全角･半角英数)<br>
<textarea rows="3" name="speciality" tabindex="200">({$form.speciality})</textarea>
<br><br>

■趣味<font color="red">(必須)(20文字まで)</font>(全角･半角英数)<br>
<textarea rows="3" name="interest" tabindex="210">({$form.interest})</textarea>
<br><br>

■自己PR<font color="red">(必須)(150文字まで)</font><br>
志望動機､芸能界での目標を必ずご記入ください｡<br>
<textarea rows="5" name="owner_PR" tabindex="220">({$form.owner_PR})</textarea>
<br><br>

■家族構成 <br>
<table >
<tr><td>氏名<font color="red">(17文字まで)</font></td><td>続柄</td></tr>
<tr><td><input type="text" name="family_f" value="({$form.family_f})" tabindex="230"  /></td><td><input type="text" name="family_f_name" value="({$form.family_f_name})" size="5" tabindex="240"   /></td></tr>
<tr><td><input type="text" name="family_m" value="({$form.family_m})" tabindex="250"   /></td><td><input type="text" name="family_m_name" value="({$form.family_m_name})" size="5" tabindex="260"   /></td></tr>
<tr><td><input type="text" name="family_b" value="({$form.family_b})" tabindex="270"   /></td><td><input type="text" name="family_b_name" value="({$form.family_b_name})" size="5" tabindex="280"  /></td></tr>
<tr><td><input type="text" name="family_yb" value="({$form.family_yb})" tabindex="290"   /></td><td><input type="text" name="family_yb_name" value="({$form.family_yb_name})" size="5" tabindex="300"   /></td></tr>
</table>
<br/>

■将来の希望<font color="red">(必須)</font><br>
({foreach from=$department key=key item=item})
<input type="radio" name="wish_department" value="({$item})" ({if $item==$form.wish_department}) checked ({/if}) tabindex="310">({$item}) <br />
({/foreach})		
<br/>

■ｻｲｽﾞ<font color="red">(必須)</font>(半角数字)<br>
<table>
<tbody>
<tr><td>身長</td><td><input type="text" name="body_1" value="({$form.body_1})" size="5" istyle="4" mode="numeric" maxlength="5" tabindex="320">cm</td></tr>
<tr><td>ﾊﾞｽﾄ</td><td><input type="text" name="body_2" value="({$form.body_2})" size="5" istyle="4" mode="numeric" maxlength="5" tabindex="330">cm</td></tr>
<tr><td>ｳｪｽﾄ</td><td><input type="text" name="body_3" value="({$form.body_3})" size="5" istyle="4" mode="numeric" maxlength="5" tabindex="340">cm</td></tr>
<tr><td>ﾋｯﾌﾟ</td><td><input type="text" name="body_4" value="({$form.body_4})" size="5" istyle="4" mode="numeric" maxlength="5" tabindex="350">cm</td></tr>
<tr><td>ｼｭｰｽﾞ</td><td><input type="text" name="body_5" value="({$form.body_5})" size="5" istyle="4" mode="numeric" maxlength="5" tabindex="360">cm</td></tr>
</tbody>
</table>
<br/>


be amieｱｶｳﾝﾄ<font color="red">(必須)</font><br>
■応募者ご本人のbe amieにご登録のﾒｰﾙｱﾄﾞﾚｽをご記入ください。<br>
ﾒｰﾙｱﾄﾞﾚｽ(半角英数)<br>
<input type="text" value="({$form.email_login})" name="email_login" istyle="3" mode="alphabet" tabindex="370"><br>
ﾒｰﾙｱﾄﾞﾚｽ確認(半角英数)<br>
<input type="text" value="({$form.email_login_check})" name="email_login_check" istyle="3" mode="alphabet" tabindex="380"><br>
ﾆｯｸﾈｰﾑ(全角･半角英数)<br>
<input type="text" name="login_nickname" value="({$form.login_nickname})" tabindex="390"><br>
<br>
<br>
<font color="red">※未成年の方は保護者の同意が必要です。</font><br>
※面接に進まれた方は別途保護者の同意書をご用意いただきます。<br>


■保護者氏名<br>
<table>
<tbody>
<tr><td>姓</td><td><input type="text" value="({$form.r_name_1})" name="r_name_1" size="12" tabindex="400">(全角)</td></tr>
<tr><td>名</td><td><input type="text" value="({$form.r_name_2})" name="r_name_2" size="12" tabindex="410">(全角)</td></tr>
<tr><td>ｾｲ</td><td><input type="text" value="({$form.r_kana_name_1})" size="12" name="r_kana_name_1" tabindex="420">(全角ｶﾅ)</td></tr>
<tr><td>ﾒｲ</td><td><input type="text" value="({$form.r_kana_name_2})" size="12" name="r_kana_name_2" tabindex="430">(全角ｶﾅ)</td></tr>
</tbody>
</table>
<br/>
■保護者住所<br>
<table>
<tbody>
<tr><td width="60px">郵便番号</td><td><input type="text" value="({$form.r_zip})" size=7 name="r_zip" istyle="4" mode="numeric" tabindex="440"></td><td>(ﾊｲﾌﾝ不要･半角数字)</td></tr>
<tr><td width="60px">都道府県</td><td colspan="2">
<select class="gray" name="r_address_pref" tabindex="450">
	<option value="">&#8213; 選択してください &#8213;</option>
	({foreach from=$pref item=item})
	<option value="({$item})"  ({if $item==$form.r_address_pref}) selected ({/if}) >({$item})</option>		
	({/foreach})
</select></td></tr>
<tr><td colspan="3">市区郡～番地(全角･半角英数)</td></tr>
<tr><td colspan="3"><input type="text" value="({$form.r_address_1})" name="r_address_1" tabindex="460"></td></tr>
<tr><td colspan="3">建物名･号室(全角･半角英数)</td></tr>
<tr><td colspan="3"><input type="text" value="({$form.r_address_2})" name="r_address_2" tabindex="470"></td></tr>
</tbody>
</table>
<br/>

■連絡先<br>
保護者連絡先(半角数字)<br>
<input type="text" value="({$form.r_tel_1})" size=4 name="r_tel_1" istyle="4" mode="numeric" tabindex="480"> - <input type="text" value="({$form.r_tel_2})" size=4 name="r_tel_2" istyle="4" mode="numeric" tabindex="490"> - <input type="text" value="({$form.r_tel_3})" size=4 name="r_tel_3" istyle="4" mode="numeric" tabindex="500"><br>
<br>

■保護者の同意<br>
<input type="checkbox" name="allow" value="1" id="allow" ({if $form.allow=='1'})checked({/if}) tabindex="510"> <label for="allow" >同意する</label>
<br><br>

■本人との関係(全角)<br>
<input type="text" value="({$form.r_relationship })" name="r_relationship" tabindex="520">
<br><br>

■ｱﾝｹｰﾄのお願い<br/>
今回の美ｼﾞﾈｽﾏﾝ&美ｼﾞﾈｽｳｰﾏﾝｺﾝﾃｽﾄを何で知りましたか？<br/>
<input type="radio" name="enquete" id="enqsel1" value="テレビ" ({if "テレビ"==$form.enquete}) checked ({/if})/><label>1.ﾃﾚﾋﾞ</label>　
<input type="radio" name="enquete" id="enqsel2" value="ラジオ" ({if "ラジオ"==$form.enquete}) checked ({/if})/><label>2.ﾗｼﾞｵ</label><br />
<input type="radio" name="enquete" id="enqsel3" value="新聞" ({if "新聞"==$form.enquete}) checked ({/if})/><label>3.新聞</label>(<input type="text" name="enqtext3" value="({$form.enqtext3})" onchange="AutoCheck('enqsel3');" />)<br />
<input type="radio" name="enquete" id="enqsel4" value="雑誌" ({if "雑誌"==$form.enquete}) checked ({/if})/><label>4.雑誌</label>(<input type="text" name="enqtext4" value="({$form.enqtext4})" onchange="AutoCheck('enqsel4');" />)<br />
<input type="radio" name="enquete" id="enqsel5" value="インターネット" ({if "インターネット"==$form.enquete}) checked ({/if})/><label>5.ｲﾝﾀｰﾈｯﾄ</label>　
<input type="radio" name="enquete" id="enqsel6" value="携帯電話サイト" ({if "携帯電話サイト"==$form.enquete}) checked ({/if})/><label>6.携帯電話ｻｲﾄ</label><br />
<input type="radio" name="enquete" id="enqsel7" value="その他" ({if "その他"==$form.enquete}) checked ({/if})/><label>7.その他</label>(<input type="text" name="enqtext7" value="({$form.enqtext7})" onchange="AutoCheck('enqsel7');" />)<br />

<table width="100%">
<tr>
<td align="left" colspan="2" bgcolor="#FECECE">
<font color="#E20080">
<font size="2">個人情報の取り扱いについて</font><br>
</font>
</td>
</tr>
<tr>
<td align="left" colspan="2">
<font size="2">｢<a href="({t_url m=ktai a=page_o_free_page})&amp;c_free_page_id=15&amp;({$tail})">ﾌﾟﾗｲﾊﾞｼｰﾎﾟﾘｼｰ</a>｣をお読みいただき､同意の上で確認画面に進んでください｡<br></font>
</td>
</tr>
<tr>
<td align="left" colspan="2" bgcolor="#FECECE">
<font color="#E20080">
<font size="2">注意事項 ※必ずご確認ください｡</font><br>
</font>
</td>
</tr>
<tr>
<td align="left" colspan="2">
<font size="2">
・一次審査(書類審査)を通過された方に限り、二次の面接審査を美ｼﾞﾈｽﾏﾝ&美ｼﾞﾈｽｳｰﾏﾝｺﾝﾃｽﾄ実行委員会事務局より通知いたします。<br/>
・お送り頂いた個人情報は、ｵｰﾃﾞｨｼｮﾝにのみ使用し、ｵｰﾃﾞｨｼｮﾝ終了後は、他の目的で使用することはありません。<br/>
・審査状況・合否に関わるお問い合わせはお答えいたしかねます。<br/>
・<font color="red">be amie内でｵｽｶｰﾌﾟﾛﾓｰｼｮﾝ公認ｽｶｳﾄ以外の悪質なｽｶｳﾄ行為には充分ご注意ください！！<br/>
美ｼﾞﾈｽﾏﾝ&美ｼﾞﾈｽｳｰﾏﾝｺﾝﾃｽﾄ実行委員会事務局より皆様宛に直接ﾒｯｾｰｼﾞをお送りすることは絶対にございません。</font><br/>
もしbe amieを通じて不信なﾒｯｾｰｼﾞが届いた場合は、下記までご連絡をお願いいたします。<br/>
美ｼﾞﾈｽﾏﾝ&美ｼﾞﾈｽｳｰﾏﾝｺﾝﾃｽﾄ実行委員会事務局<br/>
03-6427-2816（平日11:00～17:00受付）<br/>
</font>
</td>
</tr>
</table>

<div align="center">
<input type="checkbox" name="consent" value="1" id="consent" ({if $form.consent=='1'})checked({/if}) tabindex="530"><label for="consent" ><font color="red">ﾌﾟﾗｲﾊﾞｼｰﾎﾟﾘｼｰおよび注意事項に同意します。</font></label><br>
<input type="submit" name="同意の上､確認画面へ" value="同意の上､確認画面へ" accesskey="7" tabindex="540">
</div>
({/t_form_block})
<p align="right" ><a href="#top" tabindex="550">ﾍﾟｰｼﾞTOP</a>&nbsp;</p>


<!-- △contents -->
({foreach from=$bottom item=data}) 
({ext_include file="inc_portal_Contents_Layout_ktai.tpl"}) 
({/foreach})
({$inc_ktai_footer|smarty:nodefaults})