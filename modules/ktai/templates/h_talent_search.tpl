({$inc_ktai_header|smarty:nodefaults})

({if $inc_ktai_entry_point[1]})
({$inc_ktai_entry_point[1]|smarty:nodefaults})
({/if})


({if $inc_ktai_entry_point[2]})
({$inc_ktai_entry_point[2]|smarty:nodefaults})
({/if})

({foreach from=$top item=data})
    ({ext_include file="inc_portal_Contents_Layout_ktai.tpl"})
({/foreach})

<!-- ▼contents -->
<table width="100%">
<tr>
<td align="center" bgcolor="#FECECE">
<font size="2">
<font color="#E20080"><a name="top">ﾀﾚﾝﾄ・ﾓﾃﾞﾙ検索</a></font><br>
</font>
</td>
</tr>

<tr>
<td bgcolor="#F6F3EC" align="left">
<font size="2">
be amieに登録されているｵｽｶｰﾌﾟﾛﾓｰｼｮﾝ所属のﾀﾚﾝﾄ・ﾓﾃﾞﾙのﾌﾟﾛﾌｨｰﾙ情報と日記を検索します。</font><font size="1"><br>
<br></font>
<font size="1">※検索結果のﾘﾝｸ先は、ﾛｸﾞｲﾝしないと表示されない場合があります。</font><font size="1"><br>
<br></font>
</td>
</tr>
</table>

<table width="100%">
<tr>
<td bgcolor="#FECECE">
<font size="2">

<font color="#E20080"><a name="keyword_search">ｷｰﾜｰﾄﾞで検索</a></font><br>
</font>
</td>
</tr>
</table>
({t_form _method=get m=ktai a=page_search_result}) 
<input type="hidden" name="ksid" value="({$PHPSESSID})">
ｷｰﾜｰﾄﾞ:<input type="text" name="searchkey" id="searchkey" size="20" maxlength="100" value="({$smarty.const.ADMIN_KTAI_DEFAULT_KEYWORD})" accesskey="1" istyle="1"><br>
<div align="center"><input type="submit" name="検索" value="検索"></div>
</form>

<table width="100%">
<tr>
<td bgcolor="#FECECE">
<font size="2">
({if $error_msg})
<font color="Red">({$error_msg})</font><br>
({/if})
<font color="#E20080"><a name="condition_search">条件で検索</a></font><br>
</font>
</td>
</tr>
</table>

({t_form _method=get m=ktai a=page_h_talent_list _attr='id="form2"'})
<input type="hidden" name="ksid" value="({$PHPSESSID})">
<font size="2">
所属:<select name="talent_company" size="" accesskey="2">
<option value="" selected>－指定しない－</option>
<option value="タレント">ﾀﾚﾝﾄ</option>
<option value="モデル">ﾓﾃﾞﾙ</option>
<option value="歌手">歌手</option>
<option value="キャスター・レポーター">ｷｬｽﾀｰ･ﾚﾎﾟｰﾀｰ</option>
<option value="ナレーター・コンパニオン">ﾅﾚｰﾀｰ･ｺﾝﾊﾟﾆｵﾝ</option>
<option value="バラエティ">ﾊﾞﾗｴﾃｨ</option>
</select>
<br>
年齢:<input type="text" name="age1" size="3" maxlength="2" value="" accesskey="3" istyle="4">～<input type="text" name="age2" size="3" maxlength="2" value="" accesskey="" istyle="4">歳<br>
生年月日:<br>
<select name="start_year"' width="30px;">
	({foreach from=$arr_year item=v key=k})
	<option ({if $smarty.request.start_year==$k}) selected ({/if}) value="({$k})">({$v})</option>
	({/foreach})
</select>年
<select name="start_month"'>
	({foreach from=$arr_month item=v key=k})
	<option ({if $smarty.request.start_month==$k}) selected ({/if}) value="({$k})">({$v})</option>
	({/foreach})
</select>月
<select name="start_day"'>
	({foreach from=$arr_day item=v key=k})
	<option ({if $smarty.request.start_day==$k}) selected ({/if}) value="({$k})">({$v})</option>
	({/foreach})
</select>日
～
<select name="end_year"'>
	({foreach from=$arr_year item=v key=k})
	<option ({if $smarty.request.end_year==$k}) selected ({/if}) value="({$k})">({$v})</option>
	({/foreach})
</select>年
<select name="end_month"'>
	({foreach from=$arr_month item=v key=k})
	<option ({if $smarty.request.end_month==$k}) selected ({/if}) value="({$k})">({$v})</option>
	({/foreach})
</select>月
<select name="end_day"'>
	({foreach from=$arr_day item=v key=k})
	<option ({if $smarty.request.end_day==$k}) selected ({/if}) value="({$k})">({$v})</option>
	({/foreach})
</select>日<br>
性別:<select name="sex" size="">
<option value="0" selected>－指定しない－</option>
({foreach from=$sex_list item=sex})
<option  value="({$sex.c_profile_option_id})" ({if $smarty.request.sex==$sex.c_profile_option_id}) selected({/if})>({$sex.value})</option>
({/foreach})
</select>
<br>
血液型:<select name="blood_type" size="" accesskey="4">
<option value="0" selected>－指定しない－</option>
({foreach from=$blood_type_list item=blood_type})
<option value="({$blood_type.c_profile_option_id})" ({if $smarty.request.blood_type == $blood_type.c_profile_option_id}) selected ({/if})>({$blood_type.value})</option>
({/foreach})

</select>
<br>
<table width="100%" border="0">
<tr>
<td align="left" colspan="2"><font size="2">ｻｲｽﾞ:</font></td>
</tr>
<tr>
<td align="right"><font size="2">身長</font></td>
<td align="left"><font size="1"><input type="text" name="talent_height1" size="4" maxlength="3" value="" accesskey="5" istyle="4">cm～<input type="text" name="talent_height2" size="4" maxlength="3" value="" accesskey="" istyle="4">cm</font></td>
</tr>
<tr>
<td align="right"><font size="2">ﾊﾞｽﾄ</font></td>

<td align="left"><font size="1"><input type="text" name="talent_bust1" size="4" maxlength="3" value="" accesskey="" istyle="4">cm～<input type="text" name="talent_bust2" size="4" maxlength="3" value="" accesskey="" istyle="4">cm</font></td>
</tr>
<tr>
<td align="right"><font size="2">ｳｪｽﾄ</font></td>
<td align="left"><font size="1"><input type="text" name="talent_waistline1" size="4" maxlength="3" value="" accesskey="" istyle="4">cm～<input type="text" name="talent_waistline2" size="4" maxlength="3" value="" accesskey="" istyle="4">cm</font></td>
</tr>
<tr>
<td align="right"><font size="2">ﾋｯﾌﾟ</font></td>
<td align="left"><font size="1"><input type="text" name="talent_hip1" size="4" maxlength="3" value="" accesskey="" istyle="4">cm～<input type="text" name="talent_hip2" size="4" maxlength="3" value="" accesskey="" istyle="4">cm</font></td>

</tr>
<tr>
<td align="right"><font size="2">ｼｭｰｽﾞ</font></td>
<td align="left"><font size="1"><input type="text" name="talent_shoe1" size="4" maxlength="3" value="" accesskey="" istyle="4">cm～<input type="text" name="talent_shoe2" size="4" maxlength="3" value="" accesskey="" istyle="4">cm</font></td>
</tr>
</table>
出身地:<select name="old_addr_pref" size="" accesskey="6">
<option value="0" selected>－指定しない－</option>
({foreach from=$old_addr_pref_list item=old_addr_pref})
<option value="({$old_addr_pref.c_profile_option_id})" ({if $smarty.request.old_addr_pref == $old_addr_pref.c_profile_option_id}) selected ({/if})>({$old_addr_pref.value})</option>
({/foreach})	

</select>
<br>
趣味:<input type="text" name="talent_interest" size="20" maxlength="100" value="" accesskey="7" istyle="1"><br>
資格:<input type="text" name="talent_qualification" size="20" maxlength="100" value="" accesskey="" istyle="1"><br>
ｽﾎﾟｰﾂ:<input type="text" name="talent_sport" size="20" maxlength="100" value="" accesskey="" istyle="1"><br>
特技:<input type="text" name="talent_speciality" size="20" maxlength="100" value="" accesskey="" istyle="1"><br>
<br>
<div align="center"><input type="submit" name="検索" value="検索"></div>
</font>
</form>
<br>
<div align="right"><a href="#top" accesskey="9">[i:133]ﾍﾟｰｼﾞTOP</a></div>
<!-- △contents -->
({foreach from=$bottom item=data}) 
({ext_include file="inc_portal_Contents_Layout_ktai.tpl"}) 
({/foreach})
({$inc_ktai_footer|smarty:nodefaults})
