({$inc_ktai_header|smarty:nodefaults})

({if $inc_ktai_entry_point[1]})
({$inc_ktai_entry_point[1]|smarty:nodefaults})
({/if})


({if $inc_ktai_entry_point[2]})
({$inc_ktai_entry_point[2]|smarty:nodefaults})
({/if})

({foreach from=$top item=data})
    ({ext_include file="inc_portal_Contents_Layout_ktai.tpl"})
({/foreach})
<!-- ▼contents -->

<table width="100%">
<tr>
<td align="center" bgcolor="#FECECE">
<font size="2">
<font color="#E20080"><a name="top">写真送信画面</a></font><br>
</font>
</td>
</tr>
<tr>
<td>
STEP①　応募情報入力＞＞STEP②　応募情報確認＞＞<font color="#FF00FF"> STEP③　写真の送信＞＞</font>完了
<p>	  
応募は<font color="#ff0000"">まだ完了しておりません。</font>最後に写真と動画(必須ではありません)を送信して応募が完了となります。
</p>
<p>
以下の写真1､写真2を押すとﾒｰﾙ作成画面が開きますので､写真を添付しﾒｰﾙを送信してください｡
</p>
</td>
</tr>
<tr>
<td align="center">
<a href="mailto:({$mail_address1})?body=写真1(ｱｯﾌﾟ)を添付して送信してください。" >写真1(ｱｯﾌﾟ)を送る</a>
</td>
</tr>
<tr>
<td align="center">
<a href="mailto:({$mail_address2})?body=写真2(全身)を添付して送信してください。" >写真2(全身)を送る
</a>
</td>
</tr>
<tr>
	<td>
	※撮影ｲﾒｰｼﾞは以下をご確認ください｡<br/>
※ﾊﾟｰﾂﾓﾃﾞﾙ志願者は写真1､2は必須ではありません｡<br/>	
	</td>
</tr>
<tr>
<td>
<img src="./modules/ktai/img/photo_m.gif" alt="">	
</td>
</tr>
<tr>
<td>
<a href="mailto:({$mail_address3})?body=写真3(各部位写真)を添付して送信してください。" >写真3(各部位写真)を送る</a>
</td>
</tr>
<tr>
	<td>
	※写真3はﾊﾟｰﾂﾓﾃﾞﾙ志望者のみ必須｡
	</td>
</tr>
<tr>
<td>
<a href="mailto:({$mail_address4})?body=動画1(自己紹介)を添付して送信してください。" >動画(自己紹介)を送る</a>
</td>
</tr>
<tr>
	<td>
	※必須ではありません｡<br/>
	※1分程度｡長いと送信できない場合があります｡	
	</td>
</tr>
<tr>
	<td align="center">
	<p>
	<br/>
	お疲れさまでした。<br/>
	<font color="red">これで応募は完了です｡</font>
	<br/>
	</p>
	</td>
</tr>
<tr>
<td>
	<p>
	今後の選考の流れは以下をご確認ください。
	</p>
	<p>
	･選考結果は2～4週間以内に合格者のみにｵｽｶｰﾌﾟﾛﾓｰｼｮﾝから通知いたします｡<br/>
	･お電話やﾒｰﾙによる審査結果のお問合わせに関しては一切応じかねます｡<br/>
	･書類選考を通過した方のみｵｰﾃﾞｨｼｮﾝ(面接､ｶﾒﾗﾃｽﾄ他)を行います｡
	</p>
	</td>
</tr>
</table>

<!-- △contents -->
({foreach from=$bottom item=data}) 
({ext_include file="inc_portal_Contents_Layout_ktai.tpl"}) 
({/foreach})
({$inc_ktai_footer|smarty:nodefaults})