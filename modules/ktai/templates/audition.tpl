({$inc_ktai_header|smarty:nodefaults})

({if $inc_ktai_entry_point[1]})
({$inc_ktai_entry_point[1]|smarty:nodefaults})
({/if})


({if $inc_ktai_entry_point[2]})
({$inc_ktai_entry_point[2]|smarty:nodefaults})
({/if})

({foreach from=$top item=data})
    ({ext_include file="inc_portal_Contents_Layout_ktai.tpl"})
({/foreach})
<!-- ▼contents -->
<table width="100%">
<tr>
<td align="center" bgcolor="#FECECE">
<font size="2">
<font color="#E20080"><a name="top">オーディション応募フォーム</a></font><br>
</font>
</td>
</tr>
({foreach from=$errors item=error})
({if $error})
<tr>
<td>
<font color="red">({$error})</font>
</td>
</tr>
({/if})
({/foreach})
</table>

({if $msg})<p class="actionMsg">({$msg})</p>({/if})

({t_form_block m=ktai a=page_audition_confirm})
<font color="#FF00FF">STEP①　応募情報入力＞＞</font>STEP②　応募情報確認＞＞ STEP③　写真の送信＞＞完了<br/>
下記の必要項目に内容をご記入のうえ､注意事項を必ずお読みの上､応募をお願い致します｡<br/><br/>

<input type="hidden" name="ksid" value="({$PHPSESSID})">

■氏名 <font color="#FF0000">(必須)</font><br>
姓<input type="text" value="({$form.name_1})" name="name_1" size="12">(全角)<br>
名<input type="text" value="({$form.name_2})" name="name_2" size="12">(全角)<br>
ｾｲ<input type="text" value="({$form.kananame_1})" size="12" name="kananame_1">(全角ｶﾅ)<br>
ﾒｲ<input type="text" value="({$form.kananame_2})" size="12" name="kananame_2">(全角ｶﾅ)<br><br>

■生年月日<font color="#FF0000">(必須)</font><br>
<select name="birthday_year" style="">
    <option value="" >--</option>
    ({foreach from=$years item=item })
    <option  value="({$item})" ({if $form.birthday_year==$item }) selected="selected" ({/if}) >({$item})</option>
    ({/foreach})
</select>
    -
<select name="birthday_month" style="">
    <option value="" >--</option>
    ({foreach from=$months item=item })
    <option  value="({$item})" ({if $form.birthday_month==$item }) selected="selected" ({/if}) >({$item})</option>
    ({/foreach})
</select>
    -
<select name="birthday_day" style="">
    <option value="" >--</option>
    ({foreach from=$days item=item })
    <option  value="({$item})" ({if $form.birthday_day==$item }) selected="selected" ({/if}) >({$item})</option>
    ({/foreach})
</select><br>
年齢
<input name="age" type="text" value="({$form.age})" size=4 istyle="4" mode="numeric">歳<br><br>

■ご住所<font color="#FF0000">(必須)</font><br>
<font>建物名､号室は<br>必須入力項目ではありません｡</font><br>
郵便番号
<input type="text" value="({$form.zip})" size=6 name="zip" istyle="4" mode="numeric">(半角数字)<br>
都道府県
<select class="gray" name="address_pref">
	<option value="">&#8213; 選択してください &#8213;</option>
	({foreach from=$pref item=item})
	<option value="({$item})"  ({if $item==$form.address_pref}) selected ({/if}) >({$item})</option>		
	({/foreach})
</select><br>
市区町村･番地(全角･半角英数)<br>
<input type="text" value="({$form.address_1})" name="address_1"><br>
建物名･号室(全角･半角英数)<br>
<input type="text" value="({$form.address_2})" name="address_2"><br><br>

■ご連絡先の電話番号<font color="red">(必須)</font><br>
<font>携帯電話番号は必須入力項目ではありません｡</font><br>
自宅電話番号(半角数字)<br>
<input type="text" value="({$form.tel_1})" size=4 name="tel_1" istyle="4" mode="numeric"> - <input type="text" value="({$form.tel_2})" size=4 name="tel_2" istyle="4" mode="numeric"> - <input type="text" value="({$form.tel_3})" size=4 name="tel_3" istyle="4" mode="numeric"><br>
携帯電話番号(半角数字)<br>
<input type="text" value="({$form.mobile_tel_1})" size=4 name="mobile_tel_1" istyle="4" mode="numeric"> - <input type="text" value="({$form.mobile_tel_2})" size=4 name="mobile_tel_2" istyle="4" mode="numeric"> - <input type="text" value="({$form.mobile_tel_3})" size=4 name="mobile_tel_3" istyle="4" mode="numeric">
<br><br>

■ﾒｰﾙｱﾄﾞﾚｽ<font color="red">(必須)</font><br>
<br>
ﾒｰﾙｱﾄﾞﾚｽ(半角英数)<br>
<input type="text" value="({$form.email})" name="email" istyle="3" mode="alphabet"><br>
ﾒｰﾙｱﾄﾞﾚｽ確認(半角英数)<br>
<input type="text" value="({$form.email_check})" name="email_check" istyle="3" mode="alphabet"><br><br>

■最終学歴<font color="red">(必須)</font><br>
学校(全角･半角英数)<br>
<input type="text" value="({$form.education})" name="education"><br/>
<input type="radio" name="in_school" value="1" ({if $form.in_school=='1'}) checked ({/if}) >在学<br>
<input type="radio" name="in_school" value="3" ({if $form.in_school=='3'}) checked ({/if}) >中退<br/>
<input type="radio" name="in_school" value="2" ({if $form.in_school=='2'}) checked ({/if})  >卒業 <br/>
 西暦<input type="text" name="graduation_year" size=4 value="({$form.graduation_year})" istyle="4" mode="numeric">年(半角英数)<br/>
<br><br>

■現在の職業<font color="red">(必須)</font>(全角･半角英数)<br>
<input type="text" value="({$form.job})" name="job">
<br><br>

■特技<font color="red">(必須)（全角450文字まで）</font>(全角･半角英数)<br>
<textarea rows="3" name="speciality"  >({$form.speciality})</textarea>
<br><br>

■趣味<font color="red">(必須)（全角450文字まで）</font>(全角･半角英数)<br>
<textarea rows="3" name="interest"  >({$form.interest})</textarea>
<br><br>

■自己PR<font color="red">(必須)（全角450文字まで）</font><br>
志望動機､芸能界での目標を必ずご記入ください｡<br>
<textarea rows="5" name="owner_PR"  >({$form.owner_PR})</textarea>
<br><br>

■希望部門<font color="red">(必須)</font><br>
<select name="wish_department">
({foreach from=$department item=depname})
<option value="({$depname})" ({if $depname==$form.wish_department})selected({/if})>({$depname})</option>
({/foreach})
</select>
<br>

■ｻｲｽﾞ<font color="red">(必須)</font>(半角英数)<br>
身長<input type="text" name="body_1" value="({$form.body_1})" size=5 istyle="4" mode="numeric">cm<br>
ﾊﾞｽﾄ<input type="text" name="body_2" value="({$form.body_2})" size=5 istyle="4" mode="numeric">cm<br>
ｳｪｽﾄ<input type="text" name="body_3" value="({$form.body_3})" size=5 istyle="4" mode="numeric">cm<br>
ﾋｯﾌﾟ<input type="text" name="body_4" value="({$form.body_4})" size=5 istyle="4" mode="numeric">cm<br>
靴<input type="text" name="body_5" value="({$form.body_5})" size=5 istyle="4" mode="numeric">cm<br>
<br>

■現在､他の芸能ﾌﾟﾛﾀﾞｸｼｮﾝに所属していますか?<font color="red">(必須)</font><br>
<input type="radio" name="production_radio" value="1" ({if $form.production_radio==1})checked({/if})>はい(｢はい｣の場合は以下もご記入ください)<br>
<input type="radio" name="production_radio" value="2" ({if $form.production_radio==2})checked({/if})>いいえ<br>
所属事務所(全角･半角英数)<br>
<input type="text" name="production" value="({$form.production})"  ><br>
契約期間(全角･半角英数)<br>
<input type="text" name="contact_period" value="({$form.contact_period})" istyle="4" mode="numeric" ><br>
<br>

■今までにｵｰﾃﾞｨｼｮﾝを受けたことがありますか?<font color="red">(必須)</font><br>
<input type="radio" name="audition_radio" value="1" ({if $form.audition_radio=="1"})checked({/if})>はい(｢はい｣の場合は以下もご記入ください)<br>
<input type="radio" name="audition_radio" value="2" ({if $form.audition_radio=="2"})checked({/if})>いいえ<br>
ｵｰﾃﾞｨｼｮﾝ名(全角･半角英数)<br>
<input type="text" name="audition_name" value="({$form.audition_name})"><br>
結果(全角･半角英数)<br>
<input type="text" name="audition_result" value="({$form.audition_result})"><br>
<br>

■その他､経験<font color="red">（全角450文字まで）</font><br>
<textarea rows="3" name="other_experience" >({$form.other_experience})</textarea>
<br><br>

be amieｱｶｳﾝﾄ<font color="red">(必須)</font><br>
■応募者ご本人のbe amieにご登録のﾒｰﾙｱﾄﾞﾚｽをご記入ください。<br>
ﾒｰﾙｱﾄﾞﾚｽ(半角英数)<br>
<input type="text" value="({$form.email_login})" name="email_login" istyle="3" mode="alphabet"><br>
ﾒｰﾙｱﾄﾞﾚｽ確認(半角英数)<br>
<input type="text" value="({$form.email_login_check})" name="email_login_check" istyle="3" mode="alphabet"><br>
ﾆｯｸﾈｰﾑ(全角･半角英数)<br>
<input type="text" name="login_nickname" value="({$form.login_nickname})"><br>
<p align="right" ><a href="#top">ﾍﾟｰｼﾞTOP</a>&nbsp;</p>
<br>
<br>
※推薦の場合<br/>以下は推薦者様に関する内容をご記入ください｡<br>
■推薦者のbe amie登録情報<br>
ﾒｰﾙｱﾄﾞﾚｽ確認(半角英数)<br>
<input type="text" value="({$form.r_email})" name="r_email" istyle="3" mode="alphabet"><br>
ﾒｰﾙｱﾄﾞﾚｽ(半角英数)<br>
<input type="text" value="({$form.r_email_check})" name="r_email_check" istyle="3" mode="alphabet"><br>
ﾆｯｸﾈｰﾑ(全角･半角英数)<br>
<input type="text" name="r_nickname" value="({$form.r_nickname})"><br>
<br>

■推薦者の氏名<br>
姓<input type="text" value="({$form.r_name_1})" name="r_name_1" size="12">(全角)<br>
名<input type="text" value="({$form.r_name_2})" name="r_name_2" size="12">(全角)<br>
ｾｲ<input type="text" value="({$form.r_kana_name_1})" size="12" name="r_kana_name_1">(全角ｶﾅ)<br>
ﾒｲ<input type="text" value="({$form.r_kana_name_2})" size="12" name="r_kana_name_2">(全角ｶﾅ)<br><br>

■推薦者のご住所<br>
郵便番号<input type="text" value="({$form.r_zip})" size=6 name="r_zip" istyle="4" mode="numeric">(半角数字)<br>
都道府県
<select name="r_address_pref">
	<option value="">&#8213; 選択してください &#8213;</option>
	({foreach from=$pref item=item})
	<option value="({$item})"  ({if $item==$form.r_address_pref}) selected ({/if}) >({$item})</option>		
	({/foreach})
</select><br>
市区町村･番地(全角･半角英数)<br>
<input type="text" value="({$form.r_address_1})" name="r_address_1"><br>
建物名･号室(全角･半角英数)<br>
<input type="text" value="({$form.r_address_2})" name="r_address_2"><br><br>

■推薦者の連絡先<br>
電話番号(半角数字)<br>
<input type="text" value="({$form.r_tel_1})" size=4 name="r_tel_1" istyle="4" mode="numeric"> - <input type="text" value="({$form.r_tel_2})" size=4 name="r_tel_2" istyle="4" mode="numeric"> - <input type="text" value="({$form.r_tel_3})" size=4 name="r_tel_3" istyle="4" mode="numeric"><br>
<br>

■応募者と推薦者のご関係<br>
本人との関係(全角･半角英数)<br>
<input type="text" value="({$form.r_relationship })" name="r_relationship">
<br>
<table width="100%">
<tr>
<td align="left" colspan="2" bgcolor="#FECECE">
<font color="#E20080">
<font size="2">個人情報の取り扱いについて</font><br>
</font>
</td>
</tr>
<tr>
<td align="left" colspan="2">
<font size="2">｢<a href="({t_url m=ktai a=page_o_free_page})&amp;c_free_page_id=15&amp;({$tail})">ﾌﾟﾗｲﾊﾞｼｰﾎﾟﾘｼｰ</a>｣をお読みいただき､同意の上で確認画面に進んでください｡<br></font>
</td>
</tr>
<tr>
<td align="left" colspan="2" bgcolor="#FECECE">
<font color="#E20080">
<font size="2">注意事項 ※必ずご確認ください｡</font><br>
</font>
</td>
</tr>
<tr>
<td align="left" colspan="2">
<font size="2">
①一次選考(ご応募)合格者のみにｵｽｶｰﾌﾟﾛﾓｰｼｮﾝから通知をさせていただきます｡<br>
②合格の通知までに約1ｶ月ほどかかる場合があります｡<br>
③お電話やﾒｰﾙによる審査結果のお問合わせに関しては一切応じかねます｡<br>
④何度でもご応募可能です｡<br>
⑤自薦､他薦を問わずご応募いただけます｡(この場合､応募者､推薦者共にbe amieの会員登録が必要です)<br>
⑥ﾌﾞﾛｸﾞやSNSの日記等で極力､応募されたことを告知しないようにお願いします｡悪質なｽｶｳﾄ等から連絡がくる場合がございます｡<br>
</font>
</td>
</tr>
</table>

<div align="center"><input type="submit" name="同意の上､確認画面へ" value="同意の上､確認画面へ" accesskey="7"></div>
({/t_form_block})
<p align="right" ><a href="#top">ﾍﾟｰｼﾞTOP</a>&nbsp;</p>


<!-- △contents -->
({foreach from=$bottom item=data}) 
({ext_include file="inc_portal_Contents_Layout_ktai.tpl"}) 
({/foreach})
({$inc_ktai_footer|smarty:nodefaults})