({$inc_ktai_header|smarty:nodefaults})

({if $inc_ktai_entry_point[1]})
({$inc_ktai_entry_point[1]|smarty:nodefaults})
({/if})


({if $inc_ktai_entry_point[2]})
({$inc_ktai_entry_point[2]|smarty:nodefaults})
({/if})

({foreach from=$top item=data})
    ({ext_include file="inc_portal_Contents_Layout_ktai.tpl"})
({/foreach})
<!-- ▼contents -->

<table width="100%">
<tr>
<td align="center" bgcolor="#FECECE">
<font size="2">
<font color="#E20080"><a name="top">写真送信画面</a></font><br>
</font>
</td>
</tr>
<tr>
<td>
STEP①　応募情報入力＞＞STEP②　応募情報確認＞＞<font color="#FF00FF"> STEP③　写真の送信＞＞</font>完了
<p>	  
応募は<font color="#ff0000">まだ完了しておりません。</font>最後に写真を送信して応募が完了となります。
</p>
<p>
以下の写真1､写真2を押すとﾒｰﾙ作成画面が開きますので､写真を添付しﾒｰﾙを送信してください｡
</p>
</td>
</tr>
<tr>
<td align="center">
<a href="mailto:({$mail_address1})?body=写真1(ｱｯﾌﾟ)を添付して送信してください。" >写真1(ｱｯﾌﾟ)を送る</a>
</td>
</tr>
<tr>
<td align="center">
<a href="mailto:({$mail_address2})?body=写真2(全身)を添付して送信してください。" >写真2(全身)を送る
</a>
</td>
</tr>
<tr>
	<td>
	※撮影ｲﾒｰｼﾞは以下をご確認ください｡<br/>
	・<font color="red">写真は縦長で頭が上方向の写真を送付ください。</font><br/>
	横撮影のｶﾒﾗを立てて撮影すると、見た目は縦長写真ですが写真は横向きの場合があります。<u>写真確認の際に頭が上になるよう回転し保存してから送付をお願いします。</u><br/>
	※プリクラや撮影後加工した写真は不可です。<br/>
	</td>
</tr>
<tr>
<td>
<img src="./modules/ktai/img/photo_m.gif" alt="">	
</td>
</tr>
<tr>
	<td>
	<p>
	<br/>
	最近6ヶ月以内に撮影したものを送付ください。<br/>
	・顔ｱｯﾌﾟ<br/>
	真正面を向き、顔がはっきりと写っているもの。<br/>
	帽子や前髪で顔が隠れないようにしましょう。<br/>
	・全身<br/>
	身体のﾗｲﾝが分かりやすい服装、姿勢で写っているもの。人物が小さすぎたり、背景が目立ったりしないようにしましょう。<br/><br/>
	</p>
	</td>
</tr>
<tr>
	<td align="center">
	<p>
	<font color="red">写真の送信で応募は完了です。</font><br/>
	<a href="http://beamie.jp/">be amieトップページへ</a><br/><br/>
	</p>
	</td>
</tr>
<tr>
<td>
	<p>
・審査状況・合否に関わるお問い合わせはお答えいたしかねます。<br/>
・一次の書類審査合格者の方に限り、二次の面接審査を通知いたします。<br/>
・<font color="red">be amie内でｵｽｶｰﾌﾟﾛﾓｰｼｮﾝ公認ｽｶｳﾄ以外の悪質なｽｶｳﾄ行為には充分ご注意ください！！<br/>
全日本国民的美少女ｺﾝﾃｽﾄ実行委員会より皆様宛に直接ﾒｯｾｰｼﾞをお送りすることは絶対にございません。</font><br/>
もしbe amieを通じて不信なﾒｯｾｰｼﾞが届いた場合は必ずご家族にご相談いただき、下記までご連絡をお願いいたします。<br/>
全日本国民的美少女ｺﾝﾃｽﾄ実行委員会<br/>
03-6833-0650（平日11:00～17:00pm受付）<br/>
	</p>
	</td>
</tr>
</table>

<!-- △contents -->
({foreach from=$bottom item=data}) 
({ext_include file="inc_portal_Contents_Layout_ktai.tpl"}) 
({/foreach})
({$inc_ktai_footer|smarty:nodefaults})