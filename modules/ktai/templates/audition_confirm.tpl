({$inc_ktai_header|smarty:nodefaults})

({if $inc_ktai_entry_point[1]})
({$inc_ktai_entry_point[1]|smarty:nodefaults})
({/if})


({if $inc_ktai_entry_point[2]})
({$inc_ktai_entry_point[2]|smarty:nodefaults})
({/if})

({foreach from=$top item=data})
    ({ext_include file="inc_portal_Contents_Layout_ktai.tpl"})
({/foreach})
<!-- ▼contents -->




<table width="100%">
<tr>
<td align="center" bgcolor="#FECECE">
<font size="2">
<font color="#E20080"><a name="top">ｵｰﾃﾞｨｼｮﾝ応募情報確認</a></font><br>
</font>
</td>
</tr>
</table>
STEP①　応募情報入力＞＞<font color="#FF00FF">STEP②　応募情報確認＞＞</font> STEP③　写真の送信＞＞完了<br/>
ご入力いただいた内容に間違いがない事をご確認のうえ､ﾍﾟｰｼﾞ下部の送信ﾎﾞﾀﾝを押してください｡<br/>

■氏名<br>
<font color="#FF00FF">姓</font>  ({$form.name_1})<br>
<font color="#FF00FF">名</font>  ({$form.name_2})<br>
<font color="#FF00FF">ｾｲ</font> ({$form.kananame_1})<br>
<font color="#FF00FF">ﾒｲ</font> ({$form.kananame_2})<br>
<font color="#FF00FF">生年月日</font> ({$form.birthday_year})-({$form.birthday_month})-({$form.birthday_day})<br>
<font color="#FF00FF">年齢</font> ({$form.age}) 歳<br><br>
■ご住所<br>
<font color="#FF00FF">郵便番号</font> ({$form.zip})<br>
<font color="#FF00FF">都道府県</font> ({$form.address_pref})<br>
<font color="#FF00FF">市区町村･番地</font> ({$form.address_1})<br>
<font color="#FF00FF">建物名･号室 </font>({$form.address_2})<br>
<font color="#FF00FF">自宅電話番号</font> ({$form.tel_1})-({$form.tel_2})-({$form.tel_3})<br>
<font color="#FF00FF">携帯電話番号</font> ({$form.mobile_tel_1})-({$form.mobile_tel_2})-({$form.mobile_tel_3})<br>
<font color="#FF00FF">ﾒｰﾙｱﾄﾞﾚｽ </font>({$form.email})<br>
■<font color="#FF00FF">最終学歴</font><br>
<font color="#FF00FF">学校</font> ({$form.education})<br>
({if $form.in_school==1 || $form.in_school==3})
	({if $form.in_school==1})在学({else})中退({/if})
({else})卒業　西暦 ({$form.graduation_year})
({/if})  <br>
■<font color="#FF00FF">現在の職業</font> ({$form.job})<br>
■<font color="#FF00FF">特技</font>  ({$form.speciality})<br>
■<font color="#FF00FF">趣味</font>  ({$form.interest})<br>
■<font color="#FF00FF">自己PR</font> ({$form.owner_PR})<br>
■<font color="#FF00FF">希望部門</font> ({$form.wish_department})<br> 
■<font color="#FF00FF">ｻｲｽ</font>ﾞ<br>
<font color="#FF00FF">身長</font>({$form.body_1})cm<br>
<font color="#FF00FF">ﾊﾞｽﾄ</font>({$form.body_2})cm<br>
<font color="#FF00FF">ｳｪｽﾄ</font>({$form.body_3})cm<br>
<font color="#FF00FF">ﾋｯﾌﾟ</font>({$form.body_4})cm<br>
<font color="#FF00FF">靴</font>({$form.body_5})cm<br><br>
■現在､他の芸能ﾌﾟﾛﾀﾞｸｼｮﾝに所属していますか?<br>
({if $form.production_radio==1})はい({else})いいえ({/if})<br>
<font color="#FF00FF">所属事務所</font>  ({$form.production})<br>
<font color="#FF00FF">契約期間 </font>({$form.contact_period})<br>
<br>
■今までにｵｰﾃﾞｨｼｮﾝを受けたことがありますか?<br>
({if $form.audition_radio==1})はい({else})いいえ({/if})<br>
<font color="#FF00FF">ｵｰﾃﾞｨｼｮﾝ名</font> ({$form.audition_name})<br>
<font color="#FF00FF">結果 </font> ({$form.audition_result})<br>
<font color="#FF00FF">その他､経験</font>  ({$form.other_experience})<br>
■be amieご登録のﾒｰﾙｱﾄﾞﾚｽをご記入ください<br>
<font color="#FF00FF">ﾒｰﾙｱﾄﾞﾚｽ </font>({$form.email_login})<br>
<font color="#FF00FF">ﾆｯｸﾈｰﾑ</font> ({$form.login_nickname})<br>
<br>
※推薦の場合:以下は推薦者様に関する内容をご記入ください<br>
■<font color="#FF00FF">ﾒｰﾙｱﾄﾞﾚｽ</font> ({$form.r_email})<br>
■<font color="#FF00FF">ﾆｯｸﾈｰﾑ</font> ({$form.r_nickname})<br>
■<font color="#FF00FF">推薦者の氏名</font><br>
<font color="#FF00FF">姓 </font>({$form.r_name_1})<br>
<font color="#FF00FF">名 </font>({$form.r_name_2})<br>
<font color="#FF00FF">ｾｲ</font> ({$form.r_kana_name_1})<br>
<font color="#FF00FF">ﾒｲ</font> ({$form.r_kana_name_2})<br>
■推薦者のご住所<br>
<font color="#FF00FF">郵便番号</font> ({$form.r_zip})<br>
<font color="#FF00FF">都道府県</font> ({$form.r_address_pref})<br>
<font color="#FF00FF">市区町村･番地</font> ({$form.r_address_1})<br>
<font color="#FF00FF">建物名･号室</font>  ({$form.r_address_2})<br>
<font color="#FF00FF">推薦者の連絡先</font><br>
<font color="#FF00FF">電話番号</font> ({$form.r_tel_1})-({$form.r_tel_2})-({$form.r_tel_3})<br>
■応募者と推薦者のご関係 <br>
<font color="#FF00FF">本人との関係</font> ({$form.r_relationship }) <br>



<table>
<tr>
<td>
({t_form m=ktai a=do_audition})
<input type="hidden" name="ksid" value="({$PHPSESSID})">
({foreach from=$form key=key item=item})
<input type="hidden" name="({$key})" value="({$item})" />
({/foreach})

<input type="submit" value="送信">
</form>
</td>
<td width="10px"></td>
<td>
<form name="return" action="./" method="post" >
<input type="hidden" name="m" value="ktai" >
<input type="hidden" name="ksid" value="({$PHPSESSID})">
<input type="hidden" name="a" value="page_audition" >
({foreach from=$form key=key item=item})
<input type="hidden" name="({$key})" value="({$item})" />
({/foreach})

<input type="submit" value="戻る">
</form>

</td>
</tr>
</table>

<!-- △contents -->
({foreach from=$bottom item=data}) 
({ext_include file="inc_portal_Contents_Layout_ktai.tpl"}) 
({/foreach})
({$inc_ktai_footer|smarty:nodefaults})