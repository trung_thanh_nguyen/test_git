({if $portal_free_area})
({$portal_free_area.html|smarty:nodefaults})
({else})
<font color="#({$ktai_color_config.font_09})">({$msg})</font>
({/if})
({php})
	// Copyright 2009 Google Inc. All Rights Reserved.
	if (function_exists('googleAnalyticsGetImageUrl')==false){
		function googleAnalyticsGetImageUrl() {
			$GA_ACCOUNT = "MO-17662108-1";
			$GA_PIXEL = "/ga.php";
			$url = "";
			$url .= $GA_PIXEL . "?";
			$url .= "utmac=" . $GA_ACCOUNT;
			$url .= "&utmn=" . rand(0, 0x7fffffff);
			$referer = $_SERVER["HTTP_REFERER"];
			$query = $_SERVER["QUERY_STRING"];
			$path = $_SERVER["REQUEST_URI"];
			if (empty($referer)) {
			  $referer = "-";
			}
			$url .= "&utmr=" . urlencode($referer);
			if (!empty($path)) {
			  $url .= "&utmp=" . urlencode($path);
			}
			$url .= "&guid=ON";
			return str_replace("&", "&amp;", $url);
		}
  }
  $googleAnalyticsImageUrl = googleAnalyticsGetImageUrl();
  echo '<img src="' . $googleAnalyticsImageUrl . '" />';
({/php})
</body>
</html>