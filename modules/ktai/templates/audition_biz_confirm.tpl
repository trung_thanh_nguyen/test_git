({$inc_ktai_header|smarty:nodefaults})

({if $inc_ktai_entry_point[1]})
({$inc_ktai_entry_point[1]|smarty:nodefaults})
({/if})


({if $inc_ktai_entry_point[2]})
({$inc_ktai_entry_point[2]|smarty:nodefaults})
({/if})

({foreach from=$top item=data})
    ({ext_include file="inc_portal_Contents_Layout_ktai.tpl"})
({/foreach})
<!-- ▼contents -->




<table width="100%">
<tr>
<td align="center" bgcolor="#FECECE">
<font size="2">
<font color="#E20080"><a name="top">第1回 美ｼﾞﾈｽﾏﾝ&美ｼﾞﾈｽｳｰﾏﾝｺﾝﾃｽﾄ応募ﾌｫｰﾑ</a></font><br>
</font>
</td>
</tr>
</table>
STEP①　応募情報入力＞＞<font color="#FF00FF">STEP②　応募情報確認＞＞</font> STEP③　写真の送信＞＞完了<br/><br/>
ご入力いただいた内容に間違いがない事をご確認のうえ､ﾍﾟｰｼﾞ下部の送信ﾎﾞﾀﾝを押してください｡<br/><br/>

■氏名<br>
<font color="#FF00FF">姓</font>  ({$form.name_1})<br>
<font color="#FF00FF">名</font>  ({$form.name_2})<br>
<font color="#FF00FF">ｾｲ</font> ({$form.kananame_1})<br>
<font color="#FF00FF">ﾒｲ</font> ({$form.kananame_2})<br>
<font color="#FF00FF">生年月日</font> ({$form.birthday_year})-({$form.birthday_month})-({$form.birthday_day})<br>
<font color="#FF00FF">年齢</font> ({$form.age}) 歳<br><br>
■ご住所<br>
<font color="#FF00FF">郵便番号</font> ({$form.zip})<br>
<font color="#FF00FF">都道府県</font> ({$form.address_pref})<br>
<font color="#FF00FF">市区町村･番地</font> ({$form.address_1})<br>
<font color="#FF00FF">建物名･号室 </font>({$form.address_2})<br>
<font color="#FF00FF">自宅電話番号</font> ({$form.tel_1})-({$form.tel_2})-({$form.tel_3})<br>
<font color="#FF00FF">ﾒｰﾙｱﾄﾞﾚｽ </font>({$form.email})<br>
■<font color="#FF00FF">性別</font> ({$form.education})<br>
■<font color="#FF00FF">職業・学年</font>  ({$form.graduation_year})<br>
■<font color="#FF00FF">特技</font>  ({$form.speciality})<br>
■<font color="#FF00FF">趣味</font>  ({$form.interest})<br>
■<font color="#FF00FF">自己PR</font> ({$form.owner_PR})<br>
■<font color="#FF00FF">家族構成</font> <br>
<table>
		<tr><td><div style="text-align:center;">氏名</div></th><td style="text-align:center;">続柄</th></tr>
		<tr><td>({$form.family_f})</td><td>({$form.family_f_name})</td></tr>
		<tr><td>({$form.family_m})</td><td>({$form.family_m_name})</td></tr>
		<tr><td>({$form.family_b})</td><td>({$form.family_b_name})</td></tr>
		<tr><td>({$form.family_yb})</td><td>({$form.family_yb_name})</td></tr>
</table>
■<font color="#FF00FF">将来の希望</font> ({$form.wish_department})<br> 
■<font color="#FF00FF">ｻｲｽ</font>ﾞ<br>
<font color="#FF00FF">身長</font>({$form.body_1})cm<br>
<font color="#FF00FF">ﾊﾞｽﾄ</font>({$form.body_2})cm<br>
<font color="#FF00FF">ｳｪｽﾄ</font>({$form.body_3})cm<br>
<font color="#FF00FF">ﾋｯﾌﾟ</font>({$form.body_4})cm<br>
<font color="#FF00FF">ｼｭｰｽﾞ</font>({$form.body_5})cm<br><br>
■be amieご登録のﾒｰﾙｱﾄﾞﾚｽをご記入ください<br>
<font color="#FF00FF">ﾒｰﾙｱﾄﾞﾚｽ </font>({$form.email_login})<br>
<font color="#FF00FF">ﾆｯｸﾈｰﾑ</font> ({$form.login_nickname})<br>
<br>
※未成年の方は保護者の同意が必要です。<br>
※面接に進まれた方は別途保護者の同意書をご用意いただきます。<br>

■<font color="#FF00FF">保護者氏名</font><br>
<font color="#FF00FF">姓 </font>({$form.r_name_1})<br>
<font color="#FF00FF">名 </font>({$form.r_name_2})<br>
<font color="#FF00FF">ｾｲ</font> ({$form.r_kana_name_1})<br>
<font color="#FF00FF">ﾒｲ</font> ({$form.r_kana_name_2})<br>
■保護者住所<br>
<font color="#FF00FF">郵便番号</font> ({$form.r_zip})<br>
<font color="#FF00FF">都道府県</font> ({$form.r_address_pref})<br>
<font color="#FF00FF">市区町村･番地</font> ({$form.r_address_1})<br>
<font color="#FF00FF">建物名･号室</font>  ({$form.r_address_2})<br>
<font color="#FF00FF">連絡先</font><br>
<font color="#FF00FF">電話番号</font> ({$form.r_tel_1})-({$form.r_tel_2})-({$form.r_tel_3})<br>

■<font color="#FF00FF">本人との関係</font><br>
({$form.r_relationship }) <br>
■<font color="#FF00FF">ｱﾝｹｰﾄ</font><br>
({$form.enquete})
({if $form.enquete=="新聞"})( ({$form.enqtext3}) )({/if})
({if $form.enquete=="雑誌"})( ({$form.enqtext4}) )({/if})
({if $form.enquete=="その他"})( ({$form.enqtext7}) )({/if})
<br>
<table>
<tr>
<td>
({t_form m=ktai a=do_audition_biz})
<input type="hidden" name="ksid" value="({$PHPSESSID})">
({foreach from=$form key=key item=item})
<input type="hidden" name="({$key})" value="({$item})" />
({/foreach})

<input type="submit" value="送信">
</form>
</td>
<td width="10px"></td>
<td>
<form name="return" action="./" method="post" >
<input type="hidden" name="m" value="ktai" >
<input type="hidden" name="ksid" value="({$PHPSESSID})">
<input type="hidden" name="a" value="page_audition_biz" >
({foreach from=$form key=key item=item})
<input type="hidden" name="({$key})" value="({$item})" />
({/foreach})

<input type="submit" value="戻る">
</form>

</td>
</tr>
</table>

<!-- △contents -->
({foreach from=$bottom item=data}) 
({ext_include file="inc_portal_Contents_Layout_ktai.tpl"}) 
({/foreach})
({$inc_ktai_footer|smarty:nodefaults})