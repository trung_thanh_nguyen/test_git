<?php
class ktai_page_search_result extends OpenPNE_Action
{
    function execute($requests)
    {    
    	$searchtype=$requests['searchtype'];
    	$keyword=$requests['searchkey'];
    	//$keyword=mb_convert_encoding($_GET['searchkey'],'SJIS-win','UTF-8');
    	$page=$requests['page'];
    	if(strlen($keyword)<2){
    		$this->set("error_msg" , "'検索キーワードは2文字以上入力してください。");
    	} else {
	    	switch ($searchtype){
	    		case 1:     			  			  			
	    			$result=db_search($keyword,"talent",$page);    			
	    			//$result=db_search_result($keyword,"talent",$page);
	    			break;
	    		case 2:
	    			$result=db_search($keyword,"commu",$page);
	    			//$result=db_search_result($keyword,"commu",$page);
	    			break;
	    		case 3:
	    			$result=db_search($keyword,"talent",$page);
	    			//$result=db_search_result($keyword,"all",$page);
	    			break;
	    		default:
	    			$result=db_search($keyword,"talent",$page);
	    			//$result=db_search_result($keyword,"talent",$page);
	    			break;
	    	}
	    	$list= $result[0];
	//    	foreach ($result[0] as $value){
	//    		$list[]=unserialize($value['rowcontent']);    		
	//    	}
	    	$this->set('search_list',$list);
	    	$pager = array(
            "page_prev" => $result[1],
            "page_next" => $result[2],
            "total_num" => $result[3],
        );
        $limit=PORTAL_SEARCH_PAGE_SIZE;//page size
    		$pager["disp_start"] = $limit * ($page - 1) + 1;
        if (($disp_end  = $limit * $page) > $pager['total_num']) {
            $pager['disp_end'] = $pager['total_num'];
        } else {
            $pager['disp_end'] = $disp_end;
        }

        $this->set("pager", $pager);
    	}
        $this->set('searchkey',$keyword);
        $this->set('searchtype',$searchtype);
        $this->set('search_condition',"searchkey=".urlencode(mb_convert_encoding($keyword, 'SJIS-win', 'UTF-8'))."&searchtype=".urlencode(mb_convert_encoding($searchtype, 'SJIS-win', 'UTF-8'))."");
        
        $this->set('layout_free1',$this->get_contents(PORTAL_LAYOUT_FREE1));
        $this->set('layout_free2',$this->get_contents(PORTAL_LAYOUT_FREE2));
        $this->set('layout_free3',$this->get_contents(PORTAL_LAYOUT_FREE3));        
/*        $this->set('layout_free4',$this->get_contents(PORTAL_LAYOUT_FREE4));
        $this->set('layout_free5',$this->get_contents(PORTAL_LAYOUT_FREE5));
        
        $this->set('layout_free6',$this->get_contents(PORTAL_LAYOUT_FREE6));
        $this->set('layout_free7',$this->get_contents(PORTAL_LAYOUT_FREE7));
        $this->set('layout_free8',$this->get_contents(PORTAL_LAYOUT_FREE8));
        $this->set('layout_free9',$this->get_contents(PORTAL_LAYOUT_FREE9));
        $this->set('layout_free10',$this->get_contents(PORTAL_LAYOUT_FREE10));

 */        
    	
    	//var_dump($list);die();
    	
    	
//    	$this->set('result_list',$list);
//    	
//    	//var_dump($result);die();
//    	
//    	$member_list=db_member_talent_mode($keyword); 
//    	//var_dump($member_list);die();
//    	$diary_list=talent_diary_search_result($keyword,20,1);    	
//    	
//    	list($result, $is_prev, $is_next, $total_num, $start_num, $end_num)
//            = db_commu_search_c_commu4c_commu_category($keyword,'',20,1,'','');   
//    	
//    	switch ( $searchtype){
//    		case 1:
//    			$this->set('c_member_list',$member_list);
//    			$this->set('c_diary_list',$diary_list);
//    			break;
//    		case 2:
//    			$this->set('c_commu_search_list', $result);
//    			break;
//    		case 3:
//    			$this->set('c_member_list',$member_list);
//    			$this->set('c_diary_list',$diary_list);
//    			$this->set('c_commu_search_list', $result);
//    			break;
//    		default :
//    			break;
//    	}
//    	

        $this->set('back_coler','111111');
        $this->set('font_color','FFFFFF');
        $this->set('prof_color','EEEEEE');
        $this->set('title_color','333333');
        $this->set('border_color','111111');
        
    	return "success";
    
    }
    
 function get_contents($name)
    {
        $config = array();

        switch ($name) {
        case 'FREE1':
            $config['contents'] = db_free_area_ktai(1);
            break;
        case 'FREE2':
            $config['contents'] = db_free_area_ktai(2);
            break;
        case 'FREE3':
            $config['contents'] = db_free_area_ktai(3);
            break;
        case 'FREE4':
            $config['contents'] = db_free_area_ktai(4);
            break;
        case 'FREE5':
            $config['contents'] = db_free_area_ktai(5);
            break;
        case 'FREE6':
            $config['contents'] = db_free_area_ktai(6);
            break;
        case 'FREE7':
            $config['contents'] = db_free_area_ktai(7);
            break;
        case 'FREE8':
            $config['contents'] = db_free_area_ktai(8);
            break;
        case 'FREE9':
            $config['contents'] = db_free_area_ktai(9);
            break;
        case 'FREE10':
            $config['contents'] = db_free_area_ktai(10);
            break;       
        default:
            $name = null;
            break;
        }

        if (!is_null($name)) {
            $config['kind'] = $name;
        }
       

        return $config;
    } 
}