<?php 
class ktai_page_newsdetail extends OpenPNE_Action
{       
    function execute($requests)
    {
    	$u  = $GLOBALS['KTAI_C_MEMBER_ID'];
		$this->set('u',$u);
    	$newtypenames=array(
			"Main"=>'ﾒｲﾝ',
    		"Entertainment_Trends"=>'ｴﾝﾀﾒ',
    		"National"=>'社会',
    		"Sports"=>'ｽﾎﾟｰﾂ',
    		"R-25"=>'R-25'
    	);
    	
    	$_newtypenames=array(
			"Main"=>'メイン',
    		"Culture/Entertainment"=>'エンタメ',
    		"National"=>'社会',
    		"Sports"=>'スポーツ',
    		"R-25"=>'R-25'
    	);
    	
    	$this->set('title','news detail');
    	$newsid = $requests['newsid'];  
    	$this->set('newsid',$newsid);  	
    	$news=db_news_get_news_by_id($newsid); 
    	
    	if(!$news){
    		openpne_redirect("portal","page_portal_user_top_ktai");
    		die();
    	}   	
    	$this->set('newstypename_ja',$_newtypenames[$news['t_e2_news_genre']]);
    	if($news['t_e2_news_type']=='r25'){
    		$news['t_e2_news_content']=html_entity_decode($news['t_e2_news_content']);
    	}
    	//$this->set('news',$news);
    	$this->set('body',$requests['body']);
    	
    	$comment_list=db_news_comment_list($newsid);
    	$comment_count=count($comment_list);
    	$this->set('comment_count',$comment_count);
    	$this->set('comment_list',$comment_list);
    	
    	$this->set('prev',db_news_get_prev_news($newsid));
    	$this->set('next',db_news_get_next_news($newsid));
    	
    	
    	$this->set('day',$news['t_e2_news_date']);
    	
    	$type = $news['t_e2_news_genre'];
    	
    	$this->set('newtypenames',$newtypenames);
    	$this->set('type',$news['t_e2_news_genre']);
    	$this->set('nownewtypenames',$news['t_e2_news_genre']);
    	if(mb_strlen(strip_tags($news['t_e2_news_content']),"utf8")>270){
    		$this->set('show_more',true);
    		$news['t_e2_news_content']= mb_substr($news['t_e2_news_content'],0,270)."...";
    		$this->set('news',$news);
    	}else{
    		$this->set('show_more',false);
    		$this->set('news',$news);
    	}
    	//var_dump(mb_strlen(strip_tags($news['t_e2_news_content']),"utf8"));die();
    	
    	$layout_frees=array(
			"layout_free2"=>$this->get_contents(PORTAL_LAYOUT_FREE2),
			"layout_free4"=>$this->get_contents(PORTAL_LAYOUT_FREE4),
			"layout_free13"=>$this->get_contents(PORTAL_LAYOUT_FREE13),		
    	    "layout_free30"=>$this->get_contents(PORTAL_LAYOUT_FREE30),			
		);
		$regex[url] = '((http|https):\/\/)?(beamie\.jp|ssl\.beamie\.jp)\/[^"\']*|\.\/[^"\']*|\/[^"\']*'; 
		foreach ($layout_frees as $key=>$item){			
			$item["contents"]["html"]=preg_replace_callback("/<a\s+(.*)href=([\"'])($regex[url])([\"'])([^>]*)>/","regex_ktai_session_callback",$item["contents"]["html"]);
			$this->set($key,$item);
		}
    	
    	return 'success';  
    	
    }
    
	function get_contents($name)
    {
        $config = array();

        switch ($name) {
        case 'FREE1':
            $config['contents'] = db_portal_portal_free_area_ktai(1);
            break;
        case 'FREE2':
            $config['contents'] = db_portal_portal_free_area_ktai(2);
            break;
        case 'FREE3':
            $config['contents'] = db_portal_portal_free_area_ktai(3);
            break;
        case 'FREE4':
            $config['contents'] = db_portal_portal_free_area_ktai(4);
            break;
        case 'FREE5':
            $config['contents'] = db_portal_portal_free_area_ktai(5);
            break;
        case 'FREE6':
            $config['contents'] = db_portal_portal_free_area_ktai(6);
            break;
        case 'FREE7':
            $config['contents'] = db_portal_portal_free_area_ktai(7);
            break;
        case 'FREE8':
            $config['contents'] = db_portal_portal_free_area_ktai(8);
            break;
        case 'FREE9':
            $config['contents'] = db_portal_portal_free_area_ktai(9);
            break;
        case 'FREE10':
            $config['contents'] = db_portal_portal_free_area_ktai(10);
            break;
        case 'FREE11':
            $config['contents'] = db_portal_portal_free_area_ktai(11);
            break;
        case 'FREE12':
            $config['contents'] = db_portal_portal_free_area_ktai(12);
            break;
        case 'FREE13':
            $config['contents'] = db_portal_portal_free_area_ktai(13);
            break; 
        case 'FREE14':
            $config['contents'] = db_portal_portal_free_area_ktai(14);
            break;
        case 'FREE15':
            $config['contents'] = db_portal_portal_free_area_ktai(15);
            break;
        case 'FREE16':
            $config['contents'] = db_portal_portal_free_area_ktai(16);
            break;
        case 'FREE17':
            $config['contents'] = db_portal_portal_free_area_ktai(17);
            break;
        case 'FREE18':
            $config['contents'] = db_portal_portal_free_area_ktai(18);
            break; 
        case 'FREE19':
            $config['contents'] = db_portal_portal_free_area_ktai(19);
            break;
        case 'FREE20':
            $config['contents'] = db_portal_portal_free_area_ktai(20);
            break;
        case 'FREE21':
            $config['contents'] = db_portal_portal_free_area_ktai(21);
            break;
        case 'FREE22':
            $config['contents'] = db_portal_portal_free_area_ktai(22);
            break;
        case 'FREE23':
            $config['contents'] = db_portal_portal_free_area_ktai(23);
            break; 
        case 'FREE24':
            $config['contents'] = db_portal_portal_free_area_ktai(24);
            break;
        case 'FREE25':
            $config['contents'] = db_portal_portal_free_area_ktai(25);
            break;
        case 'FREE26':
            $config['contents'] = db_portal_portal_free_area_ktai(26);
            break;
        case 'FREE27':
            $config['contents'] = db_portal_portal_free_area_ktai(27);
            break;
        case 'FREE28':
            $config['contents'] = db_portal_portal_free_area_ktai(28);
            break; 
        case 'FREE29':
            $config['contents'] = db_portal_portal_free_area_ktai(29);
            break;
        case 'FREE30':
            $config['contents'] = db_portal_portal_free_area_ktai(30);
            break;
        default:
            $name = null;
            break;
        }

        if (!is_null($name)) {
            $config['kind'] = $name;
        }
        $text=trim($config['contents']['html']);
		$php_pattern='/\<\?php(.*?)\?\>/s';
    	$text=preg_replace_callback($php_pattern, 'php_eval_callback2', $text);
    	
    	$regex[url] = '((http|https):\/\/)?(beamie\.jp|ssl\.beamie\.jp)\/[^"\']*|\.\/[^"\']*|\/[^"\']*'; 
        $text=preg_replace_callback("/<a\s+(.*)href=([\"'])($regex[url])([\"'])([^>]*)>/","regex_ktai_session_callback",$text); 
	    $config['contents']['html']=$text;
        return $config;
    }
}