<?php
class ktai_page_h_talent_search extends OpenPNE_Action
{
    function execute($requests)
    {
    	$this->set('sex_list',db_profile_option_list("sex"));
    	$this->set('blood_type_list',db_profile_option_list("blood_type"));
    	$this->set('old_addr_pref_list',db_profile_option_list("old_addr_pref"));
    	$this->set('error_msg',$_REQUEST['error_msg']);
    	
    	$t = getdate();
    	$cur_year = $t['year'];
    	$arr_year = array ();
    	$arr_year[] = '選択してください';
    	for($i=1930;$i<=$cur_year;$i++){
    		$arr_year[$i] = $i;
    	}
    	$arr_month = array();
    	$arr_month[]  = '選択してください';
    	for($i=1;$i<=12;$i++){
    		$arr_month[$i] = $i;
    	}
    	$arr_day = array();
    	$arr_day[]  = '選択してください';
    	for($i=1;$i<=31;$i++){
    		$arr_day[$i] = $i;
    	}
    	
    	$this->set('arr_year',$arr_year);
    	$this->set('arr_month',$arr_month);
    	$this->set('arr_day',$arr_day);
    	
    	
    	//print_r($_REQUEST);
    	return "success";    
    }
}