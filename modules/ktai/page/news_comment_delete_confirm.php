<?php 
class ktai_page_news_comment_delete_confirm extends OpenPNE_Action
{
       
    function execute($requests)
    {
    	$u  = $GLOBALS['KTAI_C_MEMBER_ID'];
    	
    	$comment_id = $this->requests['target_c_comment_id'];
    	
    	
    	$comment = db_news_get_comment($comment_id);
    	$comment['c_member']=db_member_c_member4c_member_id($u,false,false,'private');
    	if($comment && $comment['c_member_id']== $u){
    		$comment['c_member']=db_member_c_member4c_member_id($u,false,false,'private');
    		$this->set('target_news_comment',$comment);
    	}else{
    		openpne_redirect("portal", "page_portal_user_top_ktai");
    	}
    	return 'success';   	
    	
    }
}