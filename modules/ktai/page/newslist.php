<?php 
class ktai_page_newslist extends OpenPNE_Action
{       
    function execute($requests)
    {
    	$newtypenames=array(
			"Main"=>'ﾒｲﾝ',
    		"Entertainment_Trends"=>'ｴﾝﾀﾒ',
    		"National"=>'社会',
    		"Sports"=>'ｽﾎﾟｰﾂ',
    		"R-25"=>'R-25'
    	);
    	
    	$_newtypenames = array(
			"Main"=>'メイン',
    		"Culture/Entertainment"=>'エンタメ',
    		"National"=>'社会',
    		"Sports"=>'スポーツ',
    		"R-25"=>'Ｒ－２５'
    	);
    	$this->set('newtypenames',$newtypenames);
    	
    	$this->set('title','news list');
    	$type=$requests['type'];
    	if(!$type){
    		$type="Main";
    	}
    	$this->set('type',$type);
    	
    	if($type == 'Entertainment_Trends'){
    		$type = "Culture/Entertainment";
    		
    		$sub='culture/entertainment';
    		 		
    	}elseif($type == 'National'){
    		$sub='national';
    	}elseif($type=='Sports'){
    		//$sub='sports';
    	}elseif($type=='R-25'){
    		$sub='';
    	}elseif($type == 'Main'){
    		$sub='';
    	}else{
    		die();
    	} 
    	
    	
    	
    	$this->set('typename_ja',$_newtypenames[$type]);
    	$day = $requests['day'];
    	
    	
		$days=array(
    		date("Y-m-d",time()),
    		date("Y-m-d",strtotime('-1 day')),
    		date("Y-m-d",strtotime('-2 day')),
    		date("Y-m-d",strtotime('-3 day')),
    		date("Y-m-d",strtotime('-4 day')),
    		date("Y-m-d",strtotime('-5 day')),
    		date("Y-m-d",strtotime('-6 day')),
    		date("Y-m-d",strtotime('-7 day')),
    	);

		foreach ($days as $key=> $d){
    		if(db_news_get_count_by_type($type,$sub,$d)>0 ){
    		}else{
    			unset ( $days[$key] ); 
    		}
    	} 
    	if($days){
	    	if(!$day){
	    		$day=reset($days);
	    	}
    	}else{
	    	if(!$day){
	    		$day=date('Y-m-d',time());
	    	}
    	}
		$this->set('show_day',$day);
    	$this->set('days',$days);
    	
    	$this->set('day',$day);
    	$this->set("current_day",date('Y-m-d',time()));
    	
    	

    	$page = $this->requests['page'];
    	$this->set('page',$page);
    	
    	
    	
    	$top_news = db_news_get_by_type_with_pic($type,$sub,$day);
    	
    	if($page == '1' && $top_news){
    		$this->set('top_news',$top_news);
    	}
    	
    	
    	$page_list=db_news_ktai_get_ty_type_list($type, $sub,$day, $top_news['t_e2_news_contents_id'], $page);
    	$this->set('newslist',$page_list);
    	
    	
    	
     
		$layout_frees=array(
			"layout_free2"=>$this->get_contents(PORTAL_LAYOUT_FREE2),
			"layout_free4"=>$this->get_contents(PORTAL_LAYOUT_FREE4),
			"layout_free13"=>$this->get_contents(PORTAL_LAYOUT_FREE13),	
			"layout_free30"=>$this->get_contents(PORTAL_LAYOUT_FREE30),		
		);
		$regex[url] = '((http|https):\/\/)?(beamie\.jp|ssl\.beamie\.jp)\/[^"\']*|\.\/[^"\']*|\/[^"\']*'; 
		foreach ($layout_frees as $key=>$item){			
			$item["contents"]["html"]=preg_replace_callback("/<a\s+(.*)href=([\"'])($regex[url])([\"'])([^>]*)>/","regex_ktai_session_callback",$item["contents"]["html"]);
			$this->set($key,$item);
		}
    			
    	return 'success';  
    	
    }
    
    
    
	function get_contents($name)
    {
        $config = array();

        switch ($name) {
        case 'FREE1':
            $config['contents'] = db_portal_portal_free_area_ktai(1);
            break;
        case 'FREE2':
            $config['contents'] = db_portal_portal_free_area_ktai(2);
            break;
        case 'FREE3':
            $config['contents'] = db_portal_portal_free_area_ktai(3);
            break;
        case 'FREE4':
            $config['contents'] = db_portal_portal_free_area_ktai(4);
            break;
        case 'FREE5':
            $config['contents'] = db_portal_portal_free_area_ktai(5);
            break;
        case 'FREE6':
            $config['contents'] = db_portal_portal_free_area_ktai(6);
            break;
        case 'FREE7':
            $config['contents'] = db_portal_portal_free_area_ktai(7);
            break;
        case 'FREE8':
            $config['contents'] = db_portal_portal_free_area_ktai(8);
            break;
        case 'FREE9':
            $config['contents'] = db_portal_portal_free_area_ktai(9);
            break;
        case 'FREE10':
            $config['contents'] = db_portal_portal_free_area_ktai(10);
            break;
        case 'FREE11':
            $config['contents'] = db_portal_portal_free_area_ktai(11);
            break;
        case 'FREE12':
            $config['contents'] = db_portal_portal_free_area_ktai(12);
            break;
        case 'FREE13':
            $config['contents'] = db_portal_portal_free_area_ktai(13);
            break; 
        case 'FREE14':
            $config['contents'] = db_portal_portal_free_area_ktai(14);
            break;
        case 'FREE15':
            $config['contents'] = db_portal_portal_free_area_ktai(15);
            break;
        case 'FREE16':
            $config['contents'] = db_portal_portal_free_area_ktai(16);
            break;
        case 'FREE17':
            $config['contents'] = db_portal_portal_free_area_ktai(17);
            break;
        case 'FREE18':
            $config['contents'] = db_portal_portal_free_area_ktai(18);
            break; 
        case 'FREE19':
            $config['contents'] = db_portal_portal_free_area_ktai(19);
            break;
        case 'FREE20':
            $config['contents'] = db_portal_portal_free_area_ktai(20);
            break;
        case 'FREE21':
            $config['contents'] = db_portal_portal_free_area_ktai(21);
            break;
        case 'FREE22':
            $config['contents'] = db_portal_portal_free_area_ktai(22);
            break;
        case 'FREE23':
            $config['contents'] = db_portal_portal_free_area_ktai(23);
            break; 
        case 'FREE24':
            $config['contents'] = db_portal_portal_free_area_ktai(24);
            break;
        case 'FREE25':
            $config['contents'] = db_portal_portal_free_area_ktai(25);
            break;
        case 'FREE26':
            $config['contents'] = db_portal_portal_free_area_ktai(26);
            break;
        case 'FREE27':
            $config['contents'] = db_portal_portal_free_area_ktai(27);
            break;
        case 'FREE28':
            $config['contents'] = db_portal_portal_free_area_ktai(28);
            break; 
        case 'FREE29':
            $config['contents'] = db_portal_portal_free_area_ktai(29);
            break;
        case 'FREE30':
            $config['contents'] = db_portal_portal_free_area_ktai(30);
            break;
        default:
            $name = null;
            break;
        }

        if (!is_null($name)) {
            $config['kind'] = $name;
        }
        $text=trim($config['contents']['html']);
		$php_pattern='/\<\?php(.*?)\?\>/s';
    	$text=preg_replace_callback($php_pattern, 'php_eval_callback2', $text);
    	
    	$regex[url] = '((http|https):\/\/)?(beamie\.jp|ssl\.beamie\.jp)\/[^"\']*|\.\/[^"\']*|\/[^"\']*'; 
        $text=preg_replace_callback("/<a\s+(.*)href=([\"'])($regex[url])([\"'])([^>]*)>/","regex_ktai_session_callback",$text); 
	    $config['contents']['html']=$text;
        return $config;
    }
}