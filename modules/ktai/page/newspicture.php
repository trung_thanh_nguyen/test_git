<?php 
class ktai_page_newspicture extends OpenPNE_Action
{       
    function execute($requests)
    {
    	
    	$newtypenames=array(
			"Main"=>'ﾒｲﾝ',
    		"Entertainment_Trends"=>'ｴﾝﾀﾒ',
    		"National"=>'社会',
    		"Sports"=>'ｽﾎﾟｰﾂ',
    		"R-25"=>'R-25'
    	);
    	
    	$this->set('title','news picture');
    	$newsid = $requests['newsid'];  
    	$this->set('newsid',$newsid);  	
    	$news=db_news_get_news_by_id($newsid);    	
    	$this->set('newstypename_ja',$newtypenames[$news['t_e2_news_genre']]);
    	
    	$this->set('news',$news);
    	
    	$this->set('day',$news['t_e2_news_date']);
    	foreach ($newtypenames as $key=>$value){
    		if($key== $news['t_e2_news_genre']){
    			unset($newtypenames[$key]);
    		}
    	}
    	$this->set('newtypenames',$newtypenames);
    	
    	$layout_frees=array(
			"layout_free2"=>$this->get_contents(PORTAL_LAYOUT_FREE2),
			"layout_free4"=>$this->get_contents(PORTAL_LAYOUT_FREE4),
			"layout_free13"=>$this->get_contents(PORTAL_LAYOUT_FREE13),			
		);
		$regex[url] = '((http|https):\/\/)?(beamie\.jp|ssl\.beamie\.jp)\/[^"\']*|\.\/[^"\']*|\/[^"\']*'; 
		foreach ($layout_frees as $key=>$item){			
			$item["contents"]["html"]=preg_replace_callback("/<a\s+(.*)href=([\"'])($regex[url])([\"'])([^>]*)>/","regex_ktai_session_callback",$item["contents"]["html"]);
			$this->set($key,$item);
		}
    	
    	return 'success';  
    	
    }
    
	function get_contents($name)
    {
        $config = array();

        switch ($name) {
        case 'FREE1':
            $config['contents'] = db_portal_portal_free_area_ktai(1);
            break;
        case 'FREE2':
            $config['contents'] = db_portal_portal_free_area_ktai(2);
            break;
        case 'FREE3':
            $config['contents'] = db_portal_portal_free_area_ktai(3);
            break;
        case 'FREE4':
            $config['contents'] = db_portal_portal_free_area_ktai(4);
            break;
        case 'FREE5':
            $config['contents'] = db_portal_portal_free_area_ktai(5);
            break;
        case 'FREE6':
            $config['contents'] = db_portal_portal_free_area_ktai(6);
            break;
        case 'FREE7':
            $config['contents'] = db_portal_portal_free_area_ktai(7);
            break;
        case 'FREE8':
            $config['contents'] = db_portal_portal_free_area_ktai(8);
            break;
        case 'FREE9':
            $config['contents'] = db_portal_portal_free_area_ktai(9);
            break;
        case 'FREE10':
            $config['contents'] = db_portal_portal_free_area_ktai(10);
            break;
        case 'FREE11':
            $config['contents'] = db_portal_portal_free_area_ktai(11);
            break;
        case 'FREE12':
            $config['contents'] = db_portal_portal_free_area_ktai(12);
            break;
        case 'FREE13':
            $config['contents'] = db_portal_portal_free_area_ktai(13);
            break; 
        case 'FREE14':
            $config['contents'] = db_portal_portal_free_area_ktai(14);
            break;
        case 'FREE15':
            $config['contents'] = db_portal_portal_free_area_ktai(15);
            break;
        case 'FREE16':
            $config['contents'] = db_portal_portal_free_area_ktai(16);
            break;
        case 'FREE17':
            $config['contents'] = db_portal_portal_free_area_ktai(17);
            break;
        case 'FREE18':
            $config['contents'] = db_portal_portal_free_area_ktai(18);
            break; 
        case 'FREE19':
            $config['contents'] = db_portal_portal_free_area_ktai(19);
            break;
        case 'FREE20':
            $config['contents'] = db_portal_portal_free_area_ktai(20);
            break;
        default:
            $name = null;
            break;
        }

        if (!is_null($name)) {
            $config['kind'] = $name;
        }
        $text=trim($config['contents']['html']);
		$php_pattern='/\<\?php(.*?)\?\>/s';
    	$text=preg_replace_callback($php_pattern, 'php_eval_callback2', $text);
    	
    	$regex[url] = '((http|https):\/\/)?(beamie\.jp|ssl\.beamie\.jp)\/[^"\']*|\.\/[^"\']*|\/[^"\']*'; 
        $text=preg_replace_callback("/<a\s+(.*)href=([\"'])($regex[url])([\"'])([^>]*)>/","regex_ktai_session_callback",$text); 
	    $config['contents']['html']=$text;
        return $config;
    }
}