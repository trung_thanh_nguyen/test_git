<?php
class ktai_page_h_talent_list extends OpenPNE_Action
{
    function execute($requests)
    {
    	$limit=PORTAL_SEARCH_PAGE_SIZE;//page size
    	$page=$requests['page'];
    	//print_r($requests);
    	$input_cond = false; 
    	if(strlen(trim($requests["talent_company"]))>0){
    		$input_cond = true;
    	}
    	if($requests["age1"]){
    		$input_cond = true;
    	}
    	if($requests["age2"]){
    		$input_cond = true;
    	}
    	if($requests['start_year']){
    		$input_cond = true;
    	}
    	if($requests['end_year']){
    		$input_cond = true;
    	}
    	if($requests["sex"]){
    		$input_cond = true;
    	}
    	if($requests["blood_type"]){
    		$input_cond = true;
    	}
    	if($requests["talent_height1"]){
    		$input_cond = true;
    	}
    	if($requests["talent_height2"]){
    		$input_cond = true;
    	}
    	if($requests["talent_bust1"]){
    		$input_cond = true;
    	}
    	if($requests["talent_bust2"]){
    		$input_cond = true;
    	}
    	if($requests["talent_waistline1"]){
    		$input_cond = true;
    	}
    	if($requests["talent_waistline2"]>0){
    		$input_cond = true;
    	}
    	if($requests["talent_hip1"]){
    		$input_cond = true;
    	}
    	if($requests["talent_hip2"]){
    		$input_cond = true;
    	}
    	if($requests["talent_shoe1"]){
    		$input_cond = true;
    	}
    	if($requests["talent_shoe2"]){
    		$input_cond = true;
    	}
    	if($requests["old_addr_pref"]){
    		$input_cond = true;
    	}
    	if(strlen(trim($requests["talent_interest"]))>0){
    		$input_cond = true;
    	}
    	if(strlen(trim($requests["talent_qualification"]))>0){
    		$input_cond = true;
    	}
    	if(strlen(trim($requests["talent_sport"]))>0){
    		$input_cond = true;
    	}
    	if(strlen(trim($requests["talent_speciality"]))>0){
    		$input_cond = true;
    	}

    	if(!$input_cond){
    		//$this->set('error_msg','検索条件を入力してください。');
    		$_REQUEST['error_msg'] = '検索条件を入力してください。';
    		openpne_forward("ktai",'page','h_talent_search');
    		exit();
    	}
    	$result=db_member_talent_list($requests,$page,$limit);
    	
    	$list=$result[0];
    	//var_dump($list);die();
        $this->set('search_list',$list);
    	$pager = array(
            "page_prev" => $result[1],
            "page_next" => $result[2],
            "total_num" => $result[3],
        );
    	$pager["disp_start"] = $limit * ($page - 1) + 1;
        if (($disp_end  = $limit * $page) > $pager['total_num']) {
            $pager['disp_end'] = $pager['total_num'];
        } else {
            $pager['disp_end'] = $disp_end;
        }
        $this->set("pager", $pager); 
        $search_confition="";
        foreach ($requests as $key=>$value){
        	if($value){
        		$search_confition .= "&{$key}=".urlencode(mb_convert_encoding($value,'SJIS-win', 'UTF-8'));
        	}
        }
        $this->set('search_condition',$search_confition);      
        
    	return "success";    
    }
}