<?php
class ktai_page_audition extends OpenPNE_Action
{
    function execute($requests)
    {
    	if(ISGUEST==true){
			openpne_redirect("ktai", "page_o_login",array("login_params"=>"m=ktai&a=page_audition"));
			exit();
		}
    	$u = $GLOBALS['AUTH']->uid(); 
    	$member=db_member_c_member4c_member_id($u,true,false);
    	if($requests['email_login']==""){
    		$requests['email_login']=$member['secure']['regist_address'];
    		$requests['email_login_check']=$member['secure']['regist_address'];
    	} 
    	if($requests['login_nickname']==""){
    		$requests['login_nickname']=$member['nickname'];
    	}	
    	
    	$this->set('form',$requests);     	  	
    	
    	$years=array();
    	$current_year=Date("Y");
    	for($i=1931;$i<=$current_year;$i++){
    		$years[]=$i;
    	}
    	$months=array();
    	for($i=1;$i<=12;$i++){
    		$months[]=$i;
    	}
    	$days=array();
    	for($i=1;$i<=31;$i++){
    		$days[]=$i;
    	}
    	$this->set('years',$years);
    	$this->set('months',$months);
    	$this->set('days',$days);
    	
    	$pref=array(
    	'北海道',
    	'青森県',
    	'岩手県',
    	'宮城県',
    	'秋田県',
    	'山形県',
    	'福島県',
    	'茨城県',
    	'栃木県',
    	'群馬県',
    	'埼玉県',
    	'千葉県',
    	'東京都',
    	'神奈川県',
    	'新潟県',
    	'富山県',
    	'石川県',
    	'福井県',
    	'山梨県',
    	'長野県',
    	'岐阜県',
    	'静岡県',
    	'愛知県',
    	'三重県',
    	'滋賀県',
    	'京都府',
    	'大阪府',
    	'兵庫県',
    	'奈良県',
    	'和歌山県',
    	'鳥取県',
    	'島根県',
    	'岡山県',
    	'広島県',
    	'山口県',
    	'徳島県',
    	'香川県',
    	'愛媛県',
    	'高知県',
    	'福岡県',
    	'佐賀県',
    	'長崎県',
    	'熊本県',
    	'大分県',
    	'宮崎県',
    	'鹿児島県',
    	'沖縄県',
    	);
    	$this->set('pref',$pref);
    	$department = array("女優","男性俳優","タレント","キャスター・レポーター","バラエティ","お笑い","ファッションモデル","CM・雑誌モデル","キャンペーンモデル","男性モデル","子供モデル","シルバーモデル","パーツモデル");
    	$this->set("department",$department);
    	
    	return 'success';
    	
    
    }
}