<?php

require_once OPENPNE_MODULES_DIR . '/portal/lib/db_portal.php';

class ktai_page_o_free_area extends OpenPNE_Action
{
    function isSecure()
    {
        return false;
    }

    function execute($requests)
    {
    	if($requests['free_name']){
    		$portal_free_area = db_portal_portal_free_area_ktai_by_name($requests['free_name']);
    	}else{
	    	$c_free_area_id=$requests['c_free_area_id'];
	    	if($c_free_area_id<21 || $c_free_area_id>30){
	    		$c_free_area_id=21; //default 21
	    	}        
	        $portal_free_area = db_portal_portal_free_area_ktai($c_free_area_id);
    	}
        
        
        if(!$portal_free_area){
        	$this->set("msg", "指定されたフリーページは存在しません");
        }else{
        	$regex[url] = '((http|https):\/\/)?(beamie\.jp|ssl\.beamie\.jp)\/[^"\']*|\.\/[^"\']*|\/[^"\']*'; 
        	$portal_free_area['html']=preg_replace_callback("/<a\s+(.*)href=([\"'])($regex[url])([\"'])([^>]*)>/","regex_ktai_session_callback",$portal_free_area['html']); 
        	
        	$text=trim($portal_free_area['html']);
		    $php_pattern='/\<\?php(.*?)\?\>/s';
    	    $text=preg_replace_callback($php_pattern, 'php_eval_callback2', $text);
    	    $portal_free_area['html']=$text;
        	
        	$this->set("portal_free_area", $portal_free_area);
            $this->set("title", $portal_free_area['name']);
        }
        return 'success';
    }
   
}