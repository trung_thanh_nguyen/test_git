<?php
class ktai_page_audition_girl_confirm extends OpenPNE_Action
{
 	function handleError($errors)
    {              
        openpne_forward('ktai', 'page', 'audition_girl', $errors);
        exit;
    }
    
    function execute($requests)
    {
    	if(ISGUEST==true){    		
    		openpne_redirect("portal", "page_portal_user_top_ktai");
    		exit();
    	}
    	
    	$errors=$this->checkformerror($requests);    	
    	if($errors){    		
    		openpne_forward("ktai", "page","audition_girl",$errors); 
    		exit();		
    	}
    		
    	$u = $GLOBALS['AUTH']->uid(); 
    	$fields=array("msg","msg1","msg2","msg3","sessid","message");
    	$formval=$requests;
    	$sessid = session_id();
    	foreach ($fields as $value){
    		unset($formval[$value]);
    	}
    	
    	$data = array('member_id'=>$u,'ext_data'=>serialize($formval),'status'=>'0','adddate'=>date("Y-m-d H:i:s"));
    	$audition_id=db_audition_gc_insert_audition($data);
    	$formval['audition_id']=$audition_id;
    	
        
        
    	$this->set('form',$formval);    	
    	return 'success';
    
    }
    
    function checkformerror($formval){
    	$errors=array();
    	if($formval['name_1']==""){ 
    		$errors[]="姓を入力してください。";    		
    	}
    	if($formval['name_2']==""){
    		$errors[]="名を入力してください。";
    	}
    	if($formval['kananame_1']==""){
    		$errors[]="ｾｲを入力してください。";
    	}
    	if($formval['kananame_2']=="" ){
    		$errors[]="ﾒｲを入力してください。";
    	}
    	if($formval['birthday_year']==""){
    		$errors[]="生年を入力してください。";
    	}
    	if($formval['birthday_month']==""){
    		$errors[]="生月を入力してください。";
    	}
    	if($formval['birthday_day']==""){
    		$errors[]="生日を入力してください。";
    	}
    	if($formval['age']==""){
    		$errors[]="年齢を入力してください。";
    	} else
    	if(is_numeric($formval['age'])==false){
    		$errors[]="年齢は半角数字で入力してください";
    	}
    	if($formval['zip']==""){
    		$errors[]="郵便番号を入力してください。";
    	} else
    	if(is_numeric($formval['zip'])==false){
    		$errors[]="郵便番号は半角数字で入力してください。";
    	}
    	if($formval['address_pref']==""){
    		$errors[]="都道府県を入力してください。";
    	}
    	if($formval['address_1']==""){
    		$errors[]="市区町村・番地を入力してください。";
    	}
//    	if($formval['address_2']==""){
//    		$errors[]="建物名・号室 を入力してください。";
//    	}
    	
    	if($formval['tel_1']=="" || $formval['tel_2']=="" || $formval['tel_3']=="" ){
    		$errors[]="ご連絡先の電話番号を入力してください。";
    	} else
    	if((is_numeric($formval['tel_1'])==false && $formval['tel_1']=="") ||
    		(is_numeric($formval['tel_2'])==false && $formval['tel_2']=="") ||
    		(is_numeric($formval['tel_3'])==false && $formval['tel_3']=="")){
    		$errors[]="ご連絡先の電話番号は半角数字で入力してください。";
    	}
//    	if($formval['mobile_tel_1']=="" || $formval['mobile_tel_2']=="" || $formval['mobile_tel_3']=="" ){
//    		$errors[]="mobile を入力してください。";
//    	}
    	if($formval['email']=="" || db_common_is_mailaddress($formval['email'])==false){
    		$errors[]="ﾒｰﾙｱﾄﾞﾚｽを入力してください。";
    	}    	
    	if($formval['email_check']==""){
    		$errors[]="ﾒｰﾙｱﾄﾞﾚｽを正しく入力してください。";
    	}
    	if($formval['email']!=$formval['email_check']){
    		$errors[]="ﾒｰﾙｱﾄﾞﾚｽ(確認)欄とﾒｰﾙｱﾄﾞﾚｽ欄に同じ内容を入力してください。";
    	}
    	if($formval['education']==""){
    		$errors[]="学校名を入力してください。";
    	}
    	if($this->utf8_strlen($formval['education'])>48){
    		$errors[]="学校名は24文字まで入力してください。";
    	}
    	
    	if($formval['speciality']==""){
    		$errors[]="特技を入力してください。";
    	}
        if($formval['interest']==""){
    		$errors[]="趣味を入力してください。";
    	}
    	
    	if($formval['owner_PR']==""){
    		$errors[]="自己PRを入力してください。";
    	}
    	
        if($formval['wish_department']==""){
    		$errors[]="将来の希望を入力してください。";
    	}
    	if($formval['body_1']==""){
    		$errors[]="身長を入力してください。";
    	} else
    	if(is_numeric($formval['body_1'])==false){
    		$errors[]="身長は半角数字で入力してください。";
    	}
        if($formval['body_2']==""){
    		$errors[]="ﾊﾞｽﾄを入力してください。";
    	} else
    	if(is_numeric($formval['body_2'])==false){
    		$errors[]="ﾊﾞｽﾄは半角数字で入力してください。";
    	}
    	if($formval['body_3']==""){
    		$errors[]="ｳｪｽﾄを入力してください。";
    	} else
    	if(is_numeric($formval['body_3'])==false){
    		$errors[]="ｳｪｽﾄは半角数字で入力してください。";
    	}
        if($formval['body_4']==""){
    		$errors[]="ﾋｯﾌﾟを入力してください。";
    	} else
    	if(is_numeric($formval['body_4'])==false){
    		$errors[]="ﾋｯﾌﾟは半角数字で入力してください。";
    	}
    	if($formval['body_5']==""){
    		$errors[]="ｼｭｰｽﾞのサイズを入力してください。";
    	} else
    	if(is_numeric($formval['body_5'])==false){
    		$errors[]="ｼｭｰｽﾞは半角数字で入力してください。";
    	}
//        if($_FILES['photo_filename_1']==""){
//    		$errors[]="写真1を入力してください 。";
//    	}
//    	if($_FILES['photo_filename_2']==""){
//    		$errors[]="写真2を入力してください。 ";
//    	}
//        if($_FILES['photo_filename_3']==""){
//    		$errors[]="写真3を入力してください。";
//    	}
    	if($formval['email_login']=="" || db_common_is_mailaddress($formval['email_login'])==false){
    		$errors[]="be amie登録情報のﾒｰﾙｱﾄﾞﾚｽを入力してください。";
    	}    	
        if($formval['email_login_check']==""){
    		$errors[]="be amie登録情報のﾒｰﾙｱﾄﾞﾚｽを正しく入力してください。";
    	}
    	if($formval['email_login_check']!=$formval['email_login']){
    		$errors[]="be amie登録情報のﾒｰﾙｱﾄﾞﾚｽ(確認)欄とﾒｰﾙｱﾄﾞﾚｽ欄に同じ内容を入力してください。";
    	}
    	if($this->utf8_strlen($formval['speciality'])>40){
    		$errors[]="特技は20文字まで入力してください。";
    	}
    	if($this->utf8_strlen($formval['interest'])>40){
    		$errors[]="趣味は20文字まで入力してください。";
    	}
    	if($this->utf8_strlen($formval['owner_PR'])>300){
    		$errors[]="自己PRは150文字まで入力してください。";
    	}
    	if($this->utf8_strlen($formval['family_f'])>34 ||
    		$this->utf8_strlen($formval['family_m'])>34 ||
    		$this->utf8_strlen($formval['family_b'])>34 ||
    		$this->utf8_strlen($formval['family_yb'])>34
    	){
    		$errors[]="家族構成の氏名は17文字まで入力してください。";
    	}
	    $now = date('Ymd');
	    $birthday = sprintf('%04d%02d%02d', $formval['birthday_year'], $formval['birthday_month'], $formval['birthday_day']);
	    $age = floor(($now-$birthday)/10000);

    	if($formval['allow']!="1" && $age<20){
    		$errors[] = "保護者の同意が必要です";
    	}
    	if($formval['consent']!="1"){
    		$errors[] = "ﾌﾟﾗｲﾊﾞｼｰﾎﾟｼﾘｰおよび注意事項に同意が必要です";
    	}
    	return $errors;
    }
    
    function utf8_strlen($str) {
    	$count = 0;
    	for($i = 0; $i < strlen($str); $i++){
    		$value = ord($str[$i]);
    		if($value > 127) {
    			$count++;
    			if($value >= 192 && $value <= 223) $i++;
    			elseif($value >= 224 && $value <= 239) $i = $i + 2;
    			elseif($value >= 240 && $value <= 247) $i = $i + 3;
    			else die('not a utf-8 compatible string');
    		}
    		$count++;
    	}
    	return $count;
    }
    
 
    
}