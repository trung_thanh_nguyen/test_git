<?php
class ktai_do_api_notice_report extends OpenPNE_Action
{
    function execute($requests)
    {
     	$u  = $GLOBALS['KTAI_C_MEMBER_ID'];
     	
     	
     	//$url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
   
    	//send mail 
    	$params=array();
    	$params = $requests;
    	fetch_send_mail(API_RECEIVE_REPORT_EMAIL,"m_ktai_api_receive_report",$params);
    	
    	$type=$requests['type'];
    	$oid = $requests['oid'];
    	$cdata=$requests['message'];
    	switch ($type){
    		case "diary":
    			$obj=db_diary_get_c_diary4id($oid);
    			$subject=$obj['subject'];
    			$body=$obj['body'];
    			$body=CallApi::boby2html($body, $type, $u);
    			if($obj['image_filename_3']){
    				$body=" <img src='".OPENPNE_URL."/img.php?m=pc&filename=".$obj['image_filename_3']."' />".$body;
    			}
    			if($obj['image_filename_2']){
    				$body=" <img src='".OPENPNE_URL."/img.php?m=pc&filename=".$obj['image_filename_2']."' />".$body;
    			}
    			if($obj['image_filename_1']){
    				$body=" <img src='".OPENPNE_URL."/img.php?m=pc&filename=".$obj['image_filename_1']."' />".$body;
    			}
    			break;
    		case "diary_comment":
    			$obj=_do_c_diary_comment4c_diary_comment_id($oid);
    			$subject="";
    			$body=$obj['body'];
    			$body=CallApi::boby2html($body, $type, $u);
    			if($obj['image_filename_3']){
    				$body=" <img src='".OPENPNE_URL."/img.php?m=pc&filename=".$obj['image_filename_3']."' />".$body;
    			}
    			if($obj['image_filename_2']){
    				$body=" <img src='".OPENPNE_URL."/img.php?m=pc&filename=".$obj['image_filename_2']."' />".$body;
    			}
    			if($obj['image_filename_1']){
    				$body=" <img src='".OPENPNE_URL."/img.php?m=pc&filename=".$obj['image_filename_1']."' />".$body;
    			}
    			break;
    		case "topic":
    		case "event":
    			$obj=db_commu_c_commu_topic4c_commu_topic_id($oid);
    			$subject=$obj['name'];
    			$body=$obj['body'];
    			$body=CallApi::boby2html($body, "community", $u);
    			if($obj['image_filename_3']){
    				$body=" <img src='".OPENPNE_URL."/img.php?m=pc&filename=".$obj['image_filename_3']."' />".$body;
    			}
    			if($obj['image_filename_2']){
    				$body=" <img src='".OPENPNE_URL."/img.php?m=pc&filename=".$obj['image_filename_2']."' />".$body;
    			}
    			if($obj['image_filename_1']){
    				$body=" <img src='".OPENPNE_URL."/img.php?m=pc&filename=".$obj['image_filename_1']."' />".$body;
    			}
    			break;
    		case "topic_comment":
    		case "event_comment":
    			$obj=db_commu_c_commu_topic_comment4c_commu_topic_comment_id($oid);
    			$subject=$obj["topic_name"];
    			$body=$obj["body"];
    			$body=CallApi::boby2html($body, "community", $u);
    			if($obj['image_filename_3']){
    				$body=" <img src='".OPENPNE_URL."/img.php?m=pc&filename=".$obj['image_filename_3']."' />".$body;
    			}
    			if($obj['image_filename_2']){
    				$body=" <img src='".OPENPNE_URL."/img.php?m=pc&filename=".$obj['image_filename_2']."' />".$body;
    			}
    			if($obj['image_filename_1']){
    				$body=" <img src='".OPENPNE_URL."/img.php?m=pc&filename=".$obj['image_filename_1']."' />".$body;
    			}
    			break;    		
    		case "album_image":
    			$obj=db_album_image_get_c_album_image4id($oid);
    			$subject=$obj['image_description'];
    			$body=" <img src='".OPENPNE_URL."/img.php?m=pc&filename=".$obj['image_filename']."' />".$body;
    			$cdata=serialize(array("c_member_id"=>$obj['c_member_id']));
    			break;
    		case "member":
    			$obj=db_member_c_member4c_member_id($oid,true,true);
    			$subject=$obj['nickname'];
    			$body=print_r($obj,true);
    			
    			break;
    		default :
    			exit('error');
    			break;
    			
    	}
    	
    	if(OPENPNE_USE_API  && AUTOEMA_USE && AUTOEMA_USE  ){			
			$c_member = db_member_c_member4c_member_id($u);
			//$body=CallApi::boby2html($body, $type, $u);
			
					
			$param=array(
				"enc"=>AUTOEMA_API_ENC,
				"sid"=>AUTOEMA_API_SID,
				"op"=>"9",//種別： 0 – 新規　1– 更新　2 – 削除　9-通報
				"tid"=>0,//スレッドid(オプション)
				"oid"=>$oid, //投稿id
				"poid"=>"", //親投稿id(オプション)
				"ptype"=>"",
				"url"=>"", //詳細ページURL(オプション)
				"turl"=>"",//スレッドページURL(オプション)
				"subject"=>$subject,
				"html"=>$body,
				"posted"=>date("Y-m-d H:i:s",time()),
				"ip"=>$_SERVER['REMOTE_ADDR'],
				"ua"=>$_SERVER['HTTP_USER_AGENT'],
				"att"=>"",
				"need_ok"=>"0",
				"process_op"=>"6",
				"uid"=>$u,
				"uname"=>$c_member['nickname'],
				"type"=>$type,
				"cdata"=>$cdata
				);
				
    		switch ($type){
				case "diary_comment":
					$param['tid']=$obj['c_diary_id'];
					$param['poid']=$obj['c_diary_id'];
					$param['ptype']="diary";
					break;
				case "topic_comment":
					$param['tid']=$obj['c_commu_topic_id'];
					$param['poid']=$obj['c_commu_topic_id'];
					$param['ptype']="topic";
					break;
				case "event_comment":
					$param['tid']=$obj['c_commu_topic_id'];
					$param['poid']=$obj['c_commu_topic_id'];
					$param['ptype']="event";
					break;
				case "album_image" :
					$param['tid']=$obj['c_member_id']; //					
					break;
			}
			

			CallApi::call_api($param);	
		}
		
		parse_str($requests['forward'],$arr_forward);
		$action=$arr_forward['a'];
		unset($arr_forward['a']);
		openpne_redirect("ktai", $action,$arr_forward);
		//==end=====================================================================================================
    
    	
    }
}