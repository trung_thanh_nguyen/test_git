<?php
class ktai_do_audition_biz extends OpenPNE_Action
{
	function execute($requests)
    {
    	$u  = $GLOBALS['KTAI_C_MEMBER_ID'];
    	//$fields=array("msg","msg1","msg2","msg3","sessid","message");
    	//$formval=$requests;
    	$fields=array("msg","msg1","msg2","msg3","sessid","message");
    	$formval=array();
    	 
    	foreach ($requests as $key=>$value){
    		if(!in_array($key, $fields)){
    			$formval[$key]=$requests[$key];
    		}
    	}
    	
    	$audition_id = $requests['audition_id'];
    	if($audition_id){
    		$data=array('status'=>1);
    		db_audition_biz_update_audition_data($audition_id,$data);
  			// send mail 
  			$mail=$requests['email'];    		
	    	if(db_common_is_mailaddress($mail)==false){
	    		$member=db_member_c_member4c_member_id($u,true);
	    		$mail=$member["secure"]["regist_address"];
	    	} 
	    	$requests['c_member_to']=$member;
	    		if(ADMIN_AUDITION_EMAIL !="") {
	    			fetch_send_mail($mail,"m_ktai_audition_biz",$requests,true,ADMIN_AUDITION_EMAIL);//To:応募者
					fetch_send_mail(ADMIN_AUDITION_EMAIL, "m_ktai_audition_biz", $requests,true,ADMIN_AUDITION_EMAIL);//BCC:be amie事務局
	    		}else{			
					fetch_send_mail($mail,"m_ktai_audition_biz",$requests);//To:応募者
					fetch_send_mail(ADMIN_EMAIL, "m_ktai_audition_biz", $requests);//BCC:be amie事務局
	    		}

				    	    	
	    	openpne_redirect('ktai', 'page_audition_biz_end', array("target_c_audition_id"=>$audition_id ,"message"=>"success"));
    	}else{
    		$formval["message"]="error";
    		openpne_redirect('ktai', 'page_audition_biz', $formval);
    	}
    	
    	exit();
    }
    
}

