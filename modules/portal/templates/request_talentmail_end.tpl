<div id="header_line"></div>
<!-- MAIN BEGIN -->
<div id="mainer">
	<div id="mainnertop"></div>
        <div id="main" class="page_talent_mail_end boxBase">
            ({if $from_oscar})
            <h1 class="headerLined h-content">お仕事のご依頼についてのお問い合わせ</h1>
            ({else})
            <h1 class="headerLined h-content">広告掲載・出演のご依頼のお問い合わせ</h1>
            ({/if})
            <div class="explain1">
    	  	お問い合わせ誠にありがとうございます。<br>
			内容を確認の上、折り返し連絡させていただきます。<br>
			なお、2営業日を経過しても連絡がない場合は、お問い合わせが届いていない可能性がございます。<br>
			その場合はお手数ではございますが再度のお問い合わせをお願いいたします。
	        </div>
        </div>
   <div id="mainnerbtm"></div>
</div>
<!-- MAIN END -->
