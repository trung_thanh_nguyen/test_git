				<!-- MAIN BEGIN -->
				
                ({if $from_oscar})
                <h1 class="headerLined h-content">お仕事のご依頼についてのお問い合わせ</h1>
                ({else})
                <h1 class="headerLined h-content">広告掲載・出演のご依頼のお問い合わせ</h1>
                ({/if})
				<div class="boxBase">
				ご確認のうえ、よろしければ送信してください。
				</div>

				<section class="boxBase">

				<form class="boxInput" method="POST">
					({t_form_block m=portal a=do_h_talent_mail_send})
					<dl class="listHr">
						<dt>お問い合わせ対象タレント</dt>
						<dd>({$formval.TalentName})</dd>
						<dt>お問合わせの内容</dt>
						<dd>({$formval.content})</dd>
						<dt>会社名・団体名</dt>
						<dd>({$formval.CompanyName})</dd>
						<dt>ご住所</dt>
						<dd>
							<p>({$formval.AddressPost}) ({$formval.pref}) ({$formval.Address1}) ({$formval.Address2})</p></dd>
						({if $formval.CompanyStation})
						<dt>ご担当部署名</dt>
						<dd>({$formval.CompanyStation})</dd>
						({/if})
						<dt>ご担当者名</dt>
						<dd>
							<p>({$formval.Name1})　({$formval.Name2})</p>
							<p>({$formval.Name3})　({$formval.Name4})</p></dd>
						<dt>電話番号1</dt>
							<dd>({$formval.Tel1a}) - ({$formval.Tel1b}) - ({$formval.Tel1c})</dd>
						({if $formval.Tel2a})
						<dt>電話番号2</dt>
							<dd>({$formval.Tel2a}) - ({$formval.Tel2b}) - ({$formval.Tel2c})</dd>
						({/if})
						<dt>メールアドレス</dt>
							<dd>({$formval.Mail})</dd>
						<dt>ホームページ</dt>
							<dd>({$formval.Homepage})</dd>
						<dt>お問合わせ内容</dt>
							<dd><p>({$formval.Comment|nl2br})</p></dd>
					</dl>

					
					<p class="txtCenter">
						<input type="button" name="submit" value="やり直す" class="btn-m btnCancel" onclick="history.back()"/>
						<input type="submit" name="submit" value="上記内容で送信する" class="btn-m"/>
						<input type="hidden" value="({$formval.Nickname})" name="Nickname"/>
						<input type="hidden" value="({$formval.talent_id})" name="talent_id"> 
						<input type="hidden" value="({$formval.content})" name="content"/>
						<input type="hidden" value="({$formval.CompanyName})"  name="CompanyName">
						<input type="hidden" value="({$formval.pref})" name="pref">
						<input type="hidden" value="({$formval.AddressPost})" name="AddressPost">
						<input type="hidden" value="({$formval.Address1})" name="Address1">
						<input type="hidden" value="({$formval.Address2})" name="Address2">
						<input type="hidden" value="({$formval.CompanyStation})" name="CompanyStation">
						<input type="hidden" value="({$formval.Name1})" name="Name1">	
						<input type="hidden" value="({$formval.Name2})" name="Name2">
						<input type="hidden" value="({$formval.Name3})" name="Name3">
						<input type="hidden" value="({$formval.Name4})" name="Name4">
						<input type="hidden" value="({$formval.Tel1a})" name="Tel1a">
						<input type="hidden" value="({$formval.Tel1b})" name="Tel1b">
						<input type="hidden" value="({$formval.Tel1c})" name="Tel1c">
						<input type="hidden" value="({$formval.Tel2a})" name="Tel2a">
						<input type="hidden" value="({$formval.Tel2b})" name="Tel2b">
						<input type="hidden" value="({$formval.Tel2c})" name="Tel2c">
						<input type="hidden" value="({$formval.Mail})" name="Mail">
						<input type="hidden" value="({$formval.Homepage})" name="Homepage">
						<input type="hidden" value="({$formval.Comment})" name="Comment">
						<input type="hidden" value="({$formval.captcha})" name="captcha">
						<input type="hidden" value="({$formval.TalentName})" name="TalentName">
                        ({if $from_oscar })
                        <input type="hidden" value="1" name="from_oscar">
                        ({/if})
					</p>
				({/t_form_block})
				</form>
				</section>
