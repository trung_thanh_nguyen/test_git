

            ({if $from_oscar})
            <h1 class="headerLined h-content">お仕事のご依頼についてのお問い合わせ</h1>
            ({else})
			<h1 class="headerLined h-content">広告掲載・出演のご依頼のお問い合わせ</h1>
            ({/if})
	    	({if $mailform_error})
	    	<div style="color:#FF0000;margin-bottom:10px;TEXT-ALIGN: center;">    	
	    	({foreach from=$mailform_error item=err})
	    	({$err})<br />
	    	({/foreach})    	    	
	    	</div>    	
	    	({/if})
				<div class="boxBase">
                ({if $from_oscar})
                <p>弊社所属タレント・モデルにご興味をいただきまして、誠にありがとうございます。<br>
                弊社所属タレント・モデルへの広告出演、イベント出演、後援会等のお問い合わせにつきましては、</p>
                <p>下記のフォームに必要事項のご記入の上、お問い合わせくださいますようお願い申し上げます。</p>
                <br>
                <p>※お仕事のご依頼、お仕事のご依頼についてのお問い合わせ以外に、<br>
                関してはご対応いたしかねますので、あらかじめご了承をお願いいたします。</p>
                ({else})
				<p>タレント・モデル、またbe amieへの広告出稿にご興味をいただきまして、誠にありがとうございます。<br>
				be amieはタレント・モデルとファンをつなぎ、<strong>&quot;キレイ&quot;</strong><strong>&quot;カッコイイ&quot;</strong>に関する人や情報に敏感なメンバーが集うSNSを目指しています。</p>
				<p>広告出稿のお問い合わせ、オスカープロモーション所属タレント・モデルへの出演のご依頼・お問い合わせにつきましては、下記のフォームに必要事項のご記入の上、お問い合わせくださいますようお願い申し上げます。</p>
				<p>
				※広告の掲載に関しましては、事前に審査した企業様に限られます。あらかじめご了承をお願いいたします<br>
				※お仕事のご依頼、広告掲載のお問い合わせ以外に関してはご対応いたしかねますので、あらかじめご了承をお願いいたします。</p>
                ({/if})
				</div>

				<section class="boxBase">
				<form action="./" method="post" class="boxInput">
					<input type="hidden" name="m" value="portal" />
					<input type="hidden" name="a" value="page_h_talent_mail_confirm" />
                    ({if $from_oscar})
                    <input type="hidden" name="from_oscar" value="1" />
					({/if})
					<input type="hidden" name="ssl_param" value="1" />
					<dl class="listHr">
						<dt>お問い合わせの内容<span class="errorTxt" >（必須）</span></dt>
						<dd>
							<input type="hidden" name="talent_id" value="0">
							<select name="content">
							<option value='' >お選びください</option>
							({if $is_oscar==true})
							<option value="広告出演のご依頼" ({if $mailform.content=='広告出演のご依頼'})selected({/if})>広告出演のご依頼</option>
							({/if})
							<option value="番組出演のご依頼" ({if $mailform.content=='番組出演のご依頼'})selected({/if})>番組出演のご依頼</option>
							<option value="イベント出演のご依頼" ({if $mailform.content=='イベント出演のご依頼'})selected({/if})>イベント出演のご依頼</option>
							<option value="お見積りのご依頼" ({if $mailform.content=='お見積りのご依頼'})selected({/if})>お見積りのご依頼</option>
							({if $is_oscar==true})
							<option value="広告掲載のご依頼" ({if $mailform.content=='広告掲載のご依頼'})selected({/if})>広告掲載のご依頼</option>
							({/if})
							<option value="その他" ({if $mailform.content=='その他'})selected({/if})>その他</option>
							</select>
							({if $is_oscar==true})
							<p>
							※広告の掲載に関しましては、事前に審査した企業様に限られます。<br/>
							予めご了承をお願いいたします
							</p>
							({/if})
						</dd>
						<dt>会社名・団体名<span class="errorTxt">（必須）</span></dt>
						<dd>
							<input type="text" value="({$mailform.CompanyName})" name="CompanyName">（全角・半角英数）</dd>
						<dt>ご住所<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p><label for="">郵便番号</label>
							<input type="text" value="({$mailform.AddressPost})" id="zip_textbox1" name="AddressPost" class="w60">（半角数字）<button class="btn btn-m" id="zip_search_button1" onclick="return false;" style="font-size:12px;padding:10px 0;width:150px">郵便番号から住所を入力</button></p>
							<p><label for="">都道府県</label>
							<select name="pref" id="address_pref_select1">
								<option value="">&#8213; 選択してください &#8213;</option>
								<option value="北海道" ({if $mailform.pref=='北海道'})selected({/if})>北海道</option>
								<option value="青森県" ({if $mailform.pref=='青森県'})selected({/if})>青森県</option>
								<option value="岩手県" ({if $mailform.pref=='岩手県'})selected({/if})>岩手県</option>
								<option value="宮城県" ({if $mailform.pref=='宮城県'})selected({/if})>宮城県</option>
								<option value="秋田県" ({if $mailform.pref=='秋田県'})selected({/if})>秋田県</option>
								<option value="山形県" ({if $mailform.pref=='山形県'})selected({/if})>山形県</option>
								<option value="福島県" ({if $mailform.pref=='福島県'})selected({/if})>福島県</option>
								<option value="茨城県" ({if $mailform.pref=='茨城県'})selected({/if})>茨城県</option>
								<option value="栃木県" ({if $mailform.pref=='栃木県'})selected({/if})>栃木県</option>
								<option value="群馬県" ({if $mailform.pref=='群馬県'})selected({/if})>群馬県</option>
								<option value="埼玉県" ({if $mailform.pref=='埼玉県'})selected({/if})>埼玉県</option>
								<option value="千葉県" ({if $mailform.pref=='千葉県'})selected({/if})>千葉県</option>
								<option value="東京都" ({if $mailform.pref=='東京都'})selected({/if})>東京都</option>
								<option value="神奈川県" ({if $mailform.pref=='神奈川県'})selected({/if})>神奈川県</option>
								<option value="新潟県" ({if $mailform.pref=='新潟県'})selected({/if})>新潟県</option>
								<option value="富山県" ({if $mailform.pref=='富山県'})selected({/if})>富山県</option>
								<option value="石川県" ({if $mailform.pref=='石川県'})selected({/if})>石川県</option>
								<option value="福井県" ({if $mailform.pref=='福井県'})selected({/if})>福井県</option>
								<option value="山梨県" ({if $mailform.pref=='山梨県'})selected({/if})>山梨県</option>
								<option value="長野県" ({if $mailform.pref=='長野県'})selected({/if})>長野県</option>
								<option value="岐阜県" ({if $mailform.pref=='岐阜県'})selected({/if})>岐阜県</option>
								<option value="静岡県" ({if $mailform.pref=='静岡県'})selected({/if})>静岡県</option>
								<option value="愛知県" ({if $mailform.pref=='愛知県'})selected({/if})>愛知県</option>
								<option value="三重県" ({if $mailform.pref=='三重県'})selected({/if})>三重県</option>
								<option value="滋賀県" ({if $mailform.pref=='滋賀県'})selected({/if})>滋賀県</option>
								<option value="京都府" ({if $mailform.pref=='京都府"'})selected({/if})>京都府</option>
								<option value="大阪府" ({if $mailform.pref=='大阪府'})selected({/if})>大阪府</option>
								<option value="兵庫県" ({if $mailform.pref=='兵庫県'})selected({/if})>兵庫県</option>
								<option value="奈良県" ({if $mailform.pref=='奈良県'})selected({/if})>奈良県</option>
								<option value="和歌山県" ({if $mailform.pref=='和歌山県'})selected({/if})>和歌山県</option>
								<option value="鳥取県" ({if $mailform.pref=='鳥取県'})selected({/if})>鳥取県</option>
								<option value="島根県" ({if $mailform.pref=='島根県'})selected({/if})>島根県</option>
								<option value="岡山県" ({if $mailform.pref=='岡山県'})selected({/if})>岡山県</option>
								<option value="広島県" ({if $mailform.pref=='広島県'})selected({/if})>広島県</option>
								<option value="山口県" ({if $mailform.pref=='山口県'})selected({/if})>山口県</option>
								<option value="徳島県" ({if $mailform.pref=='徳島県'})selected({/if})>徳島県</option>
								<option value="香川県" ({if $mailform.pref=='香川県'})selected({/if})>香川県</option>
								<option value="愛媛県" ({if $mailform.pref=='愛媛県'})selected({/if})>愛媛県</option>
								<option value="高知県" ({if $mailform.pref=='高知県'})selected({/if})>高知県</option>
								<option value="福岡県" ({if $mailform.pref=='福岡県'})selected({/if})>福岡県</option>
								<option value="佐賀県" ({if $mailform.pref=='佐賀県'})selected({/if})>佐賀県</option>
								<option value="長崎県" ({if $mailform.pref=='長崎県'})selected({/if})>長崎県</option>
								<option value="熊本県" ({if $mailform.pref=='熊本県'})selected({/if})>熊本県</option>
								<option value="大分県" ({if $mailform.pref=='大分県'})selected({/if})>大分県</option>
								<option value="宮崎県" ({if $mailform.pref=='宮崎県'})selected({/if})>宮崎県</option>
								<option value="鹿児島県" ({if $mailform.pref=='鹿児島県'})selected({/if})>鹿児島県</option>
								<option value="沖縄県" ({if $mailform.pref=='沖縄県'})selected({/if})>沖縄県</option>
							</select></p>
							<label for="">市区町村・番地</label>
							<p>
							<input type="text" value="({$mailform.Address1})" id="address_1_textbox1" name="Address1">（全角・半角英数）</p>
							<label for="">建物名・号室</label>
							<p>
							<input type="text" value="({$mailform.Address2})" name="Address2">（全角・半角英数）</p></dd>
						<dt>ご担当部署名</dt>
						<dd>
							<input type="text" value="({$mailform.CompanyStation})" name="CompanyStation">（全角）</dd>
						<dt>ご担当者名<span class="errorTxt">（必須）</span></dt>
						<dd>
							<p>
							<label for="">姓　</label><input type="text" value="({$mailform.Name1})" name="Name1" class="w60">
							<label for="">名　</label><input type="text" value="({$mailform.Name2})" name="Name2" class="w60">（全角）</p>
							<p>
							<label for="">セイ</label><input type="text" value="({$mailform.Name3})" name="Name3" class="w60">
							<label for="">メイ</label><input type="text" value="({$mailform.Name4})" name="Name4" class="w60">（全角カナ）</p></dd>
						<dt>電話番号1<span class="errorTxt">（必須）</span></dt>
						<dd>
							<input type="text" value="({$mailform.Tel1a})" name="Tel1a" class="w60"> - 
							<input type="text" value="({$mailform.Tel1b})" name="Tel1b" class="w60"> - 
							<input type="text" value="({$mailform.Tel1c})" name="Tel1c" class="w60">（半角数字）</dd>
						<dt>電話番号2<span class="errorTxt">必須ではありません</span></dt>
						<dd>
							<input type="text" value="({$mailform.Tel2a})" name="Tel2a" class="w60"> - 
							<input type="text" value="({$mailform.Tel2b})" name="Tel2b" class="w60"> - 
							<input type="text" value="({$mailform.Tel2c})" name="Tel2c" class="w60">（半角数字）</dd>
						<dt>メールアドレス<span class="errorTxt">（必須）</span></dt>
						<dd>
							<label for="">メールアドレス</label><p><input type="text" value="({$mailform.Mail})" name="Mail">（半角英数）</p>
							<label for="">メールアドレス（確認用）</label><p><input type="text" value="({$mailform.MailCheck})" name="MailCheck">（半角英数）</p></dd>
						<dt>ホームページ</dt>
							<dd><label for="">URL</label><input type="text" value="({if $mailform.Homepage})({$mailform.Homepage})({else})http://({/if})" name="Homepage">（半角英数）</dd>

						<dt>ご指定の<br>タレント・モデル</dt>
							<dd><label for=""></label><input type="text" value="({$TalentName})" name="TalentName"></dd>

						<dt>お問い合わせ内容<span class="errorTxt">（必須）</span></dt>
							<dd>
							<p>予算等の条件などございましたら、ご記入をお願いいたします。</p>
							<textarea rows="12" name="Comment">({$mailform.Comment})</textarea></dd>

                    	<dt><label>画像認証</label><span class="errorTxt">（必須）</span></dt>
                       	<dd>
                            <img alt="確認キーワード" src="./cap.php?rand=({math equation="rand(0,99999999)"})" id="cap">
                            <a href="javascript:updatecap();">キーワードを変更する</a>
                            <p>※上に表示されているキーワードをご記入下さい。</p>
                            <input type="text" size="30" value="" name="captcha" class="input_text">
                        </dd>

					</dl>

					<h3>個人情報の取り扱いについて</h3>
					<p>お問い合わせいただくにあたり<strong>「プライバシーポリシー」</strong>をご確認・同意の上、確認画面に進んでください。</p>

					<div class="boxPrivacy">
					<h3>プライバシーポリシー（個人情報保護方針）</h3>
					<p>株式会社オスカープロモーション（以下、当社）は、お客様の個人情報の適切な管理と保護を社会的責務であると認識し、以下の取り組みを実施いたします。</p>
					<p>1.個人情報保護マネジメントシステムの確立<br />
						be amieの個人情報は株式会社ISAO(以下、ISAO)により管理、保護されます。
						ISAOは、JIS Q 15001：2006に準拠した個人情報保護マネジメントシステム（個人情報を保護するための方針、体制、計画、実施、運用の確認及び見直しを含むマネジメントシステム）を確立し、これを実施し、維持し、継続的に改善していきます。</p>
					<p>2.個人情報の利用および提供<br />
						当社およびISAOは、事業の内容及び規模を考慮した個人情報保護のための組織管理体制を確立するとともに、個人情報の取得、利用及び提供に際しては所定の規程
・規則を遵守し、目的外利用を行なわず、適切な取扱いを実施するために十分な措置を講じます。</p>
					<p>3.法令等の遵守<br />
						当社およびISAOは、個人情報の取扱いに関する法令、国が定める指針その他の規範を遵守いたします。また、ISAOはISAOの個人情報保護マネジメントシステムを、
当該法令及びその他の規範に適合させます。</p>
					<p>4.安全管理措置<br />
						当社およびISAOは、個人情報の安全管理のために必要かつ適切な対策を講じ、個人情報の漏えい、滅失またはき損の防止および是正を行います。</p>
					<p>5.苦情および相談への対応
						当社およびISAOは、個人情報の苦情や相談に関して、適切かつ迅速に対応し、問題の解決を図るよう努めます。</p>
					<p>以上</p>
					<p>制定日：2010年8月31日<br>
						株式会社オスカープロモーション</p>
					<h3>個人情報の取扱いについて</h3>
					<p>1)個人情報の管理<br />
						be amieの個人情報は株式会社ISAO(以下、ISAO)により管理、保護されます。
						ISAOは、JIS Q 15001：2006に準拠した個人情報保護マネジメントシステム（個人情報を保護するための
						方針、体制、計画、実施、運用の確認及び見直しを含むマネジメントシステム）を確立し、
						これを実施し、維持し、継続的に改善していきます。
					</p>

					<p>2.個人情報の利用および提供<br />
						当社およびISAOは、事業の内容及び規模を考慮した個人情報保護のための組織管理体制を確立するとともに、個人情報の取得、利用及び提供に際しては所定の規程
・規則を遵守し、目的外利用を行なわず、適切な取扱いを実施するために十分な措置を講じます。</p>
					<p>3.法令等の遵守<br />
						当社およびISAOは、個人情報の取扱いに関する法令、国が定める指針その他の規範を遵守いたします。また、ISAOはISAOの個人情報保護マネジメントシステムを、
当該法令及びその他の規範に適合させます。</p>
					<p>4.安全管理措置<br />
						当社およびISAOは、個人情報の安全管理のために必要かつ適切な対策を講じ、個人情報の漏えい、滅失またはき損の防止および是正を行います。</p>
					<p>5.苦情および相談への対応<br />
						当社およびISAOは、個人情報の苦情や相談に関して、適切かつ迅速に対応し、問題の解決を図るよう努めます。</p>
					<p>以上</p>
					<p>制定日：2010年8月31日<br>
						株式会社オスカープロモーション</p>
					<h3>個人情報の取扱いについて</h3>
					<p>1)個人情報の管理
						be amieの個人情報は株式会社ISAO(以下、ISAO)により管理、保護されます。
						ISAOは、JIS Q 15001：2006に準拠した個人情報保護マネジメントシステム（個人情報を保護するための
						方針、体制、計画、実施、運用の確認及び見直しを含むマネジメントシステム）を確立し、
						これを実施し、維持し、継続的に改善していきます。
					</p>
					<p>2）個人情報の利用目的
						当社が取得した個人情報の利用目的は、以下の通りです。
						<ul>
							<li>be amieサービスの提供</li>
							<li>be amieサービスおよび関連サービスに関する案内の送付</li>
							<li>有料サービスの請求および関連する業務での利用</li>
							<li>お問い合わせに対する対応</li>
							<li>登録情報に応じた広告配信での利用</li>
							<li>規約違反による退会に関する情報の新規登録時の参照</li>
							<li>コンテンツ企画、サービス開発等で参照する統計データとしての利用</li>
						</ul>
					</p>

					<p>なお、当社では、業務を円滑に遂行するため、業務の一部を委託先に委託し、当該委託先に対し必要な範囲で個人情報を委託する場合がありますが、この場合は、当社が定めた基準を満たす者を委託先として選定するとともに、個人情報の取り扱いに関する契約の締結や適切な監督を行います。</p>
					<p>3)アクセス記録、Cookie等の利用
						当社では、be amieサービスおよび関連サービスへの登録、利用に関する記録、IPアドレス、アクセス日時、携帯個体識別番号、Cookie情報等の情報を自動的に記録します。
						記録した情報を元に、年齢、性別等の登録情報に応じたサービスの案内、広告表示、統計情報の収集を行う場合があります。
						個人を特定できない形式の統計情報は、当社は制限なく利用できるものとします。</p>
					<p>4)上記目的以外での利用
						上記2)以外の目的で個人情報を利用する必要が生じた場合には、法令により許される場合を除き、その利用について、ご本人のご同意をいただくものとします。</p>
					<p>5)第三者への開示・提供について
						当社では、上記2)に記載した委託先へ委託する場合及び以下のいずれかに該当する場合を除き、個人情報を第三者へ開示または提供いたしません。
						<ul>
							<li>ご本人のご同意をいただいている場合</li>
							<li>統計的なデータなどご本人を識別することができない状態で開示・提供する場合</li>
							<li>法令に基づき開示・提供を求められた場合</li>
							<li>人の生命、身体または財産の保護のために必要な場合であって、ご本人のご同意をいただくことが困難である場合</li>
							<li>国または地方公共団体等が法令の定める事務を実施するうえで協力する必要がある場合であって、ご本人のご同意をいただくことにより当該事務の遂
行に支障を及ぼすおそれがある場合</li>
						</ul>
					</p>
                    <p>6)保存期間
						当社およびISAOでは、法令で別段の定めがある場合を除き、利用目的に必要な範囲内でお客様の個人情報の保存期間を6ヶ月間とし、保存期間を経過あるいは利用目的を達成した個人情報は遅滞なく消去いたします。</p>

					コンテンツ内容や動作に関するお問い合わせ窓口<br>
						<a href="({$smarty.const.OPENPNE_URL})?m=pc&a=page_u_mailform1" target="_blank">お問い合わせ窓口</a><br>
					2015年11月20日
					株式会社オスカープロモーション

					</div>
					<p class="txtCenter">
						<input type="submit" name="submit" value="同意の上、確認画面へ" class="btn btn-m"/>
					</p>
				</form>
<script type="text/javascript">
$(document).ready(function(){
	$('input[name="submit"]').click(function(){
		var mailform = [];
		$('.boxInput').find('input[type="text"]').each(function(){
			if ($(this).attr('name') != 'MailCheck') {
				mailform[$(this).attr('name')] = $(this).val();
			}
		});
		$('.boxInput').find('select').each(function(){
			mailform[$(this).attr('name')] = $(this).children('option:selected').val();
		});
		$('.boxInput').find('textarea').each(function(){
			mailform[$(this).attr('name')] = $(this).val();
		});
		if (typeof sessionStorage !== 'undefined') {
			var storage = sessionStorage;
			for (var key in mailform) {
				storage.setItem(key, mailform[key]);
			}
		}
	});
	if (typeof sessionStorage !== 'undefined') {
		var storage = sessionStorage;
		if (storage.length > 0) {
			for (i=0; i<storage.length; i++) {
				var _key = storage.key(i), _val = storage.getItem(_key);
				$('.boxInput').find('input').each(function(){
					if ($(this).attr('name') == _key) {
						$(this).val(_val);
					}
				});
				$('.boxInput select').each(function(){
					if ($(this).attr('name') == _key) {
						$(this).children('option').each(function(){
							if ($(this).val() == _val) {
								$(this).attr('selected', 'selected');
							}
						});
					}
				});
				$('.boxInput textarea').each(function(){
					if ($(this).attr('name') == _key) {
						$(this).html(_val);
					}
				});
			}
		}
		storage.clear();
	}


});
</script>

<script type="text/javascript">
    $(".editor").jqte();

    function updatecap(){
            var land = Math.ceil(Math.random() * 99999999);
            $('#cap').attr('src' , './cap.php?rand=' + land);
    }
</script>

({literal})
	<script type="text/javascript" src="/js/zip_code.js"></script>
({/literal})
				</section>
