({ext_include file="inc_head.tpl"})
<div id="header_line"></div>
<!--
<script type="text/javascript" src="./js/jquery-1.4.2.min.js"></script>
-->
<script type="text/javascript" src="js/jquery.highlight-3.js"></script>
<script type="text/javascript" >
var $jquery = jQuery.noConflict();
$jquery(document).ready(function(){
	var keyword="({$requests.keyword})";
	if(keyword!=""){	
		$jquery("#main").highlight(keyword);
	}
});

</script>
<!-- MAIN BEGIN -->
<div id="mainer">
	<div id="mainnertop"></div>
    <div id="main" class="page_h_talent_list">
    	<div id="search">						
    	
    	<div class="dparts">
    		<div class="partsHeading"><h3><span>所属タレント＆モデル検索</span></h3></div>
				<div class="parts">
				
				<div class="explain1">
				be amieに登録されているオスカープロモーション所属のタレント・モデルのプロフィール情報と日記を検索します。<br>
				<span>※検索結果のリンク先は、ログインしないと表示されない場合があります。</span>
				</div>
				</div>
			</div>
			
			<br style="clear:both"/>
			
			({if $show_result})
			<div id="result" style="width:703px;min-height:600px; margin-top:10px;background-color:#ffffff;">
				<div class="dparts searchResultList">
				<div class="partsHeading"><h3 class="sr"><span>検索結果一覧</span></h3></div>
				<div class="parts">
				({capture name=pager})({strip})
				
				<div class="pagerRelative">
				<div style="float:left;">
				({if $pager.total_num >= 500})
				*** 500名以上ヒットしました。更に条件の追加をお願いします。
				({else})
				*** ({$pager.total_num})名が該当しました。
				({/if})
				
				</div>
				({if $pager.page_prev})
				<p class="prev"><a href="javascript:gopage(({if $pager.page_prev})({$pager.page_prev})({else})1({/if}))">前を表示</a></p>
				({/if})
				<p class="number">({$pager.disp_start})件～({$pager.disp_end})件を表示</p>
				({if $pager.page_next})<p class="next"><a href="javascript:gopage(({if $pager.page_next})({$pager.page_next})({else})({$pager.total_num})({/if}))">次を表示</a></p>({/if})
				</div>
				({/strip})({/capture})
				({$smarty.capture.pager|smarty:nodefaults})
				
				({if $search_list})
				<div class="block">
				({foreach from=$search_list item=item})
				
				<div class="ditem"><div class="item">
				<table><tr>
				<td class="photo" rowspan="4"><a href="({t_url m=pc a=page_f_home})&amp;target_c_member_id=({$item.c_member_id})"><img src="({t_img_url filename=$item.image_filename w=76 h=76 noimg=no_image})" alt="" /></a></td>
				<th>本名</th><td>({$item.profile.talent_name.value})</td>
				</tr><tr>
				<th>({$WORD_NICKNAME})</th><td>({$item.nickname})</td>
				</tr><tr>
				<th>自己紹介</th><td>({$item.profile.self_intro.value|t_truncate:36:"":3})</td>
				</tr><tr class="operation">
				<th>最終ログイン</th><td><span class="text">({$item.last_login})</span> <span class="moreInfo"><a href="({t_url m=pc a=page_f_home})&amp;target_c_member_id=({$item.c_member_id})"><img src="({t_img_url_skin filename=button_shosai})" alt="詳細を見る" /></a></span></td>
				</tr></table></div></div>
				
				({/foreach})
				</div>				
				({/if})
				
				({$smarty.capture.pager|smarty:nodefaults})

				</div></div>
			
			</div>	
			({/if})
			
			<div class="dparts">
    		<div class="partsHeading"><h3 class="key_search_title"><span>キーワードと検索</span></h3></div>
				<div class="parts">
				
				({t_form m=portal a=page_search_result})
				<table class="noborder">
					<tr>
						<td valign="middle" width="250px"><input id="searchkeyindetail" name="searchkey" type="text" value="" style="width:200px"></td>
						<td valign="middle"><input id="btnSearchInDetail" onclick="return checkformindetail(this)" type="image" style="height:auto;" name="submit" value="検索" src="./skin/default/img/new_img/search_button.jpg"/></td>
					</tr>
				</table>
				<br>
				</form>
				</div>
			</div>
			
			<div class="dparts">
    		<div class="partsHeading"><h3 class="search_input_title"><span>所属タレント＆モデル検索</span></h3></div>
				<div class="parts">	
				({t_form m=portal a=page_h_talent_list _attr='id="form1"'})
				<input type="hidden" name="page" id="page" value="1" />
				<table class="search_input">
				<tr><td width="100px" class="search_cap">所属</td><td>
				<select name="talent_company" size="" accesskey="2">
				<option value="">－指定しない－</option>
				<option value="タレント" ({if $smarty.request.talent_company eq "タレント"}) selected ({/if})>タレント</option>
				<option value="モデル" ({if $smarty.request.talent_company eq "モデル"}) selected ({/if})>モデル</option>
				<option value="歌手" ({if $smarty.request.talent_company eq "歌手"}) selected ({/if})>歌手</option>
				<option value="キャスター・レポーター" ({if $smarty.request.talent_company eq "キャスター・レポーター"}) selected ({/if})>キャスター・レポーター</option>
				<option value="ナレーター・コンパニオン" ({if $smarty.request.talent_company eq "ナレーター・コンパニオン"}) selected ({/if})>ナレーター・コンパニオン</option>
				<option value="バラエティ" ({if $smarty.request.talent_company eq "バラエティ"}) selected ({/if})>バラエティ</option>
				</select>
				(タレント・モデル・バラエティなど)
				({*<input type="text" size="20" name="talent_company" class="input_text" value="({$smarty.request.talent_company})" />*})
				</td>
				</tr>
				<tr>
				<td class="search_cap">年齢</td><td><input type="text" size="10"  name="age1" value="({$smarty.request.age1})" class="input_text"/>-<input type="text" size="10"  name="age2" value="({$smarty.request.age2})" class="input_text"/>歳</td>
				</tr>
				<tr>
					<td class="search_cap">生年月日</td>
					<td class="birthday">
						<select name="start_year"'>
							({foreach from=$arr_year item=v key=k})
							<option ({if $smarty.request.start_year==$k}) selected ({/if}) value="({$k})">({$v})</option>
							({/foreach})
						</select>年
						<select name="start_month"'>
							({foreach from=$arr_month item=v key=k})
							<option ({if $smarty.request.start_month==$k}) selected ({/if}) value="({$k})">({$v})</option>
							({/foreach})
						</select>月
						<select name="start_day"'>
							({foreach from=$arr_day item=v key=k})
							<option ({if $smarty.request.start_day==$k}) selected ({/if}) value="({$k})">({$v})</option>
							({/foreach})
						</select>日
						～
						<select name="end_year"'>
							({foreach from=$arr_year item=v key=k})
							<option ({if $smarty.request.end_year==$k}) selected ({/if}) value="({$k})">({$v})</option>
							({/foreach})
						</select>年
						<select name="end_month"'>
							({foreach from=$arr_month item=v key=k})
							<option ({if $smarty.request.end_month==$k}) selected ({/if}) value="({$k})">({$v})</option>
							({/foreach})
						</select>月
						<select name="end_day"'>
							({foreach from=$arr_day item=v key=k})
							<option ({if $smarty.request.end_day==$k}) selected ({/if}) value="({$k})">({$v})</option>
							({/foreach})
						</select>日
					</td>
				</tr>
				<tr><td class="search_cap">性別</td><td>
				<select name="sex"><option selected="selected" value="0">指定しない</option>
				({foreach from=$sex_list item=sex})
				<option  value="({$sex.c_profile_option_id})" ({if $smarty.request.sex==$sex.c_profile_option_id}) selected({/if})>({$sex.value})</option>
				({/foreach})
				</select></td>
				</tr>
				<tr>
				<td class="search_cap">血液型</td><td ><select name="blood_type"><option selected="selected" value="0">指定しない</option>
				({foreach from=$blood_type_list item=blood_type})
				<option value="({$blood_type.c_profile_option_id})" ({if $smarty.request.blood_type == $blood_type.c_profile_option_id}) selected ({/if})>({$blood_type.value})</option>
				({/foreach})
				</select>
				</td></tr>
				<tr><td class="search_cap">サイズ</td>
				<td class="muti">&nbsp;身長<input type="text" name="talent_height1" size="5" value="({$smarty.request.talent_height1})"/>cm-<input type="text" name="talent_height2" size="5" value="({$smarty.request.talent_height2})"/>cm
				&nbsp;
				バスト<input type="text" name="talent_bust1" size="5" value="({$smarty.request.talent_bust1})"/>cm-<input type="text" name="talent_bust2" size="5" value="({$smarty.request.talent_bust2})"/>cm
				&nbsp;
				ウェスト<input type="text" name="talent_waistline1" size="5" value="({$smarty.request.talent_waistline1})"/>cm-<input type="text" name="talent_waistline2" size="5" value="({$smarty.request.talent_waistline2})"/>cm
				<br />
				ヒップ<input type="text" name="talent_hip1" size="5" value="({$smarty.request.talent_hip1})"/>cm-<input type="text" name="talent_hip2" size="5" value="({$smarty.request.talent_hip2})"/>cm
				&nbsp;
				シューズ<input type="text" name="talent_shoe1" size="5" value="({$smarty.request.talent_shoe1})"/>cm-<input type="text" name="talent_shoe2" size="5" value="({$smarty.request.talent_shoe2})"/>cm
				</td>
				</tr>
				<tr><td class="search_cap">出身地</td><td><select name="old_addr_pref">
					<option selected="selected" value="0">指定しない</option>
					({foreach from=$old_addr_pref_list item=old_addr_pref})
					<option value="({$old_addr_pref.c_profile_option_id})" ({if $smarty.request.old_addr_pref == $old_addr_pref.c_profile_option_id}) selected ({/if})>({$old_addr_pref.value})</option>
					({/foreach})					
					</select></td></tr>
				<tr><td class="search_cap">趣味</td><td ><input type="text" size="30" name="talent_interest" class="input_text" value="({$smarty.request.talent_interest})"/></td></tr>
				<tr><td class="search_cap">資格</td><td ><input type="text" size="30" name="talent_qualification" class="input_text" value="({$smarty.request.talent_qualification})"/></td></tr>
				<tr><td class="search_cap">スポーツ</td><td ><input type="text" size="30" name="talent_sport" class="input_text" value="({$smarty.request.talent_sport})"/></td></tr>
				<tr><td class="search_cap">特技</td><td ><input type="text" size="30" name="talent_speciality" class="input_text" value="({$smarty.request.talent_speciality})"/></td></tr>
				({*<tr class="last_tr"><td class="search_cap">キーワード</td><td ><input type="text" size="30" name="keyword" class="input_text" value="({$smarty.request.keyword})"/></td></tr>*})
				</table>
				<div class="bt">
				<input type="hidden" name="isresult" value="result" id="isresult"> 
				<input onclick="return checkformdetail(this)" id="btnSearch" type="image" style="height:auto;" name="submit" value="検索" src="./skin/default/img/new_img/search_button.jpg"/></div>
				</form>
				</div>
				</div>
			</div>
			
    </div>
    
    ({ext_include file="inc_side.tpl"})
    <div id="mainnerbtm"></div>
</div>
<!-- MAIN END -->

<script>
function gopage(p){
    document.getElementById('page').value = p;
    document.getElementById('btnSearch').click();
}
</script>

({ext_include file="inc_foot.tpl"})