<?php 
class portal_page_contest_movie extends OpenPNE_Action
{
       
    function execute($requests)
    {
    	$this->set('title','国民的美少女2012 La beauté');
    	//<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
    	$metas = array(
    				array('http-equiv'=>'Pragma', 'content'=>'no-cache')
    			);
    	$this->set('metas',$metas);
    	$meta_description = 'be amie（ビー・アミー）は、「美」の最先端をいくタレント・モデルをはじめ、「キレイ」「カッコイイ」に敏感な、たくさんのユーザーと情報交換を楽しむ、オスカープロモーション公式コミュニティサイトです。';
    	$this->set('meta_description',$meta_description);
    	$ogs = array(
    			'type'=>'website',
    			'title'=>'美女・美少女のプロモーション動画をチェック！',
    			'description'=>'第13回全日本国民的美少女コンテストの受賞者たちの本選大会の舞台裏や伊豆での撮影の模様をまとめたファーストムービー。オスカープロモーション公式YouTubeチャンネルにて配信中！',
    			'image'=>'/contest/img/dts_mainimg.jpg'
    			);
    	$this->set('ogs',$ogs);
    	$links = array(
    			array('rel'=>'mixi-check-image','href'=>'/contest/img/mixicheck_img.jpg'),
    			array('rel'=>'stylesheet','type'=>'text/css','href'=>'/contest/css/moviespecial.css'),    			
    			);
    	$this->set('links',$links);
    	$scripts =
    	   array(
    			array('type'=>'text/javascript','src'=>'http://static.ak.fbcdn.net/connect.php/js/FB.Share','defer'=>'defer'),
    			);
    	$this->set('scripts',$scripts);
    	$this->set('disable_script',true);
    	
    	return 'success';    	
    }
}