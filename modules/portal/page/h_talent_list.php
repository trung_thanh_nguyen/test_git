<?php 
class portal_page_h_talent_list extends OpenPNE_Action
{
       
    function execute($requests)
    {
    		
    	$this->set('sex_list',db_profile_option_list("sex"));
    	$this->set('blood_type_list',db_profile_option_list("blood_type"));
    	$this->set('old_addr_pref_list',db_profile_option_list("old_addr_pref"));
    	$t = getdate();
    	$cur_year = $t['year'];
    	$arr_year = array ();
    	$arr_year[] = '----';
    	for($i=1930;$i<=$cur_year;$i++){
    		$arr_year[$i] = $i;
    	}
    	$arr_month = array();
    	$arr_month[]  = '----';
    	for($i=1;$i<=12;$i++){
    		$arr_month[$i] = $i;
    	}
    	$arr_day = array();
    	$arr_day[]  = '----';
    	for($i=1;$i<=31;$i++){
    		$arr_day[$i] = $i;
    	}
    	
    	$this->set('arr_year',$arr_year);
    	$this->set('arr_month',$arr_month);
    	$this->set('arr_day',$arr_day);
    	$limit=PORTAL_SEARCH_PAGE_SIZE;//page size
    	$page=$requests['page'];
    	$isresult=false; 
    	/*foreach ($requests as $key=>$value){
    		if($key!="page" && $value){
    			$isresult=true;
    		}
    	}*/
    	if($requests['isresult']=="result"){
    		$isresult=true;
    	}
    	if($isresult){
    		$result=db_member_talent_list($requests,$page,$limit);
    		$this->set('show_result',true);
    	}else{
    		$result=array(null,0,0,0);
    		$this->set('show_result',false);
    	}
    	$_SESSION['talent_search']=$requests;
    	//var_dump($_SESSION['talent_search']);die();    	
    	
    	    	
    	$list=$result[0];
    	//var_dump($list);die();
    	$this->set('search_list',$list);
    	$pager = array(
            "page_prev" => $result[1],
            "page_next" => $result[2],
            "total_num" => $result[3],
        );
        
    	$pager["disp_start"] = $limit * ($page - 1) + 1;
        if (($disp_end  = $limit * $page) > $pager['total_num']) {
            $pager['disp_end'] = $pager['total_num'];
        } else {
            $pager['disp_end'] = $disp_end;
        }

        $this->set("pager", $pager);
//        $this->set('searchkey',$keyword);
//        $this->set('searchtype',$searchtype);
//        $this->set('search_condition',"searchkey=$keyword&searchtype=$searchtype");
        
        $this->set('requests',$requests);
        
        portal_get_pc_right_common($this);
        
    	
    	//var_dump($list);die();
    	
    	
//    	$this->set('result_list',$list);
//    	
//    	//var_dump($result);die();
//    	
//    	$member_list=db_member_talent_mode($keyword); 
//    	//var_dump($member_list);die();
//    	$diary_list=talent_diary_search_result($keyword,20,1);    	
//    	
//    	list($result, $is_prev, $is_next, $total_num, $start_num, $end_num)
//            = db_commu_search_c_commu4c_commu_category($keyword,'',20,1,'','');   
//    	
//    	switch ( $searchtype){
//    		case 1:
//    			$this->set('c_member_list',$member_list);
//    			$this->set('c_diary_list',$diary_list);
//    			break;
//    		case 2:
//    			$this->set('c_commu_search_list', $result);
//    			break;
//    		case 3:
//    			$this->set('c_member_list',$member_list);
//    			$this->set('c_diary_list',$diary_list);
//    			$this->set('c_commu_search_list', $result);
//    			break;
//    		default :
//    			break;
//    	}
//    	
    	return "success";
    
    }
}