<?php 
require_once OPENPNE_WEBAPP_DIR . '/lib/util/util.php';
class portal_page_h_talent_mail_end extends OpenPNE_Action
{
       
  function execute($requests)
  {
  	portal_get_pc_right_common($this);
  	
    if(db_member_is_talent($requests['talent_id'])==true){
		$config_productions=get_config_talent_production();
		$talent_production=db_member_get_profile($requests['talent_id'], "talent_production");
		if($talent_production!=$config_productions[0]['key']){
		      $this->set('is_oscar',false);
		}else{
		      $this->set('is_oscar',true);
		}
    }else{
    	$this->set('is_oscar',true);
    }

    // oscarpro.co.jpから流入した場合はテンプレートの外枠を変更する
    if( 1 == $requests['from_oscar'] ){
        $this->set('from_oscar', true);
    }

    $this->set('login_check', $_SESSION['SAFE_CHECK']);

    $member_id = get_login_member_id();
    
	$main = array("request_talentmail_end.tpl");
    
    $this->set('main', $main);
    
    /*$results = get_member_banner($member_id);
    $this->set('side_top_contents', $results['side_top_contents']);
    $this->set('side_bannar'      , $results['side_contents']);
    
    $this->set('aside', $_SESSION['aside']);*/
        
    $this->set('sp', $_SESSION['SMARTPHONE_CHECK']);
        
  	return "success";
  
  }
}
