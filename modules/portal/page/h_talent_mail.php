<?php 
require_once OPENPNE_MODULES_DIR . '/admin/lib/db_admin.php';

class portal_page_h_talent_mail extends OpenPNE_Action
{    
    function execute($requests)
    {


    	$target_c_member_id = $requests['target_c_member_id'];    	
    	unset($_SESSION['mailform']);
    	$_SESSION['mailform']['target_c_member_id']=$target_c_member_id;
    	if($target_c_member_id){
    		$talent_name = db_member_get_profile($target_c_member_id,'talent_name');
    		if($talent_name){
			$this->set('TalentName', $talent_name);
    			$_SESSION['mailform']['Nickname'] = $talent_name;    			
    		}
    		//It's add by e2info 2010-10-28=============================
    		if(db_member_is_talent($target_c_member_id)==true){
	    		$config_productions=get_config_talent_production();
	        	$talent_production=db_member_get_profile($target_c_member_id, "talent_production");
	        	if($talent_production!=$config_productions[0]['key']){
	        		$this->set('is_oscar',false);
	        	}else{
	        		$this->set('is_oscar',true);
	        	}
    		}
    		//=============================================================
    	}else{
    		$this->set('is_oscar',true);
    	}

        $this->set('login_check', $_SESSION['SAFE_CHECK']);
        $this->set('advertise_page', true);

        $member_id = get_login_member_id();
        
        // oscarpro.co.jpから流入した場合はテンプレートの外枠を変更する
        if( false !== strpos( $_SERVER['HTTP_REFERER'], 'oscarpro.co.jp' ) || 1 == $requests['from_oscar'] ){
            $this->set('from_oscar', true);
        }


		$main = array("request_talentmail.tpl");
        
        $this->set('main', $main);
        
        /*$results = get_member_banner($member_id);
	    $this->set('side_top_contents', $results['side_top_contents']);
	    $this->set('side_bannar'      , $results['side_contents']);
        
        $this->set('aside', $_SESSION['aside']);*/
        
        $this->set('sp', $_SESSION['SMARTPHONE_CHECK']);
        
    	//print_r($target_c_member);
    	$c_free_page = db_admin_get_c_free_page_one(14);
      if (isset($_SESSION['mailform'])){              
          $this->set('mailform',$_SESSION['mailform']);
      }
      if (isset($_SESSION['mailform_error'])){
          $this->set('mailform_error',$_SESSION['mailform_error']);
          unset($_SESSION['mailform_error']);
      }
      portal_get_pc_right_common($this);
      
    	
        $this->set('privacy_policy', $c_free_page);
        
        //$this->set('formval',$requests);
    	return "success";    	
    }
}
