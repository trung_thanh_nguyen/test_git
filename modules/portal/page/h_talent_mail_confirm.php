<?php 
require_once OPENPNE_WEBAPP_DIR . '/lib/util/util.php';
require_once OPENPNE_WEBAPP_DIR . '/lib/util/mail_verify.php';
class portal_page_h_talent_mail_confirm extends OpenPNE_Action
{ 
    function execute($requests)
    {
        $_SESSION['mailform'] = $_POST;
        if (empty($_POST['content'])){
            $this->fail('お問合わせの内容を入力してください');
        }        
        if (empty($_POST['CompanyName'])){
            $this->fail('会社名・団体名を入力してください');
        }        
        if (empty($_POST['AddressPost']) || empty($_POST['pref']) || empty($_POST['Address1'])){
            $this->fail('ご住所を入力してください');
        }
        
        if (empty($_POST['Name1']) || empty($_POST['Name2']) || empty($_POST['Name3']) || empty($_POST['Name4'])){
            $this->fail('ご担当者名を入力してください');            
        }
        if (empty($_POST['Tel1a']) || empty($_POST['Tel1b']) || empty($_POST['Tel1c'])){
            $this->fail('ご連絡先の電話番号を入力してください');
        }
        if (empty($_POST['Mail']) || empty($_POST['MailCheck'])){
            $this->fail('メールアドレスを入力してください');            
        }
        if ($_POST['Mail'] != $_POST['MailCheck']){
            $this->fail('メールアドレスを入力してください');
        } else if (!VerifyEmail($_POST['Mail'])){
            $this->fail('メールアドレスに不備があります');
        }
		if (empty($_POST['Comment'])){
			$this->fail('お問合わせ内容を入力してください');
		}

		if (OPENPNE_USE_CAPTCHA) {
			@session_start();
			if (empty($_SESSION['captcha_keystring']) || $_SESSION['captcha_keystring'] !=  $requests['captcha']) {
				unset($_SESSION['captcha_keystring']);
				$this->fail('確認キーワードが誤っています');
			}
			//unset($_SESSION['captcha_keystring']);
		}

    	$keys=array("content","CompanyName","pref","Address1",'Address2','CompanyStation','Name1','Name2','Name3','Name4','Tel1a','Tel1b','Tel2a','Tel2b','Mail','Homepage','Comment');
    	
    	$this->set('formval',$requests);
    	
    	portal_get_pc_right_common($this);
         //  var_dump($_SESSION);die(); 
	    if(db_member_is_talent($requests['talent_id'])==true){
			$config_productions=get_config_talent_production();
			$talent_production=db_member_get_profile($requests['talent_id'], "talent_production");
			if($talent_production!=$config_productions[0]['key']){
			      $this->set('is_oscar',false);
			}else{
			      $this->set('is_oscar',true);
			}
	    }else{
	    	$this->set('is_oscar',true);
	    }	
        // oscarpro.co.jpから流入した場合はテンプレートの外枠を変更する
        if( 1 == $requests['from_oscar'] ){
            $this->set('from_oscar', true);
        }

        $this->set('login_check', $_SESSION['SAFE_CHECK']);

        $member_id = get_login_member_id();
        
		$main = array("request_talentmail_confirm.tpl");
        
        $this->set('main', $main);
        
        $this->set('sp', $_SESSION['SMARTPHONE_CHECK']);
        
        $this->set('session_id', session_id());
    	return "success";    	
    }
    
    function fail($msg){        
		$_SESSION['mailform_error'] = $msg;
		$host = $_SERVER['HTTP_HOST'];
		$uri = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
		$extra = '?m=portal&a=page_h_talent_mail&ssl_param=1';

        if( 1 == $_SESSION['mailform']['from_oscar'] ){
            $extra .= '&from_oscar=1';
        }

		header("Location: http://$host$uri/$extra");
        //openpne_redirect('portal', 'page_h_talent_mail');
    }
}
