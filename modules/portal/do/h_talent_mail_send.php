<?php 
class portal_do_h_talent_mail_send extends OpenPNE_Action
{ 
	   
    
    function execute($requests)
    {                        
		$params=array();
		$params = $requests;
//              // 脆弱性対応。重要な情報を隠蔽する
//                $params = $_SESSION['mailform'];

        $from_oscar = 0;
        if( 1 == $requests['from_oscar'] ){
            $from_oscar = 1;
        }
        
		// オスカーより依頼。悪戯をしてくるメールアドレスをブラックリスト化
		$spam_mailaddresses = array('0rj5223338k368c@ezweb.ne.jp', 'yamanat.isao@gmail.com');

		if (OPENPNE_USE_CAPTCHA) {
			if (empty($_SESSION['captcha_keystring']) || $_SESSION['captcha_keystring'] !=  $requests['captcha']) {
				unset($_SESSION['captcha_keystring']);
				openpne_redirect('portal', 'page_h_talent_mail', array('_data_area'=>'pub', 'from_oscar' => $from_oscar));
			}
		}

		//It's add by e2info 2010-10-28========================
		$params['PRODUCTION']="株式会社オスカープロモーション";
		$config_productions=get_config_talent_production();
		if($config_productions){
			$from_email=$config_productions[0]['mail'];	    	
			if($requests['talent_id']){
				if(db_member_is_talent($requests['talent_id'])==true){    		
					$talent_production=db_member_get_profile($requests['talent_id'], "talent_production");
					foreach ($config_productions as $key=>$value){
						if($value['key']==$talent_production && $value['mail']!=""){
							$from_email=$value['mail'];
						}
						if($value['key']==$talent_production ){		        			
							$params['PRODUCTION']=$value['key'];
						}
					}
				}
			}
		}
		//======================================================
    	if($requests['Mail']){
    		//send mail to guest
            if( 1 == $from_oscar ){
                fetch_send_mail($requests['Mail'],"h_talent_guest_from_oscar.tpl",$params); //,true,$from_email
            }else{
        		fetch_send_mail($requests['Mail'],"h_talent_guest",$params); //,true,$from_email
            }
    	}

	// 悪戯の問合せの場合スキップ（オスカーより依頼）
    	if( !in_array( $requests['Mail'], $spam_mailaddresses) ){
	    	//send mail to manager    	
	    	if($_SESSION['talent_search']){
	    		$params['talent_search']=$_SESSION['talent_search'];
	    	}
/*
	        if($from_email){
	    		fetch_send_mail($from_email,"h_talent_manager",$params);  //,true,$from_email
	    	}else{
	    		fetch_send_mail(CONTACT_ME_MAIL,"h_talent_manager",$params);
	    	}
*/

		// Backlog連携
		if( OPENPNE_SUBDOMAIN == '' ){
			// 本番環境用
			fetch_send_mail( 'issue-SALES_TEAM-ZXzu9xbZwfEqEDZes7iW9c2MY@i3.backlog.jp', "h_talent_manager", $params );
		}else{
			// ステージング環境用
			fetch_send_mail( 'issue-BEAMIE_RE-hQtlHP0pRD1W6NBID1gAVeKvo@i1.backlog.jp', "h_talent_manager", $params );
		}
	}
//		$this->jobRequestDataBackup($params);

		// 送信者のIPアドレスをセット
		$params['Ipaddress'] = $_SERVER["REMOTE_ADDR"];

		// ISAO向けのメールを送信(入力値+IPアドレス)
//		fetch_send_mail("ba_bcc@isao.co.jp","h_talent_ba_bcc",$params);

    	unset($_SESSION['mailform']);
    	unset($_SESSION['mailform_error']);
    	openpne_redirect('portal', 'page_h_talent_mail_end',array("talent_id"=>$requests['talent_id'], 'from_oscar' => $from_oscar));
    	exit();    	    	    		
    }


	/**
	 * お仕事依頼データのバックアップを取る
	 * @params array datas お仕事依頼データ
	 */
	private function jobRequestDataBackup ($datas) {

		if (empty($datas) === true) return false;

		$ignore_column = array('msg','msg1','msg2','msg3','sessid');
		$job_req_data = array();

		foreach ($datas as $key => $val) {
			if (in_array($key, $ignore_column) === true) continue;
			$job_req_data[] = $key. ':'. $val;
		}

		//$backup_path = OPENPNE_VAR_DIR. '/secure_assets/backup_job_request_mail/';
		$backup_path = '/var/www/openpne/var/secure_assets/backup_job_request_mail/';
		if (file_exists($backup_path) === false) {
			if (mkdir($backup_path) === false) return false;
		}

		$filename = $datas['Mail']. '_'. time(). '.txt';
		file_put_contents($backup_path. $filename, implode("\n", $job_req_data));

		return true;
	}
    
}
