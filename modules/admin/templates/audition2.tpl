({$inc_header|smarty:nodefaults})
({literal})
<script type="text/javascript" >
jQuery(document).ready(function(){	
	jQuery("#submit_pdf").click(function(){		
		var name="audition_id[]";
		var val="";
		jQuery("input[name="+name+"]").each(function(i,item){
			if(jQuery(this).attr("checked")==true){			
				val += jQuery(this).val()+",";
			}
		});
		jQuery("#audition_ids").val(val);		
		jQuery("#frmPdf").submit();
	});
});
</script>
({/literal})
<h2>オーディション応募2</h2>
({if $smarty.request.msg=='success'})
<br/>
<h3 style="color:red;margin-left:20px;">PDFの出力が完了しました</h3>
({/if})
<div class="contents">
<form  action="./" method="get">
<input type="hidden" name="m" value="({$module_name})" />
<input type="hidden" name="a" value="do_({$hash_tbl->hash('csv_audition2','do')})" />
<input type="hidden" name="sessid" value="({$PHPSESSID})" />
<p class="textBtn"><input type="submit" value="ダウンロード" /></p>
</form>

<form id="frmPdf" action="./" method="post">
<input type="hidden" name="m" value="({$module_name})" />
<input type="hidden" name="a" value="do_({$hash_tbl->hash('pdf_audition2','do')})" />
<input type="hidden" name="sessid" value="({$PHPSESSID})" />
<input type="hidden" name="audition_ids" id="audition_ids" value="" />
<input type="hidden" name="page" value="({$page})" />
<p class="textBtn"><input type="button" value="pdf" id="submit_pdf" /></p>
</form>

({capture name="pager"})
<div class="listControl">
<p class="display">
({$total_num})件中 ({$start_num})-({$end_num})件目を表示しています
</p>
<p class="listMove">
({if $page_list})({foreach from=$page_list item=item})({if $page!=$item})<a href="?m=({$module_name})&amp;a=page_({$hash_tbl->hash('audition2','page')})&amp;page=({$item})">({$item})</a>({else})<b>({$item})</b>({/if})&nbsp;&nbsp;({/foreach})&nbsp;({/if})
({if $prev})<a href="?m=({$module_name})&amp;a=page_({$hash_tbl->hash('audition2','page')})&amp;page=({$page-1})">＜＜前</a>　({/if})
({if $next})<a href="?m=({$module_name})&amp;a=page_({$hash_tbl->hash('audition2','page')})&amp;page=({$page+1})">次＞＞</a>({/if})
</p>
</div>
({/capture})

({if $audition_list})
<div class="listControl" id="pager01">
({$smarty.capture.pager|smarty:nodefaults})
</div>
({/if})

<table class="basicType2">
({capture name="table_header"})
<tr>
<th colspan="8"></th>
<th colspan="4">現住所</th>
<th colspan="4"></th>
<th colspan="3">最終学歴</th>
<th colspan="5"></th>
<th colspan="5">サイズ</th>
<th colspan="8"></th>
<th colspan="8">保護者同意欄</th>
<th rowspan="2">操作</th>
</tr>
<tr>
<th></th>
<th>応募日付</th>
<th>ID</th>
<th>メンバーID<br/>
(フォーム記入者)
</th>
<th>氏名（姓+名）</th>
<th>フリガナ(セイ+メイ）</th>
<th>生年月日</th>
<th>年齢</th>
<th>郵便番号</th>
<th>都道府県</th>
<th>市区町村・番地</th>
<th>建物名・号室</th>
<th>自宅電話</th>
<th>携帯電話</th>
<th>メールアドレス</th>
<th>ニックネーム</th>
<th>学校名</th>
<th>在学・卒業</th>
<th>卒業年度</th>
<th>現在のご職業</th>
<th>特技</th>
<th>趣味</th>
<th>自己PR</th>
<th>ご希望応募地区</th>
<th>身長</th>
<th>バスト</th>
<th>ウエスト</th>
<th>ヒップ</th>
<th>靴</th>
<th>写真1</th>
<th>写真2</th>
<th>所属プロダクション</th>
<th>契約期間</th>
<th>参加オーディション名</th>
<th>結果</th>
<th>その他、経験</th>
<th>be amie登録メールアドレス</th>
<th>保護者氏名（姓+名）</th>
<th>フリガナ（セイ+メイ）</th>
<th>郵便番号</th>
<th>都道府県</th>
<th>市区町村・番地</th>
<th>建物名・号室</th>
<th>電話番号</th>
<th>本人との関係</th>
</tr>
({/capture})
<thead>
({$smarty.capture.table_header|smarty:nodefaults})
</thead>
<tbody>
({foreach from=$audition_list item=item})
({if $item})
<tr>
<td class="cell01"><input type="checkbox" name="audition_id[]" value="({$item.audition_id})" checked="checked" /></td>
<td class="cell01">({$item.add_date})</td>
<td class="cell01">({$item.audition_id})</td>
<td class="cell01">({$item.member_id})</td>
<td class="cell01">({$item.name_1})({$item.name_2})</td>
<td class="cell01">({$item.kananame_1})({$item.kananame_2})</td>
<td class="cell01">({$item.birthday_year})-({$item.birthday_month})-({$item.birthday_day})</td>
<td class="cell01">({$item.age})</td>
<td class="cell01">({$item.zip})</td>
<td class="cell01">({$item.address_pref})</td>
<td class="cell01">({$item.address_1})</td>
<td class="cell01">({$item.address_2})</td>
<td class="cell01">({$item.tel_1})-({$item.tel_2})-({$item.tel_3})</td>
<td class="cell01">({$item.mobile_tel_1})-({$item.mobile_tel_2})-({$item.mobile_tel_3})</td>
<td class="cell01"><a href="mailto:({$item.email})?bcc=({$smarty.const.ADMIN_EMAIL})">({$item.email})</a></td>
<td class="cell01">({$item.login_nickname})</td>
<td class="cell01">({$item.education})</td>
<td class="cell01">
({if $item.in_school=='1' || $item.in_school=='3'})
	({if $item.in_school==1})在学({else})中退({/if})
({else})卒業
({/if})</td>
<td class="cell01">({$item.graduation_year})</td>
<td class="cell01">({$item.job})</td>
<td class="cell01">({$item.speciality})</td>
<td class="cell01">({$item.interest})</td>
<td class="cell01">({$item.owner_PR})</td>
<td class="cell01">({$item.wish_addr})</td>
<td class="cell01">({$item.body_1})</td>
<td class="cell01">({$item.body_2})</td>
<td class="cell01">({$item.body_3})</td>
<td class="cell01">({$item.body_4})</td>
<td class="cell01">({$item.body_5})</td>
<td class="cell01">
({if $item.photo_filename_1})
<a href="({t_url m=admin a=page_audition_file})&amp;filename=({$item.photo_filename_1})" target="_blank">
({if $item.photo_1_zip=='1'})
({$item.photo_filename_1})
({else})
<img src="({t_url m=admin a=page_audition_file})&amp;filename=({$item.photo_filename_1})" alt="" width="150px"/>
({/if})
</a> 
({/if})
</td>
<td class="cell01">
({if $item.photo_filename_2})
<a href="({t_url m=admin a=page_audition_file})&amp;filename=({$item.photo_filename_2})" target="_blank">
({if $item.photo_2_zip=='1'})
({$item.photo_filename_2})
({else})
<img src="({t_url m=admin a=page_audition_file})&amp;filename=({$item.photo_filename_2})" alt="" width="150px" />
({/if})
</a> 
({/if})
</td>
<td class="cell01">({$item.production})</td>
<td class="cell01">({$item.contact_period })</td>
<td class="cell01">({$item.audition_name})</td>
<td class="cell01">({$item.audition_result})</td>
<td class="cell01">({$item.other_experience})</td>
<td class="cell01">({$item.email_login}) </td>
<td class="cell01">({$item.r_name_1}) ({$item.r_name_2})</td>
<td class="cell01">({$item.r_kana_name_1}) ({$item.r_kana_name_2})</td>
<td class="cell01">({$item.r_zip})</td>	
<td class="cell01">({$item.r_address_pref})</td>
<td class="cell01">({$item.r_address_1})</td>
<td class="cell01">({$item.r_address_2})</td>
<td class="cell01">({$item.r_tel_1})-({$item.r_tel_2})-({$item.r_tel_3})</td>
<td class="cell01">({$item.r_relationship})</td>
<td class="cell01">
<form action="./" method="post">
({strip})
<input type="hidden" name="m" value="({$module_name})" />
<input type="hidden" name="a" value="do_({$hash_tbl->hash('delete_audition2','do')})" />
<input type="hidden" name="sessid" value="({$PHPSESSID})" />
<input type="hidden" name="audition_id" value="({$item.audition_id})" />
<input type="hidden" name="page" value="({$page})" />
<span class="textBtnS"><input type="submit" value="　削　除　"  onclick="javascript:return confirm('本当に削除しますか？');"/></span>
({/strip})
</form>
</td>
</tr>
({/if})
({foreachelse})
<tr>
<td colspan="5">応募データは登録されていません</td>
</tr>
({/foreach})
</table>

</div>
({$inc_footer|smarty:nodefaults})