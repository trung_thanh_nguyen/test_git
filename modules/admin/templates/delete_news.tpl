({$inc_header|smarty:nodefaults})
({ext_include file="inc_subnavi_adminAdminConfig.tpl"})

({assign var="page_name" value="ニュース記事削除"})
({ext_include file="inc_tree_adminAdminConfig.tpl"})
</div>

({*ここまで:navi*})

({if $msg})<p class="actionMsg">({$msg})</p>({/if})
<h2>ニュース記事削除</h2>
<div class="contents">
<p class="info" id="c01">
指定されたニュースIDの記事を削除します。<br/>
※ニュースIDはカンマ区切りで複数指定することが可能です。
</p>
<form action="./" method="post">
<table class="basicType1">
<tr>
<th>
<input type="hidden" name="sessid" value="({$PHPSESSID})" />
<input type="hidden" name="m" value="({$module_name})" />
<input type="hidden" name="a" value="do_({$hash_tbl->hash('delete_c_news','do')})" />
ニュースID</th>
<td><input class="basic" type="text" name="news_ids" value=""  style="width:600px;"/></td>
</tr>
</table>
<p class="textBtn"><input type="submit" value="削除する"></p>
</form>

({$inc_footer|smarty:nodefaults})
