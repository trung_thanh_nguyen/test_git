({$inc_header|smarty:nodefaults})
({ext_include file="inc_subnavi_adminDesign.tpl"})

({assign var="page_name" value="フリーページ管理"})
({ext_include file="inc_tree_adminDesign.tpl"})
</div>

({*ここまで:navi*})

<h2>ブログパーツ管理</h2>
<div class="contents">

({if $msg})
<p class="actionMsg">({$msg})</p>
({/if})
<p>任意のブログパーツをシステムに登録できます。</p>

<h3 class="item">ブログパーツリスト
</h3>

({if $pager && $pager.total_num > 0})
<div class="listControl" id="pager01">

({capture name="pager"})
<p class="display">
({$pager.total_num}) 件中 ({$pager.start_num}) - ({$pager.end_num})件目を表示しています
</p>
<p class="listMove">
({if $pager.prev_page})
<a href="?m=({$module_name})&amp;a=page_({$hash_tbl->hash('list_c_free_page')})&amp;page=({$pager.prev_page})&amp;page_size=({$pager.page_size})({$cond})">前へ</a>&nbsp;
({/if})
({foreach from=$pager.disp_pages item=i})
({if $i == $pager.page})
&nbsp;<strong>({$i})</strong>&nbsp;
({else})
<a href="?m=({$module_name})&amp;a=page_({$hash_tbl->hash('list_c_free_page')})&amp;page=({$i})&amp;page_size=({$pager.page_size})({$cond})">&nbsp;({$i})&nbsp;</a>
({/if})
({/foreach})
({if $pager.next_page})
&nbsp;<a href="?m=({$module_name})&amp;a=page_({$hash_tbl->hash('list_c_free_page')})&amp;page=({$pager.next_page})&amp;page_size=({$pager.page_size})({$cond})">次へ</a>
({/if})
</p>
({/capture})

({$smarty.capture.pager|smarty:nodefaults})

</div>
({/if})

<table class="basicType2">
<thead>
({****})
<tr>
<th>ブログパーツID</th>
<th>ブログパーツリスト </th>
<th colspan="2">操作</th>
</tr>
({****})
</thead>
<tbody>
({****})
({foreach from=$c_free_page_list item=item})
({if $item})
<tr>
<td>({$item.c_blogpart_id})</td>
<td><a href='?m=({$module_name})&amp;a=page_({$hash_tbl->hash('update_c_blog_part','page')})&amp;c_blogpart_id=({$item.c_blogpart_id})'>({$item.title|default:"タイトルなし"})</a></td>
<td><a href='?m=({$module_name})&amp;a=page_({$hash_tbl->hash('update_c_blog_part','page')})&amp;c_blogpart_id=({$item.c_blogpart_id})'>編集</a></td>
<td><a href='?m=({$module_name})&amp;a=page_({$hash_tbl->hash('delete_c_blog_part_confirm','page')})&amp;c_blogpart_id=({$item.c_blogpart_id})'>削除</a></td>
</tr>
({/if})
({foreachelse})
<tr>
<td colspan="6" class="empty">フリーページが登録されていません</td>
</tr>
({****})
({/foreach})
</tbody>
</table>

({if $pager && $pager.total_num > 0})
<div class="listControl" id="pager02">
({$smarty.capture.pager|smarty:nodefaults})
</div>
({/if})


<h3 class="item">ブログパーツの新規追加</h3>

<form action="./" method="post">
<input type="hidden" name="m" value="({$module_name})" />
<input type="hidden" name="a" value="do_({$hash_tbl->hash('insert_c_blog_part','do')})" />
<input type="hidden" name="sessid" value="({$PHPSESSID})" />

<table class="basicType2">
<tbody>
<tr>
<th>ブログパーツタイトル</th>
<td><input class="basic" type="text" name="title" size="({$cols|default:72})" /></td>
</tr>
<tr>
<th>スクリプト</th>
<td>
<textarea class="basic" name="body" cols="({$cols|default:72})" rows="({$rows|default:10})"></textarea>
</td>
</tr>

<tr>
<td colspan="2">
<p class="textBtn"><input type="submit" class="submit" value="追加する"></p>
</td>
</tr>
</tbody>
</table>

</form>

({$inc_footer|smarty:nodefaults})
