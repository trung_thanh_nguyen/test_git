({$inc_header|smarty:nodefaults})
({ext_include file="inc_subnavi_adminDesign.tpl"})

({assign var="parent_page_name" value="ブログパーツ管理"})
({capture name=parent_page_url})?m=({$module_name})&amp;a=page_({$hash_tbl->hash('list_c_free_page')})({/capture})

({assign var="page_name" value="ブログパーツリスト編集"})
({ext_include file="inc_tree_adminDesign.tpl"})
</div>

({*ここまで:navi*})

({if $msg})<p class="actionMsg">({$msg})</p>({/if})
<h2>ブログパーツリスト 編集</h2>
<div class="contents">

<form action="./" method="post">
<input type="hidden" name="m" value="({$module_name})">
<input type="hidden" name="a" value="do_({$hash_tbl->hash('update_c_blog_part','do')})">
<input type="hidden" name="sessid" value="({$PHPSESSID})">
<input type="hidden" name="c_blogpart_id" value="({$c_blogpart.c_blogpart_id})">

<table class="basicType2">
<tbody>
<tr>
<th>ブログパーツタイトル</th>
<td><input class="basic" type="text" name="title" value="({$c_blogpart.title})" size="({$cols|default:72})" /></td>
</tr>
<tr>
<th>スクリプト</th>
<td>
<textarea class="basic" name="body" cols="({$cols|default:60})" rows="({$rows|default:10})">({$c_blogpart.script})</textarea>
</td>
</tr>

<tr>
<td colspan="2">
<p class="textBtn"><input type="submit" class="submit" value="編集する"></p>
</td>
</tr>
</tbody>
</table>

</form>

<p class="groupLing"><a href="?m=({$module_name})&amp;a=page_({$hash_tbl->hash('list_c_blog_part')})">戻る</a></p>

({$inc_footer|smarty:nodefaults})
