({$inc_header|smarty:nodefaults})
({ext_include file="inc_subnavi_adminSNSConfig.tpl"})

({assign var="page_name" value="SNS設定"})
({ext_include file="inc_tree_adminSNSConfig.tpl"})


({*ここまで:navi*})


<h2>CSV出力用タレントID設定</h2>
<div class="contents">

({if $msg})<p class="actionMsg">({$msg})</p>({/if})
<p class="info"></p>
<p class="caution" id="c01"></p>
<form action="./" method="post">
<input type="hidden" name="m" value="({$module_name})" />
<input type="hidden" name="a" value="do_({$hash_tbl->hash('update_c_admin_config_talent' ,'do')})" />
<input type="hidden" name="sessid" value="({$PHPSESSID})" />
<input type="hidden" name="value" value="1" />
<p class="add">
<input type="text" name="TALENTCOMMU" value="({$smarty.const.TALENTCOMMU})" />
</p>
<p class="textBtn">
<input type="submit" value="設定変更する">
</p>
</form>
</div>

({$inc_footer|smarty:nodefaults})
