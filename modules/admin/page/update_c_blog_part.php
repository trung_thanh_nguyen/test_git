<?php
/**
 * @copyright 2005-2008 OpenPNE Project
 * @license   http://www.php.net/license/3_01.txt PHP License 3.01
 */

// フリーページ編集
class admin_page_update_c_blog_part extends OpenPNE_Action
{
    function execute($requests)
    {
        $this->set("c_blogpart", db_admin_get_c_blog_part_one($requests['c_blogpart_id']));
        return 'success';
    }
}
