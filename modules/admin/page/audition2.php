<?php
/**
 * @copyright 2005-2008 OpenPNE Project
 * @license   http://www.php.net/license/3_01.txt PHP License 3.01
 */

class admin_page_audition2 extends OpenPNE_Action
{
    function execute($requests)
    {
        $page = $requests['page'];
        $page_size = 20;
        list($audition_list, $prev, $next, $total_num, $total_page_num)
         = db_audition_new_list($page, $page_size);
         if($page>1 && count($audition_list)==0){
         	$page--;
         	list($audition_list, $prev, $next, $total_num, $total_page_num)
         		= db_audition_new_list($page, $page_size);
         }
        for($i = $page-10<1 ? 1 : $page-10 ;($i<=$total_page_num)&&($i<$page+10);$i++)
            $page_list[]=$i;
        $this->set('page_list', $page_list);

        $this->set('page', $page);
        $this->set('prev', $prev);
        $this->set('next', $next);
        $this->set('total_num', $total_num);
        $this->set('start_num', ($page-1)*$page_size+1);
        $this->set('end_num', ($page-1)*$page_size+count($audition_list));
        
        foreach ($audition_list as $key=> $item){
//        	if($item['movie_filename']){
//        		$audition_list[$key]['movie_url']=m_get_diary_upfile_movie_url($item['movie_filename']); 
//        	}
        	if($item['photo_filename_1']){
        		if(in_array(m_extname($item['photo_filename_1']),array(".zip",".rar"))){        			
        			$audition_list[$key]['photo_1_zip']=1;
        		}
        	}
         	if($item['photo_filename_2']){
        		if(in_array(m_extname($item['photo_filename_2']),array(".zip",".rar"))){
        			$audition_list[$key]['photo_2_zip']=1;
        		}
        	}
//        	if($item['photo_filename_3']){
//        		if(in_array(m_extname($item['photo_filename_3']),array(".zip",".rar"))){
//        			$audition_list[$key]['photo_3_zip']=1;
//        		}
//        	}
        }
        $this->set("audition_list", $audition_list);        
        return 'success';
    }
}

?>