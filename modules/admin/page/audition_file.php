<?php
/**
 * @copyright 2005-2008 OpenPNE Project
 * @license   http://www.php.net/license/3_01.txt PHP License 3.01
 */

class admin_page_audition_file extends OpenPNE_Action
{
    function execute($requests)
    {
    	$target_file_name=isset($_GET['filename'])? htmlspecialchars($_GET['filename']):"";        
		if($target_file_name){
			$fileurl=m_get_upfile($target_file_name);
		}
		//die(OPENPNE_DIR);		
		if( in_array(m_extname($target_file_name),array(".jpg",".gif",".png",".jpeg"))){			
			while (@ob_end_clean());
			header("Content-type: image/jpeg");
			$content=readfile($fileurl);			
			echo $content;
			return ;
		}
    	if( in_array(m_extname($target_file_name),array(".zip",".rar"))){			
			while (@ob_end_clean());
			header("Content-type: application/zip");
			$content=readfile($fileurl);			
			echo $content;
			return ;
		}
    	if( in_array(m_extname($target_file_name),array(".flv",".3gp","gp2","mov","wmv"))){			
			while (@ob_end_clean());
			header("Content-type: video/quicktime");
			$content=readfile($fileurl);			
			echo $content;
			return ;
		}		
        return ;
    }
}