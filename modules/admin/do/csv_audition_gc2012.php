<?php 
class admin_do_csv_audition_gc2012 extends OpenPNE_Action
{
    function isSecure()
    {
        session_cache_limiter('public');
        return true;
    }

    function handleError($errors)
    {
        admin_client_redirect('audition_gc2012', array_shift($errors));
    }

    function execute($requests)
    {
    	$audtion_list=db_audition_gc_list_all();    	
    	foreach ($audtion_list as $key=>$value){    		
//     		$audtion_list[$key]['in_school']=($value['in_school']=='1')?"在学":"卒業";
//     		$audtion_list[$key]['production_radio']=($value['production_radio']=='1')?"はい":"いいえ";
//     		$audtion_list[$key]['audition_radio']=($value['audition_radio']=='1')?"はい":"いいえ";
    		if($value['photo_filename_1']){
    			$fileurl=m_get_audition_gc_photofile($value['photo_filename_1'],$value['audition_id']);    			
    		}
    		if($audtion_list[$key]['photo_filename_2']){
    			$fileurl=m_get_audition_gc_photofile($value['photo_filename_2'],$value['audition_id']);   
    		}
//    		if($audtion_list[$key]['photo_filename_3']){
//    			$fileurl=m_get_upfile($value['photo_filename_3']); 
//    		}
//    		if($audtion_list[$key]['movie_filename']){
//    			$fileurl=m_get_upfile($value['movie_filename']);
//    		}
			if($value['ext_data']){
	    		$ext_data = unserialize($value['ext_data']);
	    		if($ext_data){	    		
	    			$audtion_list[$key] = array_merge($ext_data,$value); 
	    		}
			}  		
    	}
    	
    	
    	$key_array=$this->get_key_list();
    	$audition_csv_data = $this->create_csv_data($key_array, $audtion_list);
        //IE以外の場合、キャッシュをさせないヘッダを出力
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false) {
            send_nocache_headers(true);
        }
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=audition.csv");
        echo $audition_csv_data;
        exit;    
    }
    

    function get_key_list()
    {    	
    	$key_array=array(
    			'audition_id'=>'SEQ',
    			'member_id'=>'UID',
    			'null'=>'キャリア',
    			'email'=>'メールアドレス',
    			'name_1,name_2'=>'氏名',
    			'kananame_1,kananame_2'=>'ふりがな',
    			'birthday'=>'生年月日',
    			'age'=>'年齢',
    			'zip'=>'郵便番号',
    			'address_pref,address_1,address_2'=>'住所',
    			'tel_1,tel_2,tel_3'=>'電話番号',
    			'education'=>'学校名',
    			'graduation_year'=>'学年',
    			'body_1'=>'身長',
    			'body_2'=>'ﾊﾞｽﾄ',
    			'body_3'=>'ｳｪｽﾄ',
    			'body_4'=>'ﾋｯﾌﾟ',
    			'body_5'=>'ｼｭｰｽﾞ',
    			'wish_department'=>'将来の希望',
    			'speciality'=>'特技',
    			'interest'=>'趣味',
    			'owner_PR'=>'自己PR',
    			'family_f,family_f_name'=>'家族構成(1)',
    			'family_m,family_m_name'=>'家族構成(2)',
    			'family_b,family_b_name'=>'家族構成(3)',
    			'family_yb,family_yb_name'=>'家族構成(4)',
    			'family_s,family_s_name'=>'家族構成(5)',
    			'r_name_1,r_name_2'=>'保護者様のお名前',
    			'r_zip'=>'郵便番号',
    			'r_address_pref,r_address_1,r_address_2'=>'住所',    			
    			'r_tel_1,r_tel_2,r_tel_3'=>'保護者連絡先',
    			'r_relationship'=>'応募者との関係',
    			'photo_filename_1'=>'全身画像',
    			'photo_filename_2'=>'上半身画像',
    			'enquete'=>'アンケート',
    			'adddate'=>'登録日時',
    			);
        return $key_array;
    }
    
 	function create_csv_data(&$key_list, $value_list)
    {
    	
        $csv = "";
        foreach($key_list as $key=>$value){
        	$csv .='"' . mb_convert_encoding($value, 'SJIS', 'UTF-8') . '",';
        }
        $csv=rtrim($csv,',');
        $csv .= "\n";
      
        foreach($value_list as $key => $value){
        	$temp="";
        	foreach($key_list as $key_key=>$key_value){
        		if($key_key=='birthday'){
        			$value[$key_key]=sprintf('%s/%d/%d',$value['birthday_year'],$value['birthday_month'],$value['birthday_day']);
        		}
        		if ($key_key=='enquete'){
					if ($value['enquete']=="新聞") {$value[$key_key] = $value['enquete']."(".$value['enqtext3'].")";}
					if ($value['enquete']=="雑誌") {$value[$key_key] = $value['enquete']."(".$value['enqtext4'].")";}
					if ($value['enquete']=="その他") {$value[$key_key] = $value['enquete']."(".$value['enqtext7'].")";}
        		}
        		if(strstr($key_key,',')){
        			$arr=explode(',',$key_key);
        			if(strstr($key_key,'family')){
        				if($value[$arr[0]] || $value[$arr[1]] ){
        					$temp .= '"';
        					foreach($arr as $va){
        						if(in_array($va,array('family_f_name','family_m_name','family_b_name','family_yb_name','family_s_name'))){
        							$temp .='['. mb_convert_encoding($value[$va], 'SJIS', 'UTF-8') .']';
        						}else{
        							$temp .= mb_convert_encoding($value[$va], 'SJIS', 'UTF-8') .' ' ;
        						}
        					}
        					$temp = rtrim($temp,' ');
        					$temp .= '",';
        				}else{
        					$temp .= '"",';
        				}
        			}else{
	        			$temp .= '"';
	        			foreach($arr as $va){
	        				$temp .= mb_convert_encoding($value[$va], 'SJIS', 'UTF-8') .' ' ;
	        			} 
	        			$temp = rtrim($temp,' ');
	        			$temp .= '",';   
        			}  			
        		}else{
        			if($key_key=='null'){
        				$temp .= '"",';
        			}else{
        				$temp .= '"' . mb_convert_encoding($value[$key_key], 'SJIS', 'UTF-8') . '",';
        			}
        		}
        	}
        	$temp = rtrim($temp,',');
        	$temp .="\n";
        	$csv .= $temp;
        }
        

        return $csv;
    }
    
}
