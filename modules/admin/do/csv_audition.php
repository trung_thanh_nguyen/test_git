<?php 
class admin_do_csv_audition extends OpenPNE_Action
{
    function isSecure()
    {
        session_cache_limiter('public');
        return true;
    }

    function handleError($errors)
    {
        admin_client_redirect('audition', array_shift($errors));
    }

    function execute($requests)
    {
    	$audtion_list=db_audition_list_all();
    	foreach ($audtion_list as $key=>$value){    		
    		$audtion_list[$key]['in_school']=($value['in_school']=='1')?"在学":"卒業";
    		$audtion_list[$key]['production_radio']=($value['production_radio']=='1')?"はい":"いいえ";
    		$audtion_list[$key]['audition_radio']=($value['audition_radio']=='1')?"はい":"いいえ";
    		if($value['photo_filename_1']){
    			$fileurl=m_get_upfile($value['photo_filename_1']);    			
    		}
    		if($audtion_list[$key]['photo_filename_2']){
    			$fileurl=m_get_upfile($value['photo_filename_2']);   
    		}
    		if($audtion_list[$key]['photo_filename_3']){
    			$fileurl=m_get_upfile($value['photo_filename_3']); 
    		}
    		if($audtion_list[$key]['movie_filename']){
    			$fileurl=m_get_upfile($value['movie_filename']);
    		}
    		
    	}
    	
    	$key_string=$this->get_key_list();
    	$audition_csv_data = $this->create_csv_data($key_string, $audtion_list);
        //IE以外の場合、キャッシュをさせないヘッダを出力
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false) {
            send_nocache_headers(true);
        }
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=audition.csv");
        echo $audition_csv_data;
        exit;    
    }
    

    function get_key_list()
    { 
    	$ley_list=array(
    	"ID",
    	"メンバーＩＤ",
    	"姓",
    	"名",
    	"セイ",
    	"メイ",
    	"生年",
    	"生月",
    	"生日",
    	"歳",
    	"郵便番号",
    	"都道府県",
    	"市区町村・番地",
    	"建物名・号室",
    	"自宅電話番号1",
    	"自宅電話番号2",
    	"自宅電話番号3",
    	"携帯電話番号1",
    	"携帯電話番号2",
    	"携帯電話番号3",
    	"メールアドレス",
    	"学校",
    	"在学",
    	"西暦",
    	"現在の職業",
    	"特技",
    	"趣味",
    	"自己PR",
    	"希望部門",
    	"身長",
    	"バスト",
    	"ウェスト",
    	"ヒップ",
    	"靴",
    	"写真1",
    	"写真2",
    	"写真3",
    	"動画1",
    	"所属",
    	"所属事務所名",
    	"契約期間",
    	"オーディション",
    	"オーディション名",
    	"結果",
    	"その他、経験",
    	"登録情報",
    	"ニックネーム",
    	"推薦者のメールアドレス ",
    	"推薦者ニックネーム",
    	"姓",
    	"名",
    	"セイ",
    	"メイ",
    	"郵便番号",
    	"都道府県",
    	"市区町村・番地",
    	"建物名・号室",
    	"電話番号1",
    	"電話番号2",
    	"電話番号3",
    	"本人との関係",
    	"応募日",
    	);
//    	"name_1",
//    	"name_2",
//    	"kananame_1",
//    	"kananame_2",
//    	"birthday_year",
//    	"birthday_month",
//    	"birthday_day",
//    	"age",
//    	"zip",
//    	"address_pref",
//    	"address_1",
//    	"address_2",
//    	"tel_1",
//    	"tel_2",
//    	"tel_1",
//    	"mobile_tel_1",
//    	"mobile_tel_2",
//    	"mobile_tel_3",
//    	"email",
//    	"education",
//    	"in_school",
//    	"graduation_year",
//    	"job",
//    	"speciality",
//    	"interest",
//    	"owner_PR",
//    	"wish_department",
//    	"body_1",
//    	"body_2",
//    	"body_3",
//    	"body_4",
//    	"body_5",
//    	"photo_filename_1",
//    	"photo_filename_2",
//    	"photo_filename_3",
//    	"movie_filename",
//    	"production_radio",
//    	"production",
//    	"contact_period",
//    	"audition_radio",
//    	"audition_name",
//    	"audition_result",
//    	"other_experience",
//    	"email_login",
//    	"login_nickname",
//    	"r_email",
//    	"r_nickname",
//    	"r_name_1",
//    	"r_name_2",
//    	"r_kana_name_1",
//    	"r_kana_name_2",
//    	"r_zip",
//    	"r_address_pref",
//    	"r_address_1",
//    	"r_address_2",
//    	"r_tel_1",
//    	"r_tel_2",
//    	"r_tel_3",
//    	"r_relationship",    	
//    	); 
        return $ley_list;
    }
    
 	function create_csv_data($key_string, $value_list)
    {
        $csv = "";
        foreach ($key_string as $each_key) {
            if ($csv != "") {
                $csv .= ",";
            }
            $csv .= '"' . mb_convert_encoding($each_key, 'SJIS', 'UTF-8') . '"';
        }
        $csv .= "\n";

        foreach ($value_list as $key => $value) {
            $temp = "";
            foreach ($value as $key2 => $value2) {
                $value2 = mb_convert_encoding($value2, 'SJIS', 'UTF-8');
                //It's add by sunny 2010-12-11=====================
    			$t_arr=array('tel_1','tel_2','tel_3','mobile_tel_1','mobile_tel_2','mobile_tel_3','r_tel_1','r_tel_2','r_tel_3');
    			if(in_array($key2, $t_arr)){
    				$temp .= '="' . $value2 . '",';
    			}else{
	                if ($value2 != null) $value2 = str_replace('"', '""', $value2);//クォート
	                if ($value2 != null) $value2 = str_replace("\r","",$value2);//改行コードを変換
	                $temp .= '"' . $value2 . '",';
    			}
            }
            $csv .= $temp . "\n";
        }
        return $csv;
    }
    
}