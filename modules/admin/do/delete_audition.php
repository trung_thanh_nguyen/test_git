<?php
/**
 * @copyright 2005-2008 OpenPNE Project
 * @license   http://www.php.net/license/3_01.txt PHP License 3.01
 */

// バナー削除
class admin_do_delete_audition extends OpenPNE_Action
{
    function execute($requests)
    {
        db_audition_delete_auditon($requests['audition_id']);
        admin_client_redirect('audition', 'バナーを削除しました',"page=".$requests['page']);
    }
}
