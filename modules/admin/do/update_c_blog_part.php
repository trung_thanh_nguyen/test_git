<?php
/**
 * @copyright 2005-2008 OpenPNE Project
 * @license   http://www.php.net/license/3_01.txt PHP License 3.01
 */

// フリーページ編集
class admin_do_update_c_blog_part extends OpenPNE_Action
{
    function handleError($errors)
    {
        admin_client_redirect('update_c_blog_part', array_shift($errors), 'c_blogpart_id='.$this->requests['c_blogpart_id']);
    }

    function execute($requests)
    {
    	
        db_admin_update_c_blog_part($requests['c_blogpart_id'], $requests['title'], $requests['body']);

        admin_client_redirect('list_c_blog_part', 'ブログパーツを編集しました。');
    }
}