<?php 
require_once  OPENPNE_LIB_DIR . '/include/fpdf/japanese.php';

class admin_do_pdf_audition2 extends OpenPNE_Action
{
    function isSecure()
    {
        session_cache_limiter('public');
        return true;
    }

    function handleError($errors)
    {
        admin_client_redirect('audition2', array_shift($errors));
    }

    function execute($requests)
    {
    	$page = $requests['page'];
    	ob_start();
    	if (!defined('CHAR_CODE')) define('CHAR_CODE','UTF8');
    	if (!defined('RELATIVE_PATH')) define('RELATIVE_PATH','');
			if (!defined('FPDF_FONTPATH')) define('FPDF_FONTPATH',OPENPNE_LIB_DIR.'/include/fpdf/font/');
			
			define('PDF_TEMPLATE_DIR', OPENPNE_VAR_DIR. '/pdf/');
			define('PDF_IMG_DIR', HTML_PATH. USER_DIR. USER_PACKAGE_DIR. TEMPLATE_NAME. '/img/pdf/');
			$this->tpl_pdf= OPENPNE_LIB_DIR.'/include/fpdf/audition_girl.pdf';
			$ids=rtrim($_POST['audition_ids'],',');	
  	
    	$audtion_list=db_audition_list_new_by_ids($ids);
    	
    	foreach ($audtion_list as $key=>$value){    		
    		$value['owner_PR']= $this->sjis_conv($value['owner_PR']);		
    		//$value['owner_PR'] = mb_substr($value['owner_PR'], 0,450,"sjis");    		
    		$value['other_experience']=  $this->sjis_conv($value['other_experience']);
    		//$value['other_experience'] = mb_substr($value['other_experience'],0,450,"sjis"); 
    		  		
    		$value['in_school']=($value['in_school']=='1')?"在学":"卒業";
    		$value['production_radio'] = ($value['production_radio']=='1')?"はい":"いいえ";
    		$value['audition_radio'] = ($value['audition_radio']=='1')?"はい":"いいえ";
    		if($value['photo_filename_1']){
    			$value['photo_filename_1'] =m_get_upfile($value['photo_filename_1']);     						
    		}
    		if($audtion_list[$key]['photo_filename_2']){
    			$value['photo_filename_2']=m_get_upfile($value['photo_filename_2']);   
    		}
    		if($value['zip']){
    			$value['zip']=substr($value['zip'],0,3).'- '.substr($value['zip'],3,4);
    		}
    		if($value['r_zip']){
    			$value['r_zip']=substr($value['r_zip'],0,3).'- '.substr($value['r_zip'],3,4);
    		}
    		$this->create_pdf($value);
    	}     	
    	admin_client_redirect('audition2', 'success','page='.$page);
    	ob_end_flush(); 	
    	  
    	
    }
 // 文字コードSJIS変換 -> japanese.phpで使用出来る文字コードはSJISのみ
    function sjis_conv($conv_str) {
      return (mb_convert_encoding($conv_str, "SJIS", CHAR_CODE));
    }
 	function utf8_conv($conv_str) {
      return (mb_convert_encoding($conv_str,CHAR_CODE, "SJIS" ));
    }
    
 // PDF_Japanese::Text へのパーサー
    function lfText($x, $y, $text, $size=9, $style = '') {
        $text = mb_convert_encoding($text, "SJIS", CHAR_CODE);
        $this->pdf->SetFont('SJIS', $style, $size);
        //$this->pdf->SetFont('MSPGothic', $style, $size);
        
        $this->pdf->Text($x, $y, $text);
    }
    
    function lfText_sjis($x, $y, $text, $size=9, $style = '') {
        $this->pdf->SetFont('SJIS', $style, $size); 
        $this->pdf->Text($x, $y, $text);
    }
    
    function png2jpg($originalFile,$outputFile,$quality=100){
    	$image= imagecreatefrompng($originalFile);
    	imagejpeg($image,$outputFile,$quality);
    	imagedestroy($image);
    }
    function bmp2jpg($originalFile,$outpubFile,$quality=100){
    	$image =$this->ImageCreateFromBMP($originalFile);
		imagejpeg($image,$outpubFile,$quality); 
		imagedestroy($image);
    }
    
	function ImageCreateFromBMP($filename){
		$res='';
		if (! $f1 = fopen ($filename ,"rb")) return FALSE ;
		$FILE = unpack ("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread ($f1 ,14 ));
		if ($FILE ['file_type'] != 19778 ) return FALSE ;
		$BMP = unpack ('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel'. '/Vcompression/Vsize_bitmap/Vhoriz_resolution'. '/Vvert_resolution/Vcolors_used/Vcolors_important', fread ($f1 ,40 ));
		$BMP ['colors'] = pow (2 ,$BMP ['bits_per_pixel']);
		if ($BMP ['size_bitmap'] == 0 ) $BMP ['size_bitmap'] = $FILE ['file_size'] - $FILE ['bitmap_offset'];
		$BMP ['bytes_per_pixel'] = $BMP ['bits_per_pixel']/8 ;
		$BMP ['bytes_per_pixel2'] = ceil ($BMP ['bytes_per_pixel']);
		$BMP ['decal'] = ($BMP ['width']*$BMP ['bytes_per_pixel']/4 );
		$BMP ['decal'] -= floor ($BMP ['width']*$BMP ['bytes_per_pixel']/4 );
		$BMP ['decal'] = 4 -(4 *$BMP ['decal']);
		if ($BMP ['decal'] == 4 ) $BMP ['decal'] = 0 ;
		$PALETTE = array ();
		if ($BMP ['colors'] < 16777216 )
		{
		$PALETTE = unpack ('V'. $BMP ['colors'], fread ($f1 ,$BMP ['colors']*4 ));
		}
		$IMG = fread ($f1 ,$BMP ['size_bitmap']);
		$VIDE = chr (0 );
		$res=imagecreatetruecolor($BMP['width'],$BMP['height']);
		$P = 0 ;
		$Y = $BMP ['height']-1 ;
		while ($Y >= 0 )
		{
		$X =0 ;
		while ($X < $BMP ['width'])
		{
		if ($BMP ['bits_per_pixel'] == 24 ) $COLOR = unpack ("V",substr ($IMG ,$P ,3 ). $VIDE );
		elseif ($BMP ['bits_per_pixel'] == 16 )
		{
		$COLOR = unpack ("n",substr ($IMG ,$P ,2 ));
		$COLOR [1 ] = $PALETTE [$COLOR [1 ]+1 ];
		} elseif ($BMP ['bits_per_pixel'] == 8 )
		{
		$COLOR = unpack ("n",$VIDE.substr ($IMG ,$P ,1 ));
		$COLOR [1 ] = $PALETTE [$COLOR [1 ]+1 ];
		} elseif ($BMP ['bits_per_pixel'] == 4 )
		{
		$COLOR = unpack ("n",$VIDE.substr ($IMG ,floor ($P ),1 ));
		if(($P *2 )%2 == 0 ) $COLOR [1 ] = ($COLOR [1 ] >> 4 ) ;
		else $COLOR [1 ] = ($COLOR [1 ] & 0x0F );
		$COLOR [1 ] = $PALETTE [$COLOR [1 ]+1 ];
		} elseif ($BMP ['bits_per_pixel'] == 1 )
		{
		$COLOR = unpack ("n",$VIDE.substr ($IMG ,floor ($P ),1 ));
		if (($P *8 )%8 == 0 ) $COLOR [1 ] = $COLOR [1 ] >>7 ;
		elseif (($P *8 )%8 == 1 ) $COLOR [1 ] = ($COLOR [1 ] & 0x40 )>>6 ;
		elseif (($P *8 )%8 == 2 ) $COLOR [1 ] = ($COLOR [1 ] & 0x20 )>>5 ;
		elseif (($P *8 )%8 == 3 ) $COLOR [1 ] = ($COLOR [1 ] & 0x10 )>>4 ;
		elseif (($P *8 )%8 == 4 ) $COLOR [1 ] = ($COLOR [1 ] & 0x8 )>>3 ;
		elseif (($P *8 )%8 == 5 ) $COLOR [1 ] = ($COLOR [1 ] & 0x4 )>>2 ;
		elseif (($P *8 )%8 == 6 ) $COLOR [1 ] = ($COLOR [1 ] & 0x2 )>>1 ;
		elseif (($P *8 )%8 == 7 ) $COLOR [1 ] = ($COLOR [1 ] & 0x1 );
		$COLOR [1 ] = $PALETTE [$COLOR [1 ]+1 ];
		} else return FALSE ;
		imagesetpixel ($res ,$X ,$Y ,$COLOR [1 ]);
		$X ++;
		$P += $BMP ['bytes_per_pixel'];
		} $Y --;
		$P +=$BMP ['decal'];
		} fclose ($f1 );
		return $res ;
	}
    
    function create_pdf($data){
    	$padding_y=26.5;
    	$this->pdf  = new PDF_Japanese('P', 'pt');
    		// PDF BLOCKのプロパティ
		$this->block_option = "encoding=UniJIS-UCS2-H textformat=utf8 fontname=MSPGothicHeiseiMin-W3 textflow=true";		
				
		$pagecount =$this->pdf->setSourceFile($this->tpl_pdf);
		$tplidx = $this->pdf->importPage(1); 
		$this->pdf->addPage();
				//$this->pdf->useTemplate($tplidx, 10, 10, 90);
		$this->pdf->useTemplate($tplidx);
		$this->pdf->AddSJISFont();
        //$this->pdf->AddSJIShwFont();
        

        //ページ総数取得
        $this->pdf->AliasNbPages();
        //$this->pdf->SetFont('sjis', '', 9);
        $this->lfText(521.73, 65.89-$padding_y, "ID : ". $data['audition_id']);
		$this->lfText(460, 77.88-$padding_y,date("Y年m月d日",strtotime($data['add_date'])),9);
    	$length=mb_strwidth($data['kananame_1']);
		$length2 = mb_strwidth($data['name_1']);		
		//mb_strwidth
		if($length>$length2){
			$count=$length-$length2;
			$count =floatval($count)/2;
			for($i=0;$i<$count;$i++){
				$data['name_1'].='　';
			}
		}
    	if($length2>$length){
			$count=$length2-$length;
			$count = floatval($count)/2;
			for($i=0;$i<$count;$i++){
				$data['kananame_1'].='　';
			}
		}
		$this->lfText(105, 93.36-$padding_y, $data['kananame_1'].'　'.$data['kananame_2'],12);
		$this->lfText(105, 111.36-$padding_y, $data['name_1'].'　'.$data['name_2'], 12);
//		$this->lfText(105, 93.36-$padding_y, $data['kananame_1'],12);
//		$this->lfText(150, 93.36-$padding_y, $data['kananame_2'],12);
//		$this->lfText(105, 111.36-$padding_y, $data['name_1'], 12);
//		$this->lfText(150, 111.36-$padding_y, $data['name_2'], 12);
		$this->lfText(358.78,111.36-$padding_y,$data['birthday_year'].'年'.$data['birthday_month'].'月'.$data['birthday_day'].'日（満 '.$data['age'].'歳）',9);
		$this->lfText(105, 127.46-$padding_y, '〒'.$data['zip'], 9);
		$this->lfText(105, 141.05-$padding_y, $data['address_pref'].' '.$data['address_1'].' '.$data['address_2']);		
		$this->lfText(105, 152.89-$padding_y, '自宅電話 '.$data['tel_1'].' - '.$data['tel_2'].' - '.$data['tel_3']);
		$this->lfText(290.09, 152.89-$padding_y, '携帯電話 '.$data['mobile_tel_1'].' - '.$data['mobile_tel_2'].' - '.$data['mobile_tel_3']);
        $this->lfText(105, 165.79-$padding_y, $data['email']);
        $this->lfText(105, 181.94-$padding_y, $data['education']);
        $this->lfText(290.05, 181.97-$padding_y, $data['in_school']);
        $this->lfText(472.47, 181.97-$padding_y, $data['graduation_year'].'年・卒業');
		$this->lfText(105,195.97-$padding_y,$data['job']);
		$this->lfText(105, 210.22-$padding_y, $data['speciality'],7);
        $this->lfText(105,224.53-$padding_y, $data['interest'],7);
//		$this->lfText(105, 181.94, $data['email']);
//        $this->lfText(105, 195.97, $data['education']);
//        $this->lfText(290.05, 195.97, $data['in_school']);
//        $this->lfText(472.47, 195.97, $data['graduation_year'].'年・卒業');
//        $this->lfText(105,210.22,$data['job']);
//        $this->lfText(105, 224.53, $data['speciality'],7);
//        $this->lfText(105,238.81, $data['interest'],7);
        
    		$num=77;
			for($i=0;$i<6;$i++){
				$owner_pr[$i]=mb_substr($data['owner_PR'],($i*$num),$num,"sjis");
			}
		    $hm=9;
		    for($z=0;$z<=count($owner_pr);$z++){
					$y=238.75-$padding_y+($z*$hm);		
					$this->lfText_sjis(105, $y, $owner_pr[$z],6);
			}
        
        $this->lfText(153, 407.85-93.8-$padding_y, sprintf("%.1fcm", $data['body_1']),9);
        $this->lfText(245, 407.85-93.8-$padding_y, sprintf("%.1fcm", $data['body_2']),9);
        $this->lfText(332, 407.85-93.8-$padding_y, sprintf("%.1fcm", $data['body_3']),9);
        $this->lfText(422, 407.85-93.8-$padding_y, sprintf("%.1fcm", $data['body_4']),9);
        $this->lfText(516, 407.85-93.8-$padding_y, sprintf("%.1fcm", $data['body_5']),9);
        $this->lfText(200.60, 425.36-93.8-$padding_y, $data['production_radio']);
        $this->lfText(383.24, 419.79-93.8-$padding_y, $data['production']);
        $this->lfText(383.24, 432.63-93.8-$padding_y, $data['contact_period']);
        $this->lfText(200.60, 445.92-93.8-$padding_y, $data['audition_radio']);
        $this->lfText(381.48, 442.01-93.8-$padding_y, $data['audition_name']);
        $this->lfText(381.48, 454.19-93.8-$padding_y, $data['audition_result']);
       // $this->lfText($x, $y, $text) //の他、経験
    		$num=77; 
				for($i=0;$i<6;$i++){
					$other_experience[$i]=mb_substr($data['other_experience'],($i*$num),$num,"sjis");
				}
		        $hm=9;
		    	for($z=0;$z<=count($other_experience);$z++){
					$y=467.35-$padding_y-93.8+($z*$hm); 			
					$this->lfText_sjis(105, $y, $other_experience[$z],6);
				}
        $this->lfText(199.24, 608.03-$padding_y-173.67, $data['email_login']);
        $this->lfText(199.24, 618.99-$padding_y-173.67, $data['login_nickname']);
        $this->lfText(105, 630.79-$padding_y-173.67, $data['r_name_1'].' '.$data['r_name_2']);
//        $this->lfText(122, 503.46, $data['r_kana_name_1'].' '.$data['r_kana_name_2']);
//        $this->lfText(122, 512.82,'〒'.$data['r_zip'].' '.$data['r_address_pref'].' '.$data['r_address_1'].' '.$data['r_address_2'],6);
//        $this->lfText(122, 525.06, $data['r_tel_1'].'-'.$data['r_tel_2'].'-'.$data['r_tel_3']);
        $this->lfText(293.96, 630.18-$padding_y-173.67, $data['r_kana_name_1'].' '.$data['r_kana_name_2']);
        $this->lfText(105, 642.13-$padding_y-173.67,'〒'.$data['r_zip'].' '.$data['r_address_pref'].' '.$data['r_address_1'].' '.$data['r_address_2']);
        $this->lfText(105, 652.36-$padding_y-173.67, $data['r_tel_1'].'-'.$data['r_tel_2'].'-'.$data['r_tel_3']);
        $this->lfText(472.05, 653.35-$padding_y-173.67, $data['r_relationship']);
        $this->lfText(105, 665.77-$padding_y-173.67, $data['wish_addr']);
        
        
        $this->lfText(30,680.27-$padding_y-173.67,pathinfo($data['photo_filename_1'],PATHINFO_BASENAME));
        $this->lfText(300, 680.27-$padding_y-173.67,pathinfo($data['photo_filename_2'],PATHINFO_BASENAME));
        if($data['photo_filename_1'] && file_exists($data['photo_filename_1']) && pathinfo($data['photo_filename_1'], PATHINFO_EXTENSION) != 'rar'){  
        	try{
	        	if(pathinfo($data['photo_filename_1'], PATHINFO_EXTENSION) == 'zip'){
	        		$data['photo_filename_1']=$this->unzip($data['photo_filename_1'], OPENPNE_VAR_DIR."/pdf/tmp");
	        		if($data['photo_filename_1']){
	        			$data['photo_filename_1']=OPENPNE_VAR_DIR."/pdf/tmp/".$data['photo_filename_1'];
	        		}
	        	}
	        	if(pathinfo($data['photo_filename_1'], PATHINFO_EXTENSION) == 'png')
	        	{ 
	        		$outputfilename=str_replace(".png", ".jpg", $data['photo_filename_1']);
	        		$this->png2jpg($data['photo_filename_1'], $outputfilename);
	        		$data['photo_filename_1']=$outputfilename;
	        	}
        		if(pathinfo($data['photo_filename_1'], PATHINFO_EXTENSION) == 'bmp')
	        	{ 
	        		$outputfilename=str_replace(".bmp", ".jpg", $data['photo_filename_1']);
	        		$this->bmp2jpg($data['photo_filename_1'], $outputfilename);
	        		$data['photo_filename_1']=$outputfilename;
	        	}
	        	if($data['photo_filename_1']){ 
		        	list($width, $height, $type, $attr) = getimagesize($data['photo_filename_1']);  
		        	if($width>0 && $height>0 && $type>0 && (IMAGETYPE_JPEG == $type || IMAGETYPE_PNG == $type)) {      	
			        	if(((floatval($height)/floatval($width)))>1.344){  
			        		$this->pdf->Image($data['photo_filename_1'], 30, 515-$padding_y,0,336);  	        		
			        	}else{
			        		$this->pdf->Image($data['photo_filename_1'], 30, 515-$padding_y,250,0);
			        	}
		        	}
	        	}
        	}catch (Exception $e) {	        			
	        }
        	
        }
    	if($data['photo_filename_2'] && file_exists($data['photo_filename_2']) && pathinfo($data['photo_filename_2'], PATHINFO_EXTENSION) != 'rar'){
    		try{
	    		if(pathinfo($data['photo_filename_2'], PATHINFO_EXTENSION) == 'zip'){
	        		$data['photo_filename_2']=$this->unzip($data['photo_filename_2'], OPENPNE_VAR_DIR."/pdf/tmp");
	    			if($data['photo_filename_2']){
	        			$data['photo_filename_2']=OPENPNE_VAR_DIR."/pdf/tmp/".$data['photo_filename_2'];
	        		}
	        	}
	    		if(pathinfo($data['photo_filename_2'], PATHINFO_EXTENSION) == 'png')
	        	{ 
	        		$outputfilename=str_replace(".png", ".jpg", $data['photo_filename_2']);
	        		$this->png2jpg($data['photo_filename_2'], $outputfilename);
	        		$data['photo_filename_2']=$outputfilename;
	        	} 
	        	if(pathinfo($data['photo_filename_2'], PATHINFO_EXTENSION) == 'bmp')
	        	{ 
	        		$outputfilename=str_replace(".bmp", ".jpg", $data['photo_filename_2']);
	        		$this->bmp2jpg($data['photo_filename_2'], $outputfilename);
	        		$data['photo_filename_2']=$outputfilename;
	        	} 
	        	if($data['photo_filename_2']){
		        	list($width, $height, $type, $attr) = getimagesize($data['photo_filename_2']);  
		        	if($width>0 && $height>0 && $type>0 && (IMAGETYPE_JPEG == $type || IMAGETYPE_PNG == $type)) {      	
			        	if(((floatval($height)/floatval($width)))>1.344){ 
			        		$this->pdf->Image($data['photo_filename_2'], 300, 515-$padding_y,0,336);	        		
			        	}else{
			        		$this->pdf->Image($data['photo_filename_2'], 300, 515-$padding_y,250,0);
			        	}
		        	}
	        	}
    		}catch (Exception $e) {	        			
	        }
        }
        $filename= PDF_TEMPLATE_DIR.'girlsaudition_'.$data['audition_id'].'_'.$data['member_id'].'.pdf';
        $this->pdf->Output($filename,"F");
    }
    
	function   unzip($file,$fileDir){
	    $zip = zip_open($file);
	    if($zip){
	        while($zip_entry=zip_read($zip)){
	            $name = zip_entry_name($zip_entry);
	            if (zip_entry_filesize($zip_entry)>0 && zip_entry_open($zip,$zip_entry,"r") && in_array(strtolower(pathinfo($name,PATHINFO_EXTENSION)),array('jpg','gif','png','bmp','jpeg'))) {
	                $buf   =   zip_entry_read($zip_entry,   zip_entry_filesize($zip_entry));
	                //$newName=microtime().zip_entry_name($zip_entry);
	                $newName = pathinfo($file,PATHINFO_FILENAME).".".pathinfo(zip_entry_name($zip_entry),PATHINFO_EXTENSION);
	                $fileHandle=fopen( "$fileDir/$newName", 'w+');
	                fwrite($fileHandle,$buf,zip_entry_filesize($zip_entry));
	                fclose($fileHandle);
	                zip_entry_close($zip_entry);
	            }
	        }
	        zip_close($zip);
	        return $newName;
	    }
	}
}