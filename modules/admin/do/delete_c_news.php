<?php
/**
 * @copyright 2005-2008 OpenPNE Project
 * @license   http://www.php.net/license/3_01.txt PHP License 3.01
 */

// バナー削除
class admin_do_delete_c_news extends OpenPNE_Action
{
    function execute($requests)
    {
    	$ids=$requests['news_ids'];  
    	$arr=explode(",", $ids);
    	foreach ($arr as $key=>$value){
    		$arr[$key]=(int)$value;
    	}
    	$ids=implode(",", $arr); 
    	db_news_hidden_news($ids);
        admin_client_redirect('delete_news', '指定されたニュースを削除しました');
    }
}
