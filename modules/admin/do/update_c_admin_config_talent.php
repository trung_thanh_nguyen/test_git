<?php
/**
 * @copyright 2005-2008 OpenPNE Project
 * @license   http://www.php.net/license/3_01.txt PHP License 3.01
 */

// SNS設定変更
class admin_do_update_c_admin_config_talent extends OpenPNE_Action
{
    function handleError($errors)
    {
        admin_client_redirect('edit_c_admin_config', '正しく入力してください');
    }
    function execute($requests)
    {
        $config =& OpenPNE_Config::getInstance();
    	if (is_null(db_admin_c_admin_config4name('TALENTCOMMU'))) {
            db_admin_insert_c_admin_config('TALENTCOMMU', $_POST['TALENTCOMMU']);
        }else{
            db_admin_update_c_admin_config('TALENTCOMMU', $_POST['TALENTCOMMU']);
        }
        admin_client_redirect('edit_c_admin_config_talent', '設定を変更しました');        
    }
}