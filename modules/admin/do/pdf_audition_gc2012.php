<?php 
require_once  OPENPNE_LIB_DIR . '/include/fpdf/japanese.php';

class admin_do_pdf_audition_gc2012 extends OpenPNE_Action
{
    function isSecure()
    {
        session_cache_limiter('public');
        return true;
    }

    function handleError($errors)
    {
        admin_client_redirect('audition_gc2012', array_shift($errors));
    }
    
    function to_wareki($y, $m, $d)
    {
    	//年月日を文字列として結合
    	$ymd = sprintf("%02d%02d%02d", $y, $m, $d);
    	if ($ymd <= "19120729") {
    		$gg = "明治";
    		$yy = $y - 1867;
    	} elseif ($ymd >= "19120730" && $ymd <= "19261224") {
    		$gg = "大正";
    		$yy = $y - 1911;
    	} elseif ($ymd >= "19261225" && $ymd <= "19890107") {
    		$gg = "昭和";
    		$yy = $y - 1925;
    	} elseif ($ymd >= "19890108") {
    		$gg = "平成";
    		$yy = $y - 1988;
    	}
    	$wareki = "{$gg}{$yy}年{$m}月{$d}日";
    	return array('gg'=>$gg,'yy'=>$yy,'m'=>$m,'d'=>$d);
    }

    function execute($requests)
    {
    	$page = $requests['page'];
    	ob_start();
    	if (!defined('CHAR_CODE')) define('CHAR_CODE','UTF8');
    	if (!defined('RELATIVE_PATH')) define('RELATIVE_PATH','');
			if (!defined('FPDF_FONTPATH')) define('FPDF_FONTPATH',OPENPNE_LIB_DIR.'/include/fpdf/font/');
			
			define('PDF_TEMPLATE_DIR', OPENPNE_VAR2_DIR. '/pdfgc/');
			define('PDF_IMG_DIR', HTML_PATH. USER_DIR. USER_PACKAGE_DIR. TEMPLATE_NAME. '/img/pdf/');
			$this->tpl_pdf= OPENPNE_LIB_DIR.'/include/fpdf/girl.pdf';
			$ids=rtrim($_POST['audition_ids'],',');	
  	
//     	$audtion_list=db_audition_list_girl_by_ids($ids);
    	$audtion_list=db_audition_gc_list_by_ids($ids);
    	
    	foreach ($audtion_list as $key=>$value){
    		$ext_data=$value['ext_data'];
    		$ext_data = unserialize($ext_data);
    		$value =array_merge($ext_data,$value);
    		
    		if($value['photo_filename_1']){
    			$value['photo_filename_1'] =m_get_audition_gc_photofile($value['photo_filename_1'],$value['audition_id']);     						
    		}
    		if($audtion_list[$key]['photo_filename_2']){
    			$value['photo_filename_2']=m_get_audition_gc_photofile($value['photo_filename_2'],$value['audition_id']);   
    		}
    		
    		if($value['zip']){
    			$value['zip']=substr($value['zip'],0,3).'- '.substr($value['zip'],3,4);
    		}
    		if($value['r_zip']){
    			$value['r_zip']=substr($value['r_zip'],0,3).'- '.substr($value['r_zip'],3,4);
    		}
    		$value['owner_PR']= $this->sjis_conv($value['owner_PR']);
    		$value['other_experience']=  $this->sjis_conv($value['other_experience']);
    		$value['in_school']=($value['in_school']=='1')?"在学":"卒業";
    		$value['production_radio'] = ($value['production_radio']=='1')?"はい":"いいえ";
    		$value['audition_radio'] = ($value['audition_radio']=='1')?"はい":"いいえ";
     		$this->create_pdf($value);
    	}     	
    	admin_client_redirect('audition_gc2012', 'success','page='.$page);
    	ob_end_flush(); 	
    	  
    	
    }
 // 文字コードSJIS変換 -> japanese.phpで使用出来る文字コードはSJISのみ
    function sjis_conv($conv_str) {
      return (mb_convert_encoding($conv_str, "SJIS", CHAR_CODE));
    }
 	function utf8_conv($conv_str) {
      return (mb_convert_encoding($conv_str,CHAR_CODE, "SJIS" ));
    }
    
 // PDF_Japanese::Text へのパーサー
    function lfText($x, $y, $text, $size=9, $style = '') {
        $text = mb_convert_encoding($text, "SJIS", CHAR_CODE);
        $this->pdf->SetFont('SJIS', $style, $size);
        //$this->pdf->SetFont('MSPGothic', $style, $size);
        
        $this->pdf->Text($x, $y, $text);
    }
    
    function lfText_sjis($x, $y, $text, $size=9, $style = '') {
        $this->pdf->SetFont('SJIS', $style, $size); 
        $this->pdf->Text($x, $y, $text);
    }
    
    function png2jpg($originalFile,$outputFile,$quality=100){
    	$image= imagecreatefrompng($originalFile);
    	imagejpeg($image,$outputFile,$quality);
    	imagedestroy($image);
    }
    function bmp2jpg($originalFile,$outpubFile,$quality=100){
    	$image =$this->ImageCreateFromBMP($originalFile);
		imagejpeg($image,$outpubFile,$quality); 
		imagedestroy($image);
    }
    
	function ImageCreateFromBMP($filename){
		$res='';
		if (! $f1 = fopen ($filename ,"rb")) return FALSE ;
		$FILE = unpack ("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread ($f1 ,14 ));
		if ($FILE ['file_type'] != 19778 ) return FALSE ;
		$BMP = unpack ('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel'. '/Vcompression/Vsize_bitmap/Vhoriz_resolution'. '/Vvert_resolution/Vcolors_used/Vcolors_important', fread ($f1 ,40 ));
		$BMP ['colors'] = pow (2 ,$BMP ['bits_per_pixel']);
		if ($BMP ['size_bitmap'] == 0 ) $BMP ['size_bitmap'] = $FILE ['file_size'] - $FILE ['bitmap_offset'];
		$BMP ['bytes_per_pixel'] = $BMP ['bits_per_pixel']/8 ;
		$BMP ['bytes_per_pixel2'] = ceil ($BMP ['bytes_per_pixel']);
		$BMP ['decal'] = ($BMP ['width']*$BMP ['bytes_per_pixel']/4 );
		$BMP ['decal'] -= floor ($BMP ['width']*$BMP ['bytes_per_pixel']/4 );
		$BMP ['decal'] = 4 -(4 *$BMP ['decal']);
		if ($BMP ['decal'] == 4 ) $BMP ['decal'] = 0 ;
		$PALETTE = array ();
		if ($BMP ['colors'] < 16777216 )
		{
		$PALETTE = unpack ('V'. $BMP ['colors'], fread ($f1 ,$BMP ['colors']*4 ));
		}
		$IMG = fread ($f1 ,$BMP ['size_bitmap']);
		$VIDE = chr (0 );
		$res=imagecreatetruecolor($BMP['width'],$BMP['height']);
		$P = 0 ;
		$Y = $BMP ['height']-1 ;
		while ($Y >= 0 )
		{
		$X =0 ;
		while ($X < $BMP ['width'])
		{
		if ($BMP ['bits_per_pixel'] == 24 ) $COLOR = unpack ("V",substr ($IMG ,$P ,3 ). $VIDE );
		elseif ($BMP ['bits_per_pixel'] == 16 )
		{
		$COLOR = unpack ("n",substr ($IMG ,$P ,2 ));
		$COLOR [1 ] = $PALETTE [$COLOR [1 ]+1 ];
		} elseif ($BMP ['bits_per_pixel'] == 8 )
		{
		$COLOR = unpack ("n",$VIDE.substr ($IMG ,$P ,1 ));
		$COLOR [1 ] = $PALETTE [$COLOR [1 ]+1 ];
		} elseif ($BMP ['bits_per_pixel'] == 4 )
		{
		$COLOR = unpack ("n",$VIDE.substr ($IMG ,floor ($P ),1 ));
		if(($P *2 )%2 == 0 ) $COLOR [1 ] = ($COLOR [1 ] >> 4 ) ;
		else $COLOR [1 ] = ($COLOR [1 ] & 0x0F );
		$COLOR [1 ] = $PALETTE [$COLOR [1 ]+1 ];
		} elseif ($BMP ['bits_per_pixel'] == 1 )
		{
		$COLOR = unpack ("n",$VIDE.substr ($IMG ,floor ($P ),1 ));
		if (($P *8 )%8 == 0 ) $COLOR [1 ] = $COLOR [1 ] >>7 ;
		elseif (($P *8 )%8 == 1 ) $COLOR [1 ] = ($COLOR [1 ] & 0x40 )>>6 ;
		elseif (($P *8 )%8 == 2 ) $COLOR [1 ] = ($COLOR [1 ] & 0x20 )>>5 ;
		elseif (($P *8 )%8 == 3 ) $COLOR [1 ] = ($COLOR [1 ] & 0x10 )>>4 ;
		elseif (($P *8 )%8 == 4 ) $COLOR [1 ] = ($COLOR [1 ] & 0x8 )>>3 ;
		elseif (($P *8 )%8 == 5 ) $COLOR [1 ] = ($COLOR [1 ] & 0x4 )>>2 ;
		elseif (($P *8 )%8 == 6 ) $COLOR [1 ] = ($COLOR [1 ] & 0x2 )>>1 ;
		elseif (($P *8 )%8 == 7 ) $COLOR [1 ] = ($COLOR [1 ] & 0x1 );
		$COLOR [1 ] = $PALETTE [$COLOR [1 ]+1 ];
		} else return FALSE ;
		imagesetpixel ($res ,$X ,$Y ,$COLOR [1 ]);
		$X ++;
		$P += $BMP ['bytes_per_pixel'];
		} $Y --;
		$P +=$BMP ['decal'];
		} fclose ($f1 );
		return $res ;
	}
    
    function create_pdf($data){
    	$padding_y=4;
    	$pdf_height= 880;
    	$this->pdf  = new PDF_Japanese('P', 'pt');
    		// PDF BLOCKのプロパティ
		$this->block_option = "encoding=UniJIS-UCS2-H textformat=utf8 fontname=MSPGothicHeiseiMin-W3 textflow=true";		
				
		$pagecount =$this->pdf->setSourceFile($this->tpl_pdf);
		$tplidx = $this->pdf->importPage(1); 
		$this->pdf->addPage();
				//$this->pdf->useTemplate($tplidx, 10, 10, 90);
		$this->pdf->useTemplate($tplidx);
		$this->pdf->AddSJISFont();
        //$this->pdf->AddSJIShwFont();
        

        //ページ総数取得
        $this->pdf->AliasNbPages();
        //$this->pdf->SetFont('sjis', '', 9);
        
//         $length=mb_strwidth($data['kananame_1'],'utf8');        
//         $length2 = mb_strwidth($data['name_1'],'utf8')*2;
//         if($length>$length2){
//         	$count=$length-$length2;
//          	$count =floatval($count)/2;
//         	for($i=0;$i<$count;$i++){
//         		$data['name_1'].='　';
//         	}
//         }
//         if($length2>$length){
//         	$count=$length2-$length;
//          	$count = floatval($count)/2;        	
//         	for($i=0;$i<$count;$i++){
//         		$data['kananame_1'].='　';
//         	}
//         }
        $this->lfText(6*9.5, 66, $data['kananame_1'].'　'.$data['kananame_2'],9);
        $this->lfText(6*3.8, 72+6*4.5, $data['name_1'].'　'.$data['name_2'], 18);
        
        $this->lfText(216+11*6, 66, $data['zip']);
        $this->lfText(288+7*6, 70, $data['address_pref'],13);
        $this->lfText(216+11*6, 72+11, $data['address_1'],13);
        $this->lfText(216+11*6, 75+24, $data['address_2'],13);
        $this->lfText(288+6*3, 72+6*6.5, $data['email']);
        
        $this->lfText(66, 72+8.5*6, $data['birthday_year'].'年'.$data['birthday_month'].'月'.$data['birthday_day'].'日');
        $arr_y=$this->to_wareki($data['birthday_year'],$data['birthday_month'],$data['birthday_day']);
        //$this->lfText(66, 144, $arr_y['gg'],14,'B');
        $this->lfText(66, 144, $arr_y['yy'],14,'B');
        $this->lfText(72+9*6, 144, $arr_y['m'],14,'B');
        $this->lfText(144+5.5*6, 144, $arr_y['d'],14,'B');
        
        
        $this->lfText(144+6*5, 72+8.5*6, $data['age']);
        $this->lfText(216+6*2, 144, $data['tel_1'].' - '.$data['tel_2'].' - '.$data['tel_3'],14,'B');
        //$this->lfText(216+6*5, 144, '携帯電話 '.$data['mobile_tel_1'].' - '.$data['mobile_tel_2'].' - '.$data['mobile_tel_3']);
        $education = $data['education'];
        $edu_length = mb_strwidth($education);
        for($j=0;$j<2;$j++){
        	$edu[$j]= mb_substr($education,$j*12,12,'utf8');
        }
        $this->lfText(360+6*2.4, 134, $edu[0],13);
        $this->lfText(360+6*2.4, 146, $edu[1],13);
        $this->lfText(504+6*6, 146, $data['graduation_year'].' 年',13);
        
        //$this->lfText(360+6*2, 144, $data['education'].' '.$data['graduation_year']);
        //$this->lfText(504+6*5, 144, $data['graduation_year']);
        
        $this->lfText(66, 144+6*5.5, sprintf("%.1f", $data['body_1']),18,'B');
        $this->lfText(6*10.2, 213, sprintf("%.1f", $data['body_2']),14,'B');
        $this->lfText(144+6*5, 213, sprintf("%.1f", $data['body_3']),14,'B');
        $this->lfText(6*10.2, 216+6*5.5, sprintf("%.1f", $data['body_4']),14,'B');
        $this->lfText(144+6*5, 216+6*5.5, sprintf("%.1f", $data['body_5']),14,'B');
        
        $this->lfText(288+6*6.5, 144+6*5.5, $data['wish_department'],18);
        $this->lfText(288, 213, $data['speciality'],13);
        $this->lfText(288, 216+6*5.5, $data['interest'],13);
        
        $num=25;
        for($i=0;$i<6;$i++){
        	$owner_pr[$i]=mb_substr($data['owner_PR'],($i*$num),$num,"sjis");
        }
        $hm=15;
        for($z=0;$z<=count($owner_pr);$z++){
        	$y=300+($z*$hm);
        	$this->lfText_sjis(30, $y, $owner_pr[$z],10);
        }
        
        $this->lfText(288+6*3.5, 288+6*2.5, $data['family_f'],13);
        $this->lfText(504+6*5, 288+6*2.5, $data['family_f_name'],13);
        
        $this->lfText(288+6*3.5, 288+6*6.5, $data['family_m'],13);
        $this->lfText(504+6*5, 288+6*6.5, $data['family_m_name'],13);
        $this->lfText(288+6*3.5, 288+10.7*6, $data['family_b'],13);
        $this->lfText(504+6*5, 288+10.7*6, $data['family_b_name'],13);
        $this->lfText(288+6*3.5, 360+3*6, $data['family_yb'],13);
        $this->lfText(504+6*5, 360+3*6, $data['family_yb_name'],13);
        

        $this->lfText(72+6*6, 360+7*6, $data['r_kana_name_1'].'　'.$data['r_kana_name_2']);
        $this->lfText(72+4*6, 360+10.2*6, $data['r_name_1'].'　'.$data['r_name_2']);
        
        $this->lfText(288+7*6, 360+6.2*6, $data['r_zip'].'  '.$data['r_address_pref'].'  '.$data['r_address_1'],7);
        $this->lfText(288+7*6, 360+7.5*6, $data['r_address_2'],7);
       // $this->lfText(288+6*6, 360+6.5*6, $data['r_address_1'].' '.$data['r_address_2']);
        $this->lfText(288+7*6, 360+10.2*6, $data['r_tel_1'].' - '.$data['r_tel_2'].' - '.$data['r_tel_3']);
        $this->lfText(504+5*6, 360+10.3*6, $data['r_relationship']);
        

        
        $temp_photo_filename = $data['photo_filename_1'];
        $data['photo_filename_1'] = $data['photo_filename_2'];
        $data['photo_filename_2'] = $temp_photo_filename;
        $this->lfText(30,440,pathinfo($data['photo_filename_1'],PATHINFO_BASENAME));
        $this->lfText(300, 440,pathinfo($data['photo_filename_2'],PATHINFO_BASENAME));
        if($data['photo_filename_1'] && file_exists($data['photo_filename_1']) && pathinfo($data['photo_filename_1'], PATHINFO_EXTENSION) != 'rar'){  
        	try{
	        	if(pathinfo($data['photo_filename_1'], PATHINFO_EXTENSION) == 'zip'){
	        		$data['photo_filename_1']=$this->unzip($data['photo_filename_1'], OPENPNE_VAR_DIR."/pdf/tmp");
	        		if($data['photo_filename_1']){
	        			$data['photo_filename_1']=OPENPNE_VAR_DIR."/pdf/tmp/".$data['photo_filename_1'];
	        		}
	        	}
	        	if(pathinfo($data['photo_filename_1'], PATHINFO_EXTENSION) == 'png')
	        	{ 
	        		$outputfilename=str_replace(".png", ".jpg", $data['photo_filename_1']);
	        		$this->png2jpg($data['photo_filename_1'], $outputfilename);
	        		$data['photo_filename_1']=$outputfilename;
	        	}
        		if(pathinfo($data['photo_filename_1'], PATHINFO_EXTENSION) == 'bmp')
	        	{ 
	        		$outputfilename=str_replace(".bmp", ".jpg", $data['photo_filename_1']);
	        		$this->bmp2jpg($data['photo_filename_1'], $outputfilename);
	        		$data['photo_filename_1']=$outputfilename;
	        	}
	        	if($data['photo_filename_1']){ 
		        	list($width, $height, $type, $attr) = getimagesize($data['photo_filename_1']);  
		        	if($width>0 && $height>0 && $type>0 && (IMAGETYPE_JPEG == $type || IMAGETYPE_PNG == $type)) {      	
			        	if(((floatval($height)/floatval($width)))>1.427){  
			        		$this->pdf->Image($data['photo_filename_1'], 30, 450,0,374);  	        		
			        	}else{
			        		$this->pdf->Image($data['photo_filename_1'], 30, 450,262,0);
			        	}
		        	}
	        	}
        	}catch (Exception $e) {	        			
	        }
        	
        }
    	if($data['photo_filename_2'] && file_exists($data['photo_filename_2']) && pathinfo($data['photo_filename_2'], PATHINFO_EXTENSION) != 'rar'){
    		try{
	    		if(pathinfo($data['photo_filename_2'], PATHINFO_EXTENSION) == 'zip'){
	        		$data['photo_filename_2']=$this->unzip($data['photo_filename_2'], OPENPNE_VAR_DIR."/pdf/tmp");
	    			if($data['photo_filename_2']){
	        			$data['photo_filename_2']=OPENPNE_VAR_DIR."/pdf/tmp/".$data['photo_filename_2'];
	        		}
	        	}
	    		if(pathinfo($data['photo_filename_2'], PATHINFO_EXTENSION) == 'png')
	        	{ 
	        		$outputfilename=str_replace(".png", ".jpg", $data['photo_filename_2']);
	        		$this->png2jpg($data['photo_filename_2'], $outputfilename);
	        		$data['photo_filename_2']=$outputfilename;
	        	} 
	        	if(pathinfo($data['photo_filename_2'], PATHINFO_EXTENSION) == 'bmp')
	        	{ 
	        		$outputfilename=str_replace(".bmp", ".jpg", $data['photo_filename_2']);
	        		$this->bmp2jpg($data['photo_filename_2'], $outputfilename);
	        		$data['photo_filename_2']=$outputfilename;
	        	} 
	        	if($data['photo_filename_2']){
		        	list($width, $height, $type, $attr) = getimagesize($data['photo_filename_2']);  
		        	if($width>0 && $height>0 && $type>0 && (IMAGETYPE_JPEG == $type || IMAGETYPE_PNG == $type)) {      	
			        	
		        		if(((floatval($height)/floatval($width)))>1.385){ 
		        			$x=570-374*(floatval($width)/floatval($height));
			        		$this->pdf->Image($data['photo_filename_2'], $x, 450,0,374);	        		
			        	}else{
			        		$this->pdf->Image($data['photo_filename_2'], 300, 450,270,0);
			        	}
		        	}
	        	}
    		}catch (Exception $e) {	        			
	        }
        }
        $filename= PDF_TEMPLATE_DIR.'girl_'.$data['audition_id'].'_'.$data['member_id'].'.pdf';
        $this->pdf->Output($filename,"F");
    }
    
	function   unzip($file,$fileDir){
	    $zip = zip_open($file);
	    if($zip){
	        while($zip_entry=zip_read($zip)){
	            $name = zip_entry_name($zip_entry);
	            if (zip_entry_filesize($zip_entry)>0 && zip_entry_open($zip,$zip_entry,"r") && in_array(strtolower(pathinfo($name,PATHINFO_EXTENSION)),array('jpg','gif','png','bmp','jpeg'))) {
	                $buf   =   zip_entry_read($zip_entry,   zip_entry_filesize($zip_entry));
	                //$newName=microtime().zip_entry_name($zip_entry);
	                $newName = pathinfo($file,PATHINFO_FILENAME).".".pathinfo(zip_entry_name($zip_entry),PATHINFO_EXTENSION);
	                $fileHandle=fopen( "$fileDir/$newName", 'w+');
	                fwrite($fileHandle,$buf,zip_entry_filesize($zip_entry));
	                fclose($fileHandle);
	                zip_entry_close($zip_entry);
	            }
	        }
	        zip_close($zip);
	        return $newName;
	    }
	}
}