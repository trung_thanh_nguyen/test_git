<?php
/**
 * @copyright 2005-2008 OpenPNE Project
 * @license   http://www.php.net/license/3_01.txt PHP License 3.01
 */

// フリーページ編集
class admin_do_insert_c_blog_part extends OpenPNE_Action
{
    function handleError($errors)
    {
        admin_client_redirect('list_c_blog_part', array_shift($errors));
    }

    function execute($requests)
    {    	
        db_admin_insert_c_blog_part($requests['title'], $requests['body']);

        admin_client_redirect('list_c_blog_part', 'ブログパーツを追加しました。');
    }
}