({if $TalentName})【({$TalentName})】({/if})({$CompanyName}) 
■お問い合わせ種別：({$content})
■お問い合わせ対象タレント：({$TalentName})
■会社名・団体名：({$CompanyName})
■ご住所：({$AddressPost}) ({$pref}) ({$Address1}) ({$Address2})
■ご担当部署名：({$CompanyStation})
■ご担当者名（カナ）：({$Name3}) ({$Name4})
■ご担当者名（漢字）：({$Name1}) ({$Name2})
■ご連絡先の電話番号：({$Tel1a})-({$Tel1b})-({$Tel1c})
■ご連絡先の電話番号2：({$Tel2a})-({$Tel2b})-({$Tel2c})
■メールアドレス：({$Mail})
■ホームページ：({$Homepage})
■お問合わせ内容：({$Comment})
